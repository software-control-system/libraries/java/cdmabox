package fr.soleil.cdma.box.manager;

import java.awt.Component;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileSystemView;

import org.slf4j.Logger;

import fr.soleil.cdma.box.util.GUIUtilities;
import fr.soleil.cdma.box.util.comparator.LongComparator;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.source.BasicDataSource;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.SystemUtils;
import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.resource.MessageManager;

/**
 * This manager is able to list the files of a particular directory. It interacts with the file view
 * 
 * @author girardot
 */
public class FileListManager implements IFileManager {

    private final BasicDataSource<File[]> fileListSource;
    private MessageManager messageManager;
    private final Logger logger;
    private File[] availableFiles;
    private FileRefresher refresher;
    private String workingPath;
    private boolean waitForStability;
    private boolean acceptHiddenFiles;
    private String[] filteredExtensions;

    public FileListManager(MessageManager messageManager, Logger logger, String... filteredExtensions) {
        this.messageManager = messageManager;
        this.logger = logger;
        this.filteredExtensions = filteredExtensions;
        availableFiles = null;
        workingPath = null;
        fileListSource = new BasicDataSource<File[]>(null);
        // Ignore duplication test as it is already managed by this FileListManager
        fileListSource.setIgnoreDuplicationTest(true);
        refresher = null;
        waitForStability = true;
        acceptHiddenFiles = false;
    }

    public File[] getAvailableFiles() {
        return availableFiles;
    }

    /**
     * Returns whether this {@link FileListManager} waits for a file stability before considering it
     * as valid and adding it in file list
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isWaitForStability() {
        return waitForStability;
    }

    /**
     * Sets whether this {@link FileListManager} should wait for a file stability before considering
     * it as valid and adding it in file list
     * 
     * @param waitForStability Whether to wait for file stability (<code>TRUE</code> to wait for
     *            stability)
     */
    public void setWaitForStability(boolean waitForStability) {
        this.waitForStability = waitForStability;
    }

    public boolean isAcceptHiddenFiles() {
        return acceptHiddenFiles;
    }

    public void setAcceptHiddenFiles(boolean hiddenFilesAllowed) {
        this.acceptHiddenFiles = hiddenFilesAllowed;
    }

    public MessageManager getMessageManager() {
        return messageManager;
    }

    public void setMessageManager(MessageManager messageManager) {
        this.messageManager = messageManager;
    }

    /**
     * Returns the data source that handles the file list
     * 
     * @return An {@link AbstractDataSource}
     */
    public AbstractDataSource<File[]> getFileListSource() {
        return fileListSource;
    }

    @Override
    public String getWorkingPath() {
        return workingPath;
    }

    @Override
    public void setFile(File file) {
        stopRefreshing();
        workingPath = (file == null ? null : file.getAbsolutePath());
        startRefreshing(true);
    }

    /**
     * Starts the refreshing {@link Thread} if necessary
     */
    public synchronized void startRefreshing() {
        startRefreshing(false);
    }

    public synchronized void startRefreshing(boolean startWithNullFiles) {
        String path = getWorkingPath();
        if (refresher == null) {
            initRefresher(path, startWithNullFiles);
        } else {
            if (!ObjectUtils.sameObject(path, refresher.getPath())) {
                refresher.cancel();
                initRefresher(path, startWithNullFiles);
            }
        }
    }

    /**
     * Stops the refreshing {@link Thread} if necessary
     */
    public synchronized void stopRefreshing() {
        if (refresher != null) {
            refresher.cancel();
            refresher = null;
        }
    }

    protected boolean hasFilteredExtensions(String... filteredExtensions) {
        return ((filteredExtensions != null) && (filteredExtensions.length > 0));
    }

    protected boolean isFileOk(File file) {
        return acceptHiddenFiles || !file.isHidden();
    }

    private boolean checkExtension(String[] filteredExtensions, File file) {
        boolean fileOk;
        if (file == null) {
            fileOk = false;
        } else if (file.isDirectory()) {
            fileOk = isFileOk(file);
        } else if (hasFilteredExtensions(filteredExtensions)) {
            fileOk = false;
            String ext = FileUtils.getExtension(file);
            if ((ext != null) && (!ext.isEmpty())) {
                for (String extension : filteredExtensions) {
                    if (ext.equalsIgnoreCase(extension)) {
                        fileOk = isFileOk(file);
                        break;
                    }
                }
            }
        } else {
            fileOk = isFileOk(file);
        }
        return fileOk;
    }

    private File[] filterFileArray(File[] files, String... filteredExtensions) {
        File[] filteredFiles;
        if (hasFilteredExtensions(filteredExtensions) && (files != null) && (files.length > 0)) {
            Collection<File> fileList = new ArrayList<>(files.length);
            for (File file : files) {
                if (checkExtension(filteredExtensions, file)) {
                    fileList.add(file);
                }
            }
            filteredFiles = fileList.toArray(new File[fileList.size()]);
            fileList.clear();
        } else {
            filteredFiles = files;
        }
        return filteredFiles;
    }

    public File[] filterFiles(File[] files) {
        return filterFileArray(files, filteredExtensions);
    }

    private void initRefresher(String path, boolean startWithNullFiles) {
        availableFiles = null;
        refresher = new FileRefresher(path, null, startWithNullFiles);
        refresher.execute();
    }

    protected void setAvailableFiles(File[] availableFiles) {
        if (!ArrayUtils.equals(availableFiles, this.availableFiles)) {
            this.availableFiles = availableFiles;
            fileListSource.setData(availableFiles);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * This refresher regularly checks the directory to update the available file list. A file is
     * considered available when it became stable. This is typically used to avoid Nexus API
     * crashes.
     */
    private class FileRefresher extends SwingWorker<Void, File[]> {

        private static final int DEFAULT_SLEEPING_TIME = 5;
        public static final String SLEEPING_TIME_PROPERTY = "SLEEPING_TIME";
        public static final String VERBOSE_PROPERTY = "VERBOSE_REFRESHER";

        protected final Map<String, File> knownFiles;
        protected final Map<String, Long> lastKnownLength;
        protected final Map<String, Long> lastKnownModificationDate;

        private String path;
        private volatile boolean canceled;
        private long secondsToSleep;
        private final boolean verbose;

        private final Component parentComponent;
        private volatile boolean startWithNullFiles;

        private FileRefresher(String path, Component parentComponent, boolean startWithNullFiles) {
            super();
            this.parentComponent = parentComponent;
            canceled = false;
            this.path = path;
            this.startWithNullFiles = startWithNullFiles;
            secondsToSleep = SystemUtils.getSystemIntProperty(SLEEPING_TIME_PROPERTY, DEFAULT_SLEEPING_TIME);
            if (secondsToSleep < 1) {
                secondsToSleep = DEFAULT_SLEEPING_TIME;
            }
            verbose = SystemUtils.getSystemBooleanProperty(VERBOSE_PROPERTY, false);
            knownFiles = new HashMap<String, File>();
            lastKnownLength = new HashMap<String, Long>();
            lastKnownModificationDate = new HashMap<String, Long>();
        }

        public String getPath() {
            return path;
        }

        @Override
        protected Void doInBackground() {
            if (startWithNullFiles) {
                publish((File[]) null);
                try {
                    // wait a little bit to let time to EDT
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    cancel();
                }
            }
            boolean error = false;
            while ((!error) && (!isCanceled())) {
                try {
                    File dir = new File(path);
                    if (dir.isDirectory()) {
                        build(path);
                    }
                    dir = null;
                    if (verbose) {
                        long hours, minutes, seconds;
                        hours = secondsToSleep / 3600;
                        minutes = (secondsToSleep % 3600) / 60;
                        seconds = secondsToSleep % 60;
                        StringBuilder infoBuffer = new StringBuilder();
                        infoBuffer.append(GUIUtilities.nowToString());
                        infoBuffer.append(" - ");
                        infoBuffer.append(
                                messageManager.getMessage("fr.soleil.cdma.box.manager.FileListManager.Refresh.done"));
                        infoBuffer.append("\n");
                        infoBuffer.append(
                                messageManager.getMessage("fr.soleil.cdma.box.manager.FileListManager.Refresh.next"));
                        if (hours > 0) {
                            infoBuffer.append(" ");
                            infoBuffer.append(hours);
                            infoBuffer.append(messageManager
                                    .getMessage("fr.soleil.cdma.box.manager.FileListManager.Refresh.hours"));
                        }
                        if (minutes > 0) {
                            infoBuffer.append(" ");
                            infoBuffer.append(minutes);
                            infoBuffer.append(messageManager
                                    .getMessage("fr.soleil.cdma.box.manager.FileListManager.Refresh.minutes"));
                        }
                        if (seconds > 0) {
                            infoBuffer.append(" ");
                            infoBuffer.append(seconds);
                            infoBuffer.append(messageManager
                                    .getMessage("fr.soleil.cdma.box.manager.FileListManager.Refresh.seconds"));
                        }
                        logger.trace(infoBuffer.toString());
                        infoBuffer = null;
                    } // end if (verbose)
                    if (!isCanceled()) {
                        Thread.sleep(1000L * secondsToSleep);
                    }
                } catch (Throwable t) {
                    error = true;
                    if (!isCanceled()) {
                        t.printStackTrace();
                        StringBuilder messageBuffer = new StringBuilder();
                        messageBuffer.append(
                                messageManager.getMessage("fr.soleil.cdma.box.manager.FileListManager.Refresh.Error"));
                        messageBuffer.append("\n");
                        messageBuffer.append(messageManager
                                .getMessage("fr.soleil.cdma.box.manager.FileListManager.Refresh.Error.Type"));
                        messageBuffer.append(t.getClass());
                        messageBuffer.append("\n");
                        messageBuffer.append(messageManager
                                .getMessage("fr.soleil.cdma.box.manager.FileListManager.Refresh.Error.Message"));
                        messageBuffer.append(t.getMessage());
                        JOptionPane.showMessageDialog(parentComponent, messageBuffer.toString(),
                                messageManager.getMessage("fr.soleil.cdma.box.Error"), JOptionPane.ERROR_MESSAGE);
                        logger.error(messageBuffer.toString());
                    }
                }
            }
            clean();
            return null;
        }

        @Override
        protected void process(List<File[]> chunks) {
            for (File[] availableFiles : chunks) {
                if (isCanceled()) {
                    // cancel ASAP
                    break;
                }
                if ((availableFiles == null) && startWithNullFiles) {
                    startWithNullFiles = false;
                    setAvailableFiles(null);
                } else {
                    setAvailableFiles(availableFiles);
                }
            }
        }

        private void build(String buildPath) {
            final ArrayList<File> resultList = new ArrayList<>();
            FileSystemView fileSystemView = FileSystemView.getFileSystemView();
            File dir = new File(buildPath);
            String[] extensions = filteredExtensions;
            if (dir.isDirectory() && (!isCanceled())) {
                File[] dirFiles = null;
                TreeMap<Long, Collection<File>> fileMap = new TreeMap<>(new LongComparator(true));
                if (!isCanceled()) {
                    dirFiles = filterFileArray(fileSystemView.getFiles(dir, false), extensions);
                }
                if (dirFiles != null) {
                    for (File file : dirFiles) {
                        if (isCanceled()) {
                            break;
                        }
                        Long key = Long.valueOf(file.lastModified());
                        Collection<File> fileList = fileMap.get(key);
                        if (fileList == null) {
                            fileList = new ArrayList<>();
                        }
                        fileList.add(file);
                        fileMap.put(key, fileList);
                    }
                }
                Set<Long> keySet = fileMap.keySet();
                for (Long lastModified : keySet) {
                    if (isCanceled()) {
                        break;
                    }
                    Collection<File> fileList = fileMap.get(lastModified);
                    for (File file : fileList) {
                        if (isCanceled()) {
                            break;
                        }
                        String filePath = file.getAbsolutePath();
                        String fileName = file.getName();
                        Long lastLength = lastKnownLength.get(filePath);
                        final StringBuilder buffer = new StringBuilder();
                        buffer.append(messageManager
                                .getMessage("fr.soleil.cdma.box.manager.FileListManager.Refresh.File.Check"));
                        buffer.append(fileName);
                        if (lastLength == null) {
                            // File is not known yet: register it
                            if (verbose) {
                                System.out.println(buffer.toString());
                            }
                            lastKnownLength.put(filePath, Long.valueOf(file.length()));
                            lastKnownModificationDate.put(filePath, lastModified);
                            knownFiles.put(filePath, file);
                            if (!waitForStability) {
                                resultList.add(file);
                            }
                        } // end if (lastLength == null)
                        else {
                            if (((lastLength.longValue() == file.length())
                                    && (ObjectUtils.sameObject(lastModified, lastKnownModificationDate.get(filePath))))
                                    || (!waitForStability)) {
                                if (verbose) {
                                    System.out.println(buffer.toString());
                                }
                                resultList.add(knownFiles.get(filePath));
                            } // end if (lastLength.longValue() == file.length())
                            else {
                                // File changed: register its new version
                                lastKnownLength.put(filePath, Long.valueOf(file.length()));
                                lastKnownModificationDate.put(filePath, lastModified);
                                knownFiles.put(filePath, file);
                            }
                        } // end if (lastLength == null) ... else
                        if (isCanceled()) {
                            break;
                        }
                    } // end for (File file : fileList)
                    if (isCanceled()) {
                        break;
                    }
                } // end for (Long lastModified : keySet)
                fileMap.clear();
            } // end if (dir.isDirectory())
            if (!isCanceled()) {
                publish(filterFileArray(resultList.toArray(new File[resultList.size()]), extensions));
                resultList.clear();
            }
        }

        public boolean isCanceled() {
            return canceled;
        }

        public void cancel() {
            canceled = true;
        }

        protected void clean() {
            knownFiles.clear();
            lastKnownLength.clear();
            lastKnownModificationDate.clear();
            path = null;
        }

    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

}
