/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.manager;

import fr.soleil.data.target.IFileTarget;

/**
 * An interface for something that handles a File or Directory. This is an {@link IFileTarget} for
 * which you can now the path of the last set file
 * 
 * @author girardot
 */
public interface IFileManager extends IFileTarget {

    /**
     * Returns the path of the last selected file/directory
     * 
     * @return A {@link String}
     */
    public String getWorkingPath();

}
