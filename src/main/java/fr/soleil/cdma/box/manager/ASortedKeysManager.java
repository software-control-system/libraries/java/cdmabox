package fr.soleil.cdma.box.manager;

import java.text.Collator;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.cdma.box.event.KeyEvent;
import fr.soleil.cdma.box.listener.IKeyListener;

public abstract class ASortedKeysManager implements IKeyManager {

    protected final Set<String> keySet;
    protected volatile String[] keys;
    protected final Collection<IKeyListener> keyListeners;

    public ASortedKeysManager() {
        keySet = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
        keys = new String[0];
        keyListeners = Collections.newSetFromMap(new ConcurrentHashMap<IKeyListener, Boolean>());
    }

    @Override
    public String[] getKeys() {
        return keys;
    }

    @Override
    public void addKeys(String... keys) {
        boolean added = false;
        if (keys != null) {
            for (String key : keys) {
                if (key != null) {
                    added = keySet.add(key) || added;
                }
            }
        }
        if (added) {
            String[] tmp = keySet.toArray(new String[keySet.size()]);
            Arrays.sort(tmp, Collator.getInstance());
            this.keys = tmp;
            warnKeyListeners();
        }
    }

    @Override
    public void addKeyListener(IKeyListener listener) {
        if (listener != null) {
            keyListeners.add(listener);
        }
    }

    @Override
    public void removeKeyListener(IKeyListener listener) {
        if (listener != null) {
            keyListeners.remove(listener);
        }
    }

    protected void warnKeyListeners() {
        KeyEvent event = new KeyEvent(this);
        for (IKeyListener listener : keyListeners) {
            if (listener != null) {
                listener.keyChanged(event);
            }
        }
    }
}
