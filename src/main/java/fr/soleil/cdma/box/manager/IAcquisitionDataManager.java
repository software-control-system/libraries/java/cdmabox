/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.manager;

import org.cdma.interfaces.IDataItem;

import fr.soleil.cdma.box.listener.IAcquisitionDataListener;
import fr.soleil.lib.project.data.FlatDoubleData;

/**
 * An interface for something that knows acquisition data
 * 
 * @author girardot
 */
public interface IAcquisitionDataManager {

    public static final String GAMMA_0 = "Gamma0";
    public static final String DELTA_0 = "Delta0";
    public static final String ENCODER_DELTA = "EncoderDelta";
    public static final String PIXEL_SIZE = "PixelSize";
    public static final String DETECTOR_BIAS = "DetectorBias";
    public static final String DETECTOR_GAIN = "DetectorGain";
    public static final String DETECTOR_BASED_UNCERTAINTY = "DetectorBasedUncertainty";
    public static final String Z0 = "Z0";
    public static final String X0 = "X0";
    public static final String X_POSITIONS = "XPositions";
    public static final String Y_POSITIONS = "YPositions";
    public static final String MI_LIST = "MiList";
    public static final String DARK = "Dark";
    public static final String WAVE_LENGTH = "WaveLength";
    public static final String DISTANCE = "Distance";
    public static final String SHUTTER_CLOSE_DELAY = "ShutterCloseDelay";
    public static final String EXPOSURE_TIME = "ExposureTime";
    public static final String X_BIN = "XBin";
    public static final String Z_BIN = "ZBin";
    public static final String FIRST_X_CHANNEL = "FirstXChannel";
    public static final String LAST_X_CHANNEL = "LastXChannel";
    public static final String FIRST_Y_CHANNEL = "FirstYChannel";
    public static final String LAST_Y_CHANNEL = "LastYChannel";
    public static final String SLICE_SCALE = "SliceScale";
    public static final String SLICES = "Slices";
    public static final String SWEEPS = "Sweeps";
    public static final String HIGH_ENERGY = "HighEnergy";
    public static final String LOW_ENERGY = "LowEnergy";
    public static final String PASS_ENERGY = "PassEnergy";
    public static final String ENERGY_STEP = "EnergyStep";
    public static final String ENERGY_SCALE = "EnergyScale";
    public static final String STEP_TIME = "StepTime";
    public static final String LENS_MODE = "LensMode";
    public static final String ACQUISITION_MODE = "AcquisitionMode";
    public static final String TWO_THETA = "TwoTheta";
    public static final String TWO_THETA_0 = "TwoTheta0";
    public static final String THETA_IN = "ThetaIn";
    public static final String THETA_Z0 = "ThetaZ0";
    public static final String THETA_Z_CALIBRATION_FACTOR = "ThetaZCalibrationFactor";
    public static final String SAMPLE_SLIT_DISTANCE = "SampleSlitDistance";
    public static final String CHANNEL0 = "Channel0";
    public static final String GEOMETRY = "Geometry";
    public static final String QXY = "Qxy";

    public Integer getXBin();

    public Integer getZBin();

    public Short getDetectorBias();

    public double getDetectorBasedUncertainty();

    public Object getExposureTime();

    public double getShutterCloseDelay();

    /**
     * Returns the sample-detector distance
     * 
     * @return a <code>double</code>
     */
    public double getDistance();

    /**
     * Returns the wavelength
     * 
     * @return a <code>double</code>
     */
    public double getWaveLength();

    /**
     * Returns the pixel size
     * 
     * @return a <code>double</code>
     */
    public double getPixelSize();

    public double getDetectorGain();

    public String[] getIntensityMonitorList();

    public double[] getIntensityHistorised(String im);

    public double getIntensityMonitorGain(String im);

    public double getIntensityMonitorBias(String im);

    /**
     * Return the value of some data that has a given name
     * 
     * @param name The name
     * @return A {@link FlatDoubleData}
     */
    public FlatDoubleData getNamedData(String name);

    /**
     * Return the shape of some data that has a given name
     * 
     * @param name The data name
     * @return An <code>int[]</code>.
     * @see #getNamedData(String)
     */
    public int[] getNamedDataShape(String name);

    public double[] getDark();

    public double[] getEnergyScale();

    public double[] getSliceScale();

    /**
     * Returns the PONI X
     * 
     * @return a <code>double</code>
     */
    public double getX0();

    /**
     * Returns the PONI Z
     * 
     * @return a <code>double</code>
     */
    public double getZ0();

    /**
     * @return the sweeps
     */
    public Integer getSweeps();

    /**
     * @return the acquisitionMode
     */
    public String getAcquisitionMode();

    /**
     * @return the lensMode
     */
    public String getLensMode();

    /**
     * @return the passEnergy
     */
    public double getPassEnergy();

    /**
     * @return the lowEnergy
     */
    public double getLowEnergy();

    /**
     * @return the highEnergy
     */
    public double getHighEnergy();

    /**
     * @return the energyStep
     */
    public double getEnergyStep();

    /**
     * @return the stepTime
     */
    public double getStepTime();

    /**
     * @return the firstXChannel
     */
    public Integer getFirstXChannel();

    /**
     * @return the lastXChannel
     */
    public Integer getLastXChannel();

    /**
     * @return the firstYChannel
     */
    public Integer getFirstYChannel();

    /**
     * @return the lastYChannel
     */
    public Integer getLastYChannel();

    /**
     * @return the slices
     */
    public Integer getSlices();

    /**
     * Returns the encoder &delta; angle (either a scalar or an array)
     * 
     * @return The encoder &delta; angle (either a scalar or an array)
     */
    public Object getEncoderDelta();

    /**
     * Returns the &delta; offset
     * 
     * @return A <code>double</code>
     */
    public double getDelta0();

    /**
     * Returns the &gamma; offset
     * 
     * @return A <code>double</code>
     */
    public double getGamma0();

    /**
     * Returns the 2&theta; angle
     * 
     * @return a <code>double[]</code>
     */
    public double[] getTwoTheta();

    /**
     * Returns the 2&theta;<sub>0</sub> value (position of the &delta;<sub>0</sub> motor)
     * 
     * @return a <code>double</code>
     */
    public double getTwoTheta0();

    /**
     * Returns the angle of incidence &theta;in
     * 
     * @return a <code>double</code>
     */
    public double getThetaIn();

    /**
     * Returns the &theta;z<sub>0</sub> value (position of the &gamma; motor)
     * 
     * @return a <code>double</code>
     */
    public double getThetaZ0();

    /**
     * Returns the &theta;z calibration factor
     * 
     * @return a <code>double</code>
     */
    public double getThetaZCalibrationFactor();

    /**
     * Returns the sample-slit distance.
     * 
     * @return a <code>double</code>
     */
    public double getSampleSlitDistance();

    /**
     * Returns the channel<sub>0</sub> value
     * 
     * @return a <code>double</code>
     */
    public double getChannel0();

    /**
     * Returns the geometry (1D, 2D)
     * 
     * @return A {@link String}
     */
    public String getGeometry();

    /**
     * Returns the Qxy value
     * 
     * @return A <code>double[]</code>
     */
    public double[] getQxy();

    public String[] getPersistingKeys();

    public IDataItem getPersistingDataItem(String key);

    public Object getPersistingData(IDataItem item);

    public Number getPersistingData(IDataItem item, int flatIndex, int[] position);

    public String[] getPersistingKeysHeader();

    public String getPersistingDataHeader(String key);

    public String getPersistingDataHeader(String key, int flatIndex, int[] position);

    public void addAcquisitionDataListener(IAcquisitionDataListener listener);

    public void removeAcquisitionDataListener(IAcquisitionDataListener listener);

    public void removeAllAcquisitionDataListeners();
}
