/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.manager;

import java.util.HashSet;
import java.util.Set;

import fr.soleil.cdma.box.data.LoadableData;
import fr.soleil.cdma.box.exception.CDMAAccessException;
import fr.soleil.cdma.box.reader.ICDMAReader;

/**
 * A class that is used to prepare acquisition loading
 * 
 * @author girardot
 */
public class AcquisitionManager {

    protected final ICDMAReader cdmaReader;
    private String detector;
    private final Set<LoadableData> dataToLoad;

    /**
     * Constructs this {@link AcquisitionManager}
     * 
     * @param cdmaReader The {@link ICDMAReader} to associate to this {@link AcquisitionManager}
     */
    public AcquisitionManager(ICDMAReader cdmaReader) {
        this.cdmaReader = cdmaReader;
        detector = null;
        dataToLoad = new HashSet<LoadableData>();
    }

    /**
     * Asks the associated {@link ICDMAReader} to load acquisition
     * 
     * @throws CDMAAccessException If a problem occurred during acquisition loading
     */
    public void loadAcquisition() throws CDMAAccessException {
        if (cdmaReader != null) {
            LoadableData[] toLoad;
            synchronized (dataToLoad) {
                toLoad = dataToLoad.toArray(new LoadableData[dataToLoad.size()]);
            }
            cdmaReader.loadAcquisition(detector, toLoad);
        }
    }

    /**
     * Returns the previously set detector.
     * 
     * @return A {@link String}.
     */
    public String getDetector() {
        return detector;
    }

    /**
     * Sets the detector to use for acquisition loading.
     * 
     * @param detector The detector to use.
     */
    public void setDetector(String detector) {
        this.detector = detector;
    }

    /**
     * Returns whether some optional data will be loaded
     * 
     * @param data The {@link LoadableData} that represents the data to load
     * @return A <code>boolean</code>
     */
    public boolean hasLoadableData(LoadableData data) {
        boolean contains;
        if (data == null) {
            contains = false;
        } else {
            synchronized (dataToLoad) {
                contains = dataToLoad.contains(data);
            }
        }
        return contains;
    }

    /**
     * Adds some optional data to load
     * 
     * @param data The {@link LoadableData} that represents the data to load
     */
    public void addLoadableData(LoadableData data) {
        if (data != null) {
            synchronized (dataToLoad) {
                dataToLoad.add(data);
            }
        }
    }

    /**
     * Returns the associated {@link ICDMAReader}
     * 
     * @return An {@link ICDMAReader}
     */
    public ICDMAReader getCdmaReader() {
        return cdmaReader;
    }

}
