/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.manager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.cdma.dictionary.LogicalGroup;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IDataItem;

import fr.soleil.cdma.box.event.AcquisitionDataEvent;
import fr.soleil.cdma.box.listener.IAcquisitionDataListener;
import fr.soleil.cdma.box.reader.AbstractCDMAReader;
import fr.soleil.cdma.box.reader.ICDMAReader;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.data.FlatDoubleData;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.UnsignedConverter;

public class BasicAcquisitionDataManager implements IAcquisitionDataManager {

    private Integer xBin;
    private Integer zBin;
    private Object exposureTime;
    private double shutterCloseDelay;
    private double distance;
    private double waveLength;
    private double pixelSize;
    private double detectorGain;
    private Short detectorBias;
    private double detectorBasedUncertainty;
    private double[] dark;
    private double[] energyScale;
    private double[] sliceScale;
    private String[] miList;
    private Integer sweeps;
    private String acquisitionMode;
    private String lensMode;
    private double passEnergy;
    private double lowEnergy;
    private double highEnergy;
    private double energyStep;
    private double stepTime;
    private double x0;
    private double z0;
    private Integer firstXChannel;
    private Integer lastXChannel;
    private Integer firstYChannel;
    private Integer lastYChannel;
    private Integer slices;
    private Object encoderDelta;
    private double delta0;
    private double gamma0;
    private double[] twoTheta;
    private double twoTheta0;
    private double thetaIn;
    private double thetaz0;
    private double thetazCalibrationFactor;
    private double sampleSlitDistance;
    private double channel0;
    private String geometry;
    private double[] qxy;
    private final WeakReference<ICDMAReader> readerReference;
    private final LogicalGroup dataGroup, monitorGroup;

    private String[] persistingKeys;
    private Map<String, IDataItem> persistingData;
    private String[] persistingKeysHeader;
    private Map<String, IDataItem> persistingDataHeader;

    private final List<WeakReference<IAcquisitionDataListener>> listeners;

    public BasicAcquisitionDataManager(ICDMAReader reader, LogicalGroup dataGroup, LogicalGroup monitorGroup) {
        super();
        readerReference = new WeakReference<ICDMAReader>(reader);
        this.dataGroup = dataGroup;
        this.monitorGroup = monitorGroup;
        listeners = new ArrayList<WeakReference<IAcquisitionDataListener>>();
        sweeps = null;
        acquisitionMode = null;
        lensMode = null;
        passEnergy = Double.NaN;
        lowEnergy = Double.NaN;
        highEnergy = Double.NaN;
        energyStep = Double.NaN;
        stepTime = Double.NaN;
        x0 = Double.NaN;
        z0 = Double.NaN;
        firstXChannel = null;
        lastXChannel = null;
        firstYChannel = null;
        lastYChannel = null;
        slices = null;
        energyScale = null;
        sliceScale = null;
        encoderDelta = null;
        delta0 = Double.NaN;
        gamma0 = Double.NaN;
        detectorBias = null;
        twoTheta = null;
        twoTheta0 = Double.NaN;
        thetaIn = Double.NaN;
        thetaz0 = Double.NaN;
        thetazCalibrationFactor = Double.NaN;
        sampleSlitDistance = Double.NaN;
        channel0 = Double.NaN;
        geometry = null;
        qxy = null;

        persistingKeys = null;
        persistingData = null;
        persistingKeysHeader = null;
        persistingDataHeader = null;
    }

    @Override
    public Integer getXBin() {
        return xBin;
    }

    public void setXBin(Integer xBin) {
        this.xBin = xBin;
        warnListeners(X_BIN);
    }

    @Override
    public Integer getZBin() {
        return zBin;
    }

    public void setZBin(Integer zBin) {
        this.zBin = zBin;
        warnListeners(Z_BIN);
    }

    @Override
    public Object getExposureTime() {
        return exposureTime;
    }

    public void setExposureTime(Object exposureTime) {
        this.exposureTime = exposureTime;
        warnListeners(EXPOSURE_TIME);
    }

    @Override
    public double getShutterCloseDelay() {
        return shutterCloseDelay;
    }

    public void setShutterCloseDelay(double shutterCloseDelay) {
        this.shutterCloseDelay = shutterCloseDelay;
        warnListeners(SHUTTER_CLOSE_DELAY);
    }

    @Override
    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
        warnListeners(DISTANCE);
    }

    @Override
    public double getWaveLength() {
        return waveLength;
    }

    public void setWaveLength(double waveLength) {
        this.waveLength = waveLength;
        warnListeners(WAVE_LENGTH);
    }

    @Override
    public double[] getIntensityHistorised(String mi) {
        double[] intensity = null;
        ICDMAReader reader = ObjectUtils.recoverObject(readerReference);
        if (reader != null) {
            intensity = reader.getIntensityHistorised(mi, monitorGroup);
        }
        return intensity;
    }

    @Override
    public double getIntensityMonitorGain(String mi) {
        double gain = Double.NaN;
        ICDMAReader reader = ObjectUtils.recoverObject(readerReference);
        if (reader != null) {
            gain = reader.getGain(mi, monitorGroup);
        }
        return gain;
    }

    @Override
    public double getIntensityMonitorBias(String mi) {
        double bias = Double.NaN;
        ICDMAReader reader = ObjectUtils.recoverObject(readerReference);
        if (reader != null) {
            bias = reader.getBias(mi, monitorGroup);
        }
        return bias;
    }

    @Override
    public FlatDoubleData getNamedData(String name) {
        FlatDoubleData data = null;
        ICDMAReader reader = ObjectUtils.recoverObject(readerReference);
        if (reader != null) {
            data = reader.getNamedData(name, dataGroup);
        }
        return data;
    }

    @Override
    public int[] getNamedDataShape(String name) {
        int[] shape = null;
        ICDMAReader reader = ObjectUtils.recoverObject(readerReference);
        if (reader != null) {
            shape = reader.getNamedDataShape(name, dataGroup);
        }
        return shape;
    }

    @Override
    public double[] getDark() {
        return dark;
    }

    public void setDark(double[] dark) {
        this.dark = dark;
        warnListeners(DARK);
    }

    @Override
    public String[] getIntensityMonitorList() {
        return miList;
    }

    public void setMiList(String[] miList) {
        this.miList = miList;
        warnListeners(MI_LIST);
    }

    @Override
    public Integer getSweeps() {
        return sweeps;
    }

    /**
     * @param sweeps the sweeps to set
     */
    public void setSweeps(Integer sweeps) {
        this.sweeps = sweeps;
    }

    @Override
    public String getAcquisitionMode() {
        return acquisitionMode;
    }

    /**
     * @param acquisitionMode the acquisitionMode to set
     */
    public void setAcquisitionMode(String acquisitionMode) {
        this.acquisitionMode = acquisitionMode;
    }

    @Override
    public String getLensMode() {
        return lensMode;
    }

    /**
     * @param lensMode the lensMode to set
     */
    public void setLensMode(String lensMode) {
        this.lensMode = lensMode;
    }

    @Override
    public double getPassEnergy() {
        return passEnergy;
    }

    /**
     * @param passEnergy the passEnergy to set
     */
    public void setPassEnergy(double passEnergy) {
        this.passEnergy = passEnergy;
    }

    @Override
    public double getLowEnergy() {
        return lowEnergy;
    }

    /**
     * @param lowEnergy the lowEnergy to set
     */
    public void setLowEnergy(double lowEnergy) {
        this.lowEnergy = lowEnergy;
    }

    @Override
    public double getHighEnergy() {
        return highEnergy;
    }

    /**
     * @param highEnergy the highEnergy to set
     */
    public void setHighEnergy(double highEnergy) {
        this.highEnergy = highEnergy;
    }

    @Override
    public double getEnergyStep() {
        return energyStep;
    }

    /**
     * @param energyStep the energyStep to set
     */
    public void setEnergyStep(double energyStep) {
        this.energyStep = energyStep;
    }

    @Override
    public double getStepTime() {
        return stepTime;
    }

    /**
     * @param stepTime the stepTime to set
     */
    public void setStepTime(double stepTime) {
        this.stepTime = stepTime;
    }

    @Override
    public Integer getFirstXChannel() {
        return firstXChannel;
    }

    /**
     * @param firstXChannel the firstXChannel to set
     */
    public void setFirstXChannel(Integer firstXChannel) {
        this.firstXChannel = firstXChannel;
    }

    @Override
    public Integer getLastXChannel() {
        return lastXChannel;
    }

    /**
     * @param lastXChannel the lastXChannel to set
     */
    public void setLastXChannel(Integer lastXChannel) {
        this.lastXChannel = lastXChannel;
    }

    @Override
    public Integer getFirstYChannel() {
        return firstYChannel;
    }

    /**
     * @param firstYChannel the firstYChannel to set
     */
    public void setFirstYChannel(Integer firstYChannel) {
        this.firstYChannel = firstYChannel;
    }

    @Override
    public Integer getLastYChannel() {
        return lastYChannel;
    }

    /**
     * @param lastYChannel the lastYChannel to set
     */
    public void setLastYChannel(Integer lastYChannel) {
        this.lastYChannel = lastYChannel;
    }

    @Override
    public Integer getSlices() {
        return slices;
    }

    /**
     * @param slices the slices to set
     */
    public void setSlices(Integer slices) {
        this.slices = slices;
    }

    @Override
    public double[] getEnergyScale() {
        return energyScale;
    }

    /**
     * @param energyScale the energyScale to set
     */
    public void setEnergyScale(double[] energyScale) {
        this.energyScale = energyScale;
    }

    @Override
    public double[] getSliceScale() {
        return sliceScale;
    }

    /**
     * @param sliceScale the sliceScale to set
     */
    public void setSliceScale(double[] sliceScale) {
        this.sliceScale = sliceScale;
    }

    @Override
    public void addAcquisitionDataListener(IAcquisitionDataListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                List<WeakReference<IAcquisitionDataListener>> toRemove = new ArrayList<WeakReference<IAcquisitionDataListener>>();
                boolean canAdd = true;
                for (WeakReference<IAcquisitionDataListener> ref : listeners) {
                    IAcquisitionDataListener temp = ref.get();
                    if (temp == null) {
                        toRemove.add(ref);
                    } else if (temp.equals(listener)) {
                        canAdd = false;
                    }
                }
                listeners.removeAll(toRemove);
                toRemove.clear();
                if (canAdd) {
                    listeners.add(new WeakReference<IAcquisitionDataListener>(listener));
                }
            }
        }
    }

    @Override
    public void removeAcquisitionDataListener(IAcquisitionDataListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                List<WeakReference<IAcquisitionDataListener>> toRemove = new ArrayList<WeakReference<IAcquisitionDataListener>>();
                for (WeakReference<IAcquisitionDataListener> ref : listeners) {
                    IAcquisitionDataListener temp = ref.get();
                    if ((temp == null) || temp.equals(listener)) {
                        toRemove.add(ref);
                    }
                }
                listeners.removeAll(toRemove);
                toRemove.clear();
            }
        }
    }

    @Override
    public void removeAllAcquisitionDataListeners() {
        synchronized (listeners) {
            listeners.clear();
        }
    }

    /**
     * Warns {@link IAcquisitionDataListener}s for some changes
     */
    protected void warnListeners(String... properties) {
        synchronized (listeners) {
            AcquisitionDataEvent event = new AcquisitionDataEvent(this, false, properties);
            List<WeakReference<IAcquisitionDataListener>> toRemove = new ArrayList<WeakReference<IAcquisitionDataListener>>();
            for (WeakReference<IAcquisitionDataListener> ref : listeners) {
                IAcquisitionDataListener listener = ref.get();
                if (listener == null) {
                    toRemove.add(ref);
                } else {
                    listener.acquisitionDataChanged(event);
                }
            }
            listeners.removeAll(toRemove);
            toRemove.clear();
        }
    }

    @Override
    public double getX0() {
        return x0;
    }

    public void setX0(double x0) {
        this.x0 = x0;
        warnListeners(X0);
    }

    @Override
    public double getZ0() {
        return z0;
    }

    public void setZ0(double z0) {
        this.z0 = z0;
        warnListeners(Z0);
    }

    @Override
    public Short getDetectorBias() {
        return detectorBias;
    }

    public void setDetectorBias(Short bias) {
        this.detectorBias = bias;
        warnListeners(DETECTOR_BIAS);
    }

    @Override
    public double getDetectorBasedUncertainty() {
        return detectorBasedUncertainty;
    }

    public void setDetectorBasedUncertainty(double detectorBasedUncertainty) {
        this.detectorBasedUncertainty = detectorBasedUncertainty;
        warnListeners(DETECTOR_BASED_UNCERTAINTY);
    }

    @Override
    public double getPixelSize() {
        return pixelSize;
    }

    public void setPixelSize(double pixelSize) {
        this.pixelSize = pixelSize;
        warnListeners(PIXEL_SIZE);
    }

    @Override
    public double getDetectorGain() {
        return detectorGain;
    }

    public void setDetectorGain(double gain) {
        this.detectorGain = gain;
        warnListeners(DETECTOR_GAIN);
    }

    @Override
    public Object getEncoderDelta() {
        return encoderDelta;
    }

    public void setEncoderDelta(Object delta) {
        this.encoderDelta = delta;
        warnListeners(ENCODER_DELTA);
    }

    @Override
    public double getDelta0() {
        return delta0;
    }

    public void setDelta0(double delta0) {
        this.delta0 = delta0;
        warnListeners(DELTA_0);
    }

    @Override
    public double getGamma0() {
        return gamma0;
    }

    public void setGamma0(double gamma0) {
        this.gamma0 = gamma0;
        warnListeners(GAMMA_0);
    }

    @Override
    public double[] getTwoTheta() {
        return twoTheta;
    }

    public void setTwoTheta(double[] twoTheta) {
        this.twoTheta = twoTheta;
        warnListeners(TWO_THETA);
    }

    @Override
    public double getTwoTheta0() {
        return twoTheta0;
    }

    public void setTwoTheta0(double twoTheta0) {
        this.twoTheta0 = twoTheta0;
        warnListeners(TWO_THETA_0);
    }

    @Override
    public double getThetaIn() {
        return thetaIn;
    }

    public void setThetaIn(double thetaIn) {
        this.thetaIn = thetaIn;
        warnListeners(THETA_IN);
    }

    @Override
    public double getThetaZ0() {
        return thetaz0;
    }

    public void setThetaZ0(double thetaz0) {
        this.thetaz0 = thetaz0;
        warnListeners(THETA_Z0);
    }

    @Override
    public double getThetaZCalibrationFactor() {
        return thetazCalibrationFactor;
    }

    public void setThetaZCalibrationFactor(double thetazCalibrationFactor) {
        this.thetazCalibrationFactor = thetazCalibrationFactor;
        warnListeners(THETA_Z_CALIBRATION_FACTOR);
    }

    @Override
    public double getSampleSlitDistance() {
        return sampleSlitDistance;
    }

    public void setSampleSlitDistance(double sampleSlitDistance) {
        this.sampleSlitDistance = sampleSlitDistance;
        warnListeners(SAMPLE_SLIT_DISTANCE);
    }

    @Override
    public double getChannel0() {
        return channel0;
    }

    public void setChannel0(double channel0) {
        this.channel0 = channel0;
        warnListeners(CHANNEL0);
    }

    @Override
    public String getGeometry() {
        return geometry;
    }

    public void setGeometry(String geometry) {
        this.geometry = geometry == null ? null : geometry.trim();
        warnListeners(GEOMETRY);
    }

    @Override
    public double[] getQxy() {
        return qxy;
    }

    public void setQxy(double[] qxy) {
        this.qxy = qxy;
        warnListeners(QXY);
    }

    @Override
    public String[] getPersistingKeys() {
        return persistingKeys;
    }

    @Override
    public String[] getPersistingKeysHeader() {
        return persistingKeysHeader;
    }

    @Override
    public IDataItem getPersistingDataItem(String key) {
        return ((key == null) || (persistingData == null) ? null : persistingData.get(key));
    }

    @Override
    public Object getPersistingData(IDataItem item) {
        Object value = null;
        if (item != null) {
            try {
                if (item.isScalar()) {
                    value = AbstractCDMAReader.extractSignedNumber(item);
                } else {
                    value = AbstractCDMAReader.extractArrayValue(item, item.getData(), false);
                }
                if (item.isUnsigned()) {
                    value = UnsignedConverter.convertUnsigned(value);
                }
            } catch (Exception e) {
                value = null;
            }
        }
        return value;
    }

    @Override
    public String getPersistingDataHeader(String key) {
        String result = null;
        Object value = null;
        IDataItem item = ((key == null) || (persistingDataHeader == null) ? null : persistingDataHeader.get(key));
        if (item != null) {
            try {
                if (item.isScalar()) {
                    Class<?> type = item.getType();
                    if (ObjectUtils.isNumberClass(type)) {
                        value = AbstractCDMAReader.extractSignedNumber(item);
                    } else if (Boolean.TYPE.equals(type) || Boolean.class.equals(type)) {
                        value = Boolean.valueOf(item.readScalarByte() != 0);
                    } else {
                        value = item.readScalarString();
                    }
                } else {
                    value = AbstractCDMAReader.extractArrayValue(item, item.getData(), false);
                }
                result = getStringValue(value, item);
            } catch (Exception e) {
                value = null;
            }
        }
        return result;
    }

    protected String getStringValue(Object value, IDataItem item) {
        String result = null;
        if (item.isUnsigned()) {
            value = UnsignedConverter.convertUnsigned(value);
        }
        if (value != null) {
            result = ArrayUtils.toString(value);
            IAttribute unit = item.getAttribute("unit");
            if (unit != null) {
                String tmp = unit.getStringValue();
                if ((tmp != null) && (!tmp.trim().isEmpty())) {
                    result = result + " " + tmp;
                }
            }
        }
        return result;
    }

    @Override
    public Number getPersistingData(IDataItem item, int flatIndex, int[] position) {
        Number value = null;
        try {
            if ((flatIndex > -1) && (position != null) && (position.length > 0) && (item != null)) {
                Class<?> type = item.getType();
                if (ObjectUtils.isNumberClass(type)) {
                    if (item.isScalar()) {
                        value = AbstractCDMAReader.extractSignedNumber(item);
                    } else {
                        int[] shape = item.getShape();
                        IArray valueArray = null;
                        if ((shape != null) && (shape.length > 0)) {
                            if (shape.length == 1) {
                                if (shape[0] > flatIndex) {
                                    valueArray = item.getData(new int[] { flatIndex }, new int[] { 1 });
                                }
                            } else if ((shape.length == 2) && (shape[1] == 1)) {
                                // XXX stupid hack, due to JAVAAPI-377
                                valueArray = item.getData(new int[] { flatIndex, 0 }, new int[] { 1, 1 });
                            } else if (ArrayUtils.isPosition(shape, position)) {
                                valueArray = item.getData(position, new int[] { 1 });
                            }
                        }
                        if (valueArray != null) {
                            if ((Byte.class.equals(type)) || Byte.TYPE.equals(type)) {
                                value = Byte.valueOf(valueArray.getByte(valueArray.getIndex()));
                            } else if ((Short.class.equals(type)) || Short.TYPE.equals(type)) {
                                value = Short.valueOf(valueArray.getShort(valueArray.getIndex()));
                            } else if ((Integer.class.equals(type)) || Integer.TYPE.equals(type)) {
                                value = Integer.valueOf(valueArray.getInt(valueArray.getIndex()));
                            } else if ((Long.class.equals(type)) || Long.TYPE.equals(type)) {
                                value = Long.valueOf(valueArray.getLong(valueArray.getIndex()));
                            } else if ((Float.class.equals(type)) || Float.TYPE.equals(type)) {
                                value = Float.valueOf(valueArray.getFloat(valueArray.getIndex()));
                            } else if ((Double.class.equals(type)) || Double.TYPE.equals(type)
                                    || Number.class.equals(type)) {
                                value = Double.valueOf(valueArray.getDouble(valueArray.getIndex()));
                            } else {
                                value = null;
                            }
                        }
                    }
                    if (item.isUnsigned()) {
                        value = (Number) UnsignedConverter.convertUnsigned(value);
                    }
                }
            }
        } catch (Exception e) {
            value = null;
        }
        return value;
    }

    @Override
    public String getPersistingDataHeader(String key, int flatIndex, int[] position) {
        String result = null;
        Object value = null;
        try {
            if ((flatIndex > -1) && (position != null) && (position.length > 0)) {
                IDataItem item = ((key == null) || (persistingDataHeader == null) ? null
                        : persistingDataHeader.get(key));
                if (item != null) {
                    Class<?> type = item.getType();
                    if (item.isScalar()) {
                        if (ObjectUtils.isNumberClass(type)) {
                            value = AbstractCDMAReader.extractSignedNumber(item);
                        } else if (Boolean.TYPE.equals(type) || Boolean.class.equals(type)) {
                            value = Boolean.valueOf(item.readScalarByte() != 0);
                        } else {
                            value = item.readScalarString();
                        }
                    } else {
                        int[] shape = item.getShape();
                        IArray valueArray = null;
                        if ((shape != null) && (shape.length > 0)) {
                            if (shape.length == 1) {
                                if (shape[0] > flatIndex) {
                                    valueArray = item.getData(new int[] { flatIndex }, new int[] { 1 });
                                }
                            } else if ((shape.length == 2) && (shape[1] == 1)) {
                                // XXX stupid hack, due to JAVAAPI-377
                                valueArray = item.getData(new int[] { flatIndex, 0 }, new int[] { 1, 1 });
                            } else if (ArrayUtils.isPosition(shape, position)) {
                                valueArray = item.getData(position, new int[] { 1 });
                            }
                        }
                        if (valueArray != null) {
                            if ((Byte.class.equals(type)) || Byte.TYPE.equals(type)) {
                                value = Byte.valueOf(valueArray.getByte(valueArray.getIndex()));
                            } else if ((Short.class.equals(type)) || Short.TYPE.equals(type)) {
                                value = Short.valueOf(valueArray.getShort(valueArray.getIndex()));
                            } else if ((Integer.class.equals(type)) || Integer.TYPE.equals(type)) {
                                value = Integer.valueOf(valueArray.getInt(valueArray.getIndex()));
                            } else if ((Long.class.equals(type)) || Long.TYPE.equals(type)) {
                                value = Long.valueOf(valueArray.getLong(valueArray.getIndex()));
                            } else if ((Float.class.equals(type)) || Float.TYPE.equals(type)) {
                                value = Float.valueOf(valueArray.getFloat(valueArray.getIndex()));
                            } else if ((Double.class.equals(type)) || Double.TYPE.equals(type)
                                    || Number.class.equals(type)) {
                                value = Double.valueOf(valueArray.getDouble(valueArray.getIndex()));
                            } else if (Boolean.TYPE.equals(type) || Boolean.class.equals(type)) {
                                value = Boolean.valueOf(valueArray.getByte(valueArray.getIndex()) != 0);
                            } else {
                                value = valueArray.getObject(valueArray.getIndex());
                            }
                        }
                    }
                    result = getStringValue(value, item);
                }
            }
        } catch (Exception e) {
            value = null;
        }
        return result;
    }

    public void setPersistingKeys(String[] persistingKeys, Map<String, IDataItem> persistingData) {
        this.persistingKeys = persistingKeys;
        this.persistingData = persistingData;
    }

    public void setPersistingKeysHeader(String[] persistingKeys, Map<String, IDataItem> persistingData) {
        this.persistingKeysHeader = persistingKeys;
        this.persistingDataHeader = persistingData;
    }
}
