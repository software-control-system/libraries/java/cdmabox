package fr.soleil.cdma.box.manager;

import fr.soleil.cdma.box.listener.IKeyListener;

/**
 * An interface for something that manages some CDMA keys
 * 
 * @author GIRARDOT
 */
public interface IKeyManager {

    /**
     * Returns the keys known by this {@link IKeyManager}
     * 
     * @return A {@link String} array
     */
    public String[] getKeys();

    /**
     * Adds some keys to this {@link IKeyManager}
     * 
     * @param keys The keys to add
     */
    public void addKeys(String... keys);

    /**
     * Adds an {@link IKeyListener} to this {@link IKeyManager}
     * 
     * @param listener The {@link IKeyListener} to add
     */
    public void addKeyListener(IKeyListener listener);

    /**
     * Removes an {@link IKeyListener} from this {@link IKeyManager}
     * 
     * @param listener The {@link IKeyListener} to remove
     */
    public void removeKeyListener(IKeyListener listener);

}
