package fr.soleil.cdma.box.manager;

import fr.soleil.cdma.box.data.scan.ScanData;
import fr.soleil.cdma.box.target.IScanTarget;
import fr.soleil.data.mediator.Mediator;

public class PersistingKeysManager extends ASortedKeysManager implements IScanTarget {

    public PersistingKeysManager() {
        super();
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void setScan(ScanData scan) {
        if (scan != null) {
            IAcquisitionDataManager dataManager = scan.getAcquisitionDataManager();
            if (dataManager != null) {
                addKeys(dataManager.getPersistingKeys());
            }
        }
    }

}
