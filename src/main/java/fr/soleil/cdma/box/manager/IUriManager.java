/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.manager;

import fr.soleil.cdma.box.target.IUriTarget;
import fr.soleil.data.target.scalar.ITextTarget;

public interface IUriManager {

    /**
     * Returns the {@link IUriTarget} this {@link IUriManager} uses
     * 
     * @return An {@link IUriTarget}
     */
    public IUriTarget getUriHandler();

    /**
     * Sets the {@link IUriTarget} this {@link IUriManager} can use
     * 
     * @param uriHandler The {@link IUriTarget} to use
     */
    public void setUriHandler(IUriTarget uriHandler);

    /**
     * Returns the {@link ITextTarget} this {@link IUriManager} uses to recover URI simple name
     * 
     * @return An {@link ITextTarget}
     */
    public ITextTarget getUriNameHandler();

    /**
     * Sets the {@link ITextTarget} this {@link IUriManager} can use to recover URI simple name
     * 
     * @param uriNameHandler The {@link ITextTarget} to use
     */
    public void setUriNameHandler(ITextTarget uriNameHandler);

}
