/**
 * 
 */
package fr.soleil.cdma.box.util;

import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author viguier
 * 
 */
public class DataFormatter {

    public static Format getNumberFormat(int precision) {
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(precision);
        return numberFormat;
    }

    public static String getNumberAsString(double number, int precision) {
        Format format = getNumberFormat(precision);
        return format.format(number);
    }

    public static String getDateAsTimestamp(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd" + "-" + "HH.mm.ss");
        return dateFormat.format(date);
    }
}
