package fr.soleil.cdma.box.util.comparator;

import java.io.File;

import fr.soleil.lib.project.resource.MessageManager;

public class CreationDateFileNodeComparator extends FileNodeComparator {

    public CreationDateFileNodeComparator(boolean ascending, MessageManager messageManager) {
        super(ascending, messageManager);
    }

    @Override
    protected int compareNotNull(File f1, File f2) {
        return FileComparisonUtils.compareExistingFilesCreationDate(ascending, f1, f2);
    }

    @Override
    public String getName(boolean ascending) {
        return ascending
                ? messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.date.creation.ascending")
                : messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.date.creation.descending");
    }

}
