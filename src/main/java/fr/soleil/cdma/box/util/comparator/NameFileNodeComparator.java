package fr.soleil.cdma.box.util.comparator;

import java.io.File;
import java.text.Collator;

import fr.soleil.lib.project.resource.MessageManager;

public class NameFileNodeComparator extends FileNodeComparator {

    protected final Collator collator;

    public NameFileNodeComparator(boolean ascending, MessageManager messageManager) {
        super(ascending, messageManager);
        collator = Collator.getInstance();
    }

    @Override
    protected int compareNotNull(File f1, File f2) {
        return FileComparisonUtils.compareExistingFilesName(collator, ascending, f1, f2);
    }

    @Override
    public String getName(boolean ascending) {
        return ascending ? messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.name.ascending")
                : messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.name.descending");
    }

}
