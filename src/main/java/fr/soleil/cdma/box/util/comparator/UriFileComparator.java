package fr.soleil.cdma.box.util.comparator;

import java.io.File;
import java.lang.ref.WeakReference;

import javax.swing.tree.DefaultMutableTreeNode;

import fr.soleil.cdma.box.view.UriView;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.resource.MessageManager;

public abstract class UriFileComparator extends NamedComparator<DefaultMutableTreeNode> {
    protected final WeakReference<UriView> uriViewRef;

    public UriFileComparator(boolean ascending, MessageManager messageManager, UriView uriView) {
        super(ascending, messageManager);
        uriViewRef = uriView == null ? null : new WeakReference<UriView>(uriView);
    }

    @Override
    protected abstract String getName(boolean ascending);

    @Override
    public int compare(DefaultMutableTreeNode o1, DefaultMutableTreeNode o2) {
        int result = 0;
        if (o1 == null) {
            if (o2 == null) {
                result = 0;
            } else {
                result = -1;
            }
        } else if (o2 == null) {
            result = 1;
        } else {
            UriView uriView = ObjectUtils.recoverObject(uriViewRef);
            if (uriView == null) {
                result = 0;
            } else {
                ITreeNode i1 = uriView.getTreeNode(o1), i2 = uriView.getTreeNode(o2);
                File f1 = uriView.getFile(i1), f2 = uriView.getFile(i2);
                if (f1 == null) {
                    if (f2 == null) {
                        result = compareNullFiles(f1, f2, i1, i2, o1, o2);
                    } else {
                        result = -1;
                    }
                } else if (f2 == null) {
                    result = 1;
                } else if (f1.exists()) {
                    if (f2.exists()) {
                        result = compareExistingFiles(f1, f2);
                    } else {
                        result = 1;
                    }
                } else if (f2.exists()) {
                    result = -1;
                } else {
                    result = 0;
                }
            }
        }
        return result;
    }

    protected abstract int compareExistingFiles(File f1, File f2);

    protected int compareNullFiles(File f1, File f2, ITreeNode i1, ITreeNode i2, DefaultMutableTreeNode o1,
            DefaultMutableTreeNode o2) {
        return 0;
    }

}