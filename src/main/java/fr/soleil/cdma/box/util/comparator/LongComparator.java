/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.util.comparator;

import java.util.Comparator;

/**
 * A {@link Comparator} for {@link Long} values
 * 
 * @author girardot
 */
public class LongComparator implements Comparator<Long> {

    private boolean inverted = false;

    /**
     * Constructor
     */
    public LongComparator() {
        this(false);
    }

    /**
     * Constructor
     * 
     * @param inverted
     *            A boolean that allows to use inverted sorting. <code>TRUE</code> to use inverted data sorting,
     *            <code>FALSE</code> to keep classic data sorting.
     */
    public LongComparator(boolean inverted) {
        super();
        this.inverted = inverted;
    }

    @Override
    public int compare(Long l1, Long l2) {
        int result = 0;
        if (l1 != null) {
            result = l1.compareTo(l2);
        } else if (l2 != null) {
            result = -1 * l2.compareTo(l1);
        }
        if (inverted) {
            result *= -1;
        }
        return result;
    }

}
