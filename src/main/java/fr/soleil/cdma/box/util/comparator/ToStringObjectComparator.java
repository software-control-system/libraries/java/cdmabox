/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.util.comparator;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * An {@link Object} {@link Comparator}, that compares {@link Object}'s {@link String} representation
 * 
 * @author girardot
 */
public class ToStringObjectComparator implements Comparator<Object> {

    private final Collator referenceComparator;

    public ToStringObjectComparator() {
        referenceComparator = Collator.getInstance(Locale.FRENCH);
    }

    @Override
    public int compare(Object p_o1, Object p_o2) {
        if (p_o1 != null && p_o2 != null) {
            return referenceComparator.compare(p_o1.toString(), p_o2.toString());
        }
        return 0;
    }

}
