/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.util;

import java.net.URI;
import java.util.Collection;

import org.cdma.Factory;
import org.cdma.IFactory;
import org.cdma.dictionary.ExtendedDictionary;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IDatasource;
import org.cdma.utilities.LabelledURI;

import fr.soleil.cdma.box.exception.CDMAAccessException;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.ComponentUtils;

public class CDMABoxUtils extends ComponentUtils {

    public static final MessageManager MESSAGE_MANAGER = new MessageManager("fr.soleil.cdma.box");

    /**
     * Removes all leading tabs (<code>\t</code>) and line feeds (<code>\n</code>) from a {@link StringBuilder}
     * 
     * @param contextBuilder The {@link StringBuilder}.
     */
    public static void trimContextBuilder(StringBuilder contextBuilder) {
        if ((contextBuilder != null) && (contextBuilder.length() > 0)) {
            int index = contextBuilder.length() - 1;
            while (contextBuilder.charAt(index) == '\n' || contextBuilder.charAt(index) == '\t') {
                contextBuilder.deleteCharAt(index--);
            }
        }
    }

    /**
     * Recovers the path of the mapping dictionary used by an {@link IDataset}
     * 
     * @param view The dictionary view
     * @param reader The {@link IDataset}.
     * @return A {@link String}. Can be <code>null</code>.
     */
    public static String getMappingDictionary(String view, IDataset reader) {
        String dictionary = null;
        if (reader != null) {
            dictionary = getMappingDictionary(reader.getLogicalRoot(view));
        }
        return dictionary;
    }

    /**
     * Recovers the path of the mapping dictionary associated with a {@link LogicalGroup}
     * 
     * @param group The {@link LogicalGroup}.
     * @return A {@link String}. Can be <code>null</code>.
     */
    public static String getMappingDictionary(LogicalGroup group) {
        String dictionary = null;
        if (group != null) {
            ExtendedDictionary tmp = group.getDictionary();
            if (tmp != null) {
                dictionary = tmp.getMappingFilePath();
            }
        }
        return dictionary;
    }

    /**
     * Returns whether an {@link URI} represents an experiment.
     * 
     * @param uri The {@link URI} to check.
     * @return A <code>boolean</code> value.
     */
    public static boolean isExperiment(final URI uri) {
        boolean experiment;
        if (uri == null) {
            experiment = false;
        } else {
            IFactory factory = Factory.getFactory(uri);
            if (factory == null) {
                experiment = false;
            } else {
                IDatasource pluginURIDetector = factory.getPluginURIDetector();
                if (pluginURIDetector == null) {
                    experiment = false;
                } else {
                    experiment = pluginURIDetector.isExperiment(uri);
                }
            }
        }
        return experiment;
    }

    /**
     * Adds an {@link URI} to a {@link Collection} if it represents an experiment. May add {@link URI}'s direct children
     * experiment {@link URI}s if given {@link URI} does'nt reresent an experiment.
     * 
     * @param uri The {@link URI} to potentially add in {@link Collection}
     * @param uris The {@link Collection}.
     * @param searchForChildrenExperiments Whether to add {@link URI}'s direct children experiment {@link URI}s when
     *            <code>uri</code> is not an experiment.
     * @param cdmaKeyFactory The {@link CDMAKeyFactory} That will help to find for children {@link URI}s when
     *            <code>searchForChildrenExperiments</code> is <code>true</code>.
     * @throws CDMAAccessException If a problem occurred while searching for children {@link URI}s (when
     *             <code>searchForChildrenExperiments</code> is <code>true</code>)
     */
    public static void addExperimentUri(URI uri, Collection<URI> uris, boolean searchForChildrenExperiments,
            CDMAKeyFactory cdmaKeyFactory) throws CDMAAccessException {
        if ((uri != null) && (uris != null)) {
            if (cdmaKeyFactory == null) {
                cdmaKeyFactory = new CDMAKeyFactory();
            }
            if (isExperiment(uri)) {
                uris.add(uri);
            } else if (searchForChildrenExperiments) {
                IKey key = cdmaKeyFactory.generateKeyListValidURI(uri);
                String id = key.getSourceProduction();
                IDataSourceProducer producer = DataSourceProducerProvider.getProducer(id);
                if (producer != null) {
                    @SuppressWarnings("unchecked")
                    AbstractDataSource<AbstractMatrix<LabelledURI>> source = (AbstractDataSource<AbstractMatrix<LabelledURI>>) producer
                            .createDataSource(key);
                    if (source != null) {
                        try {
                            AbstractMatrix<LabelledURI> uriMatrix = source.getData();
                            if (uriMatrix != null) {
                                LabelledURI[] children = (LabelledURI[]) uriMatrix.getFlatValue();
                                if (children != null) {
                                    for (LabelledURI child : children) {
                                        if ((child != null) && isExperiment(child.getURI())) {
                                            uris.add(child.getURI());
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            if (e instanceof CDMAAccessException) {
                                throw (CDMAAccessException) e;
                            } else {
                                throw new CDMAAccessException(e);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Recovers a <code>double</code> from a value at given position
     * 
     * @param value The value
     * @param position The position
     * @return A <code>double</code>
     */
    public static double getDoubleValueAt(Object value, int... position) {
        double doubleValue;
        if (value instanceof Number) {
            doubleValue = ((Number) value).doubleValue();
        } else if ((value == null) || (!value.getClass().isArray()) || (position == null) || (position.length == 0)
                || (!ObjectUtils.isNumberClass(ArrayUtils.recoverDataType(value)))) {
            doubleValue = Double.NaN;
        } else {
            Object tmp = value;
            try {
                for (int i = 0; i < position.length - 2; i++) {
                    tmp = ((Object[]) tmp)[position[i]];
                }
                if (tmp instanceof Object[]) {
                    doubleValue = ((Number) ((Object[]) tmp)[position[position.length - 1]]).doubleValue();
                } else if (tmp instanceof double[]) {
                    doubleValue = ((double[]) tmp)[position[position.length - 1]];
                } else if (tmp instanceof byte[]) {
                    doubleValue = ((byte[]) tmp)[position[position.length - 1]];
                } else if (tmp instanceof short[]) {
                    doubleValue = ((short[]) tmp)[position[position.length - 1]];
                } else if (tmp instanceof int[]) {
                    doubleValue = ((int[]) tmp)[position[position.length - 1]];
                } else if (tmp instanceof long[]) {
                    doubleValue = ((long[]) tmp)[position[position.length - 1]];
                } else if (tmp instanceof float[]) {
                    doubleValue = ((float[]) tmp)[position[position.length - 1]];
                } else {
                    doubleValue = Double.NaN;
                }
            } catch (Exception e) {
                doubleValue = Double.NaN;
            }
        }
        return doubleValue;
    }
}
