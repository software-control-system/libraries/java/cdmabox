package fr.soleil.cdma.box.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.cdma.box.action.ExpandAllAction;
import fr.soleil.cdma.box.action.ReloadAction;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.icons.DecorableIcon;
import fr.soleil.lib.project.swing.icons.Icons;
import fr.soleil.lib.project.swing.tree.ExpandableTree;

/**
 * Tool class for graphic uses
 * 
 * @author girardot
 */
public class GUIUtilities {

    public static final GUIUtilities DEFAULT_UTILITIES = new GUIUtilities("fr.soleil.cdma.box.icons");

    public static final SimpleDateFormat DATE_FORMATER = new SimpleDateFormat("yyyy-MM-dd HH'h'mm'm'ss's'SSS'ms'");

    public static final String CRLF = System.getProperty("line.separator");

    protected static final int SMALL_SIZE = 11;
    protected static final int MEDIUM_SIZE = 12;
    protected static final int BIG_SIZE = 14;
    protected static final int HUGE_SIZE = 24;

    public static final Font DEFAULT_FONT = new Font(Font.DIALOG, Font.PLAIN, SMALL_SIZE);
    public static final Font DEFAULT_FONT_BOLD = new Font(Font.DIALOG, Font.BOLD, SMALL_SIZE);
    public static final Font BIG_FONT = new Font(Font.DIALOG, Font.PLAIN, MEDIUM_SIZE);
    public static final Font DESCRITPION_FONT = new Font(Font.DIALOG, Font.ITALIC, SMALL_SIZE);
    public static final Font TITLE_FONT = new Font(Font.DIALOG, Font.BOLD, MEDIUM_SIZE);
    public static final Font MANDATORY_FONT = new Font(Font.SERIF, Font.BOLD, BIG_SIZE);
    public static final Font MAIN_TITLE_FONT = new Font(Font.DIALOG, Font.BOLD, HUGE_SIZE);
    public static final Font HIGHLIGHT_FONT = new Font(Font.DIALOG, Font.BOLD | Font.ITALIC, SMALL_SIZE);
    public static final Font INFO_FONT = new Font(Font.DIALOG, Font.ITALIC, SMALL_SIZE);
    public static final Font ERROR_FONT = new Font(Font.DIALOG, Font.ITALIC | Font.BOLD, MEDIUM_SIZE);
    public static final Font NO_VALUE_FONT = new Font(Font.DIALOG, Font.ITALIC, MEDIUM_SIZE);

    public static final Color OK_COLOR = Color.WHITE;
    public static final Color KO_COLOR = new Color(255, 220, 150);
    public static final Color MESSAGE_COLOR = new Color(255, 255, 220);
    public static final Color PROPERTY_FILE_EDITOR_COLOR = new Color(220, 230, 255);
    public static final Color WARNING_COLOR = new Color(255, 190, 50);
    public static final Color ERROR_COLOR = new Color(255, 50, 50);
    public static final Color MANDATORY_COLOR = Color.RED;
    public static final Color MAIN_TITLE_COLOR = new Color(50, 50, 255);

    public static final Border DEFAULT_LINE_BORDER = new LineBorder(Color.BLACK, 1);
    public static final Border HIGHLIGHTED_LINE_BORDER = new LineBorder(Color.BLACK, 2);
    public static final Border ERROR_BORDER = new LineBorder(Color.RED, 2);

    public static final Insets NO_MARGIN = new Insets(0, 0, 0, 0);

    private final Icons icons;

    public static final Insets GAP = new Insets(2, 2, 5, 2);

    public static final JLabel createNameLabel(String text) {
        return createNameLabel(text, SwingConstants.LEFT);
    }

    public static final JLabel createNameLabel(MessageManager messageManager, String key, String titleSeparatorKey,
            int alignment) {
        JLabel label;
        if (messageManager == null) {
            label = createNameLabel(ObjectUtils.EMPTY_STRING, alignment);
        } else if (key == null) {
            if (titleSeparatorKey == null) {
                label = createNameLabel(ObjectUtils.EMPTY_STRING, alignment);
            } else {
                label = createNameLabel(messageManager.getMessage(titleSeparatorKey), alignment);
            }
        } else if (titleSeparatorKey == null) {
            label = createNameLabel(messageManager.getMessage(key), alignment);
        } else {
            label = createNameLabel(new StringBuilder(messageManager.getMessage(key))
                    .append(messageManager.getMessage(titleSeparatorKey)).toString(), alignment);
        }
        return label;
    }

    public static final JLabel createNameLabel(String text, int alignment) {
        JLabel label = new JLabel(text, alignment);
        label.setFont(DEFAULT_FONT_BOLD);
        return label;
    }

    public static final JLabel createValueLabel(String text) {
        return createValueLabel(text, SwingConstants.LEFT);
    }

    public static final JLabel createValueLabel(String text, int alignment) {
        JLabel label = new JLabel(text, alignment);
        label.setFont(DEFAULT_FONT);
        return label;
    }

    public static final JLabel createDescriptionLabel(String text) {
        return createDescriptionLabel(text, SwingConstants.LEFT);
    }

    public static final JLabel createDescriptionLabel(String text, int alignment) {
        JLabel label = new JLabel(text, alignment);
        label.setFont(DESCRITPION_FONT);
        return label;
    }

    public GUIUtilities(String iconPackage) {
        icons = new Icons(iconPackage);
    }

    public static JScrollPane generateTreeScrollPane(ExpandableTree tree, MessageManager messageManager) {
        JScrollPane scrollPane;
        if (tree == null) {
            scrollPane = null;
        } else {
            scrollPane = new JScrollPane(tree);
            JPanel toolbar = new JPanel(new GridBagLayout());
            toolbar.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
            int column = 0;
            JButton reloadButton = new JButton(
                    new ReloadAction(messageManager.getMessage("fr.soleil.cdma.box.view.Tree.Reload"), tree));
            reloadButton.setMargin(NO_MARGIN);
            GridBagConstraints reloadConstraints = new GridBagConstraints();
            reloadConstraints.fill = GridBagConstraints.NONE;
            reloadConstraints.gridx = column++;
            reloadConstraints.gridy = 0;
            reloadConstraints.weightx = 0;
            reloadConstraints.weighty = 0;
            reloadConstraints.insets = GAP;
            toolbar.add(reloadButton, reloadConstraints);

            JButton expandButton = new JButton(
                    new ExpandAllAction(messageManager.getMessage("fr.soleil.cdma.box.view.Tree.ExpandAll"),
                            messageManager.getMessage("fr.soleil.cdma.box.view.Tree.CollapseAll"), tree, true));
            expandButton.setMargin(NO_MARGIN);
            GridBagConstraints expandConstraints = new GridBagConstraints();
            expandConstraints.fill = GridBagConstraints.NONE;
            expandConstraints.gridx = column++;
            expandConstraints.gridy = 0;
            expandConstraints.weightx = 0;
            expandConstraints.weighty = 0;
            expandConstraints.insets = GAP;
            toolbar.add(expandButton, expandConstraints);
            JButton collapseButton = new JButton(
                    new ExpandAllAction(messageManager.getMessage("fr.soleil.cdma.box.view.Tree.ExpandAll"),
                            messageManager.getMessage("fr.soleil.cdma.box.view.Tree.CollapseAll"), tree, false));
            collapseButton.setMargin(NO_MARGIN);
            GridBagConstraints collapseConstraints = new GridBagConstraints();
            collapseConstraints.fill = GridBagConstraints.NONE;
            collapseConstraints.gridx = column++;
            collapseConstraints.gridy = 0;
            collapseConstraints.weightx = 0;
            collapseConstraints.weighty = 0;
            collapseConstraints.insets = GAP;
            toolbar.add(collapseButton, collapseConstraints);
            GridBagConstraints glueConstraints = new GridBagConstraints();
            glueConstraints.fill = GridBagConstraints.BOTH;
            glueConstraints.gridx = column++;
            glueConstraints.gridy = 0;
            glueConstraints.weightx = 1;
            glueConstraints.weighty = 1;
            glueConstraints.insets = GAP;
            toolbar.add(Box.createGlue(), glueConstraints);
            scrollPane.setColumnHeaderView(toolbar);
        }
        return scrollPane;
    }

    /**
     * <br>
     * <b>Name: getProfileColor <br>
     * <b>Description: Returns the background color used for profiles.
     * 
     * @author SOLEIL
     * @return Color
     */
    public static Color getPropertyFileEditorColor() {
        return new Color(192, 192, 255);
    }

    /**
     * <br>
     * <b>Name: getPlotSubPanelsEtchedBorder <br>
     * <b>Description: return titledborder
     * 
     * @author SOLEIL
     * @param name of type String
     * @return TitledBorder
     */
    public static TitledBorder getPlotSubPanelsEtchedBorder(String name) {
        try {
            Color fColor = new Color(99, 97, 156);
            return BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), name, TitledBorder.LEFT,
                    TitledBorder.DEFAULT_POSITION, TITLE_FONT, fColor);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * <br>
     * <b>Name: colorToString <br>
     * <b>Description: return the string corresponding to the Color
     * 
     * @author SOLEIL
     * @param color of type Color
     * @return String
     */
    public static String colorToString(Color color) {
        try {
            if (color == null) {
                return null;
            } else {
                String ret = "";
                ret += color.getRed() + ",";
                ret += color.getGreen() + ",";
                ret += color.getBlue() + ",";
                return ret;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    /**
     * <br>
     * <b>Name: isTest5OK <br>
     * <b>Description: Replaces all occurences of <CODE>toReplace</CODE> <br>
     * in <CODE>in</CODE> by <CODE>replacement</CODE>.
     * 
     * @author SOLEIL
     * @param in The string to alter.
     * @param toReplace The string to replace.
     * @param replacement The replacement string.
     * @return The altered string.
     */
    public static String replace(String in, String toReplace, String replacement) {
        try {
            int lgDelim = toReplace.length();
            ArrayList<Integer> limitersList = new ArrayList<Integer>();
            StringBuilder finalString;

            int startIdx;
            int listIdx;

            // Looking for the string to replace
            startIdx = 0;
            do {
                startIdx = in.indexOf(toReplace, startIdx);

                if (startIdx >= 0) {
                    limitersList.add(new Integer(startIdx));
                    startIdx += lgDelim;
                }
            } while (startIdx >= 0);

            // Check if there is something to do
            if (limitersList.size() == 0) {
                return in;
            }

            // Backwards replace
            finalString = new StringBuilder(in);
            listIdx = limitersList.size() - 1;

            do {
                startIdx = limitersList.get(listIdx--).intValue();
                finalString.replace(startIdx, startIdx + lgDelim, replacement);
            } while (listIdx >= 0);
            return finalString.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * <br>
     * <b>Name: write2 <br>
     * <b>Description: check if test is OK
     * 
     * @author SOLEIL
     * @param pw
     * @param s
     * @param hasNewLine
     * @throw Exception
     */
    public static void write2(PrintWriter pw, String s, boolean hasNewLine) throws Exception {
        try {
            if (hasNewLine) {
                pw.println(s);
            } else {
                pw.print(s);
            }

            pw.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * <br>
     * <b>Name:getPlotSubPanelsLineBorder <br>
     * <b>Description:
     * 
     * @author SOLEIL
     * @param name of type String
     * @param lineColor of type Color
     * @return TitledBorder
     */
    public static TitledBorder getPlotSubPanelsLineBorder(String name, Color lineColor) {
        try {
            Color fColor = new Color(99, 97, 156);
            return BorderFactory.createTitledBorder(BorderFactory.createLineBorder(lineColor, 1), name,
                    TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION, TITLE_FONT, fColor);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * <br>
     * <b>Name:getPlotSubPanelsLineBorder <br>
     * <b>Description:
     * 
     * @author SOLEIL
     * @param name of type String
     * @param textColor of type Color
     * @param lineColor of type Color
     * @return TitledBorder
     */
    public static TitledBorder getPlotSubPanelsLineBorder(String name, Color textColor, Color lineColor) {
        try {
            return BorderFactory.createTitledBorder(BorderFactory.createLineBorder(lineColor, 1), name,
                    TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION, TITLE_FONT, textColor);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public ImageIcon getIcon(String path) {
        return icons.getIcon(path);
    }

    public DecorableIcon getDecorableIcon(String path, String decoration) {
        return icons.getDecorableIcon(path, decoration);
    }

    /**
     * Returns a formated {@link String} that represents the current moment
     * 
     * @return a {@link String}
     */
    public static String nowToString() {
        return longToStringDate(System.currentTimeMillis());
    }

    public static String getApplicationNameAndVersion(MessageManager messageManager) {
        return getApplicationNameAndVersion(messageManager, null);
    }

    public static String getApplicationNameAndVersion(MessageManager messageManager, String applicationName) {
        return appendApplicationNameAndVersion(messageManager, applicationName, new StringBuilder()).toString();
    }

    public static StringBuilder appendApplicationNameAndVersion(MessageManager messageManager, String applicationName,
            StringBuilder builder) {
        if (builder == null) {
            builder = new StringBuilder();
        }
        builder.append(applicationName == null || applicationName.trim().isEmpty()
                ? messageManager.getAppMessage("project.name")
                : applicationName);
        builder.append(" ");
        appendApplicationVersion(messageManager, builder);
        return builder;
    }

    public static StringBuilder appendApplicationName(MessageManager messageManager, String applicationName,
            StringBuilder builder) {
        if (builder == null) {
            builder = new StringBuilder();
        }
        builder.append(applicationName == null || applicationName.trim().isEmpty()
                ? messageManager.getAppMessage("project.name")
                : applicationName);
        return builder;
    }

    public static StringBuilder appendApplicationVersion(MessageManager messageManager, StringBuilder builder) {
        if (builder == null) {
            builder = new StringBuilder();
        }
        builder.append(messageManager.getAppMessage("project.version"));
        // XXX buildnumber plugin does not seem to work any more with maven 3
//        builder.append(" (");
//        builder.append(messageManager.getAppMessage("build.date"));
//        builder.append(")");
        return builder;
    }

    public static void setComponentVisible(Component comp, boolean visible) {
        if (comp != null) {
            if (comp instanceof JPanel) {
                JPanel panel = (JPanel) comp;
                for (int i = 0; i < panel.getComponentCount(); i++) {
                    setComponentVisible(panel.getComponent(i), visible);
                }
            }
            comp.setVisible(visible);
        }
    }

    /**
     * Ensures a {@link Runnable} is executed in Event Dispatch Thread
     * 
     * @param runnable The {@link Runnable} to execute in Event Dispatch Thread
     */
    public static void invokeInEDT(Runnable runnable) {
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    /**
     * Ensures a {@link Runnable} is executed in Event Dispatch Thread, and waits for the end of its execution.
     * 
     * @param runnable The {@link Runnable} to execute in Event Dispatch Thread
     * @throws InvocationTargetException If an exception is thrown while running <code>runnable</code>
     * @throws InterruptedException If we're interrupted while waiting for the event dispatching thread to finish
     *             executing <code>runnable.run()</code>
     */
    public static void invokeInEDTAndWait(Runnable runnable) throws InvocationTargetException, InterruptedException {
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeAndWait(runnable);
        }
    }

    /**
     * Returns The {@link String} representation of date, given as a long
     * 
     * @param the long that represents the expected date
     * @return a {@link String}
     */
    public static String longToStringDate(long when) {
        String result;
        synchronized (DATE_FORMATER) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(when);
            result = DATE_FORMATER.format(cal.getTime());
            cal = null;
        }
        return result;
    }

}
