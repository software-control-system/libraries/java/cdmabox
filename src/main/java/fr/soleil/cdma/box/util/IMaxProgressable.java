package fr.soleil.cdma.box.util;

import fr.soleil.lib.project.progress.IProgressable;

public interface IMaxProgressable extends IProgressable {

    public int getMaxProgress();

}
