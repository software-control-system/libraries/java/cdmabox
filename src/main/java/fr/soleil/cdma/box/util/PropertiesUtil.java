/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.util;

import java.io.FileInputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

/**
 * A class used to interact with {@link Properties}
 * 
 * @author girardot
 */
public class PropertiesUtil {

    /**
     * Reads a property from some {@link Properties}
     * 
     * @param properties The {@link Properties} from which to extract the desired property
     * @param key The key that identifies the property to read
     * @return The expected property, never <code>null</code>, except when <code>key</code> is <code>null</code>. If the
     *         property is not found, <code>key</code> is returned
     */
    public static String getProperty(Properties properties, String key) {
        String result = null;
        if ((properties != null) && (key != null)) {
            result = properties.getProperty(key);
        }
        if (result == null) {
            result = key;
        }
        return result;
    }

    /**
     * Reads a file and returns is corresponding {@link Properties}
     * 
     * @param filePath The path of the file to read.
     * @return Some {@link Properties}, not <code>null</code>.
     */
    public static Properties readProperties(String filePath) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(filePath));
        } catch (Exception e) {
            properties.clear();
        }
        return properties;
    }

    /**
     * Transforms a {@link Map} into another one, replacing keys with their associated values in {@link Properties}
     * 
     * @param properties The {@link Properties}
     * @param originalMap The original {@link Map}
     * @return A {@link Map}
     */
    public static <V> Map<String, V> computeTransformedKeyMap(Properties properties, Map<String, V> originalMap) {
        Map<String, V> result;
        if ((properties == null) || (properties.isEmpty())) {
            result = originalMap;
        } else if (originalMap == null) {
            result = null;
        } else {
            result = new LinkedHashMap<String, V>();
            for (Entry<String, V> entry : originalMap.entrySet()) {
                result.put(getProperty(properties, entry.getKey()), entry.getValue());
            }
        }
        return result;
    }

}
