/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.util;

import java.util.Arrays;

/**
 * A class to use <code>int[]</code> values as {@link java.util.Map Map} keys. It is called so
 * because the <code>int[]</code> value most often represents a position in a stack
 * 
 * @author girardot
 */
public class PositionKey {

    private final int[] position;

    /**
     * Constructs this {@link PositionKey}
     * 
     * @param position The associated <code>int[]</code>
     */
    public PositionKey(int[] position) {
        this.position = position;
    }

    /**
     * Returns the associated <code>int[]</code>
     * 
     * @return An <code>int[]</code>
     */
    public int[] getPosition() {
        return position;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else {
            if (getClass().equals(obj.getClass())) {
                PositionKey positionKey = (PositionKey) obj;
                if (position == null) {
                    equals = (positionKey.getPosition() == null);
                } else {
                    equals = Arrays.equals(position, positionKey.getPosition());
                }
            } else {
                equals = false;
            }
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hashCode = 987;
        int mult = 88;
        hashCode = hashCode * mult + getClass().hashCode();
        hashCode = hashCode * mult + Arrays.hashCode(position);
        return hashCode;
    }

}
