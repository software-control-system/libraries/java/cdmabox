package fr.soleil.cdma.box.util;

public interface CDMAConstants {
    public static final String COMMON_RESOURCES_PROPERTY = "CDMA_RESOURCES_DIRECTORY";

    public static final String DEFAULT_DICTIONARY_VIEW = "data_reduction";
    public static final String DEFAULT_MANDATORY_DICTIONARY = "data_reduction_view.xml";
    public static final String DEFAULT_MANDATORY_DICTIONARY_PACKAGE = "/fr/soleil/fusion/dictionary/";
    public static final String SCAN_GROUP = "scan";
    public static final String SCAN_KEY = "acquisition_sequence";
    public static final String SCAN_ORIGIN_KEY = "acquisition_origin";
    public static final String STACK_NAME_KEY = "stack_name";

    public static final String ATTR_EQUIPMENT_NAME = "equipment";
    public static final String ATTR_LONG_NAME = "long_name";
    public static final String ATTR_UNIT = "unit";
    public static final String ATTR_FORMAT = "format";

    public static final String ENERGY_KEY = "energy";
    public static final String EXIT_SLITS_KEY = "exitSlits";
    public static final String XPOSITION_KEY = "x_position";
    public static final String YPOSITION_KEY = "y_position";

    public static final String ACQUISITION_GROUP = "acquisition";
    public static final String DETECTOR_GROUP = "detector";
    public static final String DETECTOR_PARAMETER = "detector";
    public static final String CAMERA_KEY = "camera";
    public static final String SCIENTA_DETECTOR = "Scienta";
    public static final String KEITHLEY_DETECTOR = "Keithley";
    public static final String XIA_DETECTOR = "Xia";
    public static final String XIA_DETECTOR_PATTERN = "Xia.*";
    public static final String MBS_DETECTOR_PATTERN = "MBS.*";
    public static final String LIVE_TIME_KEY = "liveTime";
    public static final String CHANNEL_SCALE_KEY = "channelScale";
    public static final String DATA_CHANNEL_SCALE_KEY = "dataChannelScale";

    public static final String SWEEPS_KEY = "sweeps";
    public static final String ACQUISITION_MODE_KEY = "acquisitionMode";
    public static final String LENS_KEY = "lensMode";
    public static final String PASS_ENERGY_KEY = "passEnergy";
    public static final String LOW_ENERGY_KEY = "lowEnergy";
    public static final String HIGH_ENERGY_KEY = "highEnergy";
    public static final String ENERGY_STEP_KEY = "energyStep";
    public static final String STEP_TIME_KEY = "stepTime";
    public static final String FIRST_X_CHANNEL_KEY = "firstXChannel";
    public static final String LAST_X_CHANNEL_KEY = "lastXChannel";
    public static final String FIRST_Y_CHANNEL_KEY = "firstYChannel";
    public static final String LAST_Y_CHANNEL_KEY = "lastYChannel";
    public static final String SLICES_KEY = "slices";

    public static final String IMAGES_KEY = "images";
    public static final String SPECTRA_KEY = "spectra";
    public static final String SPECTRA_XIA_KEY = "spectra_xia";
    public static final String SPECTRA_SCIENTA_KEY = "spectra_scienta";
    public static final String SPECTRA_MBS_KEY = "spectra_mbs";
    public static final String SCALARS_KEY = "scalars";
    public static final String TOTAL_SPECTRUM_KEY = "total_spectrum";
    public static final String TOTAL_IMAGE_KEY = "total_image";
    public static final String BEAM_ENERGY_KEY = "beamEnergy";
    public static final String SAMPLE_PHI_KEY = "samplePhi";
    public static final String SAMPLE_THETA_KEY = "sampleTheta";
    public static final String APERTURE_ANGLE_KEY = "apertureAngle";

    public static final String SCALARS_DISPLAY_NAME = SCALARS_KEY;
    public static final String SPECTRA_DISPLAY_NAME = SPECTRA_KEY;
    public static final String IMAGES_DISPLAY_NAME = IMAGES_KEY;
    public static final String TOTAL_SPECTRUM_DISPLAY_NAME = "total spectrum";
    public static final String BEAM_ENERGY_DISPLAY_NAME = "beam energy";
    public static final String SAMPLE_PHI_DISPLAY_NAME = "sample phi";
    public static final String SAMPLE_THETA_DISPLAY_NAME = "theta";
    public static final String APERTURE_ANGLE_DISPLAY_NAME = "aperture angle";

    public static final String MOTORS_GROUP = "motors";
    public static final String MOTOR_PARAMETER = "motor";
    public static final String MOTOR_POSITION_KEY = "motor_position";

    public static final String DATA_GROUP = "data";
    public static final String MONO_CHROMATOR_GROUP = "monochromator";

    public static final String MONITOR_GROUP = "monitor";
    public static final String MONITOR_PARAMETER = "monitor";
    public static final String MONITOR_KEY = "mi";
    public static final String INTENSITY_KEY = "intensity";
    public static final String MONITOR_GAIN_KEY = "gain";
    public static final String MONITOR_BIAS_KEY = "bias";

    public static final String NAMED_DATA_KEY = "namedData";

    public static final String X_BIN_KEY = "xBin";
    public static final String Z_BIN_KEY = "zBin";
    public static final String EXPOSURE_TIME_KEY = "exposureTime";
    public static final String SHUTTER_CLOSE_DELAY_KEY = "shutterCloseDelay";
    public static final String DISTANCE_KEY = "distance";
    public static final String PIXEL_SIZE_KEY = "pixelSize";
    public static final String DETECTOR_BIAS_KEY = "detectorBias";
    public static final String DETECTOR_GAIN_KEY = "detectorGain";
    public static final String WAVE_LENGTH_KEY = "lambda";
    public static final String X_CENTER_COORDINATE_KEY = "x0";
    public static final String Z_CENTER_COORDINATE_KEY = "z0";
    public static final String DARK_KEY = "dark";
    public static final String ENERGY_SCALE_KEY = "energyScale";
    public static final String SLICE_SCALE_KEY = "sliceScale";
    public static final String ENCODER_DELTA_KEY = "encoderDelta";
    public static final String DELTA_OFFSET_KEY = "delta0";
    public static final String GAMMA_OFFSET_KEY = "gamma0";
    public static final String DETECTOR_UNCERTAINTY_KEY = "detectorBasedUncertainty";
    public static final String ESCALE_KEY = "escale";
    public static final String XSCALE_KEY = "xscale";
    public static final String TWO_THETA0_KEY = "twoTheta0";
    public static final String THETAIN_KEY = "thetaIn";
    public static final String THETA_Z0_KEY = "thetaZ0";
    public static final String THETAZ_CALIBRATION_FACTOR_KEY = "thetaZCalibrationFactor";
    public static final String SAMPLE_SLIT_DISTANCE_KEY = "sampleSlitDistance";
    public static final String GEOMETRY_KEY = "geometry";
    public static final String CHANNEL0_KEY = "channel0";
    public static final String QXY_KEY = "qxy";

    public static final String KEYS_HEADER_GROUP = "header";
    public static final String INFO_GROUP = "info";
    public static final String COMMENTS_KEY = "comments";

    public static final String PERSISTING_KEYS_GROUP = "AdditionalParametersToPersist";

    public static final String SCAN_ORIGIN = "scanOrigin";
    public static final String SCAN_NAME = "scanName";
    public static final String COMMENTS = "comments";
    public static final String EXIT_SLITS = "exitSlits";
    public static final String BEAM_ENERGY = "beamEnergy";
    public static final String ENERGY = "energy";
    public static final String LAMBDA = "lambda";
    public static final String GAMMA_0 = "gamma0";
    public static final String DELTA_0 = "delta0";
    public static final String ENCODER_DELTA = "encoderDelta";
    public static final String SLICES = "slices";
    public static final String LAST_Z_CHANNEL = "lastZChannel";
    public static final String FIRST_Z_CHANNEL = "firstZChannel";
    public static final String LAST_X_CHANNEL = "lastXChannel";
    public static final String FIRST_X_CHANNEL = "firstXChannel";
    public static final String STEP_TIME = "stepTime";
    public static final String ENERGY_STEP = "energyStep";
    public static final String HIGH_ENERGY = "highEnergy";
    public static final String LOW_ENERGY = "lowEnergy";
    public static final String PASS_ENERGY = "passEnergy";
    public static final String LENS_MODE = "lensMode";
    public static final String ACQUISITION_MODE = "acquisitionMode";
    public static final String SWEEPS = "sweeps";
    public static final String Z_POSITIONS = "zPositions";
    public static final String X_POSITIONS = "xPositions";
    public static final String Z0 = "z0";
    public static final String X0 = "x0";
    public static final String SLICE_SCALE = "sliceScale";
    public static final String ENERGY_SCALE = "energyScale";
    public static final String DARK = "dark";
    public static final String DETECTOR_GAIN = "detectorGain";
    public static final String GAIN = "gain";
    public static final String PIXEL_SIZE = "pixelSize";
    public static final String DISTANCE = "distance";
    public static final String SHUTTER_CLOSE_DELAY = "shutterCloseDelay";
    public static final String EXPOSURE_TIME = "exposureTime";
    public static final String DETECTOR_BASED_UNCERTAINTY = "detectorBasedUncertainty";
    public static final String DETECTOR_BIAS = "detectorBias";
    public static final String Z_BIN = "zBin";
    public static final String X_BIN = "xBin";
    public static final String MONITOR_PREFIX = "MONITOR_";
    public static final String CAMERA = "camera";

    public static final String ATTRIBUTE_INTERPRETATION = "interpretation";
    public static final String ATTRIBUTE_CREATOR = "creator";
    public static final String ATTRIBUTE_MAPPING_DICTIONARY = "dictionary";
    public static final String ATTRIBUTE_CLASS = "NX_class";
    public static final String ATTRIBUTE_EQUIPMENT = "equipment";
    public static final String ATTRIBUTE_SCAN = "acquisition_sequence";
    public static final String ATTRIBUTE_OPERATION = "operation";
    public static final String ATTRIBUTE_FORMAT = "format";
    public static final String ATTRIBUTE_UNIT = "unit";
    public static final String ATTRIBUTE_UNITS = "units";
    public static final String ATTRIBUTE_TARGET = "target";
    public static final String ATTRIBUTE_LONG_NAME = "long_name";
    public static final String ATTRIBUTE_DESCRIPTION = "description";
    public static final String MAPPING_DICTIONARY = "image_operation.xml";
    public static final String NODE_CONTEXT = "context";
    public static final String NODE_RESULT = "result";
    public static final String NODE_IMAGE = "image";
    public static final String NODE_SPECTRUM = "spectrum";
    public static final String NODE_SCALAR = "scalar";
    public static final String NODE_INTENSITY_MONITORS = "intensity_monitors";
    public static final String NODE_INTENSITY = "intensity";
    public static final String NODE_ARGUMENTS = "arguments";
    public static final String CLASS_ENTRY = "NXentry";
    public static final String CLASS_DATA = "NXdata";
    public static final String CLASS_INSTRUMENT = "NXinstrument";
    public static final String CLASS_INTENSITY_MONITOR = "NXintensity_monitor";
    public static final String CLASS_DETECTOR = "NXdetector";
    public static final String NXS_INTERNAL_PATH_SEPARATOR = "/";
    public static final String NEXUS_EXTENSION = "nxs";
    public static final String NEXUS_FULL_EXTENSION = ".nxs";

    public static final String RANGE_START = "[";
    public static final String RANGE_END = "]";
    public static final String RANGE_SEPARATOR = "-";
    public static final String STEP_SEPARATOR = ":";
    public static final String POSITION_START = "{";
    public static final String POSITION_END = "}";
    public static final String POSITION_SEPARATOR = ",";
    public static final String INDEXES_SEPARATOR = ";";

}
