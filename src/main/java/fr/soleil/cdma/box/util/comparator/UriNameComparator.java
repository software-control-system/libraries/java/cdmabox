package fr.soleil.cdma.box.util.comparator;

import java.io.File;
import java.text.Collator;

import javax.swing.tree.DefaultMutableTreeNode;

import fr.soleil.cdma.box.view.UriView;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.lib.project.resource.MessageManager;

public class UriNameComparator extends UriFileComparator {

    private final Collator collator;

    public UriNameComparator(boolean ascending, MessageManager messageManager, UriView uriView) {
        super(ascending, messageManager, uriView);
        this.collator = Collator.getInstance();
    }

    @Override
    protected int compareExistingFiles(File f1, File f2) {
        return FileComparisonUtils.compareExistingFilesName(collator, ascending, f1, f2);
    }

    @Override
    protected int compareNullFiles(File f1, File f2, ITreeNode i1, ITreeNode i2, DefaultMutableTreeNode o1,
            DefaultMutableTreeNode o2) {
        int result;
        if (i1 == null) {
            if (i2 == null) {
                result = collator.compare(o1.toString(), o2.toString());
                if (!ascending) {
                    result *= -1;
                }
            } else {
                result = -1;
            }
        } else if (i2 == null) {
            result = 1;
        } else {
            Object d1 = i1.getData(), d2 = i2.getData();
            if (d1 == null) {
                if (d2 == null) {
                    result = collator.compare(i1.toString(), i2.toString());
                    if (!ascending) {
                        result *= -1;
                    }
                } else {
                    result = -1;
                }
            } else if (d2 == null) {
                result = 1;
            } else {
                result = collator.compare(d1.toString(), d2.toString());
                if (!ascending) {
                    result *= -1;
                }
            }
        }
        return result;
    }

    @Override
    public String getName(boolean ascending) {
        return ascending ? messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.name.ascending")
                : messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.name.descending");
    }

}
