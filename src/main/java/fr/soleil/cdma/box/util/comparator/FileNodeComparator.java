package fr.soleil.cdma.box.util.comparator;

import java.io.File;
import java.util.Comparator;

import javax.swing.tree.DefaultMutableTreeNode;

import fr.soleil.cdma.box.view.tree.node.FileNode;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.resource.MessageManager;

/**
 * A {@link FileNode} {@link Comparator}
 * 
 * @author GIRARDOT
 */
public abstract class FileNodeComparator extends NamedComparator<DefaultMutableTreeNode> {

    public FileNodeComparator(boolean ascending, MessageManager messageManager) {
        super(ascending, messageManager);
    }

    /**
     * Returns whether this {@link FileNodeComparator} compares in ascending order
     * 
     * @return A <code>boolean</code> value.
     */
    public boolean isAscending() {
        return ascending;
    }

    @Override
    public int compare(DefaultMutableTreeNode o1, DefaultMutableTreeNode o2) {
        int result;
        if (o1 == null) {
            if (o2 == null) {
                result = 0;
            } else {
                result = -1;
            }
        } else if (o2 == null) {
            result = 1;
        } else if (o1 instanceof FileNode) {
            if (o2 instanceof FileNode) {
                FileNode fn1 = (FileNode) o1;
                FileNode fn2 = (FileNode) o2;
                File f1 = fn1.getData(), f2 = fn2.getData();
                if (f1 == null) {
                    if (f2 == null) {
                        result = 0;
                    } else {
                        result = -1;
                    }
                } else if (f2 == null) {
                    result = 1;
                } else {
                    result = compareNotNull(f1, f2);
                    if (!ascending) {
                        result *= -1;
                    }
                }
            } else {
                result = 1;
            }
        } else if (o2 instanceof FileNode) {
            result = -1;
        } else {
            result = 0;
        }
        return result;
    }

    /**
     * Compares 2 {@link File}s
     * 
     * @param f1 First {@link File}. Can not be <code>null</code>.
     * @param f2 Second {@link File}. Can not be <code>null</code>.
     * @return A negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater
     *         than the second.
     */
    protected abstract int compareNotNull(File f1, File f2);

    @Override
    public boolean equals(Object obj) {
        boolean equals;
        if (obj == null) {
            equals = false;
        } else if (obj == this) {
            equals = true;
        } else if (getClass().equals(obj.getClass())) {
            equals = ((FileNodeComparator) obj).isAscending() == isAscending();
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0xF11E20DE;
        int mult = 0xC039;
        code = code * mult + getClass().hashCode();
        code = code * mult + ObjectUtils.generateHashCode(isAscending());
        return code;
    }

}
