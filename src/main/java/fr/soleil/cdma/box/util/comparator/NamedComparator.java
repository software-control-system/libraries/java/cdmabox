package fr.soleil.cdma.box.util.comparator;

import java.util.Comparator;

import fr.soleil.lib.project.resource.MessageManager;

public abstract class NamedComparator<T> implements Comparator<T> {

    protected final MessageManager messageManager;
    protected final String name;
    protected final boolean ascending;

    public NamedComparator(boolean ascending, MessageManager messageManager) {
        this.ascending = ascending;
        this.messageManager = messageManager;
        this.name = getName(ascending);
    }

    protected abstract String getName(boolean ascending);

    @Override
    public final String toString() {
        return name;
    }
}
