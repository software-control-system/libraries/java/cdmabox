package fr.soleil.cdma.box.util.comparator;

import java.io.File;

import fr.soleil.cdma.box.view.UriView;
import fr.soleil.lib.project.resource.MessageManager;

public class UriCreationDateComparator extends UriFileComparator {

    public UriCreationDateComparator(boolean ascending, MessageManager messageManager, UriView uriView) {
        super(ascending, messageManager, uriView);
    }

    @Override
    protected String getName(boolean ascending) {
        return ascending
                ? messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.date.creation.ascending")
                : messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.date.creation.descending");
    }

    @Override
    protected int compareExistingFiles(File f1, File f2) {
        return FileComparisonUtils.compareExistingFilesCreationDate(ascending, f1, f2);
    }

}
