/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.util;

import java.awt.Component;

import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;

public class ProgressDialogContainer {

    private ProgressDialog progressDialog;
    private final Object lock;

    public ProgressDialogContainer() {
        progressDialog = null;
        lock = new Object();
    }

    public ProgressDialog getProgressDialog(Component comp) {
        if (progressDialog == null) {
            synchronized (lock) {
                if (progressDialog == null) {
                    progressDialog = generateProgresDialog(comp);
                }
            }
        }
        return progressDialog;
    }

    protected ProgressDialog generateProgresDialog(Component comp) {
        ProgressDialog progressDialog = new ProgressDialog(WindowSwingUtils.getWindowForComponent(comp));
        progressDialog.setCloseOnMaxProgress(true);
        return progressDialog;
    }

}
