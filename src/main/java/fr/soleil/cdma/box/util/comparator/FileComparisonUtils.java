package fr.soleil.cdma.box.util.comparator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.Collator;

public class FileComparisonUtils {

    public static int compareExistingFilesLastModificationDate(boolean ascending, File f1, File f2) {
        int result;
        long l1 = f1.lastModified(), l2 = f2.lastModified();
        if (ascending) {
            result = Long.compare(l1, l2);
        } else {
            result = Long.compare(l2, l1);
        }
        return result;
    }

    public static int compareExistingFilesCreationDate(boolean ascending, File f1, File f2) {
        int result;
        BasicFileAttributes attr1, attr2;
        try {
            attr1 = Files.readAttributes(f1.toPath(), BasicFileAttributes.class);
        } catch (IOException e) {
            attr1 = null;
        }
        try {
            attr2 = Files.readAttributes(f2.toPath(), BasicFileAttributes.class);
        } catch (IOException e) {
            attr2 = null;
        }
        if (attr1 == null) {
            if (attr2 == null) {
                result = 0;
            } else {
                result = -1;
            }
        } else if (attr2 == null) {
            result = 1;
        } else {
            FileTime ft1 = attr1.creationTime(), ft2 = attr2.creationTime();
            if (ft1 == null) {
                if (ft2 == null) {
                    result = 0;
                } else {
                    result = -1;
                }
            } else if (ft2 == null) {
                result = 1;
            } else if (ascending) {
                result = Long.compare(ft1.toMillis(), ft2.toMillis());
            } else {
                result = Long.compare(ft2.toMillis(), ft1.toMillis());
            }
        }
        return result;
    }

    public static int compareExistingFilesName(Collator collator, boolean ascending, File f1, File f2) {
        int result;
        if (ascending) {
            result = collator.compare(f1.getName(), f2.getName());
        } else {
            result = collator.compare(f2.getName(), f1.getName());
        }
        return result;
    }

}
