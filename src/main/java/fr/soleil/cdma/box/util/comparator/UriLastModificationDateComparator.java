package fr.soleil.cdma.box.util.comparator;

import java.io.File;

import fr.soleil.cdma.box.view.UriView;
import fr.soleil.lib.project.resource.MessageManager;

public class UriLastModificationDateComparator extends UriFileComparator {

    public UriLastModificationDateComparator(boolean ascending, MessageManager messageManager, UriView uriView) {
        super(ascending, messageManager, uriView);
    }

    @Override
    protected int compareExistingFiles(File f1, File f2) {
        return FileComparisonUtils.compareExistingFilesLastModificationDate(ascending, f1, f2);
    }

    @Override
    public String getName(boolean ascending) {
        return ascending
                ? messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.date.modification.ascending")
                : messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.date.modification.descending");
    }

}
