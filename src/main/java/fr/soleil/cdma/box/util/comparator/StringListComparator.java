/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.util.comparator;

import java.text.Collator;
import java.util.Comparator;
import java.util.List;

/**
 * A {@link String} {@link Comparator} based on {@link String} position in {@link List}
 * 
 * @author GIRARDOT
 *
 */
public class StringListComparator implements Comparator<String> {

    private final String prefix;
    private final List<String> list;
    private final Collator collator;

    /**
     * Creates a new {@link StringListComparator}
     * 
     * @param list The list that sorts the {@link String} values to compare
     */
    public StringListComparator(List<String> list) {
        this(list, null, null);
    }

    /**
     * Creates a new {@link StringListComparator}
     * 
     * @param list The list that sorts the {@link String} values to compare
     * @param prefix The potential prefix to remove to {@link String} values before comparison
     */
    public StringListComparator(List<String> list, String prefix) {
        this(list, null, prefix);
    }

    /**
     * Creates a new {@link StringListComparator}
     * 
     * @param list The list that sorts the {@link String} values to compare
     * @param collator The {@link Collator}, that compares {@link String} values which are not in {@link List}
     * @param prefix The potential prefix to remove to {@link String} values before comparison
     */
    public StringListComparator(List<String> list, Collator collator, String prefix) {
        this.list = list;
        this.collator = collator == null ? Collator.getInstance() : collator;
        this.prefix = prefix;
    }

    @Override
    public int compare(String o1, String o2) {
        int result;
        if (o1 == null) {
            if (o2 == null) {
                result = 0;
            } else {
                result = 1;
            }
        } else if (o2 == null) {
            result = -1;
        } else if (list == null) {
            result = collator.compare(o1, o2);
        } else {
            String s1 = o1, s2 = o2;
            if ((prefix != null) && (!prefix.isEmpty())) {
                if (s1.startsWith(prefix)) {
                    s1 = s1.substring(prefix.length());
                }
                if (s2.startsWith(prefix)) {
                    s2 = s2.substring(prefix.length());
                }
            }
            int i1 = list.indexOf(s1);
            int i2 = list.indexOf(s2);
            if (i1 < 0) {
                if (i2 < 0) {
                    result = collator.compare(s1, s2);
                } else {
                    result = 1;
                }
            } else if (i2 < 0) {
                result = -1;
            } else if (i1 == i2) {
                result = 0;
            } else if (i1 > i2) {
                result = 1;
            } else {
                result = -1;
            }
        }
        return result;
    }
}
