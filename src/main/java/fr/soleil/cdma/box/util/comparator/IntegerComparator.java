/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.util.comparator;

import java.util.Comparator;

/**
 * A {@link Comparator} for {@link Integer} values
 * 
 * @author girardot
 */
public class IntegerComparator implements Comparator<Integer> {

    private boolean inverted = false;

    public IntegerComparator() {
        this(false);
    }

    public IntegerComparator(boolean inverted) {
        super();
        this.inverted = inverted;
    }

    @Override
    public int compare(Integer l1, Integer l2) {
        int result = 0;
        if (l1 != null) {
            result = l1.compareTo(l2);
        } else if (l2 != null) {
            result = -1 * l2.compareTo(l1);
        }
        if (inverted) {
            result *= -1;
        }
        return result;
    }

}
