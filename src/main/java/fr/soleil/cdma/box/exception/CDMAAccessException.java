/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.exception;

import org.cdma.exception.CDMAException;

/**
 * An {@link Exception} that can be thrown when trying to access data through CDMA
 * 
 * @author girardot
 */
public class CDMAAccessException extends CDMAException {

    private static final long serialVersionUID = -5251213295536087417L;

    /**
     * Constructs a new exception with <code>null</code> as its detail message. The cause is not
     * initialized, and may subsequently be initialized by a call to {@link #initCause}.
     */
    public CDMAAccessException() {
        super();
    }

    /**
     * Constructs a new exception with the specified detail message. The cause is not initialized,
     * and may subsequently be initialized by a call to {@link #initCause}.
     * 
     * @param message the detail message. The detail message is saved for later retrieval by the {@link #getMessage()}
     *            method.
     */
    public CDMAAccessException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified cause and a detail message of
     * <code>(cause==null ? null : cause.toString())</code> (which typically contains the class and
     * detail message of <code>cause</code>). This constructor is useful for exceptions that are
     * little more than wrappers for other throwables (for example, {@link java.security.PrivilegedActionException}).
     * 
     * @param cause the cause (which is saved for later retrieval by the {@link #getCause()} method). (A
     *            <code>null</code> value is permitted, and indicates that the cause is
     *            nonexistent or unknown.)
     */
    public CDMAAccessException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     * <p>
     * Note that the detail message associated with <code>cause</code> is <i>not</i> automatically incorporated in this
     * exception's detail message.
     * 
     * @param message the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
     * @param cause the cause (which is saved for later retrieval by the {@link #getCause()} method). (A
     *            <code>null</code> value is permitted, and indicates that the cause is
     *            nonexistent or unknown.)
     */
    public CDMAAccessException(String message, Throwable cause) {
        super(message, cause);
    }

}
