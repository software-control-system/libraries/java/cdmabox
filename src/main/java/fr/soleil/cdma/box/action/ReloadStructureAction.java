/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.tree.DefaultTreeModel;

import fr.soleil.cdma.box.view.component.DoubleClickUriTree;

/**
 * An {@link AbstractAction} to force a {@link DefaultTreeModel} to reload
 * 
 * @author girardot
 */
public class ReloadStructureAction extends AbstractAction {

    private static final long serialVersionUID = -4754472143717949885L;

    protected static final Icon RELOAD_ICON = new ImageIcon(
            ReloadStructureAction.class.getResource("/fr/soleil/cdma/box/icons/reloadTree.png"));

    private final DoubleClickUriTree tree;

    public ReloadStructureAction(String description, DoubleClickUriTree tree) {
        super();
        this.tree = tree;
        putValue(SHORT_DESCRIPTION, description);
        putValue(SMALL_ICON, RELOAD_ICON);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (tree != null) {
            tree.reloadStructure();
            tree.revalidate();
            tree.repaint();
        }
    }

}
