/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import fr.soleil.lib.project.swing.tree.ExpandableTree;

/**
 * An {@link AbstractAction} to fully expand/collapse an {@link ExpandableTree}
 * 
 * @author girardot
 */
public class ExpandAllAction extends AbstractAction {

    private static final long serialVersionUID = -7720470527336452510L;

    private static final Icon EXPAND_ICON = new ImageIcon(
            ExpandAllAction.class.getResource("/fr/soleil/cdma/box/icons/expand_all.gif"));
    private static final Icon COLLAPSE_ICON = new ImageIcon(
            ExpandAllAction.class.getResource("/fr/soleil/cdma/box/icons/collapse_all.gif"));

    private final ExpandableTree tree;
    private final boolean expand;

    public ExpandAllAction(String expandDescription, String collapseDescription, ExpandableTree tree, boolean expand) {
        super();
        String name;
        Icon icon;
        if (expand) {
            name = expandDescription;
            icon = EXPAND_ICON;
        } else {
            name = collapseDescription;
            icon = COLLAPSE_ICON;
        }
        putValue(SHORT_DESCRIPTION, name);
        putValue(SMALL_ICON, icon);
        this.tree = tree;
        this.expand = expand;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        tree.expandAll(expand);
    }

}
