/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

/**
 * An {@link AbstractAction} to force a {@link DefaultTreeModel} to reload
 * 
 * @author girardot
 */
public class ReloadAction extends AbstractAction {

    private static final long serialVersionUID = 4706051425845906359L;

    protected static final Icon RELOAD_ICON = new ImageIcon(
            ReloadAction.class.getResource("/fr/soleil/cdma/box/icons/repaint.png"));

    private final DefaultTreeModel model;
    private final JTree tree;
    private final boolean fromTree;

    public ReloadAction(String description, DefaultTreeModel model) {
        super();
        this.model = model;
        tree = null;
        fromTree = false;
        putValue(SHORT_DESCRIPTION, description);
        putValue(SMALL_ICON, RELOAD_ICON);
    }

    public ReloadAction(String description, JTree tree) {
        super();
        this.tree = tree;
        model = null;
        fromTree = true;
        putValue(SHORT_DESCRIPTION, description);
        putValue(SMALL_ICON, RELOAD_ICON);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (fromTree) {
            if (tree != null) {
                reload(tree.getModel());
                tree.revalidate();
                tree.repaint();
            }
        } else {
            reload(model);
        }
    }

    protected void reload(TreeModel model) {
        if (model instanceof DefaultTreeModel) {
            ((DefaultTreeModel) model).reload();
        }
    }

}
