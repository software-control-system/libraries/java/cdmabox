/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.controller;

import fr.soleil.cdma.box.data.acquisition.Acquisition;
import fr.soleil.cdma.box.target.IAcquisitionTarget;
import fr.soleil.data.controller.BasicTargetController;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;

/**
 * A {@link BasicTargetController} specialized in {@link Acquisition} data sources and {@link IAcquisitionTarget}s
 * 
 * @author girardot
 */
public class BasicAcquisitionController extends BasicTargetController<Acquisition> {

    public BasicAcquisitionController() {
        super();
    }

    @Override
    protected Acquisition generateDefaultValue() {
        return null;
    }

    @Override
    protected void setDataToTarget(ITarget target, Acquisition data, AbstractDataSource<Acquisition> source) {
        ((IAcquisitionTarget) target).setAcquisition(data);
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return (data instanceof Acquisition);
    }

    @Override
    public boolean isCompatibleWith(ITarget target) {
        return (target instanceof IAcquisitionTarget);
    }

}
