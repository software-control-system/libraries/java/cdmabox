/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.controller;

import fr.soleil.cdma.box.data.sensor.ExperimentConfig;
import fr.soleil.cdma.box.target.IExperimentConfigTarget;
import fr.soleil.data.controller.BasicTargetController;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;

/**
 * A {@link BasicTargetController} specialized in {@link ExperimentConfig} data sources and
 * {@link IExperimentConfigTarget}s
 * 
 * @author girardot
 */
public class BasicExperimentConfigController extends BasicTargetController<ExperimentConfig> {

    public BasicExperimentConfigController() {
        super();
    }

    @Override
    protected ExperimentConfig generateDefaultValue() {
        return null;
    }

    @Override
    protected void setDataToTarget(ITarget target, ExperimentConfig data, AbstractDataSource<ExperimentConfig> source) {
        ((IExperimentConfigTarget) target).setExperimentConfig(data);
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return (data instanceof ExperimentConfig);
    }

    @Override
    public boolean isCompatibleWith(ITarget target) {
        return (target instanceof IExperimentConfigTarget);
    }

}
