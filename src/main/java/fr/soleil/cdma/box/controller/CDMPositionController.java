/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.controller;

import java.util.List;
import java.util.logging.Level;

import fr.soleil.data.controller.BasicIntArrayTargetController;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IPositionTarget;
import fr.soleil.data.target.information.FullPositionInformation;
import fr.soleil.data.target.information.SubPositionInformation;
import fr.soleil.data.target.information.TargetInformation;

/**
 * This is a {@link BasicIntArrayTargetController} that handles reverse position setting for {@link IPositionTarget}
 * 
 * @author girardot
 */
public class CDMPositionController extends BasicIntArrayTargetController {

    public CDMPositionController() {
        super();
        setFirstInitAllowed(false);
    }

    @Override
    public <I, V extends TargetInformation<I>> void transmitTargetChange(V targetInformation) {
        if (targetInformation instanceof SubPositionInformation) {
            SubPositionInformation positionInformation = (SubPositionInformation) targetInformation;
            int[] subPosition = positionInformation.getInformationData();
            if ((subPosition != null) && (subPosition.length == 2)) {
                synchronized (targetAssociations) {
                    List<AbstractDataSource<int[]>> sources = targetAssociations.get(positionInformation
                            .getConcernedTarget());
                    if (sources != null) {
                        for (AbstractDataSource<int[]> source : sources) {
                            try {
                                int[] data = source.getData();
                                if (data != null) {
                                    data = data.clone();
                                    if ((subPosition[0] > -1) && (subPosition[0] < data.length)) {
                                        data[subPosition[0]] = subPosition[1];
                                    }
                                    source.setData(data);
                                }
                            } catch (Exception e) {
                                logger.log(Level.SEVERE,
                                        "Failed to transmit " + positionInformation.getConcernedTarget()
                                                + " data to source " + source, e);
                            }
                        }
                    }
                }
            }
        } else if (targetInformation instanceof FullPositionInformation) {
            FullPositionInformation positionInformation = (FullPositionInformation) targetInformation;
            int[] position = positionInformation.getInformationData();
            if (position != null) {
                synchronized (targetAssociations) {
                    List<AbstractDataSource<int[]>> sources = targetAssociations.get(positionInformation
                            .getConcernedTarget());
                    if (sources != null) {
                        for (AbstractDataSource<int[]> source : sources) {
                            try {
                                source.setData(position);
                            } catch (Exception e) {
                                logger.log(Level.SEVERE,
                                        "Failed to transmit " + positionInformation.getConcernedTarget()
                                                + " data to source " + source, e);
                            }
                        }
                    }
                }
            }
        }
    }

}