/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.controller;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import fr.soleil.cdma.box.view.model.FileTreeModel;
import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.information.TargetInformation;

/**
 * This controller handles the link between File Manager and {@link FileTreeModel}
 * 
 * @author girardot
 */
public class FileListController extends AbstractController<File[]> {

    private final Map<ITarget, AbstractDataFilter<File[]>> targetFilters;

    public FileListController() {
        super();
        targetFilters = new LinkedHashMap<ITarget, AbstractDataFilter<File[]>>();
        setFirstInitAllowed(false);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <V> boolean connectFilterToTarget(AbstractDataFilter<V> filter, ITarget target) {
        boolean result = false;
        synchronized (targetFilters) {
            if (target != null) {
                if (filter == null) {
                    targetFilters.remove(target);
                    result = true;
                } else {
                    AbstractDataFilter<File[]> expected = (AbstractDataFilter<File[]>) filter;
                    targetFilters.put(target, expected);
                    result = true;
                }
            }
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <V> boolean isFilterCompatibleWithTarget(AbstractDataFilter<V> filter, ITarget target) {
        boolean compatible = true;
        if (filter != null) {
            try {
                AbstractDataFilter<File[]> expected = (AbstractDataFilter<File[]>) filter;
                IDataContainer<File[]> previousTopContainer = null;
                AbstractDataFilter<File[]> tempFilter = expected;
                while (tempFilter.getDataContainer() instanceof AbstractDataFilter<?>) {
                    tempFilter = (AbstractDataFilter<File[]>) tempFilter.getDataContainer();
                }
                previousTopContainer = tempFilter.getDataContainer();
                updateFilter(expected, null);
                expected.setData(new File[0]);
                updateFilter(expected, previousTopContainer);
            } catch (Exception e) {
                compatible = false;
            }
        }
        return compatible;
    }

    @Override
    public boolean isTargetAssignable(ITarget target) {
        return (target instanceof FileTreeModel);
    }

    @Override
    protected void registerAdapter(AbstractDataSource<File[]> source) {
        // nothing to do: no adapter needed
    }

    @Override
    protected void unregisterAdapter(AbstractDataSource<File[]> source) {
        // nothing to do: no adapter needed
    }

    @Override
    protected void transmitDataToTarget(AbstractDataSource<File[]> source, AbstractDataFilter<File[]> associatedFilter,
            List<ITarget> targets) {
        if (targets != null) {
            IDataContainer<File[]> dataContainer;
            if (associatedFilter == null) {
                dataContainer = source;
            } else {
                dataContainer = associatedFilter;
            }
            DataDecorator dataDecorator = new DataDecorator(dataContainer);
            synchronized (targetFilters) {
                for (ITarget target : targets) {
                    if (isTargetAssignable(target)) {
                        FileTreeModel fileTreeModel = (FileTreeModel) target;
                        AbstractDataFilter<File[]> targetFilter = targetFilters.get(fileTreeModel);
                        try {
                            if (targetFilter == null) {
                                fileTreeModel.setFiles(dataDecorator.getData());
                            } else {
                                updateFilter(targetFilter, dataDecorator);
                                fileTreeModel.setFiles(targetFilter.getData());
                                updateFilter(targetFilter, null);
                            }
                        } catch (Exception e) {
                            logger.log(Level.SEVERE, "Failed to transmit " + source + " data to target " + target, e);
                        }
                    }
                }
            }
            dataDecorator.clean();
        }
    }

    @Override
    public <V> void removeFilterFromTarget(AbstractDataFilter<V> filter, ITarget target) {
        synchronized (targetFilters) {
            if ((filter != null) && (target != null) && targetFilters.containsKey(target)
                    && (filter == targetFilters.get(target))) {
                targetFilters.remove(target);
                updateFilter(filter, null);
            }
        }
    }

    @Override
    public <I, V extends TargetInformation<I>> void transmitTargetChange(V targetInformation) {
        // not managed
    }

    @Override
    public GenericDescriptor getSourceType() {
        return new GenericDescriptor(File[].class);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class DataDecorator implements IDataContainer<File[]> {
        private IDataContainer<File[]> decorated;

        public DataDecorator(IDataContainer<File[]> decorated) {
            this.decorated = decorated;
        }

        @Override
        public File[] getData() throws Exception {
            return (decorated == null ? null : decorated.getData());
        }

        @Override
        public void setData(File[] data) throws Exception {
            if (decorated != null) {
                decorated.setData(data);
            }
        }

        @Override
        public GenericDescriptor getDataType() {
            return new GenericDescriptor(File[].class);
        }

        @Override
        public boolean isSettable() {
            return (decorated != null);
        }

        public void clean() {
            decorated = null;
        }

        @Override
        public boolean isUnsigned() {
            return false;
        }
    }

}
