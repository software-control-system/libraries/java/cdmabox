/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import fr.soleil.cdma.box.target.IBeamCenterTarget;
import fr.soleil.cdma.box.target.info.BeamCenterInformation;
import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.controller.BasicTargetController;
import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.information.TargetInformation;

public class BeamCenterController extends BasicTargetController<double[]> {

    public BeamCenterController() {
        super();
    }

    @Override
    protected double[] generateDefaultValue() {
        return new double[] { 0, 0 };
    }

    @Override
    protected void setDataToTarget(ITarget target, double[] data, AbstractDataSource<double[]> source) {
        if ((target != null) && (data != null) && (data.length == 2)) {
            ((IBeamCenterTarget) target).setCenterX(data[0]);
            ((IBeamCenterTarget) target).setCenterZ(data[1]);
        }
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return (data instanceof double[]);
    }

    @Override
    public boolean isCompatibleWith(ITarget target) {
        return (target instanceof IBeamCenterTarget);
    }

    @Override
    public <I, V extends TargetInformation<I>> void transmitTargetChange(V targetInformation) {
        if (targetInformation instanceof BeamCenterInformation) {
            BeamCenterInformation info = (BeamCenterInformation) targetInformation;
            double value = (info.getInformationData() == null ? Double.NaN : info.getInformationData().doubleValue());
            IBeamCenterTarget target = info.getConcernedTarget();
            int index = (info.isX() ? 0 : 1);
            List<AbstractDataSource<double[]>> sourceList = null;
            synchronized (targetAssociations) {
                List<AbstractDataSource<double[]>> tempList = targetAssociations.get(target);
                if (tempList != null) {
                    sourceList = new ArrayList<AbstractDataSource<double[]>>();
                    sourceList.addAll(tempList);
                }
            }
            if (sourceList != null) {
                synchronized (targetFilters) {
                    AbstractDataFilter<double[]> targetFilter = targetFilters.get(target);
                    for (AbstractDataSource<double[]> source : sourceList) {
                        DataSetterTool setter = new DataSetterTool(source, source.getDataType(), target);
                        if (targetFilter == null) {
                            updateDataContainer(setter, index, value);
                        } else {
                            updateFilter(targetFilter, setter);
                            updateDataContainer(targetFilter, index, value);
                            updateFilter(targetFilter, null);
                        }
                        setter.clean();
                    }
                }
                sourceList.clear();
            }
        } else {
            super.transmitTargetChange(targetInformation);
        }
    };

    protected void updateDataContainer(IDataContainer<double[]> container, int index, double value) {
        try {
            double[] data = container.getData();
            if ((data != null) && (data.length == 2)) {
                data = data.clone();
                data[index] = value;
                container.setData(data);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to update data container " + container, e);
        }
    }

}
