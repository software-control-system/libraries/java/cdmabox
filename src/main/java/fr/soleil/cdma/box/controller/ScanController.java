/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.controller;

import java.util.List;
import java.util.logging.Level;

import fr.soleil.cdma.box.data.SoftData;
import fr.soleil.cdma.box.data.scan.ScanData;
import fr.soleil.cdma.box.target.IEnergyTarget;
import fr.soleil.cdma.box.target.IScanTarget;
import fr.soleil.comete.definition.data.target.IConvertedPositionsTarget;
import fr.soleil.comete.definition.util.ArrayPositionConvertor;
import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.comete.definition.util.MatrixPositionConvertor;
import fr.soleil.data.filter.AbstractDataFilter;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.IStackShapeTarget;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.information.TargetInformation;
import fr.soleil.lib.project.math.ArrayUtils;

public class ScanController extends AbstractController<ScanData> {

    public ScanController() {
        super();
    }

    @Override
    public <I, V extends TargetInformation<I>> void transmitTargetChange(V targetInformation) {
        // not managed
    }

    @Override
    public <V> void removeFilterFromTarget(AbstractDataFilter<V> filter, ITarget target) {
        // not managed
    }

    @Override
    public boolean isTargetAssignable(ITarget target) {
        return ((target instanceof IScanTarget) || (target instanceof IEnergyTarget)
                || (target instanceof IStackShapeTarget) || (target instanceof IConvertedPositionsTarget));
    }

    @Override
    protected <V> boolean isFilterCompatibleWithTarget(AbstractDataFilter<V> filter, ITarget target) {
        // not managed
        return false;
    }

    @Override
    protected <V> boolean connectFilterToTarget(AbstractDataFilter<V> filter, ITarget target) {
        // not managed
        return false;
    }

    @Override
    protected void registerAdapter(AbstractDataSource<ScanData> source) {
        // not managed
    }

    @Override
    protected void unregisterAdapter(AbstractDataSource<ScanData> source) {
        // not managed
    }

    @Override
    protected void transmitDataToTarget(AbstractDataSource<ScanData> source,
            AbstractDataFilter<ScanData> associatedFilter, List<ITarget> targets) {
        if ((source != null) && (targets != null)) {
            try {
                ScanData energyScan = source.getData();
                double energy = (energyScan == null ? Double.NaN : energyScan.getEnergy());
                int[] stackShape = (energyScan == null ? null : energyScan.getStackShape());
                IValueConvertor[] convertors = null;
                SoftData<?> yPosition = (energyScan == null ? null : energyScan.getPositionData(ScanData.Dimension.Y));
                SoftData<?> xPosition = (energyScan == null ? null : energyScan.getPositionData(ScanData.Dimension.X));
                Object yData = null;
                Object xData = null;
                if (yPosition != null) {
                    yData = yPosition.getData();
                }
                if (xPosition != null) {
                    xData = xPosition.getData();
                }
                for (ITarget target : targets) {
                    if (target instanceof IScanTarget) {
                        // for an IEnergyScanTarget: update its EnergyScanData
                        ((IScanTarget) target).setScan(energyScan);
                    }
                    if (target instanceof IEnergyTarget) {
                        // for an IEnergyTarget: update its energy
                        ((IEnergyTarget) target).setEnergy(energy);
                    }
                    if (target instanceof IStackShapeTarget) {
                        // update stack shape
                        ((IStackShapeTarget) target).setStackShape(stackShape);
                    }
                    if (target instanceof IConvertedPositionsTarget) {
                        // update value convertors
                        IConvertedPositionsTarget convertedPositionsTarget = (IConvertedPositionsTarget) target;
                        if (yData == null) {
                            if (xData != null) {
                                convertors = new IValueConvertor[] { new ArrayPositionConvertor() };
                                ((ArrayPositionConvertor) convertors[0]).setNumberArray(xData);
                            }
                        } else {
                            convertors = new IValueConvertor[2];
                            ArrayPositionConvertor yPositionConvertor = new ArrayPositionConvertor();
                            yPositionConvertor.setNumberArray(yData);
                            convertors[0] = yPositionConvertor;
                            int rank = ArrayUtils.recoverArrayRank(xData == null ? null : xData.getClass());
                            IValueConvertor xPositionConvertor;
                            if (rank == 2) {
                                MatrixPositionConvertor matrixConvertor = new MatrixPositionConvertor();
                                matrixConvertor.setNumberMatrix((Object[]) xData);
                                xPositionConvertor = matrixConvertor;
                            } else {
                                ArrayPositionConvertor arrayConvertor = new ArrayPositionConvertor();
                                arrayConvertor.setNumberArray(xData);
                                xPositionConvertor = arrayConvertor;
                            }
                            convertors[1] = xPositionConvertor;
                        }
                        convertedPositionsTarget.setValueConvertors(convertors);
                        convertors = null;
                    }
                }
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Failed to transmit " + source + " data to targets", e);
            }
        }
    }

    @Override
    public GenericDescriptor getSourceType() {
        return new GenericDescriptor(ScanData.class);
    }

}
