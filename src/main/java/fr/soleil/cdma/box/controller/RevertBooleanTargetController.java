/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.controller;

import fr.soleil.data.controller.BasicBooleanTargetController;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.scalar.IBooleanTarget;

/**
 * A {@link BasicBooleanTargetController} that sends the not value to targets
 * 
 * @author girardot
 */
public class RevertBooleanTargetController extends BasicBooleanTargetController {

    @Override
    protected void setDataToTarget(ITarget target, Boolean data, AbstractDataSource<Boolean> source) {
        ((IBooleanTarget) target).setSelected(data == null ? false : !data.booleanValue());
    }

}
