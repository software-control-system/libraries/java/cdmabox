/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.controller;

import java.net.URI;

import fr.soleil.cdma.box.target.IUriTarget;
import fr.soleil.data.controller.BasicTargetController;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;

public class BasicUriController extends BasicTargetController<URI> {

    public BasicUriController() {
        super();
    }

    @Override
    protected URI generateDefaultValue() {
        // Can't generate a default URI
        return null;
    }

    @Override
    protected void setDataToTarget(ITarget target, URI data, AbstractDataSource<URI> source) {
        if (isCompatibleWith(target)) {
            ((IUriTarget) target).setUri(data);
        }
    }

    @Override
    protected boolean isInstanceOfU(Object data) {
        return (data instanceof URI);
    }

    @Override
    protected boolean isCompatibleWith(ITarget target) {
        return (target instanceof IUriTarget);
    }

}
