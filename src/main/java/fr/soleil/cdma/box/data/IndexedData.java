/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data;

import fr.soleil.data.container.AbstractNamedObject;
import fr.soleil.lib.project.data.FlatData;

/**
 * A class that contains all necessary information about some data in a stack
 * 
 * @author girardot
 */
public class IndexedData extends AbstractNamedObject implements IIndexedData {

    private int[] position;
    private FlatData data;

    public IndexedData() {
        super();
        position = null;
        data = null;
    }

    @Override
    public int[] getOrigin() {
        return position;
    }

    /**
     * Sets the data position in stack
     * 
     * @param position The data position to set
     */
    public void setOrigin(int[] position) {
        this.position = position;
    }

    /**
     * Returns the data flat representation
     * 
     * @return A {@link FlatData}
     */
    public FlatData getData() {
        return data;
    }

    /**
     * Sets the data flat representation
     * 
     * @param data The data flat representation to set
     */
    public void setData(FlatData data) {
        this.data = data;
    }

}
