/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data.sensor;

public class SpectrumConfig extends SensorConfig implements Comparable<SpectrumConfig> {

    public SpectrumConfig(Sensor sensor, String name) {
        super(sensor, name);
    }

    @Override
    public int compareTo(SpectrumConfig t) {
        return name.compareTo(t.getName());
    }
}
