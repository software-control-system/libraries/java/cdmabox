/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data;

import fr.soleil.cdma.box.reader.ICDMAReader;
import fr.soleil.lib.project.math.NumberArrayUtils;

public class SoftDoubleArray extends SoftData<double[]> {

    public SoftDoubleArray(ContextDataItem contextDataItem, ICDMAReader cdmReader, double[] value) {
        super(contextDataItem, cdmReader, value);
    }

    @Override
    protected double[] recoverData() {
        double[] result = null;
        ICDMAReader cdmaReader = cdmaReaderReference.get();
        if (cdmaReader != null) {
            result = NumberArrayUtils
                    .extractDoubleArray(cdmaReader.readCDMSimpleArrayWithCtx(contextDataItem, null, null));
        }
        return result;
    }
}
