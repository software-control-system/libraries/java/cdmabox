/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data;

import java.lang.ref.SoftReference;

import fr.soleil.cdma.box.reader.ICDMAReader;

/**
 * A {@link ReferencedData} that uses a {@link SoftReference} on its associated data
 * 
 * @author girardot
 * 
 * @param <T> The type of Object this {@link SoftData} is associated with
 */
public abstract class SoftData<T> extends ReferencedData implements IRecoverable<T> {

    private static final String DATA_NAME = "data";

    protected SoftReference<T> dataReference;

    /**
     * Constructs this {@link SoftData} with all necessary information to recover its content
     * 
     * @param contextDataItem The data {@link ContextDataItem}
     * @param cdmaReader The {@link ICDMAReader} that should recover the data
     */
    public SoftData(ContextDataItem contextDataItem, ICDMAReader cdmaReader) {
        this(contextDataItem, cdmaReader, null);
        setData(recoverData());
    }

    /**
     * Constructs this {@link SoftData} with all necessary information to recover its content
     * 
     * @param contextDataItem The data {@link ContextDataItem}
     * @param cdmaReader The {@link ICDMAReader} that should recover the data
     * @param data The value associated with this {@link SoftData}
     */
    public SoftData(ContextDataItem contextDataItem, ICDMAReader cdmaReader, T data) {
        super(contextDataItem, cdmaReader);
        setData(data);
    }

    @Override
    protected String getDataName() {
        return DATA_NAME;
    }

    protected final void setData(T data) {
        if (data == null) {
            this.dataReference = null;
        } else {
            this.dataReference = new SoftReference<T>(data);
        }
    }

    /**
     * Asks the {@link ICDMAReader} to recover the data, and returns it
     * 
     * @return The recovered data
     */
    protected abstract T recoverData();

    /**
     * Returns this {@link SoftData}'s data
     * 
     * @return This {@link SoftData}'s associated data, which may have been recovered through
     *         {@link #recoverData(boolean)}
     */
    @Override
    public synchronized T getData() {
        boolean dataRecovered;
        T data;
        if (dataReference == null) {
            data = null;
            dataRecovered = true;
        } else {
            data = dataReference.get();
            dataRecovered = false;
        }
        if ((data == null) && (!dataRecovered)) {
            dataRecovered = true;
            data = recoverData();
            if (data != null) {
                if (dataReference != null) {
                    dataReference.clear();
                }
                dataReference = new SoftReference<T>(data);
            }
        }
        return data;
    }
}
