/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data;

import fr.soleil.cdma.box.reader.ICDMAReader;

/**
 * An {@link Enum} that represents some data an {@link ICDMAReader} could load, but is not forced to
 * 
 * @author girardot
 */
public enum LoadableData {
    /**
     * Load images
     */
    IMAGES,
    /**
     * Load spectra
     */
    SPECTRA,
    /**
     * Load scalars
     */
    SCALARS,
    /**
     * Load total image
     */
    TOTAL_IMAGE,
    /**
     * Load total spectrum
     */
    TOTAL_SPECTRUM,
    /**
     * Load beam energy
     */
    BEAM_ENERGY,
    /**
     * Load sample Phi
     */
    SAMPLE_PHI,
    /**
     * Load sample Theta
     */
    SAMPLE_THETA,
    /**
     * Load aperture angle
     */
    APERTURE_ANGLE
}
