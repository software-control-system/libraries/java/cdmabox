/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data;

import fr.soleil.cdma.box.reader.ICDMAReader;

/**
 * A {@link SoftData} that uses an origin and a shape to recover its content
 * 
 * @param <T> The type of Object this {@link SoftIndexedData} is associated with
 * 
 * @author girardot
 */
public abstract class SoftIndexedData<T> extends SoftData<T> implements IIndexedData {

    protected int[] origin;
    protected int[] shape;

    public SoftIndexedData(ContextDataItem contextDataItem, int[] origin, int[] shape, ICDMAReader cdmaReader) {
        this(contextDataItem, origin, shape, cdmaReader, null);
        setData(recoverData());
    }

    public SoftIndexedData(ContextDataItem contextDataItem, int[] origin, int[] shape, ICDMAReader cdmaReader, T data) {
        super(contextDataItem, cdmaReader, data);
        this.origin = origin;
        this.shape = shape;
    }

    @Override
    public int[] getOrigin() {
        return (origin == null ? null : origin.clone());
    }

    public int[] getShape() {
        return (shape == null ? null : shape.clone());
    }

}
