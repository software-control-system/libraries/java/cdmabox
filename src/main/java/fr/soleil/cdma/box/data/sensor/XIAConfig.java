/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data.sensor;

/**
 * 
 * @author viguier
 */
public class XIAConfig extends SensorConfig implements Comparable<XIAConfig> {

    public XIAConfig(String region) {
        super(Sensor.XIA, region);
    }

    @Override
    public int compareTo(XIAConfig t) {
        return name.compareTo(t.getName());
    }
}
