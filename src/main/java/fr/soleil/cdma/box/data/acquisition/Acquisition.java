/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data.acquisition;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import fr.soleil.cdma.box.data.SoftData;
import fr.soleil.cdma.box.data.scan.ScanData;
import fr.soleil.cdma.box.data.scan.ScanData.Dimension;
import fr.soleil.cdma.box.data.sensor.SensorConfig;
import fr.soleil.lib.project.math.MathConst;

/**
 * This class represents an acquisition, for a particular {@link SensorConfig}
 * 
 * @author girardot
 */
public class Acquisition {

    private final String sourceURI;
    private final String uriSimpleName;
    private final String detector;
    private final List<ScanData> scanList;
    private double[] spectrumX;
    private boolean energyAcquisition;

    /**
     * Constructs this {@link Acquisition} and associates a source URI and a detector to it.
     * 
     * @param sourceURI The path of the URI
     * @param uriSimpleName The path of the URI, or a simple name for the URI, that can be understood by human beings.
     * @param detector The detector
     */
    public Acquisition(String sourceURI, String uriSimpleName, String detector) {
        scanList = new ArrayList<ScanData>();
        this.sourceURI = sourceURI;
        this.uriSimpleName = uriSimpleName;
        this.detector = detector;
        spectrumX = null;

        energyAcquisition = false;
    }

    public double[] getSpectrumX() {
        return spectrumX;
    }

    public void setSpectrumX(double[] spectrumX) {
        this.spectrumX = spectrumX;
    }

    /**
     * Returns this {@link Acquisition}'s detector.
     * 
     * @return A {@link String}.
     */
    public String getDetector() {
        return detector;
    }

    /**
     * Returns a human readable name of the uri that contains the acquisition
     * 
     * @return A {@link String}
     */
    public String getUriSimpleName() {
        return uriSimpleName;
    }

    /**
     * Returns the path of the uri that contains the acquisition
     * 
     * @return A {@link String}
     */
    public String getSourceURI() {
        return sourceURI;
    }

    public boolean isEnergyAcquisition() {
        return energyAcquisition;
    }

    /**
     * Returns the {@link List} of {@link ScanData} of this {@link Acquisition}
     * 
     * @return An {@link ScanData} {@link List}
     */
    public List<ScanData> getScanDataList() {
        List<ScanData> copy = new ArrayList<ScanData>();
        copy.addAll(scanList);
        return copy;
    }

    /**
     * Adds a {@link ScanData} in the {@link ScanData} {@link List}
     * 
     * @param data The {@link ScanData} to add
     */
    public void addScanData(ScanData data) {
        if (data != null) {
            scanList.add(data);
        }
    }

    public static double getEnergy(ScanData scan) {
        return (scan == null ? MathConst.NAN_FOR_NULL : scan.getEnergy());
    }

    public static int getScanWidth(ScanData scan) {
        int result = 0;
        if (scan != null) {
            SoftData<?> xData = scan.getPositionData(Dimension.X);
            if (xData != null) {
                double[] value = null;
                value = (double[]) xData.getData();
                if (value != null) {
                    result = value.length;
                }
            }
        }
        return result;
    }

    public static int getScanHeight(ScanData scan) {
        int result = 0;
        if (scan != null) {
            SoftData<?> yData = scan.getPositionData(Dimension.Y);
            if (yData == null) {
                if (scan.getPositionData(Dimension.X) != null) {
                    // 1D scan case
                    result = 1;
                }
            } else {
                // 2D scan case
                double[] value = (double[]) yData.getData();
                if (value != null) {
                    result = value.length;
                }
            }
        }
        return result;

    }

    public static double[] getPositionForIndex(ScanData scan, int xIndex, int yIndex) {
        double[] result = null;
        if (scan != null) {
            double yValue = Double.NaN;
            double xValue = Double.NaN;
            SoftData<?> yData = scan.getPositionData(Dimension.Y);
            boolean hasY = (yData != null);
            double[] value;
            if (hasY) {
                value = (double[]) yData.getData();
                if ((value != null) && (yIndex > -1) && (yIndex < value.length)) {
                    yValue = value[yIndex];
                }
            }
            value = null;
            SoftData<?> xData = scan.getPositionData(Dimension.X);
            if (xData != null) {

                value = (double[]) xData.getData();
                if ((value != null) && (xIndex > -1) && (xIndex < value.length)) {
                    xValue = value[xIndex];
                }
            }
            result = new double[] { xValue, yValue };
        }
        return result;
    }

    public static int getGlobalIndexForPosition(ScanData scan, double x, double y) {
        int result = -1;
        SoftData<?> yData = scan.getPositionData(Dimension.Y);
        int yIndex = 0;
        int xIndex = -1;
        boolean hasY = (yData != null);
        double[] value;
        if (hasY) {
            value = (double[]) yData.getData();
            if (value != null) {
                yIndex = ArrayUtils.indexOf(value, y);
            }
        }
        value = null;
        SoftData<?> xData = scan.getPositionData(Dimension.X);
        if (xData != null) {
            value = (double[]) xData.getData();
            if (value != null) {
                xIndex = ArrayUtils.indexOf(value, x);
            }
        }
        if ((xIndex > -1) && (yIndex > -1)) {
            result = yIndex * getScanWidth(scan) + xIndex;
        }
        return result;
    }

    public void sortScans() {
        boolean tmpEnergy = true;
        List<Double> energyList = new ArrayList<Double>();
        for (ScanData scan : scanList) {
            // It is not an energy acquisition if:
            // - either a scan has no energy
            // - or 2 scans share the same energy
            Double energy = scan.getEnergy();
            if (energy.isNaN() || energyList.contains(energy)) {
                tmpEnergy = false;
                break;
            } else {
                energyList.add(energy);
            }
        }
        energyList.clear();
        energyAcquisition = tmpEnergy;
        Comparator<ScanData> scanComparator;
        if (energyAcquisition) {
            scanComparator = new Comparator<ScanData>() {
                @Override
                public int compare(ScanData o1, ScanData o2) {
                    return Double.compare(o1.getEnergy(), o2.getEnergy());
                }
            };
        } else {
            scanComparator = new Comparator<ScanData>() {
                private final Collator collator = Collator.getInstance();

                @Override
                public int compare(ScanData o1, ScanData o2) {
                    return collator.compare(o1.getScanName(), o2.getScanName());
                }
            };
        }
        Collections.sort(scanList, scanComparator);
    }

    /**
     * Cleans this {@link Acquisition}
     */
    public void clean() {
        if (scanList != null) {
            scanList.clear();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        clean();
        super.finalize();
    }

}
