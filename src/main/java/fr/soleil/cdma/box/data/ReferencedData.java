/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.cdma.Factory;
import org.cdma.exception.DataAccessException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IDataItem;

import fr.soleil.cdma.box.reader.ICDMAReader;

/**
 * This class represents some reference on a CDMA data
 * 
 * @author girardot
 */
public abstract class ReferencedData {

    protected ContextDataItem contextDataItem;
    protected WeakReference<ICDMAReader> cdmaReaderReference;
    protected Map<String, String> attributeMap;

    /**
     * Constructs this {@link ReferencedData}
     * 
     * @param contextDataItem The data {@link ContextDataItem}
     * @param cdmaReader The {@link ICDMAReader} that is able to access the data
     */
    public ReferencedData(ContextDataItem contextDataItem, ICDMAReader cdmaReader) {
        this.contextDataItem = contextDataItem;
        this.attributeMap = new ConcurrentHashMap<>();
        if (cdmaReader == null) {
            this.cdmaReaderReference = null;
        } else {
            this.cdmaReaderReference = new WeakReference<ICDMAReader>(cdmaReader);
        }
    }

    /**
     * Common method to recover the associated {@link ICDMAReader}
     * 
     * @return An {@link ICDMAReader}
     */
    protected final ICDMAReader getReader() {
        return (cdmaReaderReference == null ? null : cdmaReaderReference.get());
    }

    protected abstract String getDataName();

    /**
     * Finalizes what has to be finalized after big data reading
     */
    public void finalizeReading() {
        ICDMAReader reader = getReader();
        if (reader != null) {
            IDataItem item = reader.getDataItem(contextDataItem);
            if (item != null) {
                try {
                    item.finalizeReading();
                } catch (DataAccessException e) {
                    Factory.getLogger().error("Failed to finalize " + getDataName() + " after multiple reading", e);
                }
            }
        }
    }

    /**
     * Returns the value of an attribute associated with the data known by this {@link ReferencedData}
     * 
     * @param attributeName The attribute name
     * @return A {@link String}
     */
    public String getStringAttribute(String attributeName) {
        String value = null;
        if (attributeName != null) {
            value = attributeMap.get(attributeName);
            if ((value == null) && (contextDataItem != null)) {
                ICDMAReader reader = getReader();
                if (reader != null) {
                    IDataItem item = reader.getDataItem(contextDataItem);
                    if (item != null) {
                        try {
                            IAttribute attribute = item.getAttribute(attributeName);
                            if (attribute != null) {
                                value = attribute.getStringValue();
                                if (value != null) {
                                    attributeMap.put(attributeName, value);
                                }
                            }
                        } catch (Exception e) {
                            value = null;
                        }
                    }
                }
            }
        }
        return value;
    }
}
