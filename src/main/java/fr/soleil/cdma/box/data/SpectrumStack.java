package fr.soleil.cdma.box.data;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.cdma.dictionary.LogicalGroup;
import org.cdma.interfaces.IKey;

import fr.soleil.cdma.box.reader.ICDMAReader;
import fr.soleil.cdma.box.util.PositionKey;
import fr.soleil.lib.project.data.FlatData;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.math.NumberArrayUtils;

/**
 * A {@link ReferencedData} that represents an image stack
 * 
 * @author thairi
 */
public class SpectrumStack extends StackedData {

    private static final String DATA_NAME = "spectrum stack";

    protected static int[] getStackFullShape(final int[] stackShape, final int spectrumLength) {
        int[] stackFullShape;
        if (stackShape == null) {
            stackFullShape = null;
        } else {
            stackFullShape = Arrays.copyOf(stackShape, stackShape.length + 1);
            stackFullShape[stackShape.length] = spectrumLength;
        }
        return stackFullShape;
    }

    private final int[] stackShape;
    private final int spectrumLength;
    private final int stackSize;
    protected final Map<PositionKey, SoftIndexedData<double[]>> spectra;
    protected double[] spectrumX;
    protected final boolean useMap;

    public SpectrumStack(LogicalGroup parentGroup, IKey dataItemKey, ICDMAReader cdmaReader, int[] stackFullShape,
            int[] stackReducedShape) {
        super(parentGroup, dataItemKey, cdmaReader, stackFullShape);
        this.stackShape = stackReducedShape;
        if ((dataShape == null) || (dataShape.length == 0)) {
            spectrumLength = 0;
        } else {
            spectrumLength = dataShape[0];
        }
        this.stackSize = getStackSize(stackShape);
        this.spectra = null;
        this.useMap = false;

        initSpectrumX();
    }

    public SpectrumStack(Map<PositionKey, SoftIndexedData<double[]>> spectra, int[] stackReducedShape,
            int spectrumLength) {
        super(null, null, getStackFullShape(stackReducedShape, spectrumLength));
        if (spectra == null) {
            this.spectra = new HashMap<>();
        } else {
            this.spectra = spectra;
        }
        this.useMap = true;
        this.stackShape = stackReducedShape;
        this.stackSize = getStackSize(stackShape);
        this.spectrumLength = spectrumLength;
        initSpectrumX();
    }

    @Override
    protected int getDataRank() {
        return 1;
    }

    @Override
    protected String getDataName() {
        return DATA_NAME;
    }

    public int getStackSize() {
        return stackSize;
    }

    public void initSpectrumX() {
        Object spectrum = getSpectrumAt(0);
        if (spectrum != null) {
            double[] firstSpec = NumberArrayUtils.extractDoubleArray(spectrum);
            if (firstSpec != null) {
                this.spectrumX = new double[firstSpec.length];
                for (int i = 0; i < spectrumX.length; i++) {
                    spectrumX[i] = i;
                }
            } else {
                spectrumX = null;
            }
        } else {
            spectrumX = null;
        }
    }

    public Object getSpectrumAt(int... position) {
        Object spectrum = null;
        if (useMap) {
            SoftIndexedData<double[]> data = spectra.get(new PositionKey(position));
            if (data != null) {
                spectrum = data.getData();
            }
        } else {
            ICDMAReader cdmaReader = getReader();
            if ((cdmaReader != null) && (stackFullShape != null) && (position != null)) {
                if (position.length != stackFullShape.length) {
                    position = Arrays.copyOf(position, stackFullShape.length);
                }
                // For spectra, we don't need FlatData, as resulting data is already a single dimension array.
                spectrum = cdmaReader.readCDMSimpleArrayWithCtx(contextDataItem, position, dataFullShape);
            }

        }
        return spectrum;
    }

    public FlatData[] getSpectrumBlockAt(int sizeBlock, int... position) {
        FlatData[] spectrum = null;
        if (useMap) {
            SoftIndexedData<double[]> data = spectra.get(new PositionKey(position));
            if (data != null) {
                spectrum = new FlatData[1];
                spectrum[0] = new FlatData(data.getShape(), data.getData());
            }
        } else {
            ICDMAReader cdmaReader = getReader();
            if ((cdmaReader != null) && (stackFullShape != null) && (position != null)) {
                if (position.length != stackFullShape.length) {
                    position = Arrays.copyOf(position, stackFullShape.length);
                }
                // For spectra, we don't need FlatData, as resulting data is already a single dimension array
                spectrum = cdmaReader.readCDMFlatComplexArrayBlockWithCtx(contextDataItem, position, dataFullShape,
                        sizeBlock);
                // spectrum = cdmaReader.readCDMSimpleArrayWithCtx(contextDataItem, position, dataFullShape);
            }

        }
        return spectrum;

    }

    @Override
    public Object getCompleteStack() {
        Object result;
        if (useMap) {
            if ((stackShape == null) || (stackShape.length == 0) || (stackFullShape == null)
                    || (stackFullShape.length == 0)) {
                result = null;
            } else {
                // Build complete stack
                result = ArrayUtils.newInstance(Double.TYPE, stackFullShape);
                int[] position = new int[stackShape.length];
                int max = position.length - 1;
                while (ArrayUtils.isPosition(stackShape, position)) {
                    SoftIndexedData<double[]> data = spectra.get(new PositionKey(position));
                    if (data != null) {
                        double[] spectrum = data.getData();
                        if (spectrum != null) {
                            Object array = result;
                            for (int i = 0; i < max; i++) {
                                array = Array.get(array, position[i]);
                            }
                            if (array instanceof double[][]) {
                                double[][] matrix = (double[][]) array;
                                matrix[position[max]] = spectrum;
                            }
                        }
                    }
                }
            }
        } else {
            result = super.getCompleteStack();
        }
        return result;
    }

    @Override
    public FlatData getFlatBlockAt(int nbOfData, int... position) {
        FlatData result;
        if (useMap) {
            if ((nbOfData > 0) && (spectrumLength > 0) && (position != null) && (stackShape != null)
                    && (position.length == stackShape.length)) {
                int count = Math.min(nbOfData, stackShape[stackShape.length - 1] - position[position.length - 1]);
                if (count > 0) {
                    double[] array = new double[count * spectrumLength];
                    Arrays.fill(array, MathConst.NAN_FOR_NULL);
                    int[] index = position.clone();
                    int i = 0;
                    while (ArrayUtils.isPosition(stackShape, index)) {
                        SoftIndexedData<double[]> data = spectra.get(new PositionKey(index));
                        if (data != null) {
                            double[] spectrum = data.getData();
                            if (spectrum != null) {
                                System.arraycopy(spectrum, 0, array, i * spectrumLength, spectrumLength);
                            }
                        }
                        ArrayUtils.incrementPosition(stackShape, index);
                        if (++i >= count) {
                            break;
                        }
                    }
                    result = new FlatData(new int[] { count, spectrumLength }, array);
                } else {
                    result = null;
                }
            } else {
                result = null;
            }
        } else {
            result = super.getFlatBlockAt(nbOfData, position);
        }
        return result;
    }

    @Override
    public FlatData[] getBlockAt(int nbOfData, int... position) {
        FlatData[] result;
        if (useMap) {
            if ((nbOfData > 0) && (spectrumLength > 0) && (position != null) && (stackShape != null)
                    && (position.length == stackShape.length)) {
                int count = Math.min(nbOfData, stackShape[stackShape.length - 1] - position[position.length - 1]);
                if (count > 0) {
                    result = new FlatData[count];
                    int[] index = position.clone();
                    int i = 0;
                    while (ArrayUtils.isPosition(stackShape, index)) {
                        SoftIndexedData<double[]> data = spectra.get(new PositionKey(index));
                        if (data != null) {
                            double[] spectrum = data.getData();
                            if (spectrum != null) {
                                result[i] = new FlatData(stackShape, spectrum);
                            }
                        }
                        ArrayUtils.incrementPosition(stackShape, index);
                        if (++i >= count) {
                            break;
                        }
                    }
                } else {
                    result = null;
                }
            } else {
                result = null;
            }
        } else {
            result = super.getBlockAt(nbOfData, position);
        }
        return result;
    }

    public int getSpectrumLength() {
        return spectrumLength;
    }

    public int[] getStackFullShape() {
        return stackFullShape;
    }

    public int[] getStackShape() {
        return stackShape;
    }

    public double[] getSpectrumX() {
        return spectrumX;
    }

    @Override
    protected void finalize() throws Throwable {
        if (spectra != null) {
            spectra.clear();
        }
        super.finalize();
    }
}
