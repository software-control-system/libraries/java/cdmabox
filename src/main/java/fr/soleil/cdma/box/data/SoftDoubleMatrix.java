/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data;

import fr.soleil.cdma.box.reader.ICDMAReader;
import fr.soleil.lib.project.math.NumberMatrixUtils;

public class SoftDoubleMatrix extends SoftData<double[][]> {

    public SoftDoubleMatrix(ContextDataItem contextDataItem, ICDMAReader cdmaReader, double[][] value) {
        super(contextDataItem, cdmaReader, value);
    }

    @Override
    protected double[][] recoverData() {
        double[][] result = null;
        ICDMAReader cdmaReader = cdmaReaderReference.get();
        if (cdmaReader != null) {
            result = NumberMatrixUtils
                    .extractDoubleMatrix(cdmaReader.readCDMComplexArrayWithCtx(contextDataItem, null, null));
        }
        return result;
    }

}
