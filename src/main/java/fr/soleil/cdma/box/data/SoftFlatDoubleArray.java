/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data;

import fr.soleil.cdma.box.reader.ICDMAReader;
import fr.soleil.lib.project.data.FlatData;
import fr.soleil.lib.project.data.FlatDoubleData;
import fr.soleil.lib.project.math.NumberArrayUtils;

public class SoftFlatDoubleArray extends SoftData<FlatDoubleData> {

    public SoftFlatDoubleArray(ContextDataItem contextDataItem, ICDMAReader cdmReader, FlatDoubleData value) {
        super(contextDataItem, cdmReader, value);
    }

    @Override
    protected FlatDoubleData recoverData() {
        FlatDoubleData result = null;
        ICDMAReader cdmaReader = cdmaReaderReference.get();
        if ((cdmaReader != null) && (contextDataItem != null)) {
            FlatData data = cdmaReader.readCDMFlatComplexArrayWithCtx(contextDataItem, null, null);
            if (data != null) {
                result = new FlatDoubleData(data.getRealShape(), NumberArrayUtils.extractDoubleArray(data.getValue()));
            }
        }
        return result;
    }
}
