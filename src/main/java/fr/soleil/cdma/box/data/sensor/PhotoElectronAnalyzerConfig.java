package fr.soleil.cdma.box.data.sensor;

public abstract class PhotoElectronAnalyzerConfig extends SensorConfig
        implements Comparable<PhotoElectronAnalyzerConfig> {
    protected EnergyScale energyScale;
    protected double[] channelScale;// pixelToEnergy
    protected double channels;
    protected double lowEnergy;
    protected double highEnergy;

    protected double[] sliceScale;// pixelToThetaD
    protected double slices;

    public static enum EnergyScale {
        // don't modify case, it is used for comparison
        Kinetic, Binding
    }

    public PhotoElectronAnalyzerConfig(Sensor sensor, String name) {
        super(sensor, name);
    }

    /**
     * @return the channelScale
     */
    public double[] getPixelToEnergy() {
        return channelScale;
    }

    /**
     * @param channelScale the channelScale to set
     */
    public void setPixelToEnergy(final double[] channelScale) {
        this.channelScale = channelScale;
    }

    /**
     * @return the channels
     */
    public double getNbEnergySteps() {
        return channels;
    }

    /**
     * @param channels the channels to set
     */
    public void setNbEnergySteps(final double channels) {
        this.channels = channels;
    }

    /**
     * @return the lowEnergy
     */
    public double getMinEnergy() {
        return lowEnergy;
    }

    /**
     * @param energy the lowEnergy to set
     */
    public void setMinEnergy(final double energy) {
        lowEnergy = energy;
    }

    /**
     * @return the highEnergy
     */
    public double getMaxEnergy() {
        return highEnergy;
    }

    /**
     * @param energy the highEnergy to set
     */
    public void setMaxEnergy(final double energy) {
        highEnergy = energy;
    }

    /**
     * @return the sliceScale
     */
    public double[] getPixelToThetaD() {
        return sliceScale;
    }

    /**
     * @param sliceScale the sliceScale to set
     */
    public void setPixelToThetaD(final double[] sliceScale) {
        this.sliceScale = sliceScale;
    }

    /**
     * @return the slices
     */
    public double getNbThetaDSteps() {
        return slices;
    }

    /**
     * @param nbSlices the slices to set
     */
    public void setNbThetaDSteps(final double nbSlices) {
        slices = nbSlices;
    }

    /**
     * @return the energyScale
     */
    public EnergyScale getEnergyMode() {
        return energyScale;
    }

    /**
     * @param energyScale the energyScale to set
     */
    public void setEnergyMode(final EnergyScale energyScale) {
        this.energyScale = energyScale;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final PhotoElectronAnalyzerConfig o) {
        return name.compareTo(o.getName());
    }
}
