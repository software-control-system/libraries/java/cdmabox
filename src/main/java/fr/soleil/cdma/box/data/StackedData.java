package fr.soleil.cdma.box.data;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;

import org.cdma.Factory;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.exception.DataAccessException;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IKey;
import org.cdma.utils.MetricsUtils;
import org.slf4j.LoggerFactory;

import fr.soleil.cdma.box.reader.ICDMAReader;
import fr.soleil.lib.project.data.FlatData;
import fr.soleil.lib.project.date.DateUtil;

/**
 * A common class for everything able to manage a stack.
 * 
 * @author GIRARDOT
 */
public abstract class StackedData extends ReferencedData {

    private static final AtomicLong READ_ARRAY_TIME = new AtomicLong(0);

    public static void resetMetrics() {
        READ_ARRAY_TIME.set(0);
    }

    public static void traceMetrics(String applicationId) {
        StringBuilder builder = new StringBuilder(ImageStack.class.getSimpleName());
        DateUtil.elapsedTimeToStringBuilder("\nStackedData Cumulative array read time: ", READ_ARRAY_TIME, builder);
        LoggerFactory.getLogger(applicationId).trace(builder.toString());
    }

    protected static int getStackSize(int... stackShape) {
        int stackSize;
        if ((stackShape == null) || (stackShape.length < 1)) {
            stackSize = 0;
        } else {
            stackSize = 1;
            for (int dim : stackShape) {
                stackSize *= dim;
            }
        }
        return stackSize;
    }

    protected final int[] stackFullShape, dataShape, dataFullShape;

    public StackedData(LogicalGroup parentGroup, IKey dataItemKey, ICDMAReader cdmaReader, int[] stackFullShape) {
        this(new ContextDataItem(parentGroup, dataItemKey), cdmaReader, stackFullShape);
    }

    public StackedData(ContextDataItem contextDataItem, ICDMAReader cdmaReader, int[] stackFullShape) {
        super(contextDataItem, cdmaReader);
        this.stackFullShape = stackFullShape;
        int rank = getDataRank();
        if ((stackFullShape == null) || (stackFullShape.length == 0) || (rank < 0)) {
            dataShape = null;
            dataFullShape = null;
        } else if (rank > 0) {
            if (stackFullShape.length > rank) {
                int max = stackFullShape.length - rank;
                dataShape = Arrays.copyOfRange(stackFullShape, max, stackFullShape.length);
                dataFullShape = new int[stackFullShape.length];
                for (int i = 0; i < max; i++) {
                    dataFullShape[i] = 1;
                }
                System.arraycopy(stackFullShape, max, dataFullShape, max, rank);
            } else if (stackFullShape.length == rank) {
                dataShape = stackFullShape;
                dataFullShape = stackFullShape;
            } else {
                dataShape = null;
                dataFullShape = null;
            }
        } else if (rank == 0) {
            dataShape = new int[] { 1 };
            dataFullShape = new int[stackFullShape.length];
            Arrays.fill(dataFullShape, 1);
        } else {
            dataShape = null;
            dataFullShape = null;
        }
    }

    /**
     * Returns the expected data rank.
     * 
     * @return An <code>int</code>.
     */
    protected abstract int getDataRank();

    /**
     * Returns the data at given position, represented as a {@link FlatData}.
     * 
     * @param position The position.
     * @return A {@link FlatData}.
     */
    protected FlatData getDataAt(int... position) {
        return getData(position, dataShape);
    }

    /**
     * Returns a block of data, starting at given position.
     * 
     * @param nbOfData The number of data in a block
     * @param position The data position in the stack.
     * @return A {@link FlatData}.
     */
    public FlatData getFlatBlockAt(int nbOfData, int... position) {
        FlatData block = null;
        int rank = getDataRank();
        if ((stackFullShape != null) && (stackFullShape.length > rank) && (rank > -1) && (position != null)
                && (dataShape != null) && (dataShape.length == rank) && (nbOfData > 0)) {
            int[] shape = new int[rank + 1];
            shape[0] = nbOfData;
            if (rank > 0) {
                System.arraycopy(dataShape, 0, shape, 1, rank);
            }
            block = getData(position, shape);
        }
        return block;
    }

    /**
     * Returns a block of data, starting at given position.
     * 
     * @param nbOfData The number of data in a block
     * @param position The data position in the stack.
     * @return A {@link FlatData} array.
     */
    public FlatData[] getBlockAt(int nbOfData, int... position) {
        return getBlockAt(nbOfData, dataShape, position);
    }

    /**
     * Returns a block of data, starting at given position.
     * 
     * @param nbOfData The number of data in a block.
     * @param shape The data shape.
     * @param position The data position in the stack.
     * @return A {@link FlatData} array.
     */
    protected FlatData[] getBlockAt(int nbOfData, int[] shape, int... position) {
        FlatData[] block = null;
        ICDMAReader cdmaReader = getReader();
        if ((cdmaReader != null) && (stackFullShape != null) && (stackFullShape.length > 0) && (dataShape != null)
                && (position != null)) {
            if (position.length != stackFullShape.length) {
                position = Arrays.copyOf(position, stackFullShape.length);
            }
            long time;
            if (MetricsUtils.isUseMetrics()) {
                time = System.currentTimeMillis();
            } else {
                time = 0;
            }
            block = cdmaReader.readCDMFlatComplexArrayBlockWithCtx(contextDataItem, position, shape, nbOfData);
            if (MetricsUtils.isUseMetrics()) {
                READ_ARRAY_TIME.addAndGet(System.currentTimeMillis() - time);
            }
        }
        return block;
    }

    /**
     * Returns some data of given shape and at given position, as a {@link FlatData}.
     * 
     * @param position The position.
     * @param shape The shape of the data to extract.
     * @return A {@link FlatData}.
     */
    protected FlatData getData(int[] position, int[] shape) {
        FlatData data = null;
        ICDMAReader cdmaReader = getReader();
        if ((cdmaReader != null) && (stackFullShape != null) && (stackFullShape.length > 0) && (shape != null)
                && (position != null)) {
            if (position.length != stackFullShape.length) {
                position = Arrays.copyOf(position, stackFullShape.length);
            }
            long time;
            if (MetricsUtils.isUseMetrics()) {
                time = System.currentTimeMillis();
            } else {
                time = 0;
            }
            data = cdmaReader.readCDMFlatComplexArrayWithCtx(contextDataItem, position, shape);
            if (MetricsUtils.isUseMetrics()) {
                READ_ARRAY_TIME.addAndGet(System.currentTimeMillis() - time);
            }
        }
        return data;
    }

    /**
     * Returns the complete data stack (the ND java array).
     * 
     * @return An {@link Object}.
     */
    public Object getCompleteStack() {
        Object result = null;
        ICDMAReader cdmaReader = getReader();
        if ((cdmaReader != null) && (contextDataItem != null) && (stackFullShape != null)) {
            result = cdmaReader.readCDMComplexArray(contextDataItem.getParentGroup(), contextDataItem.getDataItemKey(),
                    null, null);
        }
        return result;
    }

    /**
     * Prepares everything necessary before data reading and returns the number of data that can be
     * read in a block. The reading process can be done on a single thread only or on a multiple threads.
     * <p>
     * Don't forget to call {@link #setUnsafeReadingEnabled(boolean)}, in order to say whether reading will be done in a
     * single or in multiple thread(s).
     * </p>
     * 
     */
    public int prepareForReading() {
        int dataPerBlock = 1;
        ICDMAReader reader = getReader();
        if (reader != null) {
            IDataItem item = reader.getDataItem(contextDataItem);
            if (item != null) {
                try {
                    dataPerBlock = item.prepareForReading(getDataRank());
                } catch (DataAccessException e) {
                    Factory.getLogger().error("Failed to prepare " + getDataName() + " for multiple reading", e);
                }
            }
        }
        return dataPerBlock;
    }

    /**
     * Returns whether unsafe reading is enabled.
     * 
     * @return Whether unsafe reading is enabled.
     */
    public boolean isUnsafeReadingEnabled() {
        boolean unsafeReadingEnabled = false;
        ICDMAReader reader = getReader();
        if (reader != null) {
            IDataItem item = reader.getDataItem(contextDataItem);
            if (item != null) {
                unsafeReadingEnabled = item.isUnsafeReadingEnabled();
            }
        }
        return unsafeReadingEnabled;
    }

    /**
     * Sets whether the reading process can be unsafe (i.e. no thread safe).
     * <p>
     * Unsafe reading might be more efficient than safe reading, at the cost of thread safety.
     * </p>
     * <p>
     * Typically, if you read data in a single thread, you can use unsafe reading.
     * </p>
     * 
     * @param unsafeReadingEnabled Whether the reading process can be unsafe.
     */
    public void setUnsafeReadingEnabled(boolean unsafeReadingEnabled) {
        ICDMAReader reader = getReader();
        if (reader != null) {
            IDataItem item = reader.getDataItem(contextDataItem);
            if (item != null) {
                item.setUnsafeReadingEnabled(unsafeReadingEnabled);
            }
        }
    }

}
