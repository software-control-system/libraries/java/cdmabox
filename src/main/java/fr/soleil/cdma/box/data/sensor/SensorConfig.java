/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data.sensor;

/**
 * This class represents a sub part of a Sensor. For example, if a particular Sensor is divided into
 * channels, this class represents one channel.
 * 
 * @author viguier
 */
public abstract class SensorConfig {

    protected final String name;
    private final Sensor sensor;
    private int sensorSubPart;
    private String detectorIdentifier;

    public SensorConfig(Sensor sensor, String name) {
        this.sensor = sensor;
        this.name = name;
        sensorSubPart = -1;
    }

    /**
     * @return the detectorIdentifier
     */
    public String getDetectorIdentifier() {
        return detectorIdentifier;
    }

    /**
     * @param detectorIdentifier the detectorIdentifier to set
     */
    public void setDetectorIdentifier(String detectorIdentifier) {
        this.detectorIdentifier = detectorIdentifier;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }

    public Sensor getSensor() {
        return sensor;
    }

    public int getSensorSubPart() {
        return sensorSubPart;
    }

    public void setSensorSubPart(int sensorSubPart) {
        this.sensorSubPart = sensorSubPart;
    }

}
