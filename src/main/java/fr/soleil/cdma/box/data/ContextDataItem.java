package fr.soleil.cdma.box.data;

import org.cdma.dictionary.LogicalGroup;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IKey;

public class ContextDataItem {

    private final LogicalGroup parentGroup;
    private final IKey dataItemKey;

    // TODO volatile ?
    private IDataItem dataItem;

    public ContextDataItem(LogicalGroup parentGroup, IKey dataItemKey) {
        this(parentGroup, dataItemKey, null);
    }

    public ContextDataItem(LogicalGroup parentGroup, IKey dataItemKey, IDataItem dataItem) {
        this.parentGroup = parentGroup;
        this.dataItemKey = dataItemKey;
        this.dataItem = dataItem;
    }

    public LogicalGroup getParentGroup() {
        return parentGroup;
    }

    public IKey getDataItemKey() {
        return dataItemKey;
    }

    public IDataItem getDataItem() {
        return dataItem;
    }

    public void setDataItem(IDataItem dataItem) {
        this.dataItem = dataItem;
    }
}
