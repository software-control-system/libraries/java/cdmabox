/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data.scan;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import fr.soleil.cdma.box.data.ImageStack;
import fr.soleil.cdma.box.data.SoftData;
import fr.soleil.cdma.box.data.SoftFlatDoubleArray;
import fr.soleil.cdma.box.data.SpectrumStack;
import fr.soleil.cdma.box.data.acquisition.Acquisition;
import fr.soleil.cdma.box.manager.IAcquisitionDataManager;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.data.FlatData;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.NumberArrayUtils;

/**
 * This class represents some detector data in a scan made at a particular energy level.
 * 
 * @author girardot
 */
public class ScanData {

    public static enum Dimension {
        X, Y;
    }

    private final WeakReference<Acquisition> parent;
    private final Map<Dimension, SoftData<?>> positionMap;
    private final Map<String, Double> motors;
    private ImageStack images;
    private SpectrumStack spectrumStack;
    private SoftFlatDoubleArray scalars;
    private SoftFlatDoubleArray samplePhi;
    private SoftFlatDoubleArray sampleTheta;
    private SpectrumStack apertureAngle;
    private double[] totalSpectrum;
    private double[] beamEnergy;
    private FlatData totalImage;
    private int[] stackShape;
    private final double energy;
    private double exitSlits;
    private final String scanName;
    private final String scanOrigin;
    private String comments;
    private IAcquisitionDataManager acquisitionDataManager;
//    private long durationRead = 0;

    /**
     * Constructs this {@link ScanData}
     * 
     * @param acquisition The parent acquisition
     * @param energy The energy level
     * @param scanName The name of the scan
     */
    public ScanData(Acquisition acquisition, double energy, String scanName, String scanOrigin) {
        parent = (acquisition == null ? null : new WeakReference<Acquisition>(acquisition));
        positionMap = new LinkedHashMap<Dimension, SoftData<?>>();
        motors = new LinkedHashMap<String, Double>();
        images = null;
        spectrumStack = null;
        scalars = null;
        samplePhi = null;
        sampleTheta = null;
        apertureAngle = null;
        stackShape = null;
        this.energy = energy;
        this.scanName = scanName;
        this.scanOrigin = scanOrigin;
        acquisitionDataManager = null;
        beamEnergy = null;
        exitSlits = Double.NaN;
        comments = null;
        totalSpectrum = null;
    }

    public int[] getStackShape() {
        return stackShape;
    }

    public void setStackShape(int[] stackShape) {
        if (!ObjectUtils.sameObject(stackShape, this.stackShape)) {
            this.stackShape = stackShape;
        }
    }

    public IAcquisitionDataManager getAcquisitionDataManager() {
        return acquisitionDataManager;
    }

    public void setAcquisitionDataManager(IAcquisitionDataManager acquisitionDataManager) {
        this.acquisitionDataManager = acquisitionDataManager;
    }

    /**
     * Returns the parent acquisition
     * 
     * @return A {@link Acquisition}
     */
    public Acquisition getParent() {
        return (parent == null ? null : parent.get());
    }

    /**
     * Returns the energy used for this scan
     * 
     * @return A <code>double</code>
     */
    public double getEnergy() {
        return energy;
    }

    /**
     * @return the exitSlits
     */
    public double getExitSlits() {
        return exitSlits;
    }

    /**
     * @param exitSlits the exitSlits to set
     */
    public void setExitSlits(double exitSlits) {
        this.exitSlits = exitSlits;
    }

    /**
     * Returns the name of this scan
     * 
     * @return A {@link String}
     */
    public String getScanName() {
        return scanName;
    }

    /**
     * Returns the origin of this scan
     * 
     * @return A {@link String}
     */
    public String getScanOrigin() {
        return scanOrigin;
    }

    /**
     * Returns the comments for this scan
     * 
     * @return A {@link String}
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the comments for this scan
     * 
     * @param comments The comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

//    /**
//     * Returns the {@link Map} of spectra of this {@link ScanData}
//     * 
//     * @return A {@link Map}
//     */
//    public Map<PositionKey, SoftIndexedData<double[]>> getSpectra() {
//        Map<PositionKey, SoftIndexedData<double[]>> result = new LinkedHashMap<PositionKey, SoftIndexedData<double[]>>();
//        if (spectra != null) {
//            result.putAll(spectra);
//        }
//        return result;
//    }
//
//    public void setSpectra(Map<PositionKey, SoftIndexedData<double[]>> spectra) {
//        this.spectra = spectra;
//    }

    public void setSpectra(SpectrumStack specStack) {
        this.spectrumStack = specStack;

    }

    /**
     * @return the scalars
     */
    public SoftFlatDoubleArray getScalars() {
//        Map<PositionKey, Double> result = new LinkedHashMap<PositionKey, Double>();
//        if (scalars != null) {
//            result.putAll(scalars);
//        }
//        return result;
        return scalars;
    }

    /**
     * @param scalars the scalars to set
     */
    public void setScalars(SoftFlatDoubleArray scalars) {
        this.scalars = scalars;
    }

    /**
     * @return the samplePhi
     */
    public SoftFlatDoubleArray getSamplePhi() {
//        Map<PositionKey, Double> result = new LinkedHashMap<PositionKey, Double>();
//        if (samplePhi != null) {
//            result.putAll(samplePhi);
//        }
//        return result;
        return samplePhi;
    }

    /**
     * @param samplePhi the samplePhi to set
     */
    public void setSamplePhi(SoftFlatDoubleArray samplePhi) {
        this.samplePhi = samplePhi;
    }

    /**
     * @return the sampleTheta
     */
    public SoftFlatDoubleArray getSampleTheta() {
//        Map<PositionKey, Double> result = new LinkedHashMap<PositionKey, Double>();
//        if (sampleTheta != null) {
//            result.putAll(sampleTheta);
//        }
//        return result;
        return sampleTheta;
    }

    /**
     * @param sampleTheta the sampleTheta to set
     */
    public void setSampleTheta(SoftFlatDoubleArray sampleTheta) {
        this.sampleTheta = sampleTheta;
    }

    /**
     * @return the apertureAngle
     */
    public SpectrumStack getApertureAngle() {
        return apertureAngle;
    }

    /**
     * @param apertureAngle the apertureAngle to set
     */
    public void setApertureAngle(SpectrumStack apertureAngle) {
        this.apertureAngle = apertureAngle;
    }

//    /**
//     * Returns the spectrum at a particular coordinate
//     * 
//     * @param index The coordinate
//     * @return A <code>double[]</code> {@link SoftIndexedData}. <code>null</code> if no such
//     *         spectrum.
//     */
//    public SoftIndexedData<double[]> getSpectrumAt(int... index) {
//        SoftIndexedData<double[]> result = null;
//        // if (index != null) {
//        // int[] position = index;
//        // for (SoftIndexedData<double[]> spectrum : spectra) {
//        // int[] origin = spectrum.getOrigin();
//        // if (origin != null) {
//        // if (origin.length > position.length) {
//        // position = new int[origin.length];
//        // System.arraycopy(index, 0, position, 0, index.length);
//        // }
//        // if (ArrayUtils.equals(position, origin)) {
//        // result = spectrum;
//        // break;
//        // }
//        // }
//        // }
//        // }
//        if ((index != null) && (spectra != null)) {
//            if ((stackShape != null) && (index.length != stackShape.length)) {
//                index = Arrays.copyOf(index, stackShape.length);
//            }
//            result = spectra.get(new PositionKey(index));
//        } else if ((spectra == null) && (index != null)) { // CASE SPECTRA IS NOT SETTING (INIT)
//            // TODO SET SPECTRA FROM SPECTRUMSTACK WITH THE POSITON(INDEX)
//            Object spectrum = getSpectrumAtPosition(index);
//
//            SoftIndexedDoubleArray indexedData = getIndexedArrayForSpectrum(spectrum, index);
//            spectra.put(new PositionKey(index), indexedData);
//        }
//        return result;
//    }

    /**
     * Returns the mean spectrum
     * 
     * @return A <code>double[]</code>
     */
    public double[] getMeanSpectrum() {
        double[] result = getSumSpectrum();
        if (spectrumStack != null) {
            int[] stackFullShape = spectrumStack.getStackFullShape();
            if (stackFullShape != null) {
                int[] reducedShape = Arrays.copyOf(stackFullShape, stackFullShape.length - 1);
                int size = 1;

                for (int i = 0; i < reducedShape.length; i++) {
                    size *= stackShape[i];
                }

                for (int i = 0; i < result.length; i++) {
                    result[i] /= size;
                }
            } // end if (stackFullShape != null)
        } // end if (spectrumStack != null)
        return result;
    }

    /**
     * Returns the sum spectrum
     * 
     * @return A <code>double[]</code>
     */
    public double[] getSumSpectrum() {
        double[] sumSpectrum = new double[0];
        if (spectrumStack != null) {
            int[] stackFullShape = spectrumStack.getStackFullShape();
            if (stackFullShape != null) {
                int index = 0;
                int[] reducedShape = Arrays.copyOf(stackFullShape, stackFullShape.length - 1);
                int[] position = new int[stackShape.length];

                // TODO CREATE TASK FOR READING SPECTRUM BY BLOCK
                while (ArrayUtils.isPosition(reducedShape, position)) {
                    Object spectrum = spectrumStack.getSpectrumAt(position);

                    double[] currentSpectrumValues = NumberArrayUtils.extractDoubleArray(spectrum);
                    if (currentSpectrumValues != null) {
                        if (index == 0) {
                            sumSpectrum = currentSpectrumValues;
                        } else {
                            for (int j = 0; j < currentSpectrumValues.length; j++) {
                                sumSpectrum[j] += currentSpectrumValues[j];
                            }

                        }
                        // Go to next position
                        ArrayUtils.incrementPosition(stackShape, position);
                        index++;
                    }
                }
            } // end if (stackFullShape != null)
        } // end if (spectrumStack != null)
        return sumSpectrum;
    }

    public double[] getTotalSpectrum() {
        return totalSpectrum;
    }

    public void setTotalSpectrum(double[] totalSpectrum) {
        this.totalSpectrum = totalSpectrum;
    }

    /**
     * @return the beamEnergy
     */
    public double[] getBeamEnergy() {
        return beamEnergy;
    }

    /**
     * @param beamEnergy the beamEnergy to set
     */
    public void setBeamEnergy(double[] beamEnergy) {
        this.beamEnergy = beamEnergy;
    }

    public ImageStack getImageStack() {
        return images;
    }

    public SpectrumStack getSpectrumStack() {
        return spectrumStack;
    }

    /**
     * Returns the image at a particular coordinate
     * 
     * @param index The coordinate
     * @return A {@link FlatData}. <code>null</code> if no such image.
     */
    public FlatData getImageAt(int... index) {
        FlatData result = null;
        if (images != null) {
            result = images.getImageAt(index);
        }
        return result;
    }

    /**
     * Returns the image block at a particular coordinate
     * 
     * @param index The coordinate
     * @param nbOfImages The number of images inside the block.
     * @return A {@link FlatData}. <code>null</code> if no such image.
     */
    public FlatData[] getImageBlockAt(int nbOfImages, int... index) {
        FlatData[] result = null;
        if (images != null) {
            result = images.getImageBlockAt(nbOfImages, index);
        }
        return result;
    }

    /**
     * Returns the image block at a particular coordinate
     * 
     * @param index The coordinate
     * @param nbOfImages The number of images inside the block.
     * @return A {@link FlatData}. <code>null</code> if no such image.
     */
    public FlatData getImageFlatBlockAt(int nbOfImages, int... index) {
        FlatData result = null;
        if (images != null) {
            result = images.getImageFlatBlockAt(nbOfImages, index);
        }
        return result;
    }

//    public FlatData getSpectraAt(int... index) {
//        FlatData result = null;
//
//        if (spectrumStack != null) {
//            result = spectrumStack.getSpectraAt(index);
//        }
//
//        return result;
//    }

    /**
     * Prepares everything necessary before multiple images reading and returns the number of images that can
     * be read in a block.
     * 
     * @return The number of images that can be read in a block.
     */
    public int prepareImageStackForReading() {
        return images == null ? 0 : images.prepareForReading();
    }

    /**
     * Prepares everything necessary before threaded spectra reading and returns the number of spectra that
     * can be read in a block.
     * 
     * @return The number of spectrums that can be read in a block.
     */
    public int prepareSpectrumStackForReading() {
        return spectrumStack == null ? 0 : spectrumStack.prepareForReading();
    }

    /**
     * Finalizes what has to be finalized after multiple images reading.
     */
    public void finalizeImageStackAfterReading() {
        if (images != null) {
            images.finalizeReading();
        }
    }

    /**
     * Finalizes what has to be finalized after multiple spectra reading.
     */
    public void finalizeSpectrumStackAfterReading() {
        if (spectrumStack != null) {
            spectrumStack.finalizeReading();
        }
    }

    /**
     * Returns whether unsafe (i.e. not thread safe) image reading is enabled.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isUnsafeImageReadingEnabled() {
        return images == null ? false : images.isUnsafeReadingEnabled();
    }

    /**
     * Returns whether unsafe (i.e. not thread safe) spectrum reading is enabled.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isUnsafeSpectrumReadingEnabled() {
        return spectrumStack == null ? false : spectrumStack.isUnsafeReadingEnabled();
    }

    /**
     * Sets whether unsafe (i.e. not thread safe) image reading should be enabled.
     * 
     * @param unsafeReadingEnabled Whether unsafe image reading should be enabled.
     */
    public void setUnsafeImageReadingEnabled(boolean unsafeReadingEnabled) {
        if (images != null) {
            images.setUnsafeReadingEnabled(unsafeReadingEnabled);
        }
    }

    /**
     * Sets whether unsafe (i.e. not thread safe) spectrum reading should be enabled.
     * 
     * @param unsafeReadingEnabled Whether unsafe spectrum reading should be enabled.
     */
    public void setUnsafeSpectrumReadingEnabled(boolean unsafeReadingEnabled) {
        if (spectrumStack != null) {
            spectrumStack.setUnsafeReadingEnabled(unsafeReadingEnabled);
        }
    }

    /**
     * Returns the image ROI at a particular coordinate
     * 
     * @param x the desired ROI x
     * @param y the desired ROI y
     * @param width the desired ROI width
     * @param height the desired ROI height
     * @param index The coordinate
     * @return A {@link FlatData}. <code>null</code> if no such image.
     */
    public FlatData getImageROIAt(int x, int y, int width, int height, int... index) {
        FlatData result = null;
        if (images != null) {
            result = images.getROIAt(x, y, width, height, index);
        }
        return result;
    }

    /**
     * Returns a block of image ROI at a particular coordinate
     * 
     * @param x the desired ROI x
     * @param y the desired ROI y
     * @param width the desired ROI width
     * @param height the desired ROI height
     * @param nbOfImages The number of images in the block
     * @param index The coordinate
     * @return A {@link FlatData} array. <code>null</code> if no such image block.
     */
    public FlatData[] getImageROIBlockAt(int x, int y, int width, int height, int nbOfImages, int... index) {
        FlatData[] result = null;
        if (images != null) {
            result = images.getROIBlockAt(x, y, width, height, nbOfImages, index);
        }
        return result;
    }

    /**
     * Returns a block of image ROI at a particular coordinate
     * 
     * @param x the desired ROI x
     * @param y the desired ROI y
     * @param width the desired ROI width
     * @param height the desired ROI height
     * @param nbOfImages The number of images in the block
     * @param index The coordinate
     * @return A {@link FlatData} array. <code>null</code> if no such image block.
     */
    public FlatData getImageROIFlatBlockAt(int x, int y, int width, int height, int nbOfImages, int... index) {
        FlatData result = null;
        if (images != null) {
            result = images.getROIFlatBlockAt(x, y, width, height, nbOfImages, index);
        }
        return result;
    }

    public Object getSpectrumAt(int... position) {
        Object spectrum = null;
        if (spectrumStack != null) {

            spectrum = spectrumStack.getSpectrumAt(position);
        }

//      if ((index != null) && (spectra != null)) {
//      if ((stackShape != null) && (index.length != stackShape.length)) {
//          index = Arrays.copyOf(index, stackShape.length);
//      }
//      result = spectra.get(new PositionKey(index));
//  }
//  return result;
        return spectrum;
    }

    public FlatData[] getSpectrumBlockAt(int sizeBlock, int... position) {
        FlatData[] spectrum = null;
        // TODO clean code!
//        long startReadingBlock = System.nanoTime();
        if (spectrumStack != null) {
            spectrum = spectrumStack.getSpectrumBlockAt(sizeBlock, position);
        }

//        long duration = System.nanoTime() - startReadingBlock;
//        durationRead += duration;
        // System.out.println("current total duration read is " + durationRead);

//      if ((index != null) && (spectra != null)) {
//      if ((stackShape != null) && (index.length != stackShape.length)) {
//          index = Arrays.copyOf(index, stackShape.length);
//      }
//      result = spectra.get(new PositionKey(index));
//  }
//  return result;
        return spectrum;
    }

    public String getImageNameAt(int... index) {
        String name;
        if (images == null) {
            name = null;
        } else {
            name = images.getImageNameAt(index);
        }
        return name;
    }

    public void setImages(ImageStack images) {
        this.images = images;
    }

    public void setSpectrums(SpectrumStack spectrums) {
        this.spectrumStack = spectrums;
    }

    public int[] getImageSize() {
        int[] result = null;
        if (images != null) {
            result = images.getImageSize();
        }
        return result;
    }

    public FlatData getTotalImage() {
        return totalImage;
    }

    public void setTotalImage(FlatData totalImage) {
        this.totalImage = totalImage;
    }

    /**
     * Returns the position data for a particular dimension
     * 
     * @param dim The {@link Dimension}
     * @return A {@link SoftData}
     */
    public SoftData<?> getPositionData(Dimension dim) {
        return positionMap.get(dim);
    }

    /**
     * Registers a position data for a particular dimension
     * 
     * @param dim The {@link Dimension}
     * @param positionData The position data
     */
    public void registerPositionData(Dimension dim, SoftData<?> positionData) {
        if ((dim != null) && (positionData != null)) {
            positionMap.put(dim, positionData);
        }
    }

    /**
     * Adds a motor in the motor map
     * 
     * @param motor The motor name
     * @param position The motor position
     */
    public void registerMotor(String motor, Double position) {
        if ((motor != null) && (position != null)) {
            motors.put(motor, position);
        }
    }

    /**
     * Returns a copy of the motor map
     * 
     * @return A {@link Map}
     */
    public Map<String, Double> getMotors() {
        Map<String, Double> result = new LinkedHashMap<String, Double>();
        result.putAll(motors);
        return result;
    }

    /**
     * Cleans this {@link ScanData}
     */
    public void clean() {
        if (positionMap != null) {
            positionMap.clear();
        }
        if (motors != null) {
            motors.clear();
        }
        if (parent != null) {
            parent.clear();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        clean();
        super.finalize();
    }

}
