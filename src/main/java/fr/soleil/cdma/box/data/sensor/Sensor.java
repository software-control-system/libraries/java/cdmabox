/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data.sensor;

/**
 * 
 * @author greg
 */
public class Sensor {

    public static final Sensor XIA = new Sensor("XIA", false);
    public static final Sensor SCIENTA = new Sensor("SCIENTA", false);
    public static final Sensor MBS = new Sensor("MBS", false);
    public static final Sensor KEITHLEY = new Sensor("KEITHLEY", true);

    private final boolean scalarOnly;
    private final String name;

    public Sensor(String name, boolean scalarOnly) {
        this.name = name;
        this.scalarOnly = scalarOnly;
    }

    public boolean isScalarOnly() {
        return scalarOnly;
    }

    @Override
    public String toString() {
        return name;
    }

}
