/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data.sensor;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

/**
 * 
 * @author greg
 */
public class ExperimentConfig {

    private Collection<String> detectors;

    public ExperimentConfig() {
        detectors = new LinkedHashSet<>();
    }

    public void addDetector(String detector) {
        if (detector != null) {
            detectors.add(detector);
        }
    }

    public boolean hasDetector(String detector) {
        return ((detector != null) && detectors.contains(detector));
    }

    public Collection<String> getDetectorList() {
        return Collections.unmodifiableCollection(detectors);
    }

}
