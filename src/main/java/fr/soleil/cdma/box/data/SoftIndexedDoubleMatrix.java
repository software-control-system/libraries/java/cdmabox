/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data;

import fr.soleil.cdma.box.reader.ICDMAReader;
import fr.soleil.lib.project.math.NumberMatrixUtils;

/**
 * A {@link SoftIndexedData} specialized in <code>double[][]</code>
 * 
 * @author girardot
 */
public class SoftIndexedDoubleMatrix extends SoftIndexedData<double[][]> {

    public SoftIndexedDoubleMatrix(ContextDataItem contextDataItem, int[] origin, int[] shape, ICDMAReader cdmaReader) {
        super(contextDataItem, origin, shape, cdmaReader);
    }

    public SoftIndexedDoubleMatrix(ContextDataItem contextDataItem, int[] origin, int[] shape, ICDMAReader cdmaReader,
            double[][] data) {
        super(contextDataItem, origin, shape, cdmaReader, data);
    }

    @Override
    protected double[][] recoverData() {
        double[][] result = null;
        ICDMAReader cdmaReader = getReader();
        if (cdmaReader != null) {
            result = NumberMatrixUtils
                    .extractDoubleMatrix(cdmaReader.readCDMComplexArrayWithCtx(contextDataItem, origin, shape));
        }
        return result;
    }

}
