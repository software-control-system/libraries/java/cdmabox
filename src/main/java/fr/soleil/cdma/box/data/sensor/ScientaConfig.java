/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data.sensor;

/**
 * Represents a basic region from the nexus file, as defined for Antares beamline.
 * 
 * @author MAINGUY
 * 
 */
public class ScientaConfig extends PhotoElectronAnalyzerConfig {

    public ScientaConfig(String name) {
        super(Sensor.SCIENTA, name);
    }

}
