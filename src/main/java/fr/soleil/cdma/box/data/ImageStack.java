/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.data;

import java.util.Arrays;

import org.cdma.dictionary.LogicalGroup;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IKey;

import fr.soleil.cdma.box.reader.ICDMAReader;
import fr.soleil.lib.project.data.FlatData;

/**
 * A {@link ReferencedData} that represents an image stack
 * 
 * @author girardot
 */
public class ImageStack extends StackedData {

    private static final String DATA_NAME = "image stack";

    protected IDataItem stackNameItem;

    public ImageStack(LogicalGroup parentGroup, IKey dataItemKey, IDataItem stackNameItem, ICDMAReader cdmaReader,
            int[] stackFullShape) {
        super(parentGroup, dataItemKey, cdmaReader, stackFullShape);
        this.stackNameItem = stackNameItem;
    }

    @Override
    protected int getDataRank() {
        return 2;
    }

    @Override
    protected String getDataName() {
        return DATA_NAME;
    }

    /**
     * Returns the image ROI at given position.
     * 
     * @param x The ROI x coordinate in the image.
     * @param y The ROI y coordinate in the image.
     * @param width The ROI width.
     * @param height The ROI height.
     * @param position The image position in the stack.
     * @return A {@link FlatData}.
     */
    public FlatData getROIAt(int x, int y, int width, int height, int... position) {
        FlatData image = null;
        if ((stackFullShape != null) && (position != null) && (dataShape != null) && (dataShape.length == 2) && (x > -1)
                && (y > -1) && (height > 0) && (width > 0) && (x + width <= dataShape[1])
                && (y + height <= dataShape[0])) {
            if (position.length != stackFullShape.length) {
                position = Arrays.copyOf(position, stackFullShape.length);
            }
            position[position.length - 1] = x;
            position[position.length - 2] = y;
            image = getData(position, new int[] { height, width });
        }
        return image;
    }

    /**
     * Returns a block of image ROIs, starting at given position.
     * 
     * @param x The ROI x coordinate in the image.
     * @param y The ROI y coordinate in the image.
     * @param width The ROI width.
     * @param height The ROI height.
     * @param nbOfImages The number of images in the block.
     * @param position The image position in the stack.
     * @return A {@link FlatData}.
     */
    public FlatData getROIFlatBlockAt(int x, int y, int width, int height, int nbOfImages, int... position) {
        FlatData block = null;
        if ((stackFullShape != null) && (position != null) && (dataShape != null) && (dataShape.length == 2) && (x > -1)
                && (y > -1) && (height > 0) && (width > 0) && (x + width <= dataShape[1])
                && (y + height <= dataShape[0]) && (nbOfImages > 0)) {
            if (position.length != stackFullShape.length) {
                position = Arrays.copyOf(position, stackFullShape.length);
            }
            position[position.length - 1] = x;
            position[position.length - 2] = y;
            block = getData(position, new int[] { nbOfImages, height, width });
        }
        return block;
    }

    /**
     * Returns a block of image ROIs, starting at given position.
     * 
     * @param x The ROI x coordinate in the image.
     * @param y The ROI y coordinate in the image.
     * @param width The ROI width.
     * @param height The ROI height.
     * @param nbOfImages The number of images in the block.
     * @param position The image position in the stack.
     * @return A {@link FlatData} array.
     */
    public FlatData[] getROIBlockAt(int x, int y, int width, int height, int nbOfImages, int... position) {
        FlatData[] block = null;
        if ((stackFullShape != null) && (position != null) && (dataShape != null) && (dataShape.length == 2) && (x > -1)
                && (y > -1) && (height > 0) && (width > 0) && (x + width <= dataShape[1])
                && (y + height <= dataShape[0]) && (nbOfImages > 0)) {
            block = getBlockAt(nbOfImages, new int[] { height, width }, position);
        }
        return block;
    }

    /**
     * Returns the image at given position.
     * 
     * @param position The image position in the stack.
     * @return A {@link FlatData}.
     */
    public FlatData getImageAt(int... position) {
        return getDataAt(position);
    }

    /**
     * Returns a block of images, starting at given position.
     * 
     * @param nbOfImages The number of images in the block.
     * @param position The image position in the stack.
     * @return A {@link FlatData}.
     */
    public FlatData getImageFlatBlockAt(int nbOfImages, int... position) {
        FlatData block = null;
        if ((stackFullShape != null) && (position != null) && (dataShape != null) && (dataShape.length == 2)
                && (nbOfImages > 0)) {
            block = getData(position, new int[] { nbOfImages, dataShape[0], dataShape[1] });
        }
        return block;
    }

    /**
     * Returns a block of images, starting at given position.
     * 
     * @param nbOfImages The number of images in the block.
     * @param position The image position in the stack.
     * @return A {@link FlatData} array.
     */
    public FlatData[] getImageBlockAt(int nbOfImages, int... position) {
        FlatData[] block = null;
        if ((stackFullShape != null) && (position != null) && (dataShape != null) && (dataShape.length == 2)
                && (nbOfImages > 0)) {
            block = getBlockAt(nbOfImages, position, dataShape);
        }
        return block;
    }

    /**
     * Returns the name of the image at given position.
     * 
     * @param position The image position in the stack.
     * @return A {@link String}.
     */
    public String getImageNameAt(int... position) {
        String name = null;
        ICDMAReader cdmaReader = getReader();
        if ((cdmaReader != null) && (stackNameItem != null)) {
            int[] shape = stackNameItem.getShape();
            if (shape != null) {
                shape = shape.clone();
                Arrays.fill(shape, 1);
                if (position.length != shape.length) {
                    position = Arrays.copyOf(position, shape.length);
                }
                try {
                    name = cdmaReader.readString(stackNameItem, position, shape);
                } catch (Exception e) {
                    name = null;
                    e.printStackTrace();
                }
            }
        }
        return name;
    }

    /**
     * Recovers a slice in the full image stack (considered as a point stack), at a given position.
     * 
     * @param position The position, which must be of same length as {@link #getBufferTotalSize()}
     * @param dataFullShape The data full shape, which must be of same length as {@link #getBufferTotalSize()}
     * @param dataReducedShape The data reduced shape, wich must be compatible with <code>dataFullShape</code>
     * @return A {@link FlatData}
     */
    public FlatData getSlice(int[] position, int[] dataFullShape, int[] dataReducedShape) {
        FlatData image = null;
        ICDMAReader cdmaReader = getReader();
        if ((cdmaReader != null) && (contextDataItem != null) && (stackFullShape != null) && (position != null)
                && (position.length == stackFullShape.length) && (dataFullShape != null)
                && (dataFullShape.length == stackFullShape.length)) {
            if (position.length != stackFullShape.length) {
                position = Arrays.copyOf(position, stackFullShape.length);
            }
            image = cdmaReader.readCDMFlatComplexArray(contextDataItem.getParentGroup(),
                    contextDataItem.getDataItemKey(), position, dataFullShape);
            image.updateShape(dataReducedShape);
        }
        return image;
    }

    /**
     * Returns the image stack full shape (including image dimensions).
     * 
     * @return An <code>int[]</code>.
     */
    public int[] getBufferTotalSize() {
        return stackFullShape;
    }

    /**
     * Returns the image dimensions.
     * 
     * @return An <code>int[]</code>.
     */
    public int[] getImageSize() {
        return dataShape;
    }
}
