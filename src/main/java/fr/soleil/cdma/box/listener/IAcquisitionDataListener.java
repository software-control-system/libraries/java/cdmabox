/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.listener;

import java.util.EventListener;

import fr.soleil.cdma.box.event.AcquisitionDataEvent;

/**
 * An {@link EventListener} that listens to {@link AcquisitionDataEvent}s
 * 
 * @author girardot
 */
public interface IAcquisitionDataListener extends EventListener {

    /**
     * Notifies this {@link IAcquisitionDataListener} for some changes in acquisition data
     * 
     * @param event The {@link AcquisitionDataEvent} that describes the chanegs
     */
    public void acquisitionDataChanged(AcquisitionDataEvent event);

}
