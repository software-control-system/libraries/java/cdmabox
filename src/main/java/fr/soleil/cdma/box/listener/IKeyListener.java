package fr.soleil.cdma.box.listener;

import java.util.EventListener;

import fr.soleil.cdma.box.event.KeyEvent;
import fr.soleil.cdma.box.manager.IKeyManager;

public interface IKeyListener extends EventListener {

    /**
     * Notifies this {@link IKeyListener} for some changed in the keys managed by an {@link IKeyManager}
     * 
     * @param event The {@link KeyEvent} that describes the change
     */
    public void keyChanged(KeyEvent event);

}
