/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.source;

import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;

/**
 * An {@link AbstractDataSource} that handles a loading/loaded state
 * 
 * @author girardot
 */
public class LoadingSouce extends AbstractDataSource<Boolean> {

    private volatile boolean loading;

    public LoadingSouce(IKey key) {
        super(key);
        loading = false;
    }

    /**
     * Returns whether this {@link LoadingSouce} is in loading mode
     * 
     * @return A <code>boolean</code> value.
     */
    public boolean isLoading() {
        return loading;
    }

    /**
     * Sets this {@link LoadingSouce} in loading/loaded mode
     * 
     * @param loading A <code>boolean</code> value. <code>TRUE</code> for loading
     */
    public void setLoading(boolean loading) {
        if (this.loading != loading) {
            this.loading = loading;
            notifyMediators();
        }
    }

    @Override
    public Boolean getData() {
        return loading;
    }

    @Override
    public void setData(Boolean data) {
        setLoading(data == null ? false : data.booleanValue());
    }

    @Override
    public GenericDescriptor getDataType() {
        return new GenericDescriptor(Boolean.class);
    }

}
