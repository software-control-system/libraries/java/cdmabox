/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.target.info;

import fr.soleil.cdma.box.target.IBeamCenterTarget;
import fr.soleil.data.target.information.TargetInformation;

/**
 * A {@link TargetInformation} about a full change in beam center coordinates
 * 
 * @author girardot
 */
public class BeamCenterFullInformation extends TargetInformation<double[]> {

    /**
     * Constructor
     * 
     * @param target The concerned {@link IBeamCenterTarget}
     * @param info The new coordinates
     */
    public BeamCenterFullInformation(IBeamCenterTarget target, double[] info) {
        super(target, info);
    }

    @Override
    public IBeamCenterTarget getConcernedTarget() {
        return (IBeamCenterTarget) super.getConcernedTarget();
    }

}
