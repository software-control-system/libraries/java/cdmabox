/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.target;

import fr.soleil.data.target.ITarget;

/**
 * An {@link ITarget} that is supposed to know a beam center position
 * 
 * @author girardot
 */
public interface IBeamCenterTarget extends ITarget {

    /**
     * Returns the beam center X position
     * 
     * @return A <code>float</code>
     */
    public double getCenterX();

    /**
     * Sets the beam center X position
     * 
     * @param centerX The beam center X position to set
     */
    public void setCenterX(double centerX);

    /**
     * Returns the beam center Z position
     * 
     * @return A <code>float</code>
     */
    public double getCenterZ();

    /**
     * Sets the beam center Z position
     * 
     * @param centerZ The beam center Z position to set
     */
    public void setCenterZ(double centerZ);

}
