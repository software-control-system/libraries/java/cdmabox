/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.target;

import fr.soleil.cdma.box.data.sensor.ExperimentConfig;
import fr.soleil.data.target.ITarget;

/**
 * An {@link ITarget} that knows an {@link ExperimentConfig}
 * 
 * @author girardot
 */
public interface IExperimentConfigTarget extends ITarget {

    /**
     * Sets the {@link ExperimentConfig}
     * 
     * @param config The {@link ExperimentConfig} to set
     */
    public void setExperimentConfig(ExperimentConfig config);

}
