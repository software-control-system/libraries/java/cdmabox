/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.target;

import fr.soleil.cdma.box.listener.IAcquisitionDataListener;
import fr.soleil.cdma.box.manager.IAcquisitionDataManager;
import fr.soleil.data.target.ITarget;

public interface IAcquisitionDataTarget extends ITarget, IAcquisitionDataListener {

    public void setAcquisitionDataManager(IAcquisitionDataManager acquisitionDataManager);

}
