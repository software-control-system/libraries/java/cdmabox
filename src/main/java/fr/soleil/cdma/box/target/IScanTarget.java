/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.target;

import fr.soleil.cdma.box.data.scan.ScanData;
import fr.soleil.data.target.ITarget;

/**
 * An {@link ITarget} that knows a {@link ScanData}
 * 
 * @author girardot
 */
public interface IScanTarget extends ITarget {

    /**
     * Sets the {@link ScanData}
     * 
     * @param scan The {@link ScanData} to set
     */
    public void setScan(ScanData scan);

}
