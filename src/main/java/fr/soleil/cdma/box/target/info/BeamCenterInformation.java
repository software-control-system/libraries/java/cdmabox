/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.target.info;

import fr.soleil.cdma.box.target.IBeamCenterTarget;
import fr.soleil.data.target.information.TargetInformation;

/**
 * A {@link TargetInformation} about a change in 1 beam center coordinate
 * 
 * @author girardot
 */
public class BeamCenterInformation extends TargetInformation<Double> {

    private final boolean x;

    /**
     * Constructor
     * 
     * @param target The concerned {@link IBeamCenterTarget}
     * @param info The new coordinate value
     * @param x Whether the coordinate that changed is the X one
     */
    public BeamCenterInformation(IBeamCenterTarget target, Double info, boolean x) {
        super(target, info);
        this.x = x;
    }

    /**
     * Whether the coordinate that changed is the X one
     * 
     * @return A <code>boolean</code>
     */
    public boolean isX() {
        return x;
    }

    @Override
    public IBeamCenterTarget getConcernedTarget() {
        return (IBeamCenterTarget) super.getConcernedTarget();
    }

}
