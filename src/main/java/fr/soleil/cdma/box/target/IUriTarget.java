/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.target;

import java.net.URI;

import fr.soleil.data.target.ITarget;

/**
 * An {@link ITarget} that can receive an {@link URI}
 * 
 * @author girardot
 */
public interface IUriTarget extends ITarget {

    /**
     * Returns this {@link IUriTarget}'s {@link URI}, represented as a {@link String}
     * 
     * @return A {@link String}
     */
    public String getUriToString();

    /**
     * Returns this {@link IUriTarget}'s {@link URI}
     * 
     * @return An {@link URI}
     */
    public URI getUri();

    /**
     * Changes this {@link IUriTarget}'s {@link URI}
     * 
     * @param uri
     */
    public void setUri(URI uri);

}
