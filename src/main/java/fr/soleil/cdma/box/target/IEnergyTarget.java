/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.target;

import fr.soleil.data.target.ITarget;

/**
 * An {@link ITarget} that knows an energy
 * 
 * @author girardot
 */
public interface IEnergyTarget extends ITarget {

    /**
     * Sets the energy
     * 
     * @param energy The energy to set
     */
    public void setEnergy(double energy);

}
