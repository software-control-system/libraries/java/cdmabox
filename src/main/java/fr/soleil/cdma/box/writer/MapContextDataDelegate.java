package fr.soleil.cdma.box.writer;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.cdma.IFactory;
import org.cdma.exception.CDMAException;
import org.cdma.interfaces.IGroup;

import fr.soleil.cdma.box.data.scan.ScanData;
import fr.soleil.cdma.box.manager.IAcquisitionDataManager;
import fr.soleil.cdma.box.util.CDMAConstants;

/**
 * An {@link AContextDataDelegate} that buffers context data in a {@link Map} before writing it in a file.
 * 
 * @author GIRARDOT
 */
public class MapContextDataDelegate extends AContextDataDelegate {

    protected final Map<String, Object> contextMap;

    /**
     * Creates an new {@link MapContextDataDelegate}
     * 
     * @param contextMap The context data {@link Map}
     * @param fullDataAllowed Whether full context data writing is allowed.
     */
    public MapContextDataDelegate(Map<String, Object> contextMap, boolean fullDataAllowed) {
        super(fullDataAllowed);
        this.contextMap = contextMap;
    }

    @Override
    public void setFullDataAllowed(boolean fullDataAllowed) {
        super.setFullDataAllowed(fullDataAllowed);
    }

    /**
     * Buffers a single context information
     * 
     * @param key The key to use in context map
     * @param data The data to buffer
     */
    @Override
    public void bufferData(String key, Object data) {
        if (data != null) {
            contextMap.put(key, data);
        }
    }

    /**
     * Buffers a single context information
     * 
     * @param key The key to use in context map
     * @param data The data to buffer
     */
    @Override
    public void bufferData(String key, String data) {
        if ((data != null) && (!data.trim().isEmpty())) {
            contextMap.put(key, data);
        }
    }

    /**
     * Buffers a single context information
     * 
     * @param key The key to use in context map
     * @param data The data to buffer
     */
    @Override
    public void bufferData(String key, Object[] data) {
        if ((data != null) && (data.length > 0)) {
            contextMap.put(key, data);
        }
    }

    /**
     * Buffers a single context information
     * 
     * @param key The key to use in context map
     * @param data The data to buffer
     */
    @Override
    public void bufferData(String key, double[] data) {
        if ((data != null) && (data.length > 0)) {
            contextMap.put(key, data);
        }
    }

    /**
     * Buffers a single context information
     * 
     * @param key The key to use in context map
     * @param data The data to buffer
     */
    @Override
    public void bufferData(String key, double data) {
        if (!Double.isNaN(data)) {
            contextMap.put(key, Double.valueOf(data));
        }
    }

    @Override
    public void bufferMonitorData(IAcquisitionDataManager manager, int[] position, int[] shape) {
        try {
            super.bufferMonitorData(manager, position, shape);
        } catch (CDMAException e) {
            // Will never happen, as MapContextDataDelegate never throws such an Exception in bufferData methods
        }
    }

    @Override
    public void bufferContext(ScanData scanData, int[] position) {
        try {
            super.bufferContext(scanData, position);
        } catch (CDMAException e) {
            // Will never happen, as MapContextDataDelegate never throws such an Exception in bufferData methods
        }
    }

    /**
     * Writes the previously buffered context in an {@link IGroup}, putting stored camera in an array.
     * 
     * @param factory The {@link IFactory} that knows how to create items, groups and attributes.
     * @param parentGroup The parent {@link IGroup} in which to create the context group.
     * @param scanName The scan name.
     * @param cameraArray The array in which to store camera. It should be an array of length = 1.
     * @return An {@link IGroup}: the {@link IGroup} in which context data was written.
     * @throws CDMAException If a problem occurred.
     */
    public IGroup writeContext(IFactory factory, IGroup parentGroup, String scanName, String[] cameraArray)
            throws CDMAException {
        IGroup contextGroup = createContextGroup(factory, parentGroup);
        Map<String, IGroup> monitorGroups = new HashMap<String, IGroup>();
        String camera = null;
        cameraArray[0] = camera;
        for (Entry<String, Object> entry : contextMap.entrySet()) {
            String key = entry.getKey();
            if (key.startsWith(CDMAConstants.MONITOR_PREFIX)) {
                writeMonitorData(factory, contextGroup, key, entry.getValue(), monitorGroups);
            } else if (key.startsWith(CDMAConstants.CAMERA)) {
                camera = (String) entry.getValue();
            } else if (CDMAConstants.SCAN_NAME.equals(key)) {
                // DATAREDUC-577: scan name is file name
                // However, keep previous value as attribute
                writeScanName(factory, contextGroup, scanName, String.valueOf(entry.getValue()));
            } else {
                factory.createDataItem(contextGroup, key, entry.getValue());
            }
        }
        monitorGroups.clear();
        writeDetectorData(factory, contextGroup, camera);
        return contextGroup;
    }
}
