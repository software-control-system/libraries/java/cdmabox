package fr.soleil.cdma.box.writer;

import java.lang.reflect.Array;
import java.util.Map;

import org.cdma.IFactory;
import org.cdma.exception.CDMAException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IGroup;

import fr.soleil.cdma.box.data.SoftData;
import fr.soleil.cdma.box.data.acquisition.Acquisition;
import fr.soleil.cdma.box.data.scan.ScanData;
import fr.soleil.cdma.box.data.scan.ScanData.Dimension;
import fr.soleil.cdma.box.manager.IAcquisitionDataManager;
import fr.soleil.cdma.box.util.CDMAConstants;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * An abstract {@link IContextDataDelegate}, with common methods already implemented.
 * 
 * @author GIRARDOT
 */
public abstract class AContextDataDelegate implements IContextDataDelegate {

    private boolean fullDataAllowed;

    /**
     * Creates a new {@link AContextDataDelegate}
     * 
     * @param fullDataAllowed Whether full context data writing is allowed.
     * @see #isFullDataAllowed()
     */
    public AContextDataDelegate(boolean fullDataAllowed) {
        this.fullDataAllowed = fullDataAllowed;
    }

    @Override
    public boolean isFullDataAllowed() {
        return fullDataAllowed;
    }

    protected void setFullDataAllowed(boolean fullDataAllowed) {
        this.fullDataAllowed = fullDataAllowed;
    }

    @Override
    public void buffertMonitorData(IAcquisitionDataManager manager) throws CDMAException {
        if ((manager != null) && isFullDataAllowed()) {
            String[] monitors = manager.getIntensityMonitorList();
            if (monitors != null) {
                for (String monitor : monitors) {
                    double[] intensity = manager.getIntensityHistorised(monitor);
                    double gain = manager.getIntensityMonitorGain(monitor);
                    bufferMonitorData(monitor, intensity, gain);
                }
            }
        }
    }

    @Override
    public void bufferMonitorData(IAcquisitionDataManager manager, int[] position, int[] shape) throws CDMAException {
        if ((manager != null) && (position != null) && (shape != null) && (position.length > 0)
                && (position.length == shape.length)) {
            int index = ArrayUtils.getFlatIndex(position, shape);
            if (index > -1) {
                String[] monitors = manager.getIntensityMonitorList();
                if (monitors != null) {
                    for (String monitor : monitors) {
                        double[] intensity = manager.getIntensityHistorised(monitor);
                        double gain = manager.getIntensityMonitorGain(monitor);
                        double intensityValue;
                        if ((intensity != null) && (index < intensity.length)) {
                            intensityValue = intensity[index];
                        } else {
                            intensityValue = Double.NaN;
                        }
                        bufferData(CDMAConstants.MONITOR_PREFIX + monitor, new double[] { intensityValue, gain });
                    }
                }
            }
        } else {
            buffertMonitorData(manager);
        }
    }

    @Override
    public void bufferMonitorData(String monitor, Object intensity, double gain) throws CDMAException {
        if ((monitor != null) && (!monitor.isEmpty()) && (intensity != null)) {
            bufferData(CDMAConstants.MONITOR_PREFIX + monitor, new Object[] { intensity, gain });
        }
    }

    protected void bufferCamera(String camera) throws CDMAException {
        bufferData(CDMAConstants.CAMERA, camera);
    }

    protected boolean isAppliablePosition(int[] position) {
        boolean appliable;
        if (position == null) {
            appliable = isFullDataAllowed();
        } else {
            appliable = (position.length > 0);
        }
        return appliable;
    }

    protected Object extractValue(SoftData<?> data) {
        return data == null ? null : data.getData();
    }

    @Override
    public void bufferContext(ScanData scanData, int[] position) throws CDMAException {
        if ((scanData != null) && isAppliablePosition(position)) {
            IAcquisitionDataManager manager = scanData.getAcquisitionDataManager();
            int[] shape = scanData.getStackShape();
            if ((manager != null) && (shape != null)) {
                bufferData(CDMAConstants.X_BIN, manager.getXBin());
                bufferData(CDMAConstants.Z_BIN, manager.getZBin());
                bufferData(CDMAConstants.DETECTOR_BIAS, manager.getDetectorBias());
                bufferData(CDMAConstants.DETECTOR_BASED_UNCERTAINTY, manager.getDetectorBasedUncertainty());
                bufferData(CDMAConstants.EXPOSURE_TIME, manager.getExposureTime());
                bufferData(CDMAConstants.SHUTTER_CLOSE_DELAY, manager.getShutterCloseDelay());
                bufferData(CDMAConstants.DISTANCE, manager.getDistance());
                bufferData(CDMAConstants.PIXEL_SIZE, manager.getPixelSize());
                bufferData(CDMAConstants.DETECTOR_GAIN, manager.getDetectorGain());
                bufferMonitorData(manager, position, shape);
                bufferData(CDMAConstants.DARK, manager.getDark());
                bufferData(CDMAConstants.ENERGY_SCALE, manager.getEnergyScale());
                bufferData(CDMAConstants.SLICE_SCALE, manager.getSliceScale());
                bufferData(CDMAConstants.X0, manager.getX0());
                bufferData(CDMAConstants.Z0, manager.getZ0());
                bufferData(CDMAConstants.X_POSITIONS, extractValue(scanData.getPositionData(Dimension.X)));
                bufferData(CDMAConstants.Z_POSITIONS, extractValue(scanData.getPositionData(Dimension.Y)));
                bufferData(CDMAConstants.SWEEPS, manager.getSweeps());
                bufferData(CDMAConstants.ACQUISITION_MODE, manager.getAcquisitionMode());
                bufferData(CDMAConstants.LENS_MODE, manager.getLensMode());
                bufferData(CDMAConstants.PASS_ENERGY, manager.getPassEnergy());
                bufferData(CDMAConstants.LOW_ENERGY, manager.getLowEnergy());
                bufferData(CDMAConstants.HIGH_ENERGY, manager.getHighEnergy());
                bufferData(CDMAConstants.ENERGY_STEP, manager.getEnergyStep());
                bufferData(CDMAConstants.STEP_TIME, manager.getStepTime());
                bufferData(CDMAConstants.FIRST_X_CHANNEL, manager.getFirstXChannel());
                bufferData(CDMAConstants.LAST_X_CHANNEL, manager.getLastXChannel());
                bufferData(CDMAConstants.FIRST_Z_CHANNEL, manager.getFirstYChannel());
                bufferData(CDMAConstants.LAST_Z_CHANNEL, manager.getLastYChannel());
                bufferData(CDMAConstants.SLICES, manager.getSlices());
                Object delta = manager.getEncoderDelta();
                if ((delta != null) && (position != null) && (!(delta instanceof Number))) {
                    for (int pos : position) {
                        delta = Array.get(delta, pos);
                    }
                }
                if ((delta instanceof Number) || isFullDataAllowed()) {
                    bufferData(CDMAConstants.ENCODER_DELTA, delta);
                }
                bufferData(CDMAConstants.DELTA_0, manager.getDelta0());
                bufferData(CDMAConstants.GAMMA_0, manager.getGamma0());
                bufferData(CDMAConstants.LAMBDA, manager.getWaveLength());

                bufferData(CDMAConstants.ENERGY, scanData.getEnergy());
                bufferData(CDMAConstants.BEAM_ENERGY, scanData.getBeamEnergy());
                bufferData(CDMAConstants.EXIT_SLITS, scanData.getExitSlits());
                bufferData(CDMAConstants.SCAN_NAME, scanData.getScanName());
                bufferData(CDMAConstants.SCAN_ORIGIN, scanData.getScanOrigin());
                bufferData(CDMAConstants.COMMENTS, scanData.getComments());
                String detector;
                Acquisition acquisition = scanData.getParent();
                if (acquisition == null) {
                    detector = null;
                } else {
                    detector = acquisition.getDetector();
                }
                bufferCamera(detector);
                // TODO other scan context data, like motors and so on
            }
        }
    }

    /**
     * Creates an {@link IGroup} for a given intensity monitor
     * 
     * @param factory The {@link IFactory} that is able to create {@link IGroup}s, {@link IDataItem}s and
     *            {@link IAttribute}s
     * @param contextGroup The parent {@link IGroup}
     * @param monitor The intensity monitor
     * @return An {@link IGroup}
     */
    protected IGroup createMonitorGroup(IFactory factory, IGroup contextGroup, String monitor) {
        int index = monitor.lastIndexOf('/');
        String groupName;
        if (index > -1) {
            groupName = monitor.substring(index + 1);
        } else {
            groupName = monitor;
        }
        IGroup monitorGroup = factory.createGroup(contextGroup, groupName);
        monitorGroup.addStringAttribute(CDMAConstants.ATTRIBUTE_CLASS, CDMAConstants.CLASS_INTENSITY_MONITOR);
        monitorGroup.addStringAttribute(CDMAConstants.ATTRIBUTE_LONG_NAME, monitor);
        return monitorGroup;
    }

    /**
     * Creates an {@link IGroup} in which to write context data
     * 
     * @param factory The {@link IFactory} that is able to create {@link IGroup}s, {@link IDataItem}s and
     *            {@link IAttribute}s
     * @param parentGroup The parent {@link IGroup}
     * @return An {@link IGroup}
     */
    protected IGroup createContextGroup(IFactory factory, IGroup parentGroup) {
        IGroup contextGroup = factory.createGroup(parentGroup, CDMAConstants.NODE_CONTEXT);
        contextGroup.addStringAttribute(CDMAConstants.ATTRIBUTE_CLASS, CDMAConstants.CLASS_INSTRUMENT);
        return contextGroup;
    }

    /**
     * Retrieves the IGroup that correspond to an intensity monitor, creating it when necessary.
     * 
     * @param factory The {@link IFactory} that is able to create {@link IGroup}s, {@link IDataItem}s and
     *            {@link IAttribute}s
     * @param contextGroup The parent {@link IGroup}
     * @param monitor The intensity monitor
     * @param monitorGroups The {@link Map} that stores the created intensity monitor {@link IGroup}s
     * @param monitorGroup The previously found monitor {@link IGroup}, if any.
     * @return The intensity monitor {@link IGroup}
     */
    protected IGroup getMonitorGroup(IFactory factory, IGroup contextGroup, String monitor,
            Map<String, IGroup> monitorGroups, IGroup monitorGroup) {
        if (monitorGroup == null) {
            monitorGroup = createMonitorGroup(factory, contextGroup, monitor);
            monitorGroups.put(monitor, monitorGroup);
        }
        return monitorGroup;
    }

    /**
     * Writes the intensity of an intensity monitor
     * 
     * @param factory The {@link IFactory} that is able to create {@link IGroup}s, {@link IDataItem}s and
     *            {@link IAttribute}s
     * @param contextGroup The parent {@link IGroup}
     * @param monitor The intensity monitor
     * @param monitorGroups The {@link Map} that stores the created intensity monitor {@link IGroup}s
     * @param monitorGroup The previously found monitor {@link IGroup}, if any.
     * @param intensity The intensity to write
     * @return The intensity monitor {@link IGroup}
     * @throws CDMAException If a problem occurred
     */
    protected IGroup writeMonitorIntensity(IFactory factory, IGroup contextGroup, String monitor,
            Map<String, IGroup> monitorGroups, IGroup monitorGroup, double[] intensity) throws CDMAException {
        if ((intensity != null) && (intensity.length > 0)) {
            monitorGroup = getMonitorGroup(factory, contextGroup, monitor, monitorGroups, monitorGroup);
            factory.createDataItem(monitorGroup, CDMAConstants.NODE_INTENSITY, intensity);
        }
        return monitorGroup;
    }

    /**
     * Writes the intensity of an intensity monitor
     * 
     * @param factory The {@link IFactory} that is able to create {@link IGroup}s, {@link IDataItem}s and
     *            {@link IAttribute}s
     * @param contextGroup The parent {@link IGroup}
     * @param monitor The intensity monitor
     * @param monitorGroups The {@link Map} that stores the created intensity monitor {@link IGroup}s
     * @param monitorGroup The previously found monitor {@link IGroup}, if any.
     * @param intensity The intensity to write
     * @return The intensity monitor {@link IGroup}
     * @throws CDMAException If a problem occurred
     */
    protected IGroup writeMonitorIntensity(IFactory factory, IGroup contextGroup, String monitor,
            Map<String, IGroup> monitorGroups, IGroup monitorGroup, double intensity) throws CDMAException {
        if (!Double.isNaN(intensity)) {
            monitorGroup = getMonitorGroup(factory, contextGroup, monitor, monitorGroups, monitorGroup);
            factory.createDataItem(monitorGroup, CDMAConstants.NODE_INTENSITY, Double.valueOf(intensity));
        }
        return monitorGroup;
    }

    /**
     * Writes the gain of an itensity monitor
     * 
     * @param factory The {@link IFactory} that is able to create {@link IGroup}s, {@link IDataItem}s and
     *            {@link IAttribute}s
     * @param contextGroup The parent {@link IGroup}
     * @param monitor The intensity monitor
     * @param monitorGroups The {@link Map} that stores the created intensity monitor {@link IGroup}s
     * @param monitorGroup The previously found monitor {@link IGroup}, if any.
     * @param gain The gain to write
     * @return The intensity monitor {@link IGroup}
     * @throws CDMAException If a problem occurred
     */
    protected IGroup writeMonitorGain(IFactory factory, IGroup contextGroup, String monitor,
            Map<String, IGroup> monitorGroups, IGroup monitorGroup, double gain) throws CDMAException {
        if (!Double.isNaN(gain)) {
            monitorGroup = getMonitorGroup(factory, contextGroup, monitor, monitorGroups, monitorGroup);
            factory.createDataItem(monitorGroup, CDMAConstants.GAIN, Double.valueOf(gain));
        }
        return monitorGroup;
    }

    /**
     * Writes all the data about an intensity monitor
     * 
     * @param factory The {@link IFactory} that is able to create {@link IGroup}s, {@link IDataItem}s and
     *            {@link IAttribute}s
     * @param contextGroup The parent {@link IGroup} in which to write the intensity monitor data
     * @param key The key that identifies the intensity monitor
     * @param value The data to write
     * @param monitorGroups The map that stores the previously created intensity monitor {@link IGroup}s
     * @throws CDMAException If a problem occurred
     */
    protected void writeMonitorData(IFactory factory, IGroup contextGroup, String key, Object value,
            Map<String, IGroup> monitorGroups) throws CDMAException {
        IGroup monitorGroup = null;
        String monitor = key.substring(CDMAConstants.MONITOR_PREFIX.length());
        monitorGroup = monitorGroups.get(monitor);
        double gain;
        if (value instanceof double[]) {
            double[] val = (double[]) value;
            double intensity = val[0];
            gain = val[1];
            monitorGroup = writeMonitorIntensity(factory, contextGroup, monitor, monitorGroups, monitorGroup,
                    intensity);
        } else if (value instanceof Object[]) {
            Object[] val = (Object[]) value;
            double[] intensity = (double[]) val[0];
            monitorGroup = writeMonitorIntensity(factory, contextGroup, monitor, monitorGroups, monitorGroup,
                    intensity);
            Double tmp = (Double) val[1];
            gain = (tmp == null ? Double.NaN : tmp.doubleValue());
        } else {
            gain = Double.NaN;
        }
        monitorGroup = writeMonitorGain(factory, contextGroup, monitor, monitorGroups, monitorGroup, gain);
    }

    /**
     * Writes all the data about a detector
     * 
     * @param factory The {@link IFactory} that is able to create {@link IGroup}s, {@link IDataItem}s and
     *            {@link IAttribute}s
     * @param contextGroup The parent {@link IGroup} in which to write the detector data
     * @param camera The camera
     * @throws CDMAException If a problem occurred
     */
    protected void writeDetectorData(IFactory factory, IGroup contextGroup, String camera) throws CDMAException {
        if (camera != null) {
            IGroup detectorGroup = factory.createGroup(contextGroup, camera);
            detectorGroup.addStringAttribute(CDMAConstants.ATTRIBUTE_CLASS, CDMAConstants.CLASS_DETECTOR);
            IDataItem item = factory.createDataItem(detectorGroup, CDMAConstants.CAMERA, camera);
            item.addStringAttribute(CDMAConstants.ATTRIBUTE_EQUIPMENT, camera);
        }
    }

    /**
     * Writes scan name in context data
     * 
     * @param factory The {@link IFactory} that is able to create {@link IGroup}s, {@link IDataItem}s and
     *            {@link IAttribute}s
     * @param contextGroup The parent {@link IGroup} in which to write the detector data
     * @param scanName The scan name to write
     * @param scanRealName The real scan name, that will be stored in an {@link IAttribute}
     * @throws CDMAException If a problem occurred
     */
    protected void writeScanName(IFactory factory, IGroup contextGroup, String scanName, String scanRealName)
            throws CDMAException {
        // DATAREDUC-577: scan name is file name
        IDataItem scanNameItem = factory.createDataItem(contextGroup, CDMAConstants.SCAN_NAME, scanName);
        if (scanRealName != null) {
            // However, keep previous value as attribute
            scanNameItem.addStringAttribute(CDMAConstants.SCAN_NAME, scanRealName);
        }
    }
}
