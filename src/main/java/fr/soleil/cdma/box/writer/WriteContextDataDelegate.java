package fr.soleil.cdma.box.writer;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.cdma.IFactory;
import org.cdma.exception.CDMAException;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IGroup;

import fr.soleil.cdma.box.util.CDMAConstants;

/**
 * An {@link AContextDataDelegate} that directly writes the data in a file
 * 
 * @author GIRARDOT
 */
public class WriteContextDataDelegate extends AContextDataDelegate {

    protected final IFactory factory;
    protected final IGroup contextGroup;
    protected final String fileName;
    protected final Map<String, IGroup> monitorGroups;

    /**
     * Creates a new {@link WriteContextDataDelegate}
     * 
     * @param fullDataAllowed Whether full context data writing is allowed.
     * @param factory The {@link IFactory} that is able to create {@link IGroup}s, {@link IDataItem}s and
     *            {@link IAttribute}s
     * @param parentGroup The {@link IGroup} that should be the parent of the context data {@link IGroup}
     * @param fileName The name of the file in which context data will be written
     */
    public WriteContextDataDelegate(boolean fullDataAllowed, IFactory factory, IGroup parentGroup, String fileName) {
        super(fullDataAllowed);
        this.monitorGroups = new ConcurrentHashMap<String, IGroup>();
        this.factory = factory;
        this.contextGroup = createContextGroup(factory, parentGroup);
        this.fileName = fileName;
    }

    @Override
    public void bufferData(String key, Object data) throws CDMAException {
        if (data != null) {
            factory.createDataItem(contextGroup, key, factory.createArray(data));
        }
    }

    @Override
    public void bufferData(String key, String data) throws CDMAException {
        if ((data != null) && (data.length() > 0)) {
            if (CDMAConstants.SCAN_NAME.equals(key)) {
                writeScanName(factory, contextGroup, fileName, data);
            } else {
                factory.createDataItem(contextGroup, key, factory.createArray(data));
            }
        }
    }

    @Override
    public void bufferData(String key, Object[] data) throws CDMAException {
        if ((data != null) && (data.length > 0)) {
            if (key.startsWith(CDMAConstants.MONITOR_PREFIX)) {
                writeMonitorData(factory, contextGroup, key, data, monitorGroups);
            } else {
                factory.createDataItem(contextGroup, key, factory.createArray(data));
            }
        }
    }

    @Override
    public void bufferData(String key, double[] data) throws CDMAException {
        if ((data != null) && (data.length > 0)) {
            if (key.startsWith(CDMAConstants.MONITOR_PREFIX)) {
                writeMonitorData(factory, contextGroup, key, data, monitorGroups);
            } else {
                factory.createDataItem(contextGroup, key, factory.createArray(data));
            }
        }
    }

    @Override
    public void bufferData(String key, double data) throws CDMAException {
        if (!Double.isNaN(data)) {
            factory.createDataItem(contextGroup, key, factory.createArray(Double.valueOf(data)));
        }
    }

    @Override
    protected void bufferCamera(String camera) throws CDMAException {
        writeDetectorData(factory, contextGroup, camera);
    }
}
