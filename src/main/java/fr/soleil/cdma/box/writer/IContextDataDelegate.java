package fr.soleil.cdma.box.writer;

import org.cdma.exception.CDMAException;

import fr.soleil.cdma.box.data.scan.ScanData;
import fr.soleil.cdma.box.manager.IAcquisitionDataManager;

/**
 * An interface for something able to buffer context data
 * 
 * @author GIRARDOT
 */
public interface IContextDataDelegate {

    /**
     * Returns whether full context buffering is allowed, or only the one corresponding to a single image.
     * 
     * @return A <code>boolean/code>. <code>true</code> if full context buffering is allowed.
     */
    public boolean isFullDataAllowed();

    /**
     * Buffers a single context information
     * 
     * @param key The key to use in context map
     * @param data The data to buffer
     * @throws CDMAException If a problem occurred
     */
    public void bufferData(String key, Object data) throws CDMAException;

    /**
     * Buffers a single context information
     * 
     * @param key The key to use in context map
     * @param data The data to buffer
     * @throws CDMAException If a problem occurred
     */
    public void bufferData(String key, String data) throws CDMAException;

    /**
     * Buffers a single context information
     * 
     * @param key The key to use in context map
     * @param data The data to buffer
     * @throws CDMAException If a problem occurred
     */
    public void bufferData(String key, Object[] data) throws CDMAException;

    /**
     * Buffers a single context information
     * 
     * @param key The key to use in context map
     * @param data The data to buffer
     * @throws CDMAException If a problem occurred
     */
    public void bufferData(String key, double[] data) throws CDMAException;

    /**
     * Buffers a single context information
     * 
     * @param key The key to use in context map
     * @param data The data to buffer
     * @throws CDMAException If a problem occurred
     */
    public void bufferData(String key, double data) throws CDMAException;

    /**
     * Buffers the intensity monitors information, if {@link #isFullDataAllowed()} returns <code>true</code>.
     * 
     * @param manager The {@link IAcquisitionDataManager} that will recover the information
     * @param position The image position
     * @param shape The image stack shape
     * @throws CDMAException If a problem occurred
     */
    public void buffertMonitorData(IAcquisitionDataManager manager) throws CDMAException;

    /**
     * Buffers the intensity monitors information corresponding to an image
     * 
     * @param manager The {@link IAcquisitionDataManager} that will recover the information
     * @param position The image position
     * @param shape The image stack shape
     * @throws CDMAException If a problem occurred
     */
    public void bufferMonitorData(IAcquisitionDataManager manager, int[] position, int[] shape) throws CDMAException;

    /**
     * Buffers the an intensity monitor information
     * 
     * @param monitor The monitor name
     * @param intensity The monitor intensity
     * @param gain The monitor gain
     * @throws CDMAException If a problem occurred
     */
    public void bufferMonitorData(String monitor, Object intensity, double gain) throws CDMAException;

    /**
     * Buffers the context information about an image of a {@link ScanData}
     * 
     * @param scanData The {@link ScanData}
     * @param position The image position. Can be <code>null</code> is {@link #isFullDataAllowed()} returns
     *            <code>true</code>. In which case, all context data is buffered, not only the data corresponding to an
     *            image.
     * @throws CDMAException If a problem occurred
     */
    public void bufferContext(ScanData scanData, int[] position) throws CDMAException;

}
