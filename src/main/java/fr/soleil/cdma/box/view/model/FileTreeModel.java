/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.view.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

import fr.soleil.cdma.box.listener.DirectoryListener;
import fr.soleil.cdma.box.view.tree.node.FileNode;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.ITarget;
import fr.soleil.lib.project.swing.model.SortTreeModel;

/**
 * A {@link DefaultTreeModel} for file view
 * 
 * @author girardot
 */
public class FileTreeModel extends SortTreeModel implements ITarget {

    private static final long serialVersionUID = 1562495149203313443L;

    private final Map<File, FileNode> nodeMap;
    private final Collection<DirectoryListener> listeners;

    public FileTreeModel(String workingPath) {
        super(new DefaultMutableTreeNode(workingPath));
        nodeMap = new LinkedHashMap<File, FileNode>();
        listeners = Collections.newSetFromMap(new ConcurrentHashMap<DirectoryListener, Boolean>());
    }

    @Override
    public DefaultMutableTreeNode getRoot() {
        return super.getRoot();
    }

    public void addDirectoryListener(DirectoryListener listener) {
        listeners.add(listener);
    }

    public void removeDirectoryListener(DirectoryListener listener) {
        listeners.remove(listener);
    }

    public void fireFilesRemoved(File... files) {
        for (DirectoryListener listener : listeners) {
            listener.filesRemoved(files);
        }
    }

    public void fireFilesAdded(File... files) {
        for (DirectoryListener listener : listeners) {
            listener.filesAdded(files);
        }
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed: no backward communication with mediators
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed: no backward communication with mediators
    }

    public void setFiles(File[] files) {
        synchronized (nodeMap) {
            if ((files == null) || (files.length == 0)) {
                DefaultMutableTreeNode root = getRoot();
                Collection<TreeNode> removedNodes = new ArrayList<TreeNode>();
                for (int i = 0; i < root.getChildCount(); i++) {
                    removedNodes.add(root.getChildAt(i));
                }
                root.removeAllChildren();
                if (!removedNodes.isEmpty()) {
                    int[] indexes = new int[removedNodes.size()];
                    TreeNode[] removedChildren = new TreeNode[removedNodes.size()];
                    int i = 0;
                    for (TreeNode node : removedNodes) {
                        indexes[i] = i;
                        removedChildren[i++] = node;
                    }
                    nodeMap.clear();
                    fireTreeNodesRemoved(this, getPathToRoot(root), indexes, removedChildren);
                }
            } else {
                // first, remove useless nodes
                Collection<File> fileList = Arrays.asList(files);
                Collection<File> toRemove = new ArrayList<File>();
                for (File file : nodeMap.keySet()) {
                    if (!fileList.contains(file)) {
                        toRemove.add(file);
                    }
                }
                removeFiles(toRemove.toArray(new File[toRemove.size()]));
                toRemove.clear();
                // then, add files
                addFiles(files);
            }
        }
    }

    private boolean removeFiles(File... files) {
        boolean removed = false;
        if (files != null) {
            Collection<FileNode> childrenToRemove = new ArrayList<FileNode>();
            for (File toRemove : files) {
                if (toRemove != null) {
                    FileNode node = nodeMap.get(toRemove);
                    if (node != null) {
                        nodeMap.remove(toRemove);
                        childrenToRemove.add(node);
                    }
                }
            }
            fireFilesRemoved(files);
            for (FileNode child : childrenToRemove) {
                if (child != null) {
                    removed = true;
                    if (child.getParent() != null) {
                        removeNodeFromParent(child);
                    }
                }
            }
            childrenToRemove.clear();
        }
        return removed;
    }

    private boolean addFiles(File... files) {
        boolean added = false;
        if (files != null) {
            Collection<File> filesReallyAdded = new ArrayList<File>(files.length);
            for (File file : files) {
                if (file != null) {
                    if (!nodeMap.containsKey(file)) {
                        filesReallyAdded.add(file);
                        added = true;
                        FileNode fileNode = new FileNode(file);
                        nodeMap.put(file, fileNode);
                        DefaultMutableTreeNode rootNode = getRoot();
                        insertNodeInto(fileNode, rootNode, rootNode.getChildCount());
                    }
                }
            }
            if (!filesReallyAdded.isEmpty()) {
                fireFilesAdded(filesReallyAdded.toArray(new File[filesReallyAdded.size()]));
                filesReallyAdded.clear();
            }
        }
        return added;
    }

    public DefaultMutableTreeNode getNodeForFile(File file) {
        DefaultMutableTreeNode node = null;
        if (file != null) {
            synchronized (nodeMap) {
                node = nodeMap.get(file);
            }
        }
        return node;
    }

//    private boolean addFile(File file, int fileIndex) {
//        boolean added = false;
//        if (file != null) {
//            if (!nodeMap.containsKey(file)) {
//                FileNode fileNode = new FileNode(file);
//                nodeMap.put(file, fileNode);
//                DefaultMutableTreeNode rootNode = getRoot();
//                if (fileIndex > rootNode.getChildCount()) {
//                    fileIndex = rootNode.getChildCount();
//                }
//                added = true;
//                insertNodeInto(fileNode, rootNode, fileIndex);
//                fireFilesAdded(file);
//            }
//        }
//        return added;
//    }

}
