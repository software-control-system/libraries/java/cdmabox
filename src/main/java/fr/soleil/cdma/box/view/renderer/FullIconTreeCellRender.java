/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.view.renderer;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 * A {@link DefaultTreeCellRenderer} that gives an easy way to set label icons
 * 
 * @author girardot
 */
public class FullIconTreeCellRender extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = 7376307713189322632L;

    public FullIconTreeCellRender() {
        super();
    }

    /**
     * Sets both icon and disabled icon to a label
     * 
     * @param comp The label
     * @param icon The icon to set
     */
    protected void setComponentIcon(JLabel comp, Icon icon) {
        comp.setIcon(icon);
        comp.setDisabledIcon(icon);
    }

}
