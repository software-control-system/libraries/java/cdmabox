/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.view.tree.node;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * A {@link DefaultMutableTreeNode} that handles a particular type of {@link Object}
 * 
 * @author girardot
 * 
 * @param <T> The type of {@link Object} handled by this {@link ObjectNode}
 */
public abstract class ObjectNode<T> extends AbstractCheckableNode {

    private static final long serialVersionUID = 6218625494954325216L;

    protected T data;

    public ObjectNode() {
        this(null);
    }

    public ObjectNode(T data) {
        super();
        this.data = data;
    }

    /**
     * Returns the {@link Object} handled by this {@link ObjectNode}
     * 
     * @return An {@link Object} of type <code>T</code>
     */
    public T getData() {
        return data;
    }

    /**
     * Sets the {@link Object} this {@link ObjectNode} should handle
     * 
     * @param data The {@link Object} to set
     */
    protected void setData(T data) {
        this.data = data;
    }

    /**
     * Cleans this {@link ObjectNode}
     */
    public void clean() {
        cleanData();
        setUserObject(null);
    }

    /**
     * Cleans this {@link ObjectNode}'s data. By default, sets its handled {@link Object} to <code>null</code>
     */
    protected void cleanData() {
        data = null;
    }

    @Override
    protected void finalize() throws Throwable {
        clean();
        super.finalize();
    }

}
