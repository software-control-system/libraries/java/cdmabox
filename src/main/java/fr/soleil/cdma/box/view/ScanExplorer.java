package fr.soleil.cdma.box.view;

import java.awt.Color;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.slf4j.LoggerFactory;

import fr.soleil.cdma.box.data.acquisition.Acquisition;
import fr.soleil.cdma.box.data.scan.ScanData;
import fr.soleil.cdma.box.target.IAcquisitionTarget;
import fr.soleil.cdma.box.util.CDMABoxUtils;
import fr.soleil.cdma.box.util.DataFormatter;
import fr.soleil.comete.definition.util.AbstractValueConvertor;
import fr.soleil.comete.swing.Slider;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.source.BasicDataSource;
import fr.soleil.lib.project.resource.MessageManager;
import net.miginfocom.swing.MigLayout;

/**
 * This view allow to select an {@link ScanData} of an {@link Acquisition} through a {@link Slider}
 * 
 * @author girardot
 */
public class ScanExplorer extends JPanel implements IAcquisitionTarget, ChangeListener {

    private static final long serialVersionUID = -4277814127198319028L;

    private static final String SLIDER_FORMAT = "%.2f";

    private final JLabel scanLabel;
    private final Slider scanSlider;
    private final JTextField scanTextField;
    private final Dictionary<Integer, JLabel> defaultScanLabelTable;

    private Acquisition acquisition;
    private List<ScanData> copy;
    private final BasicDataSource<ScanData> scanDataSource;
    private final ScanConvertor scanConvertor;
    private final MessageManager messageManager;
    protected final String applicationId;

    public ScanExplorer(String applicationId) {
        this(applicationId, CDMABoxUtils.MESSAGE_MANAGER);
    }

    /**
     * Constructor
     */
    public ScanExplorer(String applicationId, MessageManager messageManager) {
        super(new MigLayout());
        this.applicationId = (applicationId == null ? Mediator.LOGGER_ACCESS : applicationId);
        this.messageManager = messageManager;
        scanDataSource = new BasicDataSource<ScanData>(null);
        scanDataSource.setIgnoreDuplicationTest(true);

        defaultScanLabelTable = new Hashtable<Integer, JLabel>();
        defaultScanLabelTable.put(0, new JLabel("Unavailable"));

        scanLabel = new JLabel();
        scanConvertor = new ScanConvertor();
        scanSlider = generateSlider();
        cleanScanSlider();
        scanTextField = new JTextField(9);
        scanTextField.setEditable(false);
        setScanTitle(null);

        add(scanLabel, "shrink");
        add(scanSlider, "growx, push");
        add(scanTextField, "wmin 50");
    }

    public void setGlobalBackground(Color bg) {
        setBackground(bg);
        scanSlider.setBackground(bg);
        scanTextField.setBackground(bg);
    }

    /**
     * Returns the {@link AbstractDataSource} that knows the selected {@link ScanData}
     * 
     * @return An {@link AbstractDataSource}
     */
    public AbstractDataSource<ScanData> getScanDataSource() {
        return scanDataSource;
    }

    @Override
    public synchronized void setAcquisition(Acquisition acquisition) {
        this.acquisition = acquisition;
        copy = (acquisition == null ? null : acquisition.getScanDataList());
        refresh();
    }

    /**
     * Creates a correctly initialized {@link Slider}
     * 
     * @return A {@link Slider}
     */
    protected Slider generateSlider() {
        Slider slider = new Slider();
        slider.setAutoScaleOnMinorTicksToo(false);
        slider.setPreferedMajorTickColor(Color.BLACK);
        slider.setMinorTickSpacing(1);
        slider.setMajorTickSpacing(1);
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        slider.setSnapToTicks(true);
        return slider;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public synchronized void stateChanged(ChangeEvent e) {
        if ((e != null) && (e.getSource() == scanSlider) && (!scanSlider.isEditingData())) {
            updateScan();
        }
    }

    public void updateScan() {
        int index = scanSlider.getValue();
        ScanData scanData = null;
        if (acquisition == null) {
            cleanScanSlider();
        } else {
            if ((copy != null) && (index < copy.size())) {
                scanData = copy.get(index);
            }
            if (scanData == null) {
                setScanText("");
            } else {
                if (Double.isNaN(scanData.getEnergy())) {
                    setScanText(scanData.getScanName());
                } else {
                    setScanText(DataFormatter.getNumberAsString(scanData.getEnergy(), 5));
                }
            }
        }
        try {
            scanDataSource.setData(scanData);
        } catch (Exception e) {
            LoggerFactory.getLogger(applicationId).error("Could not set scanDataSource", e);
        }
    }

    /**
     * Sets the "scan" text and reset caret position in energy textfield
     * 
     * @param text The text to set
     */
    private void setScanText(String text) {
        if ((scanTextField != null) && (text != null)) {
            scanTextField.setText(text);
            scanTextField.setToolTipText(text);
            scanTextField.setCaretPosition(0);
        }
    }

    public String getScanTitle() {
        return scanLabel.getText();
    }

    public void setScanTitle(String title) {
        if (title == null) {
            scanLabel.setText(messageManager.getMessage("fr.soleil.cdma.box.view.ScanExplorer.title"));
        } else {
            scanLabel.setText(title);
        }
    }

    /**
     * Updates the energy slider, so that it is adapted to last set acquisition
     */
    private void refresh() {
        if (acquisition != null) {
            if ((copy != null) && (!copy.isEmpty())) {
                scanSlider.removeChangeListener(this);
                Dictionary<?, ?> labels;
                if (acquisition.isEnergyAcquisition()) {
                    scanSlider.setFormat(SLIDER_FORMAT);
                    scanSlider.setValueConvertor(scanConvertor);
                    labels = null;
                } else {
                    scanSlider.setFormat(null);
                    scanSlider.setValueConvertor(null);
                    Dictionary<Integer, JLabel> tmp = new Hashtable<Integer, JLabel>();
                    for (int i = 0; i < copy.size(); i++) {
                        ScanData scan = copy.get(i);
                        JLabel scanLabel = new JLabel(Integer.toString(i));
                        scanLabel.setToolTipText(scan.getScanName());
                        scanLabel.setHorizontalAlignment(JLabel.CENTER);
                        scanLabel.setVerticalAlignment(JLabel.CENTER);
                        tmp.put(i, scanLabel);
                    }
                    labels = tmp;
                }
                scanSlider.setToolTipText(null);
                scanSlider.setEnabled(true);
                scanSlider.setMinimum(0);
                scanSlider.setMaximum(copy.size() - 1);
                scanSlider.setValue(0);
                scanSlider.setLabelTable(labels);
                scanSlider.setPaintLabels(true);
                scanSlider.addChangeListener(this);
            }
        }
        scanConvertor.revalidate();
        updateScan();
    }

    /**
     * Cleans the scan slider, as if no {@link ScanData} is available
     */
    private void cleanScanSlider() {
        scanSlider.removeChangeListener(this);
        scanSlider.setFormat(null);
        scanSlider.setValueConvertor(null);
        setScanText("Unavailable");
        scanSlider.setEnabled(false);
        scanSlider.setValue(0);
        scanSlider.setMinimum(0);
        scanSlider.setMaximum(0);
        scanSlider.setLabelTable(defaultScanLabelTable);
        setScanText(scanSlider.getToolTipText());
        scanSlider.addChangeListener(this);
    }

    protected class ScanConvertor extends AbstractValueConvertor {

        @Override
        public double convertValue(double value) {
            double result = value;
            int index = (int) value;
            if (isValid() && (index > -1) && (index < copy.size())) {
                result = copy.get(index).getEnergy();
            }
            return result;
        }

        @Override
        public boolean isValid() {
            return ((acquisition != null) && (acquisition.isEnergyAcquisition()) && (copy != null)
                    && (!copy.isEmpty()));
        }

        public void revalidate() {
            warnListeners();
        }

    }

}
