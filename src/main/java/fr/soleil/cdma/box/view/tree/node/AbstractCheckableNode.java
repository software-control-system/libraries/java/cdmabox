/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.view.tree.node;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * A {@link DefaultMutableTreeNode} that cn be checked
 * 
 * @author girardot
 */
public abstract class AbstractCheckableNode extends DefaultMutableTreeNode {

    private static final long serialVersionUID = -1122619215436770486L;

    protected boolean checked;

    public AbstractCheckableNode() {
        super();
        initChecked();
    }

    public AbstractCheckableNode(Object userObject) {
        super(userObject);
        initChecked();
    }

    public AbstractCheckableNode(Object userObject, boolean allowsChildren) {
        super(userObject, allowsChildren);
        initChecked();
    }

    protected void initChecked() {
        checked = false;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

}
