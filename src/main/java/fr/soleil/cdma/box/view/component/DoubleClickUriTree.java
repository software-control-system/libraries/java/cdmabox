/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.view.component;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.slf4j.LoggerFactory;

import fr.soleil.cdma.box.util.CDMABoxUtils;
import fr.soleil.comete.box.matrixbox.TreeNodeMatrixBox;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.definition.event.TreeNodeEvent;
import fr.soleil.comete.definition.event.TreeNodeEvent.NodeEventReason;
import fr.soleil.comete.definition.listener.ITreeNodeListener;
import fr.soleil.comete.definition.widget.util.BasicTreeNode;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.Tree;
import fr.soleil.comete.swing.util.CometeTreeModel;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.ITextTransferable;

/**
 * A {@link Tree} that contains an {@link URI} {@link AbstractDataSource} and searches for children {@link URI}s on
 * double click. The {@link DoubleClickUriTree} also contains a {@link CometeImage} {@link AbstractDataSource}, used to
 * associate {@link URI}s with icons when available, and a {@link String} {@link AbstractDataSource}, used to associate
 * {@link URI}s with short names when available
 * 
 * @author girardot
 */
public abstract class DoubleClickUriTree extends Tree implements ITreeNodeListener, ITextTransferable {

    private static final long serialVersionUID = 5247579750450411705L;

    protected static final String DEFAULT_URI_SELECTION_ERROR_MESSAGE = "URI selection error";
    protected static final String FILE = "file";
    protected static final String SLASH = "/";

    protected final AbstractDataSource<URI> uriSource;
    protected final AbstractDataSource<CometeImage> imageSource;
    protected final AbstractDataSource<String> nameSource;
    protected final String applicationId;
    protected final TreeNodeMatrixBox treeNodeMatrixBox;
    protected CDMAKeyFactory cdmaKeyFactory;
    private volatile boolean clickDone;
    private String uriSelectionErrorMesage;
    protected volatile URI lastConnectedURI;
    protected int refreshingPeriod;

    /**
     * Constructs a new {@link DoubleClickUriTree} with a given {@link URI} {@link AbstractDataSource}
     * 
     * @param applicationId The application id
     * @param uriSource The {@link URI} {@link AbstractDataSource}
     * @param imageSource The {@link CometeImage} {@link AbstractDataSource}
     * @param nameSource The uri name {@link AbstractDataSource}
     * @param cdmaKeyFactory The {@link CDMAKeyFactory} that knows to which {@link IKey} to connect each
     *            {@link ITreeNode}
     */
    public DoubleClickUriTree(String applicationId, AbstractDataSource<URI> uriSource,
            AbstractDataSource<CometeImage> imageSource, AbstractDataSource<String> nameSource,
            CDMAKeyFactory cdmaKeyFactory) {
        this(applicationId, null, uriSource, imageSource, nameSource, cdmaKeyFactory);
    }

    /**
     * Constructs a new {@link DoubleClickUriTree} with a given {@link CometeTreeModel} and a given {@link URI}
     * {@link AbstractDataSource}
     * 
     * @param applicationId The application id
     * @param model The {@link CometeTreeModel} to use
     * @param uriSource The {@link URI} {@link AbstractDataSource}
     * @param imageSource The {@link CometeImage} {@link AbstractDataSource}
     * @param nameSource The uri name {@link AbstractDataSource}
     * @param cdmaKeyFactory The {@link CDMAKeyFactory} that knows to which {@link IKey} to connect each
     *            {@link ITreeNode}
     */
    public DoubleClickUriTree(String applicationId, CometeTreeModel model, AbstractDataSource<URI> uriSource,
            AbstractDataSource<CometeImage> imageSource, AbstractDataSource<String> nameSource,
            CDMAKeyFactory cdmaKeyFactory) {
        super(model);
        refreshingPeriod = 1000;
        this.applicationId = (applicationId == null ? Mediator.LOGGER_ACCESS : applicationId);
        clickDone = false;
        this.uriSelectionErrorMesage = DEFAULT_URI_SELECTION_ERROR_MESSAGE;
        this.uriSource = uriSource;
        this.imageSource = imageSource;
        this.nameSource = nameSource;
        this.cdmaKeyFactory = cdmaKeyFactory;
        this.treeNodeMatrixBox = new TreeNodeMatrixBox();
        // TextTansfertHandler.registerTransferHandler(this);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                mouseChanged(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                mouseChanged(e);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                mouseChanged(e);
            }
        });
    }

    @Override
    public String getTextToTransfert() {
        String textToTransfert = null;
        List<URI> selectedURI = getSelectedURIs();
        if ((selectedURI != null) && (!selectedURI.isEmpty())) {
            StringBuilder builder = new StringBuilder();
            for (URI uri : selectedURI) {
                builder.append("\t").append(uri);
            }
            textToTransfert = builder.toString();
        }
        return textToTransfert;
    }

    /**
     * Returns the refreshing period
     * 
     * @return the refreshingPeriod
     */
    public int getRefreshingPeriod() {
        return refreshingPeriod;
    }

    /**
     * Sets the refreshing period
     * 
     * @param refreshingPeriod the refreshingPeriod to set
     */
    public void setRefreshingPeriod(int refreshingPeriod) {
        this.refreshingPeriod = refreshingPeriod;
    }

    /**
     * Returns the message displayed in case of uri selection error
     * 
     * @return A {@link String}
     */
    public String getUriSelectionErrorMesage() {
        return uriSelectionErrorMesage;
    }

    /**
     * Sets the message to display in case of uri selection error
     * 
     * @param uriSelectionErrorMesage The message to display in case of uri selection error. If <code>null</code>,
     *            default one is used
     */
    public void setUriSelectionErrorMesage(String uriSelectionErrorMesage) {
        if (uriSelectionErrorMesage == null) {
            this.uriSelectionErrorMesage = DEFAULT_URI_SELECTION_ERROR_MESSAGE;
        } else {
            this.uriSelectionErrorMesage = uriSelectionErrorMesage;
        }
    }

    /**
     * Filters whether to treat {@link MouseEvent} as double click
     * 
     * @param e The {@link MouseEvent}
     */
    private void mouseChanged(MouseEvent e) {
        if ((e != null) && (e.getSource() == this) && (e.getClickCount() == 2)) {
            // Limit events to avoid UI bugs
            if (!clickDone) {
                e.consume();
                treatDoubleClick();
            }
            clickDone = true;
        } else {
            clickDone = false;
        }
    }

    public void connectSelectedNodes() {
        connectNodes(getSelectedNodes());
    }

    protected void connectNodes(final ITreeNode... nodes) {
        if ((nodes != null) && (nodes.length > 0) && (nodes[0].getData() instanceof URI)
                && (nodes[0].getChildren().isEmpty())) {
            final IKey key = cdmaKeyFactory.generateKeyListValidURI((URI) nodes[0].getData());
            prepareKey(key);
            if (key != null) {
                SwingWorker<Boolean, Void> updateWorker = new SwingWorker<Boolean, Void>() {
                    private ITreeNode refNode = null;

                    @Override
                    protected Boolean doInBackground() throws Exception {
                        boolean isExperiment;
                        ITreeNode node = nodes[0];
                        if (!node.isTreeNodeListenerRegistered(DoubleClickUriTree.this)) {
                            node.addTreeNodeListener(DoubleClickUriTree.this);
                        }
                        // search for children only when not an experiment
                        URI uri = (URI) node.getData();
                        try {
                            isExperiment = CDMABoxUtils.isExperiment(uri);
                        } catch (Exception e) {
                            isExperiment = false;
                        }
                        refNode = node;
                        if (!isExperiment) {
                            treeNodeMatrixBox.setSynchronTransmission(node, true);
                            treeNodeMatrixBox.connectWidget(node, key, true, true, true);
                            updateRefreshingStrategy(key);
                        }
                        return isExperiment;
                    }

                    @Override
                    protected void done() {
                        try {
                            final Boolean isExperiment = get();
                            TreePath path = getSelectionPath();
                            expandPath(path);
                            if (isExperiment.booleanValue()) {
                                sendURI(refNode);
                            }
                        } catch (Exception e) {
                            if (!(e instanceof InterruptedException)) {
                                Throwable error = e;
                                if (e instanceof ExecutionException) {
                                    error = e.getCause();
                                }
                                LoggerFactory.getLogger(applicationId).error(getUriSelectionErrorMesage(), error);
                            }
                        } finally {
                            refNode = null;
                        }
                    }
                };
                updateWorker.execute();
            }
        }
    }

    protected abstract void prepareKey(IKey key);

    protected final void updateRefreshingStrategy(IKey key) {
        if (key != null) {
            String id = key.getSourceProduction();
            IDataSourceProducer producer = DataSourceProducerProvider.getProducer(id);
            if (producer instanceof AbstractRefreshingManager<?>) {
                applyRefreshingStrategy(key, (AbstractRefreshingManager<?>) producer);
            }
        }
    }

    protected void applyRefreshingStrategy(IKey key, AbstractRefreshingManager<?> manager) {
        manager.setRefreshingStrategy(key, new PolledRefreshingStrategy(refreshingPeriod));
    }

    /**
     * Action to be done on double click
     */
    protected void treatDoubleClick() {
        connectSelectedNodes();
    }

    /**
     * Connects uri and name, returning whether it was done
     * 
     * @param uri The uri to connect
     * @param name The name to connect
     * @param allowDuplicateUri Whether to allow connecting the same (strict equality) uri twice
     * @return Whether the connection was done
     */
    protected boolean updateUriAndName(URI uri, String name, boolean allowDuplicateUri) {
        boolean connected = false;
        try {
            if (allowDuplicateUri || (uri != uriSource.getData())) {
                uriSource.setData(uri);
                try {
                    if (name != null) {
                        nameSource.setData(name);
                        lastConnectedURI = uri;
                        connected = true;
                    }
                } catch (Exception e) {
                    // Nothing to do: ignore this exception, because not that important
                }
            }
        } catch (Exception e) {
            LoggerFactory.getLogger(applicationId).error(getClass().getName() + " failed to update URI to " + uri, e);
        }
        return connected;
    }

    /**
     * Sends an {@link ITreeNode}'s URI and name to associated sources.
     * 
     * @param treeNode The {@link ITreeNode}.
     */
    protected void sendURI(final ITreeNode treeNode) {
        if ((uriSource != null) && (imageSource != null) && (treeNode != null) && (treeNode.getData() instanceof URI)) {
            URI uri = (URI) treeNode.getData();
            if (updateUriAndName(uri, treeNode.getName(), false)) {
                try {
                    imageSource.setData(treeNode.getImage());
                } catch (Exception e) {
                    // Nothing to do: ignore this exception, because not that important
                }
            }
        }
    }

    /**
     * Connect the given {@link URI} {@link AbstractDataSource} if it's an experiment.
     */
    public void updateURI(URI uri) {
        updateURI(uri, true);
    }

    public void updateURI(URI uri, boolean testForFragment) {
        if ((uriSource != null) && (uri != null)) {
            String name = null;
            // XXX ugly name recovery. Should have a common CDMA method
            if (testForFragment) {
                if (uri.getFragment() != null) {
                    name = uri.getFragment().replaceFirst(SLASH, ObjectUtils.EMPTY_STRING);
                }
            } else {
                if (uri.getFragment() == null) {
                    name = uri.toString();
                    if (name.startsWith(FILE)) {
                        if (name.endsWith(SLASH)) {
                            name = name.substring(0, name.length() - 1);
                        }
                        int index = name.lastIndexOf('/');
                        if (index > -1) {
                            name = name.substring(index + 1);
                        }
                    }
                } else {
                    name = uri.getFragment().replaceFirst(SLASH, ObjectUtils.EMPTY_STRING);
                }
            }
            updateUriAndName(uri, name, true);
        }
    }

    @Override
    public void setRootNode(ITreeNode rootNode) {
        ITreeNode previousRoot = getRootNode();
        if (!ObjectUtils.sameObject(rootNode, this.rootNode)) {
            deepDisconnect(previousRoot);
            super.setRootNode(rootNode);
            connectRootLater();
        }
    }

    public void reloadStructure() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ITreeNode root = rootNode;
                if (root != null) {
                    BasicTreeNode newRoot = new BasicTreeNode();
                    newRoot.setImage(root.getImage());
                    newRoot.setData(root.getData());
                    newRoot.setName(root.getName());
                    deepDisconnect(root, true);
                    setRootNode(null);
                    setRootNode(newRoot);
                }
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    protected void deepDisconnect(ITreeNode node) {
//        deepDisconnect(node, false);
        deepDisconnect(node, true);
    }

    protected void deepDisconnect(ITreeNode node, boolean clean) {
        if (node != null) {
            CometeTreeModel model = getModel();
            node.removeTreeNodeListener(this);
            node.removeTreeNodeListener(model);
            List<ITreeNode> children = node.getChildren();
            if (children != null) {
                children = new ArrayList<>(children);
            }
            treeNodeMatrixBox.disconnectWidgetFromAll(node);
            if (children != null) {
                for (ITreeNode child : children) {
                    deepDisconnect(child, clean);
                }
                children.clear();
            }
            if (clean) {
                // clean option is necessary for reload button (DATAREDUC-802)
                if (model != null) {
                    model.removeTreeNodes(node);
                }
            }
        }
    }

    /**
     * Adds a {@link TreePath} in a {@link List} if it corresponds to an {@link URI}, or looks for the children to add.
     * 
     * @param selectedPath the {@link TreePath}
     * @param pathList the {@link List}
     */
    protected void addUriTreePaths(TreePath selectedPath, List<TreePath> pathList) {
        CometeTreeModel model = getModel();
        if (model != null) {
            if (selectedPath.getLastPathComponent() instanceof DefaultMutableTreeNode) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) selectedPath.getLastPathComponent();
                ITreeNode treeNode = model.getTreeNode(node);
                if ((treeNode != null) && (treeNode.getData() instanceof URI)) {
                    if (!pathList.contains(selectedPath)) {
                        // the path can be added
                        pathList.add(selectedPath);
                    }
                } else {
                    for (int i = 0; i < node.getChildCount(); i++) {
                        if (node.getChildAt(i) instanceof DefaultMutableTreeNode) {
                            DefaultMutableTreeNode child = (DefaultMutableTreeNode) node.getChildAt(i);
                            TreePath childPath = new TreePath(child.getPath());
                            addUriTreePaths(childPath, pathList);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void nodeChanged(TreeNodeEvent event) {
        if ((event != null) && (event.getReason() == NodeEventReason.REMOVED)) {
            ITreeNode[] children = event.getChildren();
            if (children != null) {
                for (ITreeNode node : children) {
                    deepDisconnect(node);
                }
            }
        }
    }

    /**
     * Returns this {@link DoubleClickUriTree}'s associated {@link URI} {@link AbstractDataSource}
     * 
     * @return An {@link AbstractDataSource}
     */
    public AbstractDataSource<URI> getUriSource() {
        return uriSource;
    }

    /**
     * Returns this {@link DoubleClickUriTree}'s associated {@link CometeImage} {@link AbstractDataSource}
     * 
     * @return An {@link AbstractDataSource}
     */
    public AbstractDataSource<CometeImage> getImageSource() {
        return imageSource;
    }

    @Override
    public void treeDidChange() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                DoubleClickUriTree.super.treeDidChange();
                if (getRowCount() > 0) {
                    expandPath(getPathForRow(0));
                }
            }
        });
    }

    protected void connectRootLater() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                connectRoot();
            }
        });
    }

    protected void connectRoot() {
        CometeTreeModel model = getModel();
        if (model instanceof DefaultTreeModel) {
            final DefaultTreeModel treeModel = (DefaultTreeModel) model;
            treeModel.reload();
            DefaultMutableTreeNode root = model.getRoot();
            if (root != null) {
                setSelectionPath(new TreePath(root.getPath()));
                connectSelectedNodes();
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        treeModel.reload();
                        expandAll(true);
                    }
                });
            }
        }
    }

    public Collection<URI> getSelectedExperimentURIs(boolean searchForChildrenExperiments,
            MessageManager messageManager) {
        return getSelectedExperimentURIs(searchForChildrenExperiments, false, messageManager);
    }

    public Collection<URI> getSelectedExperimentURIs(boolean searchForChildrenExperiments, boolean byTreeOrder,
            MessageManager messageManager) {
        Collection<URI> result;
        Collection<URI> selected = getSelectedURIs(byTreeOrder);
        if (selected == null) {
            result = null;
        } else {
            result = new LinkedHashSet<URI>();
            for (URI uri : selected) {
                try {
                    CDMABoxUtils.addExperimentUri(uri, result, searchForChildrenExperiments, cdmaKeyFactory);
                } catch (Exception e) {
                    LoggerFactory.getLogger(applicationId)
                            .trace(messageManager.getMessage("fr.soleil.cdma.box.uri.recovery.error"), e);
                    continue;
                }
            }
        }
        return result;
    }

    public List<URI> getSelectedURIs() {
        return getSelectedURIs(false);
    }

    public List<URI> getSelectedURIs(boolean sortByTreeOrder) {
        List<URI> result = new ArrayList<URI>();
        ITreeNode[] selectedNodes = getSelectedNodes(sortByTreeOrder);
        if (selectedNodes != null) {
            CometeTreeModel model = getModel();
            DefaultMutableTreeNode root = model.getRoot();
            for (ITreeNode treeNode : selectedNodes) {
                if (model.recoverNode(treeNode) != root) {
                    URI data = (URI) treeNode.getData();
                    result.add(data);
                }
            }
        }
        return result;
    }

    public URI getLastConnectedURI() {
        return lastConnectedURI;
    }

}
