package fr.soleil.cdma.box.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.cdma.Factory;

import fr.soleil.cdma.box.action.ExpandAllAction;
import fr.soleil.cdma.box.action.ReloadAction;
import fr.soleil.cdma.box.action.ReloadStructureAction;
import fr.soleil.cdma.box.controller.BasicUriController;
import fr.soleil.cdma.box.reader.ICDMAReader;
import fr.soleil.cdma.box.target.IUriTarget;
import fr.soleil.cdma.box.util.GUIUtilities;
import fr.soleil.cdma.box.util.comparator.UriCreationDateComparator;
import fr.soleil.cdma.box.util.comparator.UriLastModificationDateComparator;
import fr.soleil.cdma.box.util.comparator.UriNameComparator;
import fr.soleil.cdma.box.view.component.BrowseTree;
import fr.soleil.cdma.box.view.component.DoubleClickUriTree;
import fr.soleil.cdma.box.view.component.ExperimentTree;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.definition.data.controller.CometeImageController;
import fr.soleil.comete.definition.widget.util.BasicTreeNode;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.util.CometeTreeModel;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.swing.util.DefaultCometeTreeModel;
import fr.soleil.data.controller.BasicTextTargetController;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.source.BasicDataSource;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.SystemUtils;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.panel.LoadingPanel;

/**
 * A {@link JPanel} that manages experiment URI selection
 * 
 * @author girardot
 */
public class UriView extends JPanel implements IBooleanTarget {

    private static final long serialVersionUID = 3388378584815565775L;

    private static final String DEFAULT_PACKAGE = "fr.soleil.cdma.box";

    public static final Font URI_FONT = new Font(Font.DIALOG, Font.PLAIN | Font.ITALIC, 12);
    public static final Insets GAP = new Insets(5, 2, 2, 2);

    private final BasicDataSource<URI> uriSource;
    private final BasicDataSource<CometeImage> imageSource;
    private final BasicDataSource<String> nameSource;
    private final ExperimentTree experimentTree;
    private final JScrollPane experimentScrollPane;
    private final Component experimentTreeToolPanel;
    private final LoadingPanel<JScrollPane> experimentLoadingPanel;
    private final BrowseTree browseTree;
    private final JScrollPane browseScrollPane;
    private UriSelectionMode selectionMode;
    private JDialog browseDialog;
    private final JPanel browsePanel;
    private final JTextField topUriField;
    private final JButton topUriFileChooserButton;
    private final JButton repaintButton;
    private final JButton reloadButton;
    private final JButton expandButton;
    private final JButton collapseButton;
    private final JPanel topUriPanel;
    private final JButton pathChangerButton;
    private final Label uriLabel;
    private final TextField uriField;
    private final JLabel uriTitleLabel;
    private final JPanel uriPanel;
    private final JPanel sortPanel;
    private final JLabel sortLabel;
    private final JComboBox<Object> sortComboBox;

    private final MessageManager messageManager;
    private final String uriErrorMessage;

    private URI topUri;
    private String topUriText;
    private final UriListener uriListener;
    private final ToolTipUpdater toolTipUpdater;
    private final CDMAKeyFactory cdmaKeyFactory;
    private final BasicUriController uriController;
    private final CometeImageController imageController;
    private final BasicTextTargetController nameController;
    private final String repaintMessage;
    private final String reloadMessage;
    private final String expandMessage;
    private final String collapseMessage;

    public UriView(String applicationId) {
        this(applicationId, null, null, null, null, null);
    }

    public UriView(String applicationId, MessageManager messageManager) {
        this(applicationId, null, null, null, null, messageManager);
    }

    public UriView(String applicationId, CDMAKeyFactory cdmaKeyFactory, BasicUriController uriController,
            CometeImageController imageController, BasicTextTargetController nameController,
            MessageManager messageManager) {
        super(new BorderLayout(5, 5));
        this.messageManager = (messageManager == null ? new MessageManager(DEFAULT_PACKAGE) : messageManager);
        if ((messageManager != null) && (!DEFAULT_PACKAGE.equals(messageManager.getResourcePackage()))) {
            messageManager.pushPropertiesIfAbsent(new MessageManager(DEFAULT_PACKAGE));
        }
        this.cdmaKeyFactory = (cdmaKeyFactory == null ? new CDMAKeyFactory() : cdmaKeyFactory);
        uriErrorMessage = this.messageManager.getMessage("fr.soleil.cdma.box.uri.selected.no");
        uriSource = new BasicDataSource<URI>(null);
        uriSource.setIgnoreDuplicationTest(true);
        imageSource = new BasicDataSource<CometeImage>(null);
        nameSource = new BasicDataSource<String>(null);
        nameSource.setIgnoreDuplicationTest(true);
        String uriError = this.messageManager.getMessage("fr.soleil.cdma.box.uri.select.error");
        experimentTree = new ExperimentTree(applicationId, uriSource, imageSource, nameSource, this.cdmaKeyFactory);
        experimentTree.setUriSelectionErrorMesage(uriError);
        experimentTree.setFont(GUIUtilities.DEFAULT_FONT);
        sortPanel = new JPanel(new BorderLayout(5, 5));
        sortLabel = new JLabel(this.messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.title"));
        sortLabel.setFont(GUIUtilities.DEFAULT_FONT_BOLD);
        sortPanel.add(sortLabel, BorderLayout.WEST);
        sortComboBox = new JComboBox<>(
                new Object[] { this.messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.default"),
                        new UriLastModificationDateComparator(true, messageManager, this),
                        new UriLastModificationDateComparator(false, messageManager, this),
                        new UriCreationDateComparator(true, messageManager, this),
                        new UriCreationDateComparator(false, messageManager, this),
                        new UriNameComparator(true, messageManager, this),
                        new UriNameComparator(false, messageManager, this) });
        sortComboBox.setBackground(Color.WHITE);
        sortComboBox.setFont(GUIUtilities.DEFAULT_FONT);
        sortComboBox.setSelectedIndex(0);
        sortComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if ((e != null) && (e.getStateChange() == ItemEvent.SELECTED)) {
                    updateSortFilter();
                }
            }
        });
        sortPanel.add(sortComboBox, BorderLayout.CENTER);

        experimentScrollPane = GUIUtilities.generateTreeScrollPane(experimentTree, this.messageManager);

        experimentTreeToolPanel = experimentScrollPane.getColumnHeader().getView();
        experimentLoadingPanel = new LoadingPanel<JScrollPane>(experimentScrollPane);
        experimentLoadingPanel.setOpaque(false);
        experimentLoadingPanel.setLoading(false);

        browseTree = new BrowseTree(applicationId, uriSource, imageSource, nameSource, cdmaKeyFactory);
        browseTree.setUriSelectionErrorMesage(uriError);
        browseScrollPane = new JScrollPane(browseTree);
        topUri = null;
        topUriText = "";
        browseDialog = null;
        browsePanel = new JPanel(new BorderLayout(5, 5));
        topUriField = new JTextField();
        topUriField.setFont(GUIUtilities.DEFAULT_FONT);
        final ActionListener folderChosenActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                topUriField.setEnabled(false);
                experimentLoadingPanel.setLoading(true);
                SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        setTopUri(topUriField.getText());
                        return null;
                    }

                    @Override
                    protected void done() {
                        experimentLoadingPanel.setLoading(false);
                        topUriField.setEnabled(true);
                        experimentTree.updateUI();
                    }
                };
                worker.execute();
            }
        };
        topUriField.addActionListener(folderChosenActionListener);
        topUriFileChooserButton = new JButton(
                GUIUtilities.DEFAULT_UTILITIES.getIcon("fr.soleil.cdma.box.view.BrowserButton"));
        topUriFileChooserButton.setMargin(CometeUtils.getzInset());
        topUriFileChooserButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                URI uri = topUri;
                if (uri == null) {
                    uri = new File(".").toURI();
                }
                JFileChooser jfc = new JFileChooser(new File(uri));
                jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int result = jfc.showOpenDialog(topUriFileChooserButton);
                if (result == JFileChooser.APPROVE_OPTION) {
                    File chosenFile = jfc.getSelectedFile();
                    topUriField.setText(chosenFile.getAbsolutePath());
                    folderChosenActionListener.actionPerformed(e);
                }
            }
        });
        repaintMessage = this.messageManager.getMessage("fr.soleil.cdma.box.view.Tree.Repaint");
        reloadMessage = this.messageManager.getMessage("fr.soleil.cdma.box.view.Tree.Reload");
        expandMessage = this.messageManager.getMessage("fr.soleil.cdma.box.view.Tree.ExpandAll");
        collapseMessage = this.messageManager.getMessage("fr.soleil.cdma.box.view.Tree.CollapseAll");
        repaintButton = new JButton(new ReloadAction(repaintMessage, experimentTree));
        repaintButton.setMargin(GUIUtilities.NO_MARGIN);
        reloadButton = new JButton(new ReloadAction(reloadMessage, experimentTree));
        reloadButton.setMargin(GUIUtilities.NO_MARGIN);
        reloadButton.setVisible(false);
        expandButton = new JButton(new ExpandAllAction(expandMessage, collapseMessage, experimentTree, true));
        expandButton.setMargin(GUIUtilities.NO_MARGIN);
        collapseButton = new JButton(new ExpandAllAction(expandMessage, collapseMessage, experimentTree, false));
        collapseButton.setMargin(GUIUtilities.NO_MARGIN);

        int column = 0;
        topUriPanel = new JPanel(new GridBagLayout());
        topUriPanel.setOpaque(false);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridx = column++;
        gbc.gridy = 0;
        gbc.weightx = 0;
        gbc.weighty = 1;
        gbc.insets = GAP;
        topUriPanel.add(topUriFileChooserButton, gbc);

        gbc.gridx = column++;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.BOTH;
        topUriPanel.add(topUriField, gbc);

        gbc.weightx = 0;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridx = column++;
        topUriPanel.add(repaintButton, gbc);
        gbc.gridx = column++;
        topUriPanel.add(reloadButton, gbc);

        gbc.gridx = column++;
        topUriPanel.add(expandButton, gbc);

        gbc.gridx = column++;
        topUriPanel.add(collapseButton, gbc);
        GridBagConstraints sortPanelConstraints = new GridBagConstraints();
        sortPanelConstraints.fill = GridBagConstraints.BOTH;
        sortPanelConstraints.gridx = 0;
        sortPanelConstraints.gridy = 2;
        sortPanelConstraints.weightx = 1;
        sortPanelConstraints.weighty = 1;
        sortPanelConstraints.insets = GAP;
        sortPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        topUriPanel.add(sortPanel, sortPanelConstraints);

        pathChangerButton = new JButton(this.messageManager.getMessage("fr.soleil.cdma.box.uri.change"));
        pathChangerButton.setMargin(CometeUtils.getzInset());
        pathChangerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getBrowseDialog().pack();
                getBrowseDialog().setLocationRelativeTo(UriView.this);
                getBrowseDialog().setVisible(true);
            }
        });
        uriTitleLabel = new JLabel(this.messageManager.getMessage("fr.soleil.cdma.box.uri.selected.title"));
        uriLabel = new Label();
        uriLabel.setToolTipText(uriErrorMessage);
        uriLabel.setCometeBackground(CometeColor.WHITE);
        uriField = new TextField();
        uriField.setText(uriErrorMessage);
        uriField.setToolTipText(uriErrorMessage);
        uriField.setBorder(null);
        uriField.setFont(URI_FONT);
        uriField.setEditable(false);
        uriField.setColumns(10);
        uriPanel = new JPanel(new BorderLayout());
        uriPanel.setOpaque(false);
        uriPanel.add(uriLabel, BorderLayout.WEST);
        uriPanel.add(uriField, BorderLayout.CENTER);

        uriListener = new UriListener();
        toolTipUpdater = new ToolTipUpdater();

        this.uriController = (uriController == null ? new BasicUriController() : uriController);
        this.uriController.addLink(uriSource, uriListener);
        this.imageController = (imageController == null ? new CometeImageController() : imageController);
        this.imageController.addLink(imageSource, uriLabel);
        this.nameController = (nameController == null ? new BasicTextTargetController() : nameController);
        this.nameController.addLink(nameSource, uriField);
        this.nameController.addLink(nameSource, toolTipUpdater);
    }

    public AbstractDataSource<URI> getUriSource() {
        return uriSource;
    }

    public AbstractDataSource<String> getNameSource() {
        return nameSource;
    }

    public JDialog getBrowseDialog() {
        if (browseDialog == null) {
            browseDialog = new JDialog(CometeUtils.getWindowForComponent(this),
                    messageManager.getMessage("fr.soleil.cdma.box.uri.select"));
            browseDialog.setContentPane(browsePanel);
            browseDialog.setModal(true);
        }
        return browseDialog;
    }

    protected void buildBrowsePanel() {
        browsePanel.add(topUriPanel, BorderLayout.NORTH);
        browsePanel.add(browseScrollPane, BorderLayout.CENTER);
    }

    /**
     * @return the selectionMode
     */
    public UriSelectionMode getSelectionMode() {
        return selectionMode;
    }

    /**
     * @param selectionMode
     *            the selectionMode to set
     */
    public void setSelectionMode(UriSelectionMode selectionMode) {
        if (!ObjectUtils.sameObject(selectionMode, this.selectionMode)) {
            this.selectionMode = selectionMode;
            updateSelectionMode();
            updateSortFilter();
        }
    }

    protected void updateSelectionMode() {
        getBrowseDialog().setVisible(false);
        removeAll();
        browsePanel.removeAll();
        if (selectionMode != null) {
            repaintButton.removeActionListener(repaintButton.getAction());
            reloadButton.removeActionListener(reloadButton.getAction());
            expandButton.removeActionListener(expandButton.getAction());
            collapseButton.removeActionListener(collapseButton.getAction());
            switch (selectionMode) {
                case MODE_BROWSE:
                    repaintButton.setAction(new ReloadAction(repaintMessage, browseTree));
                    reloadButton.setAction(new ReloadStructureAction(reloadMessage, browseTree));
                    expandButton.setAction(new ExpandAllAction(expandMessage, collapseMessage, browseTree, true));
                    collapseButton.setAction(new ExpandAllAction(expandMessage, collapseMessage, browseTree, false));
                    buildBrowsePanel();
                    add(uriTitleLabel, BorderLayout.WEST);
                    add(uriPanel, BorderLayout.CENTER);
                    add(pathChangerButton, BorderLayout.EAST);
                    break;
                case MODE_EXPERIMENT:
                    repaintButton.setAction(new ReloadAction(repaintMessage, experimentTree));
                    reloadButton.setAction(new ReloadStructureAction(reloadMessage, experimentTree));
                    expandButton.setAction(new ExpandAllAction(expandMessage, collapseMessage, experimentTree, true));
                    collapseButton
                            .setAction(new ExpandAllAction(expandMessage, collapseMessage, experimentTree, false));
                    add(topUriPanel, BorderLayout.NORTH);
                    add(experimentLoadingPanel, BorderLayout.CENTER);
                    break;
            }
            updateTopURI();
        }
    }

    public boolean isReloadButtonVisible() {
        return reloadButton.isVisible();
    }

    public void setReloadButtonVisible(boolean visible) {
        reloadButton.setVisible(visible);
        topUriPanel.revalidate();
        topUriPanel.repaint();
    }

    public DefaultTreeModel getExperimentTreeModel() {
        return (DefaultTreeModel) experimentTree.getModel();
    }

    /**
     * @return the topUri
     */
    public URI getTopUri() {
        return topUri;
    }

    /**
     * @param topUri
     *            the topUri to set
     */
    public void setTopUri(URI topUri) {
        setTopUri(topUri == null ? null : topUri.toString(), topUri);
    }

    public void setTopUri(String topUriPath) {
        String uriText = topUriPath;
        URI uri = null;
        if (uriText != null) {
            boolean uriSet = false;
            uriText = uriText.replace(File.separator, "/");
            if (uriText.indexOf("//") < 0) {
                // No protocol defined: check for file path
                try {
                    File tempFile = new File(topUriPath);
                    if (tempFile.exists()) {
                        uri = tempFile.toURI();
                        uriSet = true;
                    }
                } catch (Exception e) {
                    uriSet = false;
                }
            }
            if (!uriSet) {
                try {
                    uri = URI.create(uriText);
                } catch (Exception e) {
                    uri = null;
                }
            }
        }
        setTopUri(topUriPath, uri);
    }

    /*
     * /!\ WARINING /!\
     * 
     * This code is mostly to be replaced as soon as possible.
     * Evolution MANTIS #26074
     * 
     */

    public void connectUri(URI uri) {
        experimentTree.updateURI(uri);
    }

    public void connectUri(URI uri, boolean testForFragment) {
        experimentTree.updateURI(uri, testForFragment);
    }

    public Collection<URI> getSelectedExperimentURIs() {
        Collection<URI> result;
        if (selectionMode == UriSelectionMode.MODE_EXPERIMENT) {
            result = experimentTree.getSelectedExperimentURIs(true, true, messageManager);
        } else {
            URI uri = uriSource.getData();
            if (uri == null) {
                result = null;
            } else {
                result = Arrays.asList(uri);
            }
        }
        return result;
    }

    public CDMAKeyFactory getCDMAKeyFactory() {
        return experimentTree.getCdmaKeyFactory();
    }

    public URI getLastConnectedURI() {
        return experimentTree.getLastConnectedURI();
    }

    /*
     * /!\ WARINING /!\
     * 
     * End of MANTIS #26074
     * 
     */

    protected void setTopUri(String text, URI topUri) {
        if (!ObjectUtils.sameObject(topUri, this.topUri)) {
            String nodeText = text;
            if (nodeText == null) {
                nodeText = "";
            }
            this.topUriText = nodeText;
            this.topUri = topUri;
            topUriField.setText(nodeText);
            boolean visible;
            if (topUri == null) {
                visible = false;
            } else {
                try {
                    File tmp = new File(topUri);
                    visible = tmp.exists();
                } catch (Exception e) {
                    visible = false;
                }
            }
            topUriFileChooserButton.setVisible(visible);
            updateTopURI();
        }
    }

    protected void updateTopURI() {
        if (selectionMode != null) {
            BasicTreeNode rootNode = new BasicTreeNode();
            rootNode.setName(topUriText);
            rootNode.setData(topUri);
            DoubleClickUriTree doubleClickTree = null;
            switch (selectionMode) {
                case MODE_BROWSE:
                    doubleClickTree = browseTree;
                    break;
                case MODE_EXPERIMENT:
                    doubleClickTree = experimentTree;
                    break;
            }
            final DoubleClickUriTree tree = doubleClickTree;
            if (tree != null) {
                tree.setRootNode(rootNode);
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected void updateSortFilter() {
        DoubleClickUriTree oldTree, newTree;
        if (selectionMode == null) {
            oldTree = null;
            newTree = null;
        } else {
            switch (selectionMode) {
                case MODE_BROWSE:
                    newTree = browseTree;
                    oldTree = experimentTree;
                    break;
                case MODE_EXPERIMENT:
                    newTree = experimentTree;
                    oldTree = browseTree;
                    break;
                default:
                    oldTree = null;
                    newTree = null;
                    break;
            }
        }
        if ((oldTree != null) && (oldTree.getModel() instanceof DefaultCometeTreeModel)) {
            ((DefaultCometeTreeModel) oldTree.getModel()).setComparator(null);
        }
        if ((newTree != null) && (newTree.getModel() instanceof DefaultCometeTreeModel)) {
            DefaultCometeTreeModel model = (DefaultCometeTreeModel) newTree.getModel();
            Object item = sortComboBox.getSelectedItem();
            if (item instanceof Comparator<?>) {
                model.setComparator((Comparator<DefaultMutableTreeNode>) item);
            } else {
                model.setComparator(null);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Comparator<DefaultMutableTreeNode>[] getSorters() {
        int count = sortComboBox.getItemCount();
        Comparator<DefaultMutableTreeNode>[] result = new Comparator[count];
        for (int i = 0; i < count; i++) {
            Object item = sortComboBox.getItemAt(i);
            if (item instanceof Comparator<?>) {
                result[i] = (Comparator<DefaultMutableTreeNode>) item;
            }
        }
        return result;
    }

    public void addSorter(Comparator<DefaultMutableTreeNode> sorter) {
        if (sorter != null) {
            sortComboBox.addItem(sorter);
        }
    }

    public void setSorter(Comparator<DefaultMutableTreeNode> sorter) {
        if (sorter == null) {
            sortComboBox.setSelectedIndex(0);
        } else {
            sortComboBox.setSelectedItem(sorter);
        }
    }

    public void setSorter(int index) {
        if ((index > -1) && (index < sortComboBox.getItemCount())) {
            sortComboBox.setSelectedIndex(index);
        }
    }

    public void clean() {
        setTopUri((URI) null);
        setSelectionMode(null);
    }

    public void setScrollBarsBackground(Color bg) {
        experimentScrollPane.setBackground(bg);
        experimentScrollPane.getHorizontalScrollBar().setBackground(bg);
        experimentScrollPane.getVerticalScrollBar().setBackground(bg);
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public boolean isSelected() {
        return experimentTree.isEnabled();
    }

    @Override
    public void setSelected(boolean bool) {
        experimentTree.setEnabled(bool);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if ((g != null) && (getComponentCount() == 0)) {
            Color previous = g.getColor();
            g.setColor(Color.RED);
            g.drawLine(getX(), getY(), getWidth(), getHeight());
            g.drawLine(getX(), getHeight(), getWidth(), getY());
            g.setColor(previous);
        }
    }

    /**
     * Returns whether single experiment will automatically be connected, in case of experiment tree (CTRLRFC-525)
     * 
     * @return A <code>boolean</code>
     */
    public boolean isAutoConnectExperiment() {
        return experimentTree.isAutoConnectExperiment();
    }

    /**
     * Sets whether single experiment should automatically be connected, in case of experiment tree (CTRLRFC-525)
     * 
     * @param autoConnectExperiment Whether single experiment should automatically be connected
     */
    public void setAutoConnectExperiment(boolean autoConnectExperiment) {
        experimentTree.setAutoConnectExperiment(autoConnectExperiment);
    }

    public boolean isExperimentTreeToolPanelVisible() {
        return ((experimentScrollPane.getColumnHeader() != null)
                && (experimentScrollPane.getColumnHeader().getView() == experimentTreeToolPanel));
    }

    public void setExperimentTreeToolPanelVisible(boolean visible) {
        experimentScrollPane.setColumnHeaderView(visible ? experimentTreeToolPanel : null);
    }

    public ITreeNode getTreeNode(DefaultMutableTreeNode node) {
        CometeTreeModel model;
        switch (selectionMode) {
            case MODE_BROWSE:
                model = browseTree.getModel();
                break;
            case MODE_EXPERIMENT:
                model = experimentTree.getModel();
                break;
            default:
                model = null;
                break;
        }
        return (model == null) ? null : model.getTreeNode(node);
    }

    public File getFile(ITreeNode node) {
        File file;
        if (node == null) {
            file = null;
        } else {
            Object data = node.getData();
            if (data instanceof File) {
                file = (File) data;
            } else if (data instanceof URI) {
                file = new File((URI) data);
            } else {
                file = null;
            }
        }
        return file;
    }

    public static void main(String[] args) {
        String view = "";
        for (String arg : args) {
            String[] values = arg.split("=");
            if (values.length > 1) {
                String param = values[0];
                String value = values[1];

                if (param.equals("VIEW")) {
                    view = value;
                }
            }
        }
        final JFrame testFrame = new JFrame(UriView.class.getName());
        Factory.setDictionariesFolder(SystemUtils.getSystemProperty(ICDMAReader.COMMON_RESOURCES_PROPERTY));
        final UriView uriView = new UriView(null, new CDMAKeyFactory(view), null, null, null, null);
        uriView.setExperimentTreeToolPanelVisible(true);
        JPanel testPanel = new JPanel(new BorderLayout());
        JLabel infoLabel = new JLabel("Click to change URI selection mode");
        infoLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                UriSelectionMode selectionMode = uriView.getSelectionMode();
                if (selectionMode == null) {
                    uriView.setSelectionMode(UriSelectionMode.MODE_EXPERIMENT);
                } else {
                    switch (selectionMode) {
                        case MODE_EXPERIMENT:
                            uriView.setSelectionMode(UriSelectionMode.MODE_BROWSE);
                            break;
                        case MODE_BROWSE:
                            uriView.setSelectionMode(UriSelectionMode.MODE_EXPERIMENT);
                            break;
                    }
                }
                testFrame.pack();
                if (testFrame.getHeight() < 70) {
                    testFrame.setSize(testFrame.getWidth(), 70);
                }
            }
        });
        testPanel.add(uriView, BorderLayout.CENTER);
        testPanel.add(infoLabel, BorderLayout.SOUTH);
        testFrame.pack();
        if (testFrame.getHeight() < 70) {
            testFrame.setSize(testFrame.getWidth(), 70);
        }
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setContentPane(testPanel);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public static enum UriSelectionMode {
        MODE_EXPERIMENT, MODE_BROWSE
    }

    protected class UriListener implements IUriTarget {

        @Override
        public String getUriToString() {
            // not managed
            return null;
        }

        @Override
        public URI getUri() {
            // not managed
            return null;
        }

        @Override
        public void setUri(URI uri) {
            getBrowseDialog().setVisible(false);
        }

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }
    }

    protected class ToolTipUpdater implements ITextTarget {

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public String getText() {
            return uriField.getToolTipText();
        }

        @Override
        public void setText(String text) {
            if ((text == null) || text.trim().isEmpty()) {
                uriField.setText(uriErrorMessage);
                uriField.setToolTipText(uriErrorMessage);
                uriLabel.setToolTipText(uriErrorMessage);
            } else {
                uriField.setToolTipText(text);
                uriLabel.setToolTipText(text);
            }
        }
    }

}
