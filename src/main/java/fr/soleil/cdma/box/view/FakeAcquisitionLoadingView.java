package fr.soleil.cdma.box.view;

import javax.swing.SwingWorker;

import org.slf4j.LoggerFactory;

import fr.soleil.cdma.box.data.sensor.ExperimentConfig;
import fr.soleil.cdma.box.manager.AcquisitionManager;
import fr.soleil.cdma.box.util.ProgressDialogContainer;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;

/**
 * An {@link AcquisitionLoadingView} that displays nothing and immediately loads acquisition when it
 * receives an {@link ExperimentConfig}
 * 
 * @author girardot
 */
public class FakeAcquisitionLoadingView extends AcquisitionLoadingView {

    private static final long serialVersionUID = -6791307141659194248L;

    /**
     * Constructor
     * 
     * @param acquisitionManager The associated {@link AcquisitionManager}
     * @param messageManager The {@link MessageManager} that recovers the texts to display
     */
    public FakeAcquisitionLoadingView(String applicationId, AcquisitionManager acquisitionManager,
            MessageManager messageManager, ProgressDialogContainer progressDialogContainer) {
        super(applicationId, acquisitionManager, messageManager, progressDialogContainer);
    }

    @Override
    protected void addComponents() {
        // nothing to do;
    }

//    @Override
//    protected void updateDetectorList() {
//        // nothing to do;
//    }
//
//    @Override
//    protected void updateRegionList() {
//        // nothing to do;
//    }

    @Override
    protected ProgressDialog getProgressDialog() {
        return progressDialogContainer.getProgressDialog(this);
    }

    @Override
    public void setExperimentConfig(ExperimentConfig experimentConfig) {
        synchronized (experimentLock) {
            if (this.experimentConfig != experimentConfig) {
                this.experimentConfig = experimentConfig;
                updateDetectorList(true);
            } else {
                manageDialogClosing(getProgressDialog());
            }
        }
    }

    /**
     * Displays the loading dialog, and load acquisition in a {@link SwingWorker}
     * 
     * @see #loadAcquisition()
     */
    @Override
    protected void validateAcquisitionLoading() {
        try {
            loadingSource.setData(true);
        } catch (Exception e) {
            LoggerFactory.getLogger(applicationId).error("Could not set loadingSource", e);
        }
        SwingWorker<Void, Void> acquisitionWorker = new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {
                loadAcquisition();
                return null;
            }

            @Override
            protected void done() {
                try {
                    loadingSource.setData(false);
                } catch (Exception e) {
                    LoggerFactory.getLogger(applicationId).error("Could not set loadingSource", e);
                }
            }
        };
        acquisitionWorker.execute();
    }

}
