package fr.soleil.cdma.box.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;

import org.slf4j.LoggerFactory;

import fr.soleil.cdma.box.data.sensor.ExperimentConfig;
import fr.soleil.cdma.box.exception.CDMAAccessException;
import fr.soleil.cdma.box.manager.AcquisitionManager;
import fr.soleil.cdma.box.target.IExperimentConfigTarget;
import fr.soleil.cdma.box.util.CDMABoxUtils;
import fr.soleil.cdma.box.util.ProgressDialogContainer;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.source.BasicDataSource;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;

/**
 * This is a view that allows to choose a Sensor in order to load an acquisition, using an {@link AcquisitionManager}
 * 
 * @author girardot
 */
public class AcquisitionLoadingView extends JPanel implements ActionListener, IExperimentConfigTarget {

    private static final long serialVersionUID = 5531594971108171912L;

    protected JComboBox<String> detectorCombo;
    protected JButton validateButton;
    protected AcquisitionManager acquisitionManager;
    protected final TargetDelegate targetDelegate;
    protected ExperimentConfig experimentConfig;
    protected ProgressDialogContainer progressDialogContainer;
    protected MessageManager messageManager;
    protected JLabel detectorLabel;
    protected JPanel fieldsPanel;
    protected AbstractDataSource<Boolean> loadingSource;
    protected final List<String> detectorList;
    protected int selectedSensorConfig = -1;
    protected final String applicationId;
    protected final Object experimentLock;

    /**
     * Constructor
     * 
     * @param acquisitionManager The associated {@link AcquisitionManager}
     * @param messageManager The {@link MessageManager} that recovers the texts to display
     */
    public AcquisitionLoadingView(String applicationId, AcquisitionManager acquisitionManager,
            MessageManager messageManager, ProgressDialogContainer progressDialogContainer) {
        super();
        this.applicationId = (applicationId == null ? Mediator.LOGGER_ACCESS : applicationId);
        detectorList = new ArrayList<>();
        targetDelegate = new TargetDelegate();
        this.progressDialogContainer = progressDialogContainer;
        if (progressDialogContainer == null) {
            progressDialogContainer = new ProgressDialogContainer();
        }
        experimentConfig = null;
        this.acquisitionManager = acquisitionManager;
        if (messageManager == null) {
            messageManager = CDMABoxUtils.MESSAGE_MANAGER;
        }
        this.messageManager = messageManager;
        loadingSource = new BasicDataSource<Boolean>(null);
        experimentLock = new Object();
        initComponents();
        addComponents();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        detectorCombo.setEnabled(enabled);
        validateButton.setEnabled(enabled);
        detectorLabel.setEnabled(enabled);
    }

    /**
     * Initializes the components to display
     */
    protected void initComponents() {
        detectorCombo = new JComboBox<>();
        detectorCombo.addActionListener(this);

        validateButton = new JButton(
                messageManager.getMessage("fr.soleil.cdma.box.view.AcquisitionLoadingView.button.validate"));
        validateButton.addActionListener(this);
        detectorLabel = new JLabel(
                messageManager.getMessage("fr.soleil.cdma.box.view.AcquisitionLoadingView.combo.detector"));

        fieldsPanel = new JPanel(new GridBagLayout());
    }

    /**
     * layouts all the components
     */
    protected void addComponents() {
        Insets labelInsets = new Insets(0, 0, 5, 5);
        Insets comboInsets = new Insets(0, 0, 5, 0);

        GridBagConstraints detectorLabelConstraints = new GridBagConstraints();
        detectorLabelConstraints.fill = GridBagConstraints.BOTH;
        detectorLabelConstraints.gridx = 0;
        detectorLabelConstraints.gridy = 0;
        detectorLabelConstraints.weightx = 0;
        detectorLabelConstraints.weighty = 0;
        detectorLabelConstraints.insets = labelInsets;
        fieldsPanel.add(detectorLabel, detectorLabelConstraints);
        GridBagConstraints detectorComboConstraints = new GridBagConstraints();
        detectorComboConstraints.fill = GridBagConstraints.BOTH;
        detectorComboConstraints.gridx = 1;
        detectorComboConstraints.gridy = 0;
        detectorComboConstraints.weightx = 1;
        detectorComboConstraints.weighty = 0;
        detectorComboConstraints.insets = comboInsets;
        fieldsPanel.add(detectorCombo, detectorComboConstraints);

        setLayout(new GridBagLayout());

        GridBagConstraints fieldsPanelConstraints = new GridBagConstraints();
        fieldsPanelConstraints.fill = GridBagConstraints.BOTH;
        fieldsPanelConstraints.gridx = 0;
        fieldsPanelConstraints.gridy = 0;
        fieldsPanelConstraints.weightx = 1;
        fieldsPanelConstraints.weighty = 1;
        add(new JScrollPane(fieldsPanel), fieldsPanelConstraints);

        GridBagConstraints validateButtonConstraints = new GridBagConstraints();
        validateButtonConstraints.fill = GridBagConstraints.NONE;
        validateButtonConstraints.gridx = 0;
        validateButtonConstraints.gridy = 1;
        validateButtonConstraints.anchor = GridBagConstraints.CENTER;
        add(validateButton, validateButtonConstraints);
    }

    /**
     * Returns the {@link AbstractDataSource} that indicates when Acquisition is being calculated
     * 
     * @return An {@link AbstractDataSource} of {@link Boolean}
     */
    public AbstractDataSource<Boolean> getLoadingSource() {
        return loadingSource;
    }

    /**
     * Returns the {@link ProgressDialog} used by this {@link AcquisitionLoadingView}
     * 
     * @return A {@link ProgressDialog}
     */
    protected ProgressDialog getProgressDialog() {
        ProgressDialog progressDialog = progressDialogContainer.getProgressDialog(this);
        progressDialog.setProgressIndeterminate(true);
        progressDialog.setCancelable(acquisitionManager.getCdmaReader());
        return progressDialog;
    }

    /**
     * Updates the detector list displayed in the detector combo box
     */
    protected void updateDetectorList() {
        updateDetectorList(false);
    }

    /**
     * Updates the detector list displayed in the detector combo box
     */
    protected void updateDetectorList(boolean forceDirectTransmission) {
        boolean directTransmission = forceDirectTransmission;
        detectorCombo.removeActionListener(this);
        detectorCombo.removeAllItems();
        detectorList.clear();
        if (experimentConfig != null) {
            detectorList.addAll(experimentConfig.getDetectorList());
            if (detectorList.size() == 0) {
                directTransmission = true;
            } else {
                for (String detector : detectorList) {
                    detectorCombo.addItem(detector);
                }
            }
        }
        detectorCombo.addActionListener(this);
        selectedSensorConfig = detectorCombo.getSelectedIndex();
        if (directTransmission) {
            validateAcquisitionLoading();
        } else {
            manageDialogClosing(getProgressDialog());
        }
    }

    /**
     * Prepares the {@link AcquisitionManager} and asks it to load acquisition
     * 
     * @throws CDMAAccessException If a problem occurred during acquisition loading
     */
    protected void loadAcquisition() throws CDMAAccessException {
        acquisitionManager.setDetector(getSelectedDetector());
        acquisitionManager.loadAcquisition();
    }

    /**
     * Returns the detected to use
     * 
     * @return A {@link String}
     */
    protected String getSelectedDetector() {
        String detector = null;
        if (selectedSensorConfig > -1) {
            detector = detectorList.get(selectedSensorConfig);
        }
        return detector;
    }

    /**
     * Displays the loading dialog, and load acquisition in a {@link SwingWorker}
     * 
     * @see #loadAcquisition()
     */
    protected void validateAcquisitionLoading() {
        final ProgressDialog dialog = getProgressDialog();
        if (dialog != null) {
            dialog.setTitle(messageManager.getMessage("fr.soleil.cdma.box.view.data.loading.scan"));
            String detector = getSelectedDetector();
            if (detector == null) {
                dialog.setMainMessage(dialog.getTitle());
            } else {
                dialog.setMainMessage(String
                        .format(messageManager.getMessage("fr.soleil.cdma.box.view.data.loading.detector"), detector));
            }
            dialog.pack();
            dialog.setLocationRelativeTo(WindowSwingUtils.getWindowForComponent(this));
            dialog.setVisible(true);
        }
        try {
            loadingSource.setData(true);
        } catch (Exception e) {
            LoggerFactory.getLogger(applicationId).error("Could not set loadingSource", e);
        }
        SwingWorker<Void, Void> acquisitionWorker = new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {
                loadAcquisition();
                return null;
            }

            @Override
            protected void done() {
                manageDialogClosing(dialog);
                try {
                    loadingSource.setData(false);
                } catch (Exception e) {
                    LoggerFactory.getLogger(applicationId).error("Could not set loadingSource", e);
                }
            }
        };
        acquisitionWorker.execute();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == detectorCombo) {
            selectedSensorConfig = detectorCombo.getSelectedIndex();
        } else if (source == validateButton) {
            validateAcquisitionLoading();
        }
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        targetDelegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        targetDelegate.removeMediator(mediator);
    }

    @Override
    public void setExperimentConfig(ExperimentConfig experimentConfig) {
        synchronized (experimentLock) {
            if (this.experimentConfig == experimentConfig) {
                manageDialogClosing(getProgressDialog());
            } else {
                this.experimentConfig = experimentConfig;
                updateDetectorList();
            }
        }
    }

    protected void manageDialogClosing(ProgressDialog dialog) {
        if (dialog != null) {
            dialog.setVisible(false);
        }
    }

}
