/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.view.component;

import java.net.URI;

import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import fr.soleil.cdma.box.util.CDMABoxUtils;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.definition.event.TreeNodeEvent;
import fr.soleil.comete.definition.event.TreeNodeEvent.NodeEventReason;
import fr.soleil.comete.definition.listener.ITreeNodeListener;
import fr.soleil.comete.definition.widget.util.BasicTreeNode;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.util.CometeTreeModel;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;

/**
 * A {@link DoubleClickUriTree} that can browse {@link URI}s to find experiment one, and uses refreshing strategies to
 * always display up-to-date {@link URI}s
 * 
 * @author girardot
 */
public class ExperimentTree extends DoubleClickUriTree implements ITreeNodeListener {

    private static final long serialVersionUID = -8672329509532999156L;

    protected boolean autoConnectExperiment;

    public ExperimentTree(String applicationId, AbstractDataSource<URI> uriSource,
            AbstractDataSource<CometeImage> imageSource, AbstractDataSource<String> nameSource,
            CDMAKeyFactory cdmaKeyFactory) {
        super(applicationId, uriSource, imageSource, nameSource, cdmaKeyFactory);
        autoConnectExperiment = true;
    }

    public ExperimentTree(String applicationId, CometeTreeModel model, AbstractDataSource<URI> uriSource,
            AbstractDataSource<CometeImage> imageSource, AbstractDataSource<String> nameSource,
            CDMAKeyFactory cdmaKeyFactory) {
        super(applicationId, model, uriSource, imageSource, nameSource, cdmaKeyFactory);
        autoConnectExperiment = true;
    }

    @Override
    protected void prepareKey(IKey key) {
        // Nothing to do: the key is good as it is
    }

    /**
     * Returns whether single experiment will automatically be connected (CTRLRFC-525)
     * 
     * @return A <code>boolean</code>
     */
    public boolean isAutoConnectExperiment() {
        return autoConnectExperiment;
    }

    /**
     * Sets whether single experiment should automatically be connected (CTRLRFC-525)
     * 
     * @param autoConnectExperiment Whether single experiment should automatically be connected
     */
    public void setAutoConnectExperiment(boolean autoConnectExperiment) {
        this.autoConnectExperiment = autoConnectExperiment;
    }

    /**
     * Returns the {@link CDMAKeyFactory} used by this {@link ExperimentTree}
     * 
     * @return A {@link CDMAKeyFactory}
     */
    public CDMAKeyFactory getCdmaKeyFactory() {
        return cdmaKeyFactory;
    }

    /**
     * Sets this {@link ExperimentTree}'s {@link CDMAKeyFactory}
     * 
     * @param cdmaKeyFactory The {@link CDMAKeyFactory} to set
     */
    public void setCdmaKeyFactory(CDMAKeyFactory cdmaKeyFactory) {
        if ((cdmaKeyFactory != null) && (cdmaKeyFactory != this.cdmaKeyFactory)) {
            this.cdmaKeyFactory = cdmaKeyFactory;
            ITreeNode root = rootNode;
            if (root != null) {
                BasicTreeNode newRoot = new BasicTreeNode();
                newRoot.setImage(root.getImage());
                newRoot.setData(root.getData());
                newRoot.setName(root.getName());
                setRootNode(newRoot);
            }
        }
    }

    @Override
    public void nodeChanged(final TreeNodeEvent event) {
        if (isAutoConnectExperiment() && (event != null) && (event.getReason() == NodeEventReason.INSERTED)) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    ITreeNode[] children = event.getChildren();
                    ITreeNode parent = event.getSource();
                    if ((parent != null) && (children != null) && (children.length == 1)) {
                        DefaultMutableTreeNode node = getModel().recoverNode(parent);
                        if ((node != null) && (node.getChildCount() == 1)) {
                            ITreeNode child = children[0];
                            if ((child != null) && (child.getData() instanceof URI)) {
                                URI uri = (URI) child.getData();
                                if (CDMABoxUtils.isExperiment(uri)) {
                                    DefaultMutableTreeNode childNode = getModel().recoverNode(child);
                                    if (childNode != null) {
                                        setSelectionPath(new TreePath(childNode.getPath()));
                                        connectNodes(child);
                                    }
                                }
                            }
                        }
                    }
                }
            };
            if (SwingUtilities.isEventDispatchThread()) {
                runnable.run();
            } else {
                SwingUtilities.invokeLater(runnable);
            }
        } else {
            super.nodeChanged(event);
        }
    }
}
