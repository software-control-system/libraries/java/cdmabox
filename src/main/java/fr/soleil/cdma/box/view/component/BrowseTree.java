/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.view.component;

import java.net.URI;

import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.swing.util.CometeTreeModel;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;

/**
 * A {@link DoubleClickUriTree} that allows to browse {@link URI}s, without any kind of refreshing (one shot browsing)
 * 
 * @author girardot
 */
public class BrowseTree extends DoubleClickUriTree {

    private static final long serialVersionUID = -3288752851072317512L;

    private static final String ALONE = "alone";

    public BrowseTree(String applicationId, AbstractDataSource<URI> uriSource,
            AbstractDataSource<CometeImage> imageSource, AbstractDataSource<String> nameSource,
            CDMAKeyFactory cdmaKeyFactory) {
        super(applicationId, uriSource, imageSource, nameSource, cdmaKeyFactory);
    }

    public BrowseTree(String applicationId, CometeTreeModel model, AbstractDataSource<URI> uriSource,
            AbstractDataSource<CometeImage> imageSource, AbstractDataSource<String> nameSource,
            CDMAKeyFactory cdmaKeyFactory) {
        super(applicationId, model, uriSource, imageSource, nameSource, cdmaKeyFactory);
    }

    @Override
    protected void prepareKey(IKey key) {
        if (key != null) {
            key.registerProperty(ALONE, CometeUtils.generateIdForClass(CDMAKey.class));
        }
    }

}
