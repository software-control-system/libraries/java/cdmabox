package fr.soleil.cdma.box.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.slf4j.LoggerFactory;

import fr.soleil.cdma.box.controller.FileListController;
import fr.soleil.cdma.box.manager.FileListManager;
import fr.soleil.cdma.box.util.comparator.CreationDateFileNodeComparator;
import fr.soleil.cdma.box.util.comparator.LastModificationDateFileNodeComparator;
import fr.soleil.cdma.box.util.comparator.NameFileNodeComparator;
import fr.soleil.cdma.box.view.model.FileTreeModel;
import fr.soleil.cdma.box.view.renderer.FileTreeRenderer;
import fr.soleil.cdma.box.view.tree.node.FileNode;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.source.BasicDataSource;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.lib.project.resource.MessageManager;
import fr.soleil.lib.project.swing.listener.TreeUiUpdater;
import fr.soleil.lib.project.swing.panel.LoadingPanel;

/**
 * The file view. This is the JPanel that contains the file tree.
 * <p>
 * {@link FileView} is an {@link IBooleanTarget} to have the possibility to enable/disable its tree
 * </p>
 * 
 * @author girardot
 */
public class FileView extends JPanel implements IBooleanTarget {

    private static final long serialVersionUID = 3466659248708251379L;

    private static final String DEFAULT_PACKAGE = "fr.soleil.cdma.box";

    protected final JPanel sortPanel;
    protected final JLabel sortLabel;
    protected final JComboBox<Object> sortComboBox;
    protected final JTree fileTree;
    protected final JScrollPane fileTreeScrollPane;
    protected final FileTreeModel fileTreeModel;
    protected final LoadingPanel<JScrollPane> fileTreeLoadingPanel;
    protected final TreeUiUpdater fileTreeUiUpdater;
    protected final TreeSelectionListener fileTreeSelectionListener;
    protected final TreeModelListener fileTreeModelListener;
    protected final FileListManager fileManager;
    protected final FileListController fileListController;
    protected final JFileChooser pathChanger;
    protected final JButton pathChangerButton;

    protected final JLabel titleLabel;
    protected final JFileChooser fileSelector;
    protected final JTextField fileDisplayer;
    protected final JButton fileSelectorButton;

    protected final AbstractDataSource<File> fileSource;
    protected FileBrowsingMode fileBrowsingMode;
    protected final String applicationId;
    protected final MessageManager messageManager;

    public FileView(String applicationId, String path) {
        this(applicationId, path, (String[]) null);
    }

    public FileView(String applicationId, String path, String... filteredExtensions) {
        this(applicationId, path, new MessageManager(DEFAULT_PACKAGE), filteredExtensions);
    }

    public FileView(String applicationId, String path, MessageManager messageManager, String... filteredExtensions) {
        super(new BorderLayout());
        this.applicationId = (applicationId == null ? Mediator.LOGGER_ACCESS : applicationId);
        this.messageManager = messageManager;
        if ((messageManager != null) && (!DEFAULT_PACKAGE.equals(messageManager.getResourcePackage()))) {
            messageManager.pushPropertiesIfAbsent(new MessageManager(DEFAULT_PACKAGE));
        }
        File tempFile = (path == null ? null : new File(path));
        fileBrowsingMode = null;

        // file tree mode
        fileTreeModel = new LoggingFileTreeModel(path);
        fileTree = new JTree(fileTreeModel);
        fileTree.setExpandsSelectedPaths(false);
        fileTreeUiUpdater = new TreeUiUpdater(fileTree, true, true);
        fileTreeModel.addTreeModelListener(fileTreeUiUpdater);
        fileTree.setCellRenderer(new FileTreeRenderer());
        fileTree.setEditable(false);
        fileTree.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_F5) {
                    fileTreeModel.reload();
                }
            }
        });
        fileManager = new FileListManager(messageManager, LoggerFactory.getLogger(this.applicationId),
                filteredExtensions);
        fileManager.setWaitForStability(false);
        fileTreeModelListener = new TreeModelListener() {
            @Override
            public void treeStructureChanged(TreeModelEvent e) {
                updateRootNode();
            }

            @Override
            public void treeNodesRemoved(TreeModelEvent e) {
                updateRootNode();
            }

            @Override
            public void treeNodesInserted(TreeModelEvent e) {
                updateRootNode();
            }

            @Override
            public void treeNodesChanged(TreeModelEvent e) {
                updateRootNode();
            }
        };
        fileListController = new FileListController();
        fileListController.addLink(fileManager.getFileListSource(), fileTreeModel);
        fileTreeScrollPane = new JScrollPane(fileTree);
        sortPanel = new JPanel(new BorderLayout(5, 5));
        sortLabel = new JLabel(this.messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.title"));
        sortPanel.add(sortLabel, BorderLayout.WEST);
        sortComboBox = new JComboBox<>(new Object[] {
                this.messageManager.getMessage("fr.soleil.cdma.box.view.UriOrFileView.sort.default"),
                new LastModificationDateFileNodeComparator(true, messageManager),
                new LastModificationDateFileNodeComparator(false, messageManager),
                new CreationDateFileNodeComparator(true, messageManager),
                new CreationDateFileNodeComparator(false, messageManager),
                new NameFileNodeComparator(true, messageManager), new NameFileNodeComparator(false, messageManager) });
        sortComboBox.setSelectedIndex(0);
        sortComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if ((e != null) && (e.getStateChange() == ItemEvent.SELECTED)) {
                    updateSortFilter();
                }
            }
        });
        sortPanel.add(sortComboBox, BorderLayout.CENTER);
        sortPanel.setBackground(Color.WHITE);
        fileTreeScrollPane.setColumnHeaderView(sortPanel);
        fileTreeLoadingPanel = new LoadingPanel<JScrollPane>(fileTreeScrollPane);
        fileManager.setFile(tempFile);
        fileSource = new BasicDataSource<File>(null);
        fileTreeSelectionListener = new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                File selectedFile = null;
                TreePath selection = fileTree.getSelectionPath();
                if ((selection != null) && (selection.getLastPathComponent() instanceof FileNode)) {
                    selectedFile = ((FileNode) selection.getLastPathComponent()).getData();
                }
                selectFile(selectedFile);
            }
        };
        pathChanger = new JFileChooser(path);
        pathChanger.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        pathChanger.setMultiSelectionEnabled(false);
        pathChangerButton = new JButton(messageManager.getMessage("fr.soleil.cdma.box.view.FileView.ChangePath"));
        pathChangerButton.setMargin(new Insets(0, 0, 0, 0));
        pathChangerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int choice = pathChanger.showOpenDialog(pathChangerButton);
                if (choice == JFileChooser.APPROVE_OPTION) {
                    changePathTo(pathChanger.getSelectedFile().getAbsolutePath());
                }
            }
        });

        // File browsing mode
        titleLabel = new JLabel(messageManager.getMessage("fr.soleil.cdma.box.manager.FileListManager.title"));
        fileSelector = new JFileChooser(path);
        fileSelector.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fileSelectorButton = new JButton("...");
        fileSelectorButton.setToolTipText(messageManager.getMessage("fr.soleil.cdma.box.view.FileView.ChooseFile"));
        fileSelectorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int choice = fileSelector.showOpenDialog(fileSelectorButton);
                if (choice == JFileChooser.APPROVE_OPTION) {
                    File file = fileSelector.getSelectedFile();
                    changePathTo(file.getParentFile().getAbsolutePath());
                    selectFile(file);
                }
            }
        });
        fileDisplayer = new JTextField(10);
        fileDisplayer.setEnabled(true);
        fileDisplayer.setEditable(false);
        fileDisplayer.setOpaque(true);
        fileDisplayer.setBackground(Color.WHITE);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        fileTree.setEnabled(enabled);
        fileTreeScrollPane.setEnabled(enabled);
        pathChanger.setEnabled(enabled);
        pathChangerButton.setEnabled(enabled);
        titleLabel.setEnabled(enabled);
        fileSelector.setEnabled(enabled);
        fileDisplayer.setEnabled(enabled);
        fileSelectorButton.setEnabled(enabled);
    }

    /**
     * Returns whether this {@link FileView} waits for a file stability before displaying it in tree
     * mode
     * 
     * @return A <code>boolean</code> value
     */
    public boolean isWaitForStability() {
        return fileManager.isWaitForStability();
    }

    public File[] filterFiles(File... files) {
        return fileManager.filterFiles(files);
    }

    /**
     * Sets whether this {@link FileView} should wait for a file stability before displaying it in
     * tree mode
     * 
     * @param waitForStability Whether to wait for file stability (<code>TRUE</code> to wait for
     *            stability)
     */
    public void setWaitForStability(boolean waitForStability) {
        fileManager.setWaitForStability(waitForStability);
    }

    public void selectFile(File file) {
        if (file == null) {
            fileDisplayer.setText("");
            fileDisplayer.setToolTipText(null);
        } else {
            fileDisplayer.setText(file.getName());
            fileDisplayer.setToolTipText(file.getAbsolutePath());
            fileDisplayer.setCaretPosition(0);
        }
        try {
            fileSource.setData(file);
        } catch (Exception e) {
            LoggerFactory.getLogger(applicationId).error("Could not set fileSource", e);
        }
    }

    /**
     * Returns this {@link FileView}'s {@link FileBrowsingMode}
     * 
     * @return A {@link FileBrowsingMode}
     */
    public FileBrowsingMode getFileBrowsingMode() {
        return fileBrowsingMode;
    }

    /**
     * Changes the way to browse files, using a {@link FileBrowsingMode}
     * 
     * @param fileBrowsingMode The {@link FileBrowsingMode} to use
     */
    public synchronized void setFileBrowsingMode(FileBrowsingMode fileBrowsingMode) {
        if (!ObjectUtils.sameObject(fileBrowsingMode, this.fileBrowsingMode)) {
            this.fileBrowsingMode = fileBrowsingMode;
            removeAll();
            fileManager.stopRefreshing();
            fileTreeModel.removeTreeModelListener(fileTreeModelListener);
            fileTree.removeTreeSelectionListener(fileTreeSelectionListener);
            fileSelector.cancelSelection();
            pathChanger.cancelSelection();
            fileDisplayer.setText("");
            if (fileBrowsingMode != null) {
                switch (fileBrowsingMode) {
                    case MODE_TREE:
                        add(pathChangerButton, BorderLayout.NORTH);
                        fileTreeModel.addTreeModelListener(fileTreeModelListener);
                        fileTree.addTreeSelectionListener(fileTreeSelectionListener);
                        fileManager.startRefreshing(true);
                        add(fileTreeLoadingPanel, BorderLayout.CENTER);
                        break;
                    case MODE_FILE_CHOOSER:
                        add(titleLabel, BorderLayout.WEST);
                        add(fileDisplayer, BorderLayout.CENTER);
                        add(fileSelectorButton, BorderLayout.EAST);
                }
            }
            updateSortFilter();
            repaint();
        }
    }

    @SuppressWarnings("unchecked")
    protected void updateSortFilter() {
        if (fileBrowsingMode == FileBrowsingMode.MODE_TREE) {
            Object item = sortComboBox.getSelectedItem();
            if (item instanceof Comparator<?>) {
                fileTreeModel.setComparator((Comparator<DefaultMutableTreeNode>) item);
            } else {
                fileTreeModel.setComparator(null);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Comparator<DefaultMutableTreeNode>[] getSortFilters() {
        Comparator<DefaultMutableTreeNode>[] filters = new Comparator[sortComboBox.getItemCount()];
        for (int i = 0; i < sortComboBox.getItemCount(); i++) {
            Object item = sortComboBox.getItemAt(i);
            if (item instanceof Comparator<?>) {
                filters[i] = (Comparator<DefaultMutableTreeNode>) item;
            } else {
                filters[i] = null;
            }
        }
        return filters;
    }

    public void setSelectedSortFilter(Comparator<DefaultMutableTreeNode> filter) {
        if (filter == null) {
            sortComboBox.setSelectedIndex(0);
        } else {
            sortComboBox.setSelectedItem(filter);
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if ((g != null) && (getComponentCount() == 0)) {
            Color previous = g.getColor();
            g.setColor(Color.RED);
            g.drawLine(getX(), getY(), getWidth(), getHeight());
            g.drawLine(getX(), getHeight(), getWidth(), getY());
            g.setColor(previous);
        }
    }

    public JTree getFileTree() {
        return fileTree;
    }

    public DefaultTreeModel getFileTreeModel() {
        return fileTreeModel;
    }

    public void setScrollBarsBackground(Color bg) {
        fileTreeScrollPane.getHorizontalScrollBar().setBackground(bg);
        fileTreeScrollPane.getVerticalScrollBar().setBackground(bg);
    }

    private void updateRootNode() {
        if (!fileTree.isExpanded(0)) {
            fileTree.expandRow(0);
        }
    }

    /**
     * Returns the path of the used directory
     * 
     * @return A {@link String}
     */
    public String getCurrentPath() {
        return fileManager.getWorkingPath();
    }

    /**
     * Sets the path of the directory to use
     * 
     * @param path The path to set
     */
    public void changePathTo(String path, boolean debug) {
        if (debug) {
            System.out.println(new StringBuilder("- ")
                    .append(new SimpleDateFormat(IDateConstants.US_DATE_FORMAT).format(new Date())).append(" ")
                    .append(getClass().getSimpleName()).append("changePathTo(").append(path).append(")"));
        }
        if (path != null) {
            final File directory = new File(path);
            if (directory.isDirectory()) {
                if (debug) {
                    System.out.println(new StringBuilder("- ")
                            .append(new SimpleDateFormat(IDateConstants.US_DATE_FORMAT).format(new Date())).append(" ")
                            .append(getClass().getSimpleName()).append(" '").append(path)
                            .append("' is a directory :-)"));
                }
                fileTreeLoadingPanel.setLoadingText(
                        String.format(messageManager.getMessage("fr.soleil.cdma.box.view.FileView.Loading"), path));
                fileTreeModel.getRoot().setUserObject(path);
                // Necessary to display "loading" animation
                if (SwingUtilities.isEventDispatchThread()) {
                    fileTreeModel.setFiles(null);
                }
                fileManager.setFile(directory);
                if (!ObjectUtils.sameObject(directory, fileSelector.getCurrentDirectory())) {
                    fileSelector.setCurrentDirectory(directory);
                }
                if (!ObjectUtils.sameObject(directory, pathChanger.getSelectedFile())) {
                    File parent = directory.getParentFile();
                    if ((parent != null) && (!ObjectUtils.sameObject(directory, pathChanger.getCurrentDirectory()))) {
                        pathChanger.setCurrentDirectory(parent);
                    }
                    pathChanger.setSelectedFile(directory);
                }
            } else if (debug) {
                System.out.println(new StringBuilder("- ")
                        .append(new SimpleDateFormat(IDateConstants.US_DATE_FORMAT).format(new Date())).append(" ")
                        .append(getClass().getSimpleName()).append(" '").append(path)
                        .append("' is not considered as a directory!!!!"));
            }
        }
    }

    /**
     * Sets the path of the directory to use
     * 
     * @param path The path to set
     */
    public void changePathTo(String path) {
        changePathTo(path, false);
    }

    /**
     * Cleans this file view
     */
    public void clean() {
        fileManager.stopRefreshing();
        fileTreeModel.setFiles(null);
    }

    /**
     * Returns the data source that handles the selected {@link File}
     * 
     * @return An {@link AbstractDataSource}
     */
    public AbstractDataSource<File> getFileSource() {
        return fileSource;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public boolean isSelected() {
        return fileTree.isEnabled();
    }

    @Override
    public void setSelected(boolean enabled) {
        fileTree.setEnabled(enabled);
    }

    public JFileChooser getPathChanger() {
        return this.pathChanger;
    }

    public static void main(String[] args) {
        final JFrame testFrame = new JFrame(FileView.class.getName());
        final FileView fileView = new FileView(null, System.getProperty("user.home"),
                new MessageManager("fr.soleil.cdma.box.image.resources"));
        JPanel testPanel = new JPanel(new BorderLayout());
        JLabel infoLabel = new JLabel("Click to change file browsing mode");
        infoLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                FileBrowsingMode fileBrowsingMode = fileView.getFileBrowsingMode();
                if (fileBrowsingMode == null) {
                    fileView.setFileBrowsingMode(FileBrowsingMode.MODE_TREE);
                } else {
                    switch (fileBrowsingMode) {
                        case MODE_TREE:
                            fileView.setFileBrowsingMode(FileBrowsingMode.MODE_FILE_CHOOSER);
                            break;
                        case MODE_FILE_CHOOSER:
                            fileView.setFileBrowsingMode(null);
                            break;
                    }
                }
                testFrame.pack();
                if (testFrame.getHeight() < 70) {
                    testFrame.setSize(testFrame.getWidth(), 70);
                }
            }
        });
        testPanel.add(fileView, BorderLayout.CENTER);
        testPanel.add(infoLabel, BorderLayout.SOUTH);
        testFrame.pack();
        if (testFrame.getHeight() < 70) {
            testFrame.setSize(testFrame.getWidth(), 70);
        }
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setContentPane(testPanel);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public enum FileBrowsingMode {
        MODE_TREE, MODE_FILE_CHOOSER
    }

    protected class LoggingFileTreeModel extends FileTreeModel {

        private static final long serialVersionUID = 8380840165088684306L;

        public LoggingFileTreeModel(String workingPath) {
            super(workingPath);
        }

        @Override
        public void setFiles(final File[] files) {
            if (files == null) {
                fileTreeLoadingPanel.setLoading(true);
            }
            try {
                super.setFiles(files);
            } catch (Exception e) {
                LoggerFactory.getLogger(applicationId).error(
                        messageManager.getMessage("fr.soleil.cdma.box.view.FileView.FileTreeModel.setFiles.Error"), e);
            }
            if (files != null) {
                fileTreeLoadingPanel.setLoading(false);
            }
        }

    }

}
