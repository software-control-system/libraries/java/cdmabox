/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.view.tree.node;

import java.io.File;

/**
 * A {@link File} {@link ObjectNode}
 * 
 * @author girardot
 */
public class FileNode extends ObjectNode<File> {

    private static final long serialVersionUID = -3572389912596434286L;

    public FileNode() {
        super();
    }

    public FileNode(File data) {
        super(data);
        if (data != null) {
            setUserObject(data.getName());
        }
    }

}
