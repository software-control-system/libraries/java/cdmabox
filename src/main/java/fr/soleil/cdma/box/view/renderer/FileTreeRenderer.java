/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.view.renderer;

import java.awt.Component;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;

import fr.soleil.cdma.box.view.tree.node.FileNode;
import fr.soleil.lib.project.file.FileUtils;

/**
 * A {@link DefaultTreeCellRenderer} for file tree
 * 
 * @author girardot
 */
public class FileTreeRenderer extends FullIconTreeCellRender {

    private static final long serialVersionUID = 7255839505213346843L;

    public static final String NEXUS_EXTENSION = "nxs";
    public static final String EDF_EXTENSION = "edf";

    protected static final Icon ROOT_ICON = new ImageIcon(
            FileTreeRenderer.class.getResource("/org/tango-project/tango-icon-theme/16x16/places/folder.png"));
    protected static final Icon ROOT_ICON_EXPANDED = new ImageIcon(FileTreeRenderer.class
            .getResource("/org/tango-project/tango-icon-theme/16x16/status/folder-drag-accept.png"));
    protected static final Icon NEXUS_ICON = new ImageIcon(
            FileTreeRenderer.class.getResource("/fr/soleil/cdma/box/icons/nexusIcon.png"));
    protected static final Icon NEXUS_ICON_EXPANDED = new ImageIcon(
            FileTreeRenderer.class.getResource("/fr/soleil/cdma/box/icons/nexusIconExpanded.png"));
    protected static final Icon EDF_ICON = new ImageIcon(
            FileTreeRenderer.class.getResource("/fr/soleil/cdma/box/icons/edf-folder.png"));
    protected static final Icon EDF_ICON_EXPANDED = new ImageIcon(
            FileTreeRenderer.class.getResource("/fr/soleil/cdma/box/icons/edf-folder-open.png"));
    protected static final Icon FILE_ICON = new ImageIcon(
            FileTreeRenderer.class.getResource("/com/famfamfam/silk/book.png"));
    protected static final Icon FILE_ICON_EXPANDED = new ImageIcon(
            FileTreeRenderer.class.getResource("/com/famfamfam/silk/book_open.png"));
    protected static final Icon FILE_ICON_ERROR = new ImageIcon(
            FileTreeRenderer.class.getResource("/com/famfamfam/silk/book_error.png"));

    // Border used to keep a margin between each node
    protected static final Border MARGIN_BORDER = new EmptyBorder(1, 1, 1, 1);

    public FileTreeRenderer() {
        super();
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf,
            int row, boolean hasFocus) {
        JLabel component = (JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        component.setBorder(MARGIN_BORDER);
        String tooltip = component.getText();
        if (tree != null) {
            TreePath path = tree.getPathForRow(row);
            if (path != null) {
                Object node = path.getLastPathComponent();
                if (node instanceof FileNode) {
                    File file = ((FileNode) node).getData();
                    if (isNexusFile(file)) {
                        if (expanded) {
                            setComponentIcon(component, NEXUS_ICON_EXPANDED);
                        } else {
                            setComponentIcon(component, NEXUS_ICON);
                        }
                    } else if (isEDFDirectory(file)) {
                        if (expanded) {
                            setComponentIcon(component, EDF_ICON_EXPANDED);
                        } else {
                            setComponentIcon(component, EDF_ICON);
                        }
                    } else {
                        if (expanded) {
                            setComponentIcon(component, FILE_ICON_EXPANDED);
                        } else {
                            setComponentIcon(component, FILE_ICON);
                        }
                    }
                } else if (node instanceof DefaultMutableTreeNode) {
                    if (((DefaultMutableTreeNode) node).isRoot()) {
                        if (expanded) {
                            setComponentIcon(component, ROOT_ICON_EXPANDED);
                        } else {
                            setComponentIcon(component, ROOT_ICON);
                        }
                    }
                }
                node = null;
            }
            path = null;
        }
        component.setToolTipText(tooltip);
        tree.setToolTipText(component.getToolTipText());
        return component;
    }

    /**
     * Returns whether a file is a Nexus file
     * 
     * @param file The {@link File} to test
     * @return A boolean value
     */
    protected static boolean isNexusFile(File file) {
        return NEXUS_EXTENSION.equals(FileUtils.getExtension(file));
    }

    /**
     * Returns whether a file is a Nexus file
     * 
     * @param path The path of the file to test
     * @return A boolean value
     */
    protected static boolean isNexusFile(String path) {
        boolean isNexus = false;
        if (path != null) {
            isNexus = isNexusFile(new File(path));
        }
        return isNexus;
    }

    /**
     * Returns whether a file is a EDF file
     * 
     * @param file The {@link File} to test
     * @return A boolean value
     */
    public static boolean isEDFFile(File file) {
        return EDF_EXTENSION.equals(FileUtils.getExtension(file));
    }

    /**
     * Returns whether a file is a EDF file
     * 
     * @param path The path of the file to test
     * @return A boolean value
     */
    protected static boolean isEDFFile(String path) {
        boolean isEDF = false;
        if (path != null) {
            isEDF = isEDFFile(new File(path));
        }
        return isEDF;
    }

    /**
     * Returns whether a {@link File} is an EDF files directory
     * 
     * @param file The {@link File} to test
     * @return A boolean value
     */
    protected static boolean isEDFDirectory(File file) {
        boolean isEDF = false;
        if ((file != null) && (file.isDirectory())) {
            for (File child : file.listFiles()) {
                if (isEDFFile(child)) {
                    isEDF = true;
                    break;
                }
            }
        }
        return isEDF;
    }

    /**
     * Returns whether a directory is an EDF files directory
     * 
     * @param path The path of the directory to test
     * @return A boolean value
     */
    protected static boolean isEDFDirectory(String path) {
        boolean isEDF = false;
        if (path != null) {
            isEDF = isEDFDirectory(new File(path));
        }
        return isEDF;
    }

}
