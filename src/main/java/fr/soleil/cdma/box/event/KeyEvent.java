package fr.soleil.cdma.box.event;

import java.util.EventObject;

import fr.soleil.cdma.box.listener.IKeyListener;
import fr.soleil.cdma.box.manager.IKeyManager;

/**
 * An {@link EventObject} used to notify {@link IKeyListener}s for some changes in the keys of an {@link IKeyManager}
 * 
 * @author GIRARDOT
 */
public class KeyEvent extends EventObject {

    private static final long serialVersionUID = -4516938712267398318L;

    public KeyEvent(IKeyManager source) {
        super(source);
    }

    @Override
    public IKeyManager getSource() {
        return (IKeyManager) super.getSource();
    }

}
