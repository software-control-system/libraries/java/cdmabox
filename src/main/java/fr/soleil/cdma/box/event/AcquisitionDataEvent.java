/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.event;

import java.util.EventObject;

import fr.soleil.cdma.box.manager.IAcquisitionDataManager;

/**
 * An {@link EventObject} to warn for some changes about an {@link IAcquisitionDataManager}
 * 
 * @author girardot
 */
public class AcquisitionDataEvent extends EventObject {

    private static final long serialVersionUID = 4611107371715143378L;

    private final String[] properties;
    private final boolean fromScan;

    /**
     * Constructor
     * 
     * @param source The {@link IAcquisitionDataManager} that generated this event
     * @param properties The concerned properties
     */
    public AcquisitionDataEvent(IAcquisitionDataManager source, boolean fromScan, String... properties) {
        super(source);
        this.fromScan = fromScan;
        this.properties = properties;
    }

    @Override
    public IAcquisitionDataManager getSource() {
        return (IAcquisitionDataManager) super.getSource();
    }

    /**
     * Returns the properties that were updated
     * 
     * @return A {@link String} array. If this array <code>null</code> or with length = 0, this means all the properties
     *         changed.
     */
    public String[] getProperties() {
        return properties;
    }

    public boolean isFromScan() {
        return fromScan;
    }

}
