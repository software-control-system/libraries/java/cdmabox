/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.reader;

import org.cdma.dictionary.LogicalGroup;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IKey;

import fr.soleil.cdma.box.data.ContextDataItem;
import fr.soleil.cdma.box.data.LoadableData;
import fr.soleil.cdma.box.data.acquisition.Acquisition;
import fr.soleil.cdma.box.data.sensor.ExperimentConfig;
import fr.soleil.cdma.box.exception.CDMAAccessException;
import fr.soleil.cdma.box.util.CDMAConstants;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.data.FlatData;
import fr.soleil.lib.project.data.FlatDoubleData;

/**
 * An interface used to access data by using CDMA. An {@link ICDMAReader} is expected to be an {@link ICancelable}, so
 * that user may cancel data loading actions
 * 
 * @author girardot
 */
public interface ICDMAReader extends CDMAConstants, ICancelable {

    /**
     * Recovers an {@link IDataItem} from a {@link ContextDataItem}, updating it when necessary.
     * 
     * @param ctxDataItem The {@link ContextDataItem}
     * @return An {@link IDataItem}
     */
    public IDataItem getDataItem(ContextDataItem ctxDataItem);

    /**
     * Recovers an {@link IDataItem} and reads its array, and returns it as a single dimension array
     * 
     * @param ctxDataItem context data
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A single dimension array (example: <code>double[]</code>)
     */
    public Object readCDMSimpleArrayWithCtx(ContextDataItem ctxDataItem, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its array, and returns it as a single dimension array
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A single dimension array (example: <code>double[]</code>)
     */
    public Object readCDMSimpleArray(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its array, and returns it as a multi dimension array
     * 
     * @param ctxDataItem context data
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A multi dimension array (example: <code>double[][]</code>)
     */
    public Object readCDMComplexArrayWithCtx(ContextDataItem ctxDataItem, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its array, and returns it as a multi dimension array
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A multi dimension array (example: <code>double[][]</code>)
     */
    public Object readCDMComplexArray(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem}, reads its array, and returns it as a {@link FlatData}
     * 
     * @param ctxDataItem context data
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A {@link FlatData}
     */
    public FlatData readCDMFlatComplexArrayWithCtx(ContextDataItem ctxDataItem, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem}, reads its array, consider it as a block of data, and returns it as a
     * {@link FlatData} array
     * 
     * @param ctxDataItem context data
     * @param origin The origin of the data block to read
     * @param resultShape The shape of the data to read
     * @param blockSize The number of data in the block
     * @return A {@link FlatData} array
     */
    public FlatData[] readCDMFlatComplexArrayBlockWithCtx(ContextDataItem ctxDataItem, int[] origin, int[] resultShape,
            int blockSize);

    /**
     * Recovers an {@link IDataItem}, reads its array, and returns it as a {@link FlatData}
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A {@link FlatData}
     */
    public FlatData readCDMFlatComplexArray(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem}, reads its array, consider it as a block of data, and returns it as a
     * {@link FlatData} array
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data block to read
     * @param resultShape The shape of the data to read
     * @param blockSize The number of data in the block
     * @return A {@link FlatData} array
     */
    public FlatData[] readCDMFlatComplexArrayBlock(LogicalGroup parent, IKey key, int[] origin, int[] resultShape,
            int blockSize);

    /**
     * Recovers an {@link IDataItem} and reads its <code>byte</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A <code>byte</code>
     */
    public byte readByte(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its <code>byte</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A {@link Byte}
     */
    public Byte readByteObject(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its <code>short</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A <code>short</code>
     */
    public short readShort(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its <code>short</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A {@link Short}
     */
    public Short readShortObject(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its <code>int</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return An <code>int</code>
     */
    public int readInt(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its <code>int</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A {@link Integer}
     */
    public Integer readInteger(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its <code>long</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A <code>long</code>
     */
    public long readLong(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its <code>long</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A {@link Long}
     */
    public Long readLongObject(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its <code>float</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A <code>float</code>
     */
    public float readFloat(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its <code>double</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A <code>double</code>
     */
    public double readDouble(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Recovers an {@link IDataItem} and reads its {@link String} data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A {@link String}
     */
    public String readString(LogicalGroup parent, IKey key, int[] origin, int[] shape);

    /**
     * Reads an {@link IDataItem}'s {@link String} data at a given position
     * 
     * @param item The {@link IDataItem}
     * @param origin The origin of the data to read
     * @param shape The shape of the data to read
     * @return A {@link String}
     */
    public String readString(IDataItem item, int[] origin, int[] shape);

    /**
     * Reads a {@link LogicalGroup} to recover an intensity for a particular intensity monitor in a particular scan
     * 
     * @param im The intensity monitor name
     * @param monitorGroup The {@link LogicalGroup} that should be able to recover the desired
     *            intensity
     * @return A <code>double[]</code>
     */
    public double[] getIntensityHistorised(String im, LogicalGroup monitorGroup);

    /**
     * Reads a {@link LogicalGroup} to recover some data with a given name in a particular scan
     * 
     * @param name The data name
     * @param dataGroup The {@link LogicalGroup} that should be able to recover the desired data
     * @return A {@link FlatDoubleData}
     */
    public FlatDoubleData getNamedData(String name, LogicalGroup dataGroup);

    /**
     * Reads a {@link LogicalGroup} to recover the shape of some data with a given name in a particular scan
     * 
     * @param name The data name
     * @param dataGroup The {@link LogicalGroup} that should be able to recover the desired data
     * @return An <code>int[]</code>.
     * @see #getNamedData(String, LogicalGroup)
     */
    public int[] getNamedDataShape(String name, LogicalGroup dataGroup);

    /**
     * Reads a {@link LogicalGroup} to recover a gain for a particular intensity monitor in a particular scan
     * 
     * @param im The intensity monitor name
     * @param monitorGroup The {@link LogicalGroup} that should be able to recover the desired gain
     * @return A <code>double</code>
     */
    public double getGain(String im, LogicalGroup monitorGroup);

    /**
     * Reads a {@link LogicalGroup} to recover a bias for a particular intensity monitor in a particular scan
     * 
     * @param im The intensity monitor name
     * @param monitorGroup The {@link LogicalGroup} that should be able to recover the desired bias
     * @return A <code>double</code>
     */
    public double getBias(String im, LogicalGroup monitorGroup);

    /**
     * Recovers an {@link Acquisition} from a detector
     * 
     * @param detector The concerned detector
     * @param dataToLoad The optional data to load
     * @throws CDMAAccessException If a problem occurred during acquisition loading
     */
    public void loadAcquisition(String detector, LoadableData... dataToLoad) throws CDMAAccessException;

    /**
     * Returns the {@link AbstractDataSource} that contains the loaded {@link ExperimentConfig}
     * 
     * @return An {@link AbstractDataSource}
     */
    public AbstractDataSource<ExperimentConfig> getExperimentConfigSource();

    /**
     * Returns the {@link AbstractDataSource} that contains the loaded {@link Acquisition}
     * 
     * @return An {@link AbstractDataSource}
     */
    public AbstractDataSource<Acquisition> getAcquisitionSource();

    /**
     * Returns the x keys
     * 
     * @param reader The {@link IDataset} from which to recover x keys
     * @return A {@link String} array. Can be <code>null</code>.
     */
    public String[] getXKeys(IDataset reader);
}
