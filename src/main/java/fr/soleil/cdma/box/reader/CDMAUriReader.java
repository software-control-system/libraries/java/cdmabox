/***********************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.reader;

import java.net.URI;

import org.cdma.IFactory;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IGroup;

import fr.soleil.cdma.box.target.IUriTarget;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;

public class CDMAUriReader extends AbstractCDMAReader implements IUriTarget, ITextTarget {

    private URI uri;
    private String uriToString;
    private String uriName;
    private String uriSimpleName;
    private volatile boolean applyChangeOnNameChange;
    private volatile boolean uriDidChange;

    public CDMAUriReader(String applicationId) {
        super(applicationId);
        uri = null;
        uriToString = null;
        uriName = null;
        uriSimpleName = null;
        applyChangeOnNameChange = false;
        uriDidChange = true;
    }

    @Override
    protected boolean uriChanged() {
        return uriDidChange;
    }

    @Override
    protected void setUriUnchanged() {
        uriDidChange = false;
    }

    public boolean isApplyChangeOnNameChange() {
        return applyChangeOnNameChange;
    }

    public void setApplyChangeOnNameChange(boolean applyChangeOnNameChange) {
        this.applyChangeOnNameChange = applyChangeOnNameChange;
    }

    @Override
    protected String getUriErrorMessage() {
        return "URI access error";
    }

    @Override
    protected String getReaderError() {
        return "Open URI Error";
    }

    @Override
    protected String getCloseUriErrorMessage() {
        return "Close URI Error";
    }

    @Override
    protected String getLogicalRootError() {
        return "Failed to access to root for URI " + uri;
    }

    @Override
    protected String getGroupError(String group) {
        return "Failed to access to group '" + group + "' for URI " + uri;
    }

    @Override
    protected String getAcquisitionOrigin() {
        String result;
        if (uriName == null) {
            if (uri == null) {
                result = null;
            } else {
                result = uri.toString();
            }
        } else {
            result = uriName;
        }
        return result;
    }

    @Override
    public String getUriToString() {
        return uriToString;
    }

    @Override
    protected String getAcquisitionSimpleName() {
        return uriSimpleName;
    }

    @Override
    protected void updateReader() {
        super.updateReader();
        if (reader == null) {
            uriSimpleName = null;
        } else {
            String name;
            LogicalGroup root = reader.getLogicalRoot(getDictionaryView());
            if (root == null) {
                name = null;
            } else {
                LogicalGroup scanGroup = root.getGroup(ICDMAReader.SCAN_GROUP);
                if (scanGroup == null) {
                    name = null;
                } else {
                    try {
                        IFactory factory = getFactory();
                        name = checkVirtualData(
                                readString(scanGroup, factory.createKey(ICDMAReader.SCAN_KEY), null, null));
                    } catch (Exception e) {
                        e.printStackTrace();
                        IDataItem item = scanGroup.getDataItem(ICDMAReader.SCAN_KEY);
                        if (item == null) {
                            name = null;
                        } else {
                            try {
                                name = item.getShortName();
                            } catch (Exception e2) {
                                e2.printStackTrace();
                                name = null;
                            }
                        }
                    }
                }
            }
            if ((name == null) || (name.trim().isEmpty())) {
                IGroup grp = reader.getRootGroup();
                if (grp == null) {
                    name = null;
                } else {
                    name = grp.getName();
                    if ((name != null) && name.startsWith("/")) {
                        name = name.substring(1);
                    }
                }
            }
            uriSimpleName = name;
        }
    }

    @Override
    public URI getUri() {
        return uri;
    }

    @Override
    public void setUri(URI uri) {
        URI oldURi = this.uri;
        this.uri = uri;
        uriToString = (uri == null ? null : uri.toString());
        // Update "uriDidChange" only if it was false.
        // Otherwise, it could provoke a data update bug (JAVAAPI-561)
        if (!uriDidChange) {
            uriDidChange = (!ObjectUtils.sameObject(oldURi, uri));
        }
        if (!applyChangeOnNameChange) {
            updateReader();
        }
    }

    @Override
    protected String getLockError() {
        return "Change URI Error";
    }

    @Override
    public String getText() {
        return uriSimpleName;
    }

    @Override
    public void setText(String text) {
        // here, we check for strict equality, as 2 different experiences may share the same name
        if (text != uriName) {
            uriName = text;
            if (applyChangeOnNameChange) {
                updateReader();
            }
        }
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

}
