/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.reader;

import java.io.File;
import java.net.URI;

import fr.soleil.cdma.box.manager.IFileManager;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.lib.project.ObjectUtils;

/**
 * An {@link AbstractCDMAReader} that uses files to access CDMA
 * 
 * @author girardot
 */
public class CDMAFileReader extends AbstractCDMAReader implements IFileManager {

    private String workingPath;
    private volatile boolean uriChanged;

    public CDMAFileReader(String applicationId) {
        super(applicationId);
        workingPath = null;
        uriChanged = true;
    }

    @Override
    protected boolean uriChanged() {
        return uriChanged;
    }

    @Override
    protected void setUriUnchanged() {
        uriChanged = false;
    }

    @Override
    protected String getUriErrorMessage() {
        return "Failed to access file " + workingPath;
    }

    @Override
    protected String getReaderError() {
        return "Failed to open file " + workingPath;
    }

    @Override
    protected String getCloseUriErrorMessage() {
        return "Failed to close file " + workingPath;
    }

    @Override
    protected String getLogicalRootError() {
        return "Failed to access to root in file " + workingPath;
    }

    @Override
    protected String getGroupError(String group) {
        return "Failed to access to group '" + group + "' for file " + workingPath;
    }

    @Override
    protected String getAcquisitionSimpleName() {
        return getWorkingPath();
    }

    @Override
    protected String getAcquisitionOrigin() {
        return getWorkingPath();
    }

    @Override
    protected URI getUri() throws Exception {
        URI uri;
        if ((workingPath == null) || workingPath.trim().isEmpty()) {
            uri = null;
        } else {
            uri = new File(workingPath).toURI();
        }
        return uri;
    }

    /**
     * Returns the path of the last selected file
     * 
     * @return A {@link String}
     */
    @Override
    public String getWorkingPath() {
        return workingPath;
    }

    @Override
    public void setFile(File file) {
        String path = (file == null ? null : file.getAbsolutePath());
        if (!ObjectUtils.sameObject(path, this.workingPath)) {
            this.workingPath = path;
            uriChanged = true;
            updateReader();
        }
    }

    @Override
    protected String getLockError() {
        return "Change File Error";
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // not managed
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // not managed
    }

}
