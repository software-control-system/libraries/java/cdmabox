/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.cdma.box.reader;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

import org.cdma.Factory;
import org.cdma.IFactory;
import org.cdma.dictionary.ExtendedDictionary;
import org.cdma.dictionary.LogicalGroup;
import org.cdma.dictionary.filter.FilterAttribute;
import org.cdma.dictionary.filter.IFilter;
import org.cdma.exception.CDMAException;
import org.cdma.exception.DataAccessException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IAttribute;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IKey;
import org.cdma.interfaces.ISliceIterator;
import org.cdma.utilities.CDMATool;
import org.cdma.utils.IArrayUtils;
import org.cdma.utils.MetricsUtils;
import org.slf4j.LoggerFactory;

import fr.soleil.cdma.box.data.ContextDataItem;
import fr.soleil.cdma.box.data.ImageStack;
import fr.soleil.cdma.box.data.LoadableData;
import fr.soleil.cdma.box.data.SoftData;
import fr.soleil.cdma.box.data.SoftDoubleArray;
import fr.soleil.cdma.box.data.SoftDoubleMatrix;
import fr.soleil.cdma.box.data.SoftFlatDoubleArray;
import fr.soleil.cdma.box.data.SoftIndexedData;
import fr.soleil.cdma.box.data.SoftIndexedDoubleArray;
import fr.soleil.cdma.box.data.SpectrumStack;
import fr.soleil.cdma.box.data.acquisition.Acquisition;
import fr.soleil.cdma.box.data.scan.ScanData;
import fr.soleil.cdma.box.data.scan.ScanData.Dimension;
import fr.soleil.cdma.box.data.sensor.ExperimentConfig;
import fr.soleil.cdma.box.exception.CDMAAccessException;
import fr.soleil.cdma.box.manager.BasicAcquisitionDataManager;
import fr.soleil.cdma.box.util.PositionKey;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.source.BasicDataSource;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.SystemUtils;
import fr.soleil.lib.project.data.FlatData;
import fr.soleil.lib.project.data.FlatDoubleData;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.NumberArrayUtils;
import fr.soleil.lib.project.math.NumberMatrixUtils;

/**
 * Basic implementation of {@link ICDMAReader}
 * 
 * @author girardot
 */
public abstract class AbstractCDMAReader implements ICDMAReader {

    protected static final String COULD_NOT_READ_BYTE = "Could not read byte";
    protected static final String COULD_NOT_READ_SHORT = "Could not read short";
    protected static final String COULD_NOT_READ_INT = "Could not read int";
    protected static final String COULD_NOT_READ_LONG = "Could not read long";
    protected static final String COULD_NOT_READ_FLOAT = "Could not read float";
    protected static final String COULD_NOT_READ_DOUBLE = "Could not read double";
    protected static final String COULD_NOT_READ_VARIABLE_DOUBLE = "Could not read variable double";
    protected static final String COULD_NOT_READ_VARIABLE_FLOAT = "Could not read variable float";
    protected static final String COULD_NOT_READ_STRING = "Could not read String";
    protected static final String EMPTY = ObjectUtils.EMPTY_STRING;
    protected static final String UNKNOWN = "UNKNOWN";
    protected static final boolean LOAD_FULL_SPECTRUM_STACK = "true"
            .equalsIgnoreCase(System.getProperty("LoadAllSpectra", ObjectUtils.EMPTY_STRING));
    protected static final String SCIENTA_PATTERN = SCIENTA_DETECTOR + "Att\\d+";

    private static final AtomicLong CHECK_OPEN_TIME = new AtomicLong(0);
    private static final AtomicLong ITEM_RECOVERING_TIME = new AtomicLong(0);
    private static final AtomicLong GET_DATA_TIME = new AtomicLong(0);
    private static final AtomicLong EXTRACT_TIME = new AtomicLong(0);
    private static final AtomicLong COPY_TIME = new AtomicLong(0);
    private static final AtomicLong UNISGNED_TIME = new AtomicLong(0);

    public static void resetMetrics() {
        CHECK_OPEN_TIME.set(0);
        ITEM_RECOVERING_TIME.set(0);
        GET_DATA_TIME.set(0);
        EXTRACT_TIME.set(0);
        COPY_TIME.set(0);
        UNISGNED_TIME.set(0);
    }

    public static void traceMetrics(String applicationId) {
        StringBuilder builder = new StringBuilder(AbstractCDMAReader.class.getSimpleName());
        DateUtil.elapsedTimeToStringBuilder("\nAbstractCDMAReader Cumulative dataset check/open time: ",
                CHECK_OPEN_TIME, builder);
        DateUtil.elapsedTimeToStringBuilder("\nAbstractCDMAReader Cumulative item recovering time: ",
                ITEM_RECOVERING_TIME, builder);
        DateUtil.elapsedTimeToStringBuilder("\nAbstractCDMAReader Cumulative item getData time: ", GET_DATA_TIME,
                builder);
        DateUtil.elapsedTimeToStringBuilder("\nAbstractCDMAReader Cumulative array value extraction time: ",
                EXTRACT_TIME, builder);
        DateUtil.elapsedTimeToStringBuilder("\nAbstractCDMAReader Cumulative copy to 1D array time: ", COPY_TIME,
                builder);
        DateUtil.elapsedTimeToStringBuilder("\nAbstractCDMAReader Cumulative unsigned array transformation time: ",
                UNISGNED_TIME, builder);
        LoggerFactory.getLogger(applicationId).trace(builder.toString());
    }

    protected final CDMATool cdmaTool;
    protected final BasicDataSource<ExperimentConfig> experimentConfigSource;
    protected final BasicDataSource<Acquisition> acquisitionSource;
    protected IDataset reader;
    protected volatile boolean canceled;
    protected String dictionaryView;
    protected String mandatoryDictionary;
    protected String mandatoryDictionaryPackage;
    protected final String applicationId;

    public AbstractCDMAReader(String applicationId) {
        if (applicationId == null) {
            this.applicationId = Mediator.LOGGER_ACCESS;
        } else {
            this.applicationId = applicationId;
        }
        dictionaryView = DEFAULT_DICTIONARY_VIEW;
        mandatoryDictionary = DEFAULT_MANDATORY_DICTIONARY;
        mandatoryDictionaryPackage = DEFAULT_MANDATORY_DICTIONARY_PACKAGE;
        cdmaTool = new CDMATool(SystemUtils.getSystemProperty(COMMON_RESOURCES_PROPERTY));
        experimentConfigSource = new BasicDataSource<>(null);
        experimentConfigSource.setIgnoreDuplicationTest(true);
        acquisitionSource = new BasicDataSource<>(null);
        acquisitionSource.setIgnoreDuplicationTest(true);
        reader = null;
        canceled = false;
    }

    protected abstract boolean uriChanged();

    protected abstract void setUriUnchanged();

    /**
     * Returns the {@link CDMATool} used by this {@link AbstractCDMAReader}
     * 
     * @return A {@link CDMATool}
     */
    public CDMATool getCdmaTool() {
        return cdmaTool;
    }

    /**
     * @return the dictionaryView
     */
    public String getDictionaryView() {
        return dictionaryView;
    }

    /**
     * @param dictionaryView
     *            the dictionaryView to set
     */
    public void setDictionaryView(String dictionaryView) {
        this.dictionaryView = (dictionaryView == null || dictionaryView.trim().isEmpty() ? DEFAULT_DICTIONARY_VIEW
                : dictionaryView);
    }

    /**
     * @return the mandatoryDictionary
     */
    public String getMandatoryDictionary() {
        return mandatoryDictionary;
    }

    /**
     * @param mandatoryDictionary the mandatoryDictionary to set
     */
    public void setMandatoryDictionary(String mandatoryDictionary) {
        this.mandatoryDictionary = (mandatoryDictionary == null || mandatoryDictionary.trim().isEmpty()
                ? DEFAULT_MANDATORY_DICTIONARY
                : mandatoryDictionary);
    }

    /**
     * @return the mandatoryDictionaryPackage
     */
    public String getMandatoryDictionaryPackage() {
        return mandatoryDictionaryPackage;
    }

    /**
     * @param mandatoryDictionaryPackage the mandatoryDictionaryPackage to set
     */
    public void setMandatoryDictionaryPackage(String mandatoryDictionaryPackage) {
        this.mandatoryDictionaryPackage = (mandatoryDictionaryPackage == null
                || mandatoryDictionaryPackage.trim().isEmpty() ? DEFAULT_MANDATORY_DICTIONARY_PACKAGE
                        : mandatoryDictionaryPackage);
    }

    /**
     * Recovers the {@link IFactory} to use
     * 
     * @return An {@link IFactory}
     */
    protected IFactory getFactory() {
        IFactory factory;
        Exception uriError = null;
        URI uri;
        try {
            uri = getUri();
        } catch (Exception e) {
            uri = null;
            uriError = e;
        }
        if (uri == null) {
            factory = null;
        } else {
            try {
                factory = Factory.getFactory(uri);
            } catch (Exception e) {
                factory = null;
                uriError = e;
            }
        }
        if (factory == null) {
            if (uriError == null) {
                LoggerFactory.getLogger(applicationId).error(getUriErrorMessage());
            } else {
                LoggerFactory.getLogger(applicationId).error(getUriErrorMessage(), uriError);
            }
        }
        return factory;
    }

    /**
     * Returns the {@link URI} this {@link AbstractCDMAReader} is expected to access to
     * 
     * @return An {@link URI}
     * @throws Exception If a problem occured while trying to solve the {@link URI}
     */
    protected abstract URI getUri() throws Exception;

    /**
     * Returns the error message to display when there is an {@link URI} access problem
     * 
     * @return A {@link String}
     */
    protected abstract String getUriErrorMessage();

    protected boolean shouldChangeReader(IDataset reader) {
        return (reader == null) || reader.isDead() || uriChanged();
    }

    protected IFactory updateReaderAccordingToFactoy(IFactory factory) {
        IDataset reader = this.reader;
        if (shouldChangeReader(reader)) {
            synchronized (this) {
                reader = this.reader;
                if (shouldChangeReader(reader)) {
                    try {
                        closeReader();
                        if (factory == null) {
                            reader = null;
                        } else {
                            reader = factory.createDatasetInstance(getUri());
                        }
                    } catch (Exception ex) {
                        LoggerFactory.getLogger(applicationId).error(getReaderError(), ex);
                        reader = null;
                    }
                }
                this.reader = reader;
                setUriUnchanged();
            }
        }
        return factory;
    }

    /**
     * Updates the {@link URI} reader to have it access the current {@link URI}
     */
    protected void updateReader() {
        loadExperimentConfigNoLock(getFactory());
    }

    /**
     * Returns the error message displayed when trying to update reader to new {@link URI}
     * 
     * @return A {@link String}
     */
    protected abstract String getReaderError();

    /**
     * Returns the error message displayed when trying to lock before updating reader to new {@link URI}
     * 
     * @return A {@link String}
     */
    protected abstract String getLockError();

    /**
     * Ensures that the right dictionary is used in CDMA
     */
    protected void checkDictionary() {
        cdmaTool.checkDictionaryFolder();
        String view = getDictionaryView();
        if (!CDMATool.hasDictionary(view)) {
            synchronized (Factory.class) {
                if (!CDMATool.hasDictionary(view)) {
                    try {
                        CDMATool.createDictionary(view, getClass()
                                .getResource(getMandatoryDictionaryPackage() + getMandatoryDictionary()).openStream());
                    } catch (IOException e) {
                        // It should not happen
                        LoggerFactory.getLogger(applicationId).error("Failed to create dictionary " + view, e);
                    }
                }
            }
        }
    }

    protected abstract String getLogicalRootError();

    protected abstract String getGroupError(String group);

    protected String getStringAttribute(IDataItem item, String attributeName, String defaultValue) {
        String result;
        if ((item == null) || (attributeName == null)) {
            result = defaultValue;
        } else {
            IAttribute attribute = item.getAttribute(attributeName);
            if (attribute == null) {
                result = defaultValue;
            } else {
                try {
                    result = attribute.getStringValue();
                } catch (Exception e) {
                    result = defaultValue;
                }
            }
        }
        return result;
    }

    /**
     * Recovers an {@link ExperimentConfig} without using any lock, and updates {@link ExperimentConfig}
     * {@link AbstractDataSource}
     * 
     * @param factory The {@link IFactory} that is able to access to CDMA plug-in dictionary
     * @see #getExperimentConfigSource()
     */
    protected void loadExperimentConfigNoLock(IFactory factory) {
        updateReaderAccordingToFactoy(factory);
        ExperimentConfig result = new ExperimentConfig();
        if ((factory != null) && (reader != null)) {
            try {
                if (!reader.isOpen()) {
                    reader.open();
                }
                try {
                    LogicalGroup logicalRoot = reader.getLogicalRoot(dictionaryView);
                    if (logicalRoot == null) {
                        LoggerFactory.getLogger(applicationId).error(getLogicalRootError());
                    } else {
                        StringBuilder traceBuilder = new StringBuilder(
                                getAcquisitionOrigin() + " mapping dictionary: ");
                        try {
                            if (logicalRoot.getDictionary() == null) {
                                traceBuilder.append("None");
                            } else {
                                traceBuilder.append(logicalRoot.getDictionary().getMappingFilePath());
                            }
                            LoggerFactory.getLogger(applicationId).info(traceBuilder.toString());
                        } catch (Exception e) {
                            traceBuilder.append("not found (").append(e.getMessage()).append(")");
                            LoggerFactory.getLogger(applicationId).info(traceBuilder.toString(), e);
                        }
                        LogicalGroup acquisitionGroup = logicalRoot.getGroup(SCAN_GROUP);
                        if (acquisitionGroup == null) {
                            LoggerFactory.getLogger(applicationId).error(getGroupError(SCAN_GROUP));
                        } else {
                            LogicalGroup detectorGroup = acquisitionGroup.getGroup(DETECTOR_GROUP);
                            if (detectorGroup == null) {
                                LoggerFactory.getLogger(applicationId).error(getGroupError(DETECTOR_GROUP));
                            } else {
                                IKey cameraKey = factory.createKey(CAMERA_KEY);
                                List<IDataItem> cameras = detectorGroup.getDataItemList(cameraKey);
                                for (IDataItem camera : cameras) {
                                    String cameraName = getStringAttribute(camera, ATTR_EQUIPMENT_NAME, null);
                                    if ((cameraName != null) && (!result.hasDetector(cameraName))) {
                                        result.addDetector(cameraName);
                                    }
                                }
                                cameras.clear();
                            }
                        }
                    }
                } catch (Exception e) {
                    result = new ExperimentConfig();
                    LoggerFactory.getLogger(applicationId).error("Dataset opening error", e);
                }
            } catch (DataAccessException dae) {
                LoggerFactory.getLogger(applicationId).error("Dataset opening error: file access", dae);
            }
        }
        try {
            experimentConfigSource.setData(result);
        } catch (Exception e) {
            LoggerFactory.getLogger(applicationId).error("Could not set experimentConfigSource", e);
        }
    }

    // XXX hack to avoid first "/" from nexus files
    protected String checkVirtualData(final String value) {
        String result = value;
        if ((result != null) && (!result.isEmpty()) && (result.charAt(0) == '/')) {
            result = result.substring(1);
        }
        return result;
    }

    @Override
    public void loadAcquisition(String detector, LoadableData... dataToLoad) throws CDMAAccessException {
        List<LoadableData> toLoad;
        if (dataToLoad == null) {
            toLoad = new ArrayList<>();
        } else {
            toLoad = Arrays.asList(dataToLoad);
        }
        canceled = false;
        Acquisition acquisition = new Acquisition(getAcquisitionOrigin(), getAcquisitionSimpleName(), detector);
        checkDictionary();
        if (!isCanceled()) {
            IDataset reader = this.reader;
            IFactory factory = getFactory();
            if ((reader != null) && (factory != null)) {
                try {
                    updateReaderAccordingToFactoy(factory);
                    reader = this.reader;
                    if (!reader.isOpen()) {
                        reader.open();
                    }
                    try {
                        LogicalGroup logicalRoot = reader.getLogicalRoot(dictionaryView);
                        if (logicalRoot == null) {
                            LoggerFactory.getLogger(applicationId).error(getLogicalRootError());
                        } else {
                            LogicalGroup scanGroup = logicalRoot.getGroup(SCAN_GROUP);
                            if (scanGroup == null) {
                                LoggerFactory.getLogger(applicationId).error(getGroupError(SCAN_GROUP));
                            } else {
                                LogicalGroup chromatorGroup = scanGroup.getGroup(MONO_CHROMATOR_GROUP);
                                LogicalGroup infoGroup = scanGroup.getGroup(INFO_GROUP);
                                if (chromatorGroup == null) {
                                    LoggerFactory.getLogger(applicationId).error(getGroupError(MONO_CHROMATOR_GROUP));
                                } else if (infoGroup == null) {
                                    LoggerFactory.getLogger(applicationId).error(getGroupError(INFO_GROUP));
                                } else {
                                    IKey energyKey = factory.createKey(ENERGY_KEY);
                                    IKey exitSlitsKey = factory.createKey(EXIT_SLITS_KEY);
                                    IKey commentsKey = factory.createKey(COMMENTS_KEY);
                                    IKey x0Key = factory.createKey(X_CENTER_COORDINATE_KEY);
                                    IKey z0Key = factory.createKey(Z_CENTER_COORDINATE_KEY);
                                    IKey originKey = factory.createKey(SCAN_ORIGIN_KEY);

                                    IFilter detectorParameter = null;
                                    if ((detector != null) && (!detector.isEmpty())) {
                                        detectorParameter = new FilterAttribute(ATTR_EQUIPMENT_NAME, detector);
                                    }
                                    LogicalGroup detectorGroup = scanGroup.getGroup(DETECTOR_GROUP);

                                    // extract scan name, origin and energy
                                    IKey scanKey = factory.createKey(SCAN_KEY);
                                    String scanName = checkVirtualData(readString(scanGroup, scanKey, null, null));
                                    String comments = readString(infoGroup, commentsKey, null, null);
                                    double energy = readDouble(chromatorGroup, energyKey, null, null);
                                    String origin = checkVirtualData(readString(infoGroup, originKey, null, null));
                                    if ((origin != null) && (origin.startsWith(scanName))) {
                                        origin = checkVirtualData(origin.substring(scanName.length()));
                                    }
                                    ScanData energyScanData = new ScanData(acquisition, energy, scanName, origin);
                                    energyScanData.setExitSlits(readDouble(chromatorGroup, exitSlitsKey, null, null));
                                    exitSlitsKey.popFilter();
                                    energyScanData.setComments(comments);
                                    acquisition.addScanData(energyScanData);
                                    LogicalGroup dataGroup = scanGroup.getGroup(DATA_GROUP);

                                    double x0 = readDouble(dataGroup, x0Key, null, null);
                                    double z0 = readDouble(dataGroup, z0Key, null, null);

                                    // extract x and y positions for current scan
                                    IKey yKey = factory.createKey(YPOSITION_KEY);
                                    IKey xKey = factory.createKey(XPOSITION_KEY);
                                    if (!isCanceled()) {
                                        IDataItem yPositionItem = dataGroup.getDataItem(yKey);
                                        IDataItem xPositionItem = dataGroup.getDataItem(xKey);
                                        boolean yAvailable = false;
                                        if (yPositionItem != null) {
                                            IArray temp = null;
                                            try {
                                                temp = yPositionItem.getData();
                                            } catch (Exception e) {
                                                temp = null;
                                            }
                                            if (temp != null) {
                                                yAvailable = true;
                                                SoftDoubleArray yArray = new SoftDoubleArray(
                                                        new ContextDataItem(dataGroup, yKey, yPositionItem), this,
                                                        NumberArrayUtils.extractDoubleArray(
                                                                extractArrayValue(yPositionItem, temp, true)));
                                                energyScanData.registerPositionData(Dimension.Y, yArray);
                                            }
                                        }
                                        if (xPositionItem != null) {
                                            IArray temp = null;
                                            try {
                                                temp = xPositionItem.getData();
                                            } catch (Exception e) {
                                                temp = null;
                                            }
                                            if (temp != null) {
                                                SoftData<?> xArray;
                                                ContextDataItem xContextDataItem = new ContextDataItem(dataGroup, xKey,
                                                        xPositionItem);
                                                if (yAvailable) {
                                                    int[] shape = temp.getShape();
                                                    if (shape == null) {
                                                        xArray = new SoftDoubleArray(xContextDataItem, this,
                                                                NumberArrayUtils.extractDoubleArray(
                                                                        extractArrayValue(xPositionItem, temp, true)));
                                                    } else if (shape.length == 2) {
                                                        boolean flat = false;
                                                        for (int dim : shape) {
                                                            if (dim == 1) {
                                                                flat = true;
                                                                break;
                                                            }
                                                        }
                                                        if (flat) {
                                                            xArray = new SoftDoubleArray(xContextDataItem, this,
                                                                    NumberArrayUtils.extractDoubleArray(
                                                                            extractArrayValue(xPositionItem, temp,
                                                                                    true)));
                                                        } else {
                                                            Object tempMatrix = extractArrayValue(xPositionItem, temp,
                                                                    false);
                                                            double[][] matrixValue = NumberMatrixUtils
                                                                    .extractDoubleMatrix(tempMatrix);
                                                            xArray = new SoftDoubleMatrix(xContextDataItem, this,
                                                                    matrixValue);
                                                        }
                                                    } else {
                                                        xArray = new SoftDoubleArray(
                                                                new ContextDataItem(scanGroup, xKey, xPositionItem),
                                                                this, NumberArrayUtils.extractDoubleArray(
                                                                        extractArrayValue(xPositionItem, temp, true)));
                                                    }
                                                } else {
                                                    xArray = new SoftDoubleArray(xContextDataItem, this,
                                                            NumberArrayUtils.extractDoubleArray(
                                                                    extractArrayValue(xPositionItem, temp, true)));
                                                }
                                                energyScanData.registerPositionData(Dimension.X, xArray);
                                            }
                                        }

                                        if (toLoad.contains(LoadableData.SPECTRA) && (!isCanceled())) {
                                            // extract desired spectra
                                            energyScanData.setSpectra(
                                                    extractSpectrumStackData(energyScanData, factory, dataGroup,
                                                            getSpectraKey(detector), SPECTRA_DISPLAY_NAME, true, true));
                                        }
                                        if (toLoad.contains(LoadableData.IMAGES) && (!isCanceled())) {
                                            // extract desired images
                                            loadImageStack(energyScanData, factory, dataGroup);
                                        }
                                        if (toLoad.contains(LoadableData.SCALARS) && (!isCanceled())) {
                                            // extract desired scalars
                                            energyScanData.setScalars(extractScalarStackData(energyScanData, factory,
                                                    dataGroup, SCALARS_KEY, SCALARS_DISPLAY_NAME, true, true));
                                        }
                                        if (toLoad.contains(LoadableData.TOTAL_SPECTRUM) && (!isCanceled())) {
                                            // extract total spectrum
                                            energyScanData.setTotalSpectrum(extractSpectrumData(energyScanData, factory,
                                                    dataGroup, TOTAL_SPECTRUM_KEY, TOTAL_SPECTRUM_DISPLAY_NAME));
                                        }
                                        if (toLoad.contains(LoadableData.TOTAL_IMAGE) && (!isCanceled())) {
                                            // extract total image
                                            loadTotalImage(energyScanData, factory, dataGroup);
                                        }
                                        if (toLoad.contains(LoadableData.BEAM_ENERGY) && (!isCanceled())) {
                                            // extract beam energy
                                            energyScanData.setBeamEnergy(extractSpectrumData(energyScanData, factory,
                                                    dataGroup, BEAM_ENERGY_KEY, BEAM_ENERGY_DISPLAY_NAME));
                                        }
                                        if (toLoad.contains(LoadableData.SAMPLE_PHI) && (!isCanceled())) {
                                            // extract sample phi
                                            energyScanData.setSamplePhi(extractScalarStackData(energyScanData, factory,
                                                    dataGroup, SAMPLE_PHI_KEY, SAMPLE_PHI_DISPLAY_NAME, false, false));
                                        }
                                        if (toLoad.contains(LoadableData.SAMPLE_THETA) && (!isCanceled())) {
                                            // extract sample theta
                                            energyScanData.setSampleTheta(
                                                    extractScalarStackData(energyScanData, factory, dataGroup,
                                                            SAMPLE_THETA_KEY, SAMPLE_THETA_DISPLAY_NAME, false, false));
                                        }
                                        if (toLoad.contains(LoadableData.APERTURE_ANGLE) && (!isCanceled())) {
                                            // extract aperture angle
                                            energyScanData.setApertureAngle(extractSpectrumStackData(energyScanData,
                                                    factory, dataGroup, APERTURE_ANGLE_KEY, APERTURE_ANGLE_DISPLAY_NAME,
                                                    false, false));
                                        }
                                        // extract acquisition data
                                        LogicalGroup monitorGroup = scanGroup.getGroup(MONITOR_GROUP);
                                        BasicAcquisitionDataManager acquisitionDataManager = new BasicAcquisitionDataManager(
                                                this, dataGroup, monitorGroup);
                                        if (!isCanceled()) {
                                            acquisitionDataManager
                                                    .setXBin(readInteger(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, X_BIN_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setZBin(readInteger(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, Z_BIN_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager.setExposureTime(readVariableDouble(
                                                    detectorGroup, generateKey(factory, acquisition, detectorGroup,
                                                            EXPOSURE_TIME_KEY, detectorParameter),
                                                    energyScanData.getStackShape(), null));
                                            acquisitionDataManager
                                                    .setShutterCloseDelay(readDouble(detectorGroup,
                                                            generateKey(factory, acquisition, detectorGroup,
                                                                    SHUTTER_CLOSE_DELAY_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setDistance(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, DISTANCE_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setPixelSize(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, PIXEL_SIZE_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setDelta0(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, DELTA_OFFSET_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setGamma0(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, GAMMA_OFFSET_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setTwoTheta0(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, TWO_THETA0_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setThetaIn(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, THETAIN_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setThetaZ0(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, THETA_Z0_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager.setThetaZCalibrationFactor(readDouble(detectorGroup,
                                                    generateKey(factory, acquisition, detectorGroup,
                                                            THETAZ_CALIBRATION_FACTOR_KEY, detectorParameter),
                                                    null, null));
                                            acquisitionDataManager
                                                    .setSampleSlitDistance(readDouble(detectorGroup,
                                                            generateKey(factory, acquisition, detectorGroup,
                                                                    SAMPLE_SLIT_DISTANCE_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setGeometry(readString(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, GEOMETRY_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setQxy(NumberArrayUtils.extractDoubleArray(readCDMSimpleArray(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, QXY_KEY, detectorParameter),
                                                            null, null)));
                                            acquisitionDataManager
                                                    .setChannel0(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, CHANNEL0_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager.setEncoderDelta(readVariableDouble(
                                                    detectorGroup, generateKey(factory, acquisition, detectorGroup,
                                                            ENCODER_DELTA_KEY, detectorParameter),
                                                    energyScanData.getStackShape(), null));
                                            acquisitionDataManager
                                                    .setDetectorBasedUncertainty(readDouble(detectorGroup,
                                                            generateKey(factory, acquisition, detectorGroup,
                                                                    DETECTOR_UNCERTAINTY_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setDetectorGain(
                                                            readDouble(detectorGroup,
                                                                    generateKey(factory, acquisition, detectorGroup,
                                                                            DETECTOR_GAIN_KEY, detectorParameter),
                                                                    null, null));
                                            acquisitionDataManager.setDetectorBias(readShortObject(detectorGroup,
                                                    generateKey(factory, acquisition, monitorGroup, DETECTOR_BIAS_KEY),
                                                    null, null));
                                            acquisitionDataManager.setWaveLength(readDouble(chromatorGroup,
                                                    generateKey(factory, acquisition, chromatorGroup, WAVE_LENGTH_KEY),
                                                    null, null));
                                            acquisitionDataManager
                                                    .setDark(NumberArrayUtils.extractDoubleArray(readCDMSimpleArray(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, DARK_KEY, detectorParameter),
                                                            null, null)));
                                            double[] energyScale = NumberArrayUtils
                                                    .extractDoubleArray(readCDMSimpleArray(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, DATA_CHANNEL_SCALE_KEY),
                                                            null, null));
                                            if (energyScale == null) {
                                                energyScale = NumberArrayUtils
                                                        .extractDoubleArray(readCDMSimpleArray(detectorGroup,
                                                                generateKey(factory, acquisition, detectorGroup,
                                                                        ENERGY_SCALE_KEY, detectorParameter),
                                                                null, null));
                                            }
                                            if (energyScale == null) {
                                                energyScale = NumberArrayUtils
                                                        .extractDoubleArray(readCDMSimpleArray(
                                                                detectorGroup, generateKey(factory, acquisition,
                                                                        detectorGroup, ESCALE_KEY, detectorParameter),
                                                                null, null));
                                            }
                                            acquisitionDataManager.setEnergyScale(energyScale);
                                            double[] sliceScale = NumberArrayUtils
                                                    .extractDoubleArray(readCDMSimpleArray(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, SLICE_SCALE_KEY, detectorParameter),
                                                            null, null));

                                            if (sliceScale == null) {
                                                sliceScale = readXScale(factory, dataGroup);
                                            }
                                            acquisitionDataManager.setSliceScale(sliceScale);

                                            acquisitionDataManager
                                                    .setSweeps(readInteger(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, SWEEPS_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setAcquisitionMode(readString(detectorGroup,
                                                            generateKey(factory, acquisition, detectorGroup,
                                                                    ACQUISITION_MODE_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setLensMode(readString(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, LENS_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setPassEnergy(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, PASS_ENERGY_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setLowEnergy(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, LOW_ENERGY_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setHighEnergy(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, HIGH_ENERGY_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setEnergyStep(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, ENERGY_STEP_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setStepTime(readDouble(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, STEP_TIME_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setFirstXChannel(readInteger(detectorGroup,
                                                            generateKey(factory, acquisition, detectorGroup,
                                                                    FIRST_X_CHANNEL_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setLastXChannel(readInteger(detectorGroup,
                                                            generateKey(factory, acquisition, detectorGroup,
                                                                    LAST_X_CHANNEL_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setFirstYChannel(readInteger(detectorGroup,
                                                            generateKey(factory, acquisition, detectorGroup,
                                                                    FIRST_Y_CHANNEL_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setLastYChannel(readInteger(detectorGroup,
                                                            generateKey(factory, acquisition, detectorGroup,
                                                                    LAST_Y_CHANNEL_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager
                                                    .setSlices(readInteger(
                                                            detectorGroup, generateKey(factory, acquisition,
                                                                    detectorGroup, SLICES_KEY, detectorParameter),
                                                            null, null));
                                            acquisitionDataManager.setX0(x0);
                                            acquisitionDataManager.setZ0(z0);

                                            IKey miKey = factory.createKey(MONITOR_KEY);
                                            List<IDataItem> monitors = monitorGroup.getDataItemList(miKey);
                                            List<String> miList = new ArrayList<>();
                                            String mi;
                                            for (IDataItem item : monitors) {
                                                mi = item.getAttribute(ATTR_EQUIPMENT_NAME).getStringValue();
                                                if (!miList.contains(mi)) {
                                                    miList.add(mi);
                                                }
                                            }

                                            acquisitionDataManager.setMiList(miList.toArray(new String[miList.size()]));
                                            miList.clear();
                                        } // end if (!isCanceled())
                                        energyScanData.setAcquisitionDataManager(acquisitionDataManager);
                                        // extract motors
                                        if (!isCanceled()) {
                                            LogicalGroup motorGroup = scanGroup.getGroup(MOTORS_GROUP);

                                            IKey motorKey = factory.createKey(MOTOR_POSITION_KEY);

                                            List<IDataItem> motors = motorGroup.getDataItemList(motorKey);
                                            String motorName;
                                            for (IDataItem item : motors) {
                                                motorName = item.getAttribute(ATTR_EQUIPMENT_NAME).getStringValue();
                                                energyScanData.registerMotor(motorName, item.readScalarDouble());
                                            }
                                            motors.clear();
                                        } // end if (!isCanceled())

                                        // extract header keys
                                        if (!isCanceled()) {
                                            LogicalGroup keysGroup = logicalRoot.getGroup(KEYS_HEADER_GROUP);
                                            setPersistingKeysHeader(acquisitionDataManager, keysGroup);
                                        }
                                        // extract x keys
                                        if (!isCanceled()) {
                                            LogicalGroup keysGroup = logicalRoot.getGroup(PERSISTING_KEYS_GROUP);
                                            setPersistingKeys(acquisitionDataManager, keysGroup);
                                        } // end if (!isCanceled())
                                    } // end if (!isCanceled())
                                }
                            }
                        }
                    } catch (Exception e) {
                        acquisition = null;
                        LoggerFactory.getLogger(applicationId).error("Failed to load Acquisition", e);
                    }
                } catch (DataAccessException e) {
                    LoggerFactory.getLogger(applicationId)
                            .error("Failed to load Acquisition, due to file access problem", e);
                }
            } // end if ((reader != null) && (factory != null))
        } // end if (!isCanceled())
        if (acquisition != null) {
            acquisition.sortScans();
        }
        try {
            acquisitionSource.setData(acquisition);
        } catch (Exception e) {
            LoggerFactory.getLogger(applicationId).error("Could not set acquisitionSource", e);
        }
    }

    private void setPersistingKeysHeader(BasicAcquisitionDataManager acquisitionDataManager, LogicalGroup xKeysGroup) {
        if ((acquisitionDataManager != null) && (xKeysGroup != null)) {
            List<String> keyList = new ArrayList<>();
            Map<String, IDataItem> persistingData = new LinkedHashMap<>();
            ExtendedDictionary dictionary = xKeysGroup.getDictionary();
            if (dictionary != null) {
                List<IKey> keys = dictionary.getAllKeys();
                if (keys != null) {
                    for (IKey key : keys) {
                        String name = key.getName();
                        keyList.add(name);
                        IKey filteredKey = key.clone();
                        IDataItem item = xKeysGroup.getDataItem(filteredKey);
                        if (item != null) {
                            persistingData.put(name, item);
                        }
                    } // end for (IKey key : keys)
                } // end if (keys != null)
            } // end if (dictionary != null)
            acquisitionDataManager.setPersistingKeysHeader(keyList.toArray(new String[keyList.size()]), persistingData);
            keyList.clear();
        }
    }

    private void setPersistingKeys(BasicAcquisitionDataManager acquisitionDataManager, LogicalGroup xKeysGroup) {
        if ((acquisitionDataManager != null) && (xKeysGroup != null)) {
            List<String> keyList = new ArrayList<>();
            Map<String, IDataItem> persistingData = new LinkedHashMap<>();
            ExtendedDictionary dictionary = xKeysGroup.getDictionary();
            if (dictionary != null) {
                List<IKey> keys = dictionary.getAllKeys();
                if (keys != null) {
                    for (IKey key : keys) {
                        String name = key.getName();
                        keyList.add(name);
                        IKey filteredKey = key.clone();
                        IDataItem item = xKeysGroup.getDataItem(filteredKey);
                        if (item != null) {
                            persistingData.put(name, item);
                        }
                    } // end for (IKey key : keys)
                } // end if (keys != null)
            } // end if (dictionary != null)

            acquisitionDataManager.setPersistingKeys(keyList.toArray(new String[keyList.size()]), persistingData);
            keyList.clear();
        }
    }

    public static Number extractSignedNumber(IDataItem item) throws DataAccessException {
        Number value;
        Class<?> type = item.getType();
        if ((Byte.class.equals(type)) || Byte.TYPE.equals(type)) {
            value = Byte.valueOf(item.readScalarByte());
        } else if ((Short.class.equals(type)) || Short.TYPE.equals(type)) {
            value = Short.valueOf(item.readScalarShort());
        } else if ((Integer.class.equals(type)) || Integer.TYPE.equals(type)) {
            value = Integer.valueOf(item.readScalarInt());
        } else if ((Long.class.equals(type)) || Long.TYPE.equals(type)) {
            value = Long.valueOf(item.readScalarLong());
        } else if ((Float.class.equals(type)) || Float.TYPE.equals(type)) {
            value = Float.valueOf(item.readScalarFloat());
        } else if ((Double.class.equals(type)) || Double.TYPE.equals(type) || Number.class.equals(type)) {
            value = Double.valueOf(item.readScalarDouble());
        } else {
            value = null;
        }
        return value;
    }

    @Override
    public String[] getXKeys(IDataset reader) {
        String[] xKeys;
        boolean wasClosed = false;
        if (reader == null) {
            xKeys = null;
        } else {
            try {
                wasClosed = !reader.isOpen();
                checkDictionary();
                LogicalGroup logicalRoot = reader.getLogicalRoot(dictionaryView);
                if (logicalRoot == null) {
                    xKeys = null;
                    wasClosed = false;
                } else {
                    LogicalGroup xKeysGroup = logicalRoot.getGroup(PERSISTING_KEYS_GROUP);
                    if (xKeysGroup == null) {
                        xKeys = null;
                    } else {
                        ExtendedDictionary dictionary = xKeysGroup.getDictionary();
                        if (dictionary == null) {
                            xKeys = null;
                        } else {
                            List<IKey> keys = dictionary.getAllKeys();
                            if (keys == null) {
                                xKeys = null;
                            } else {
                                List<String> keyList = new ArrayList<>(keys.size());
                                for (IKey key : keys) {
                                    if (key != null) {
                                        String name = key.getName();
                                        if (name != null) {
                                            keyList.add(name);
                                        }
                                    }
                                }
                                if (keyList.isEmpty()) {
                                    xKeys = null;
                                } else {
                                    xKeys = keyList.toArray(new String[keyList.size()]);
                                    keyList.clear();
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                xKeys = null;
            } finally {
                try {
                    maybeCloseDataset(reader, wasClosed);
                } catch (DataAccessException e) {
                    // nothing to do;
                }
            }
        }
        return xKeys;
    }

    /**
     * Creates a new {@link IKey} with the given name and parameters
     * 
     * @param factory
     *            The {@link IFactory} that can produce the key
     * @param keyName
     *            The key name
     * @param parameters
     *            The parameters to use
     * @return An {@link IKey}
     */
    protected IKey generateKey(IFactory factory, Acquisition acquisition, LogicalGroup group, String keyName,
            IFilter... parameters) {
        IKey key = null;
        if (factory != null) {
            key = factory.createKey(keyName);
            if (key != null) {
                if (parameters != null) {
                    for (IFilter parameter : parameters) {
                        if (parameter != null) {
                            key.pushFilter(parameter);
                        }
                    }
                }
//                pushDetectorSubPart(acquisition, group, key);
            }
        }
        return key;
    }

    /**
     * Returns a {@link String} that identifies the origin of an acquisition. It
     * may be a file path, a data base access, etc...
     * 
     * @return A {@link String}
     */
    protected abstract String getAcquisitionOrigin();

    /**
     * Returns a human readable {@link String} that identifies the origin of an acquisition. It
     * may be a file path, a data base access, etc..., but with no escape characters (for example: with real spaces
     * instead of %20)
     * 
     * @return A {@link String}
     */
    protected abstract String getAcquisitionSimpleName();

    /**
     * Closes the associated {@link URI} reader
     * 
     * @param forceClose Whether to force IDataset closing
     */
    protected void closeReader() {
        try {
            if ((reader != null) && reader.isOpen()) {
                reader.close();
            }
        } catch (Exception ex) {
            LoggerFactory.getLogger(applicationId).error(getCloseUriErrorMessage(), ex);
        }
    }

    /**
     * Closes a dataset only if necessary
     * 
     * @param dataset The dataset
     * @param wasClosed Whether it is necessary to close the dataset
     * @throws DataAccessException If a problem occurred.
     */
    protected void maybeCloseDataset(IDataset dataset, boolean wasClosed) throws DataAccessException {
        if (wasClosed) {
            dataset.close();
        }
    }

    protected abstract String getCloseUriErrorMessage();

    @Override
    public Object readCDMSimpleArrayWithCtx(ContextDataItem ctxDataItem, int[] origin, int[] shape) {
        return readCDMSimpleArrayWithCtx(null, null, ctxDataItem, origin, shape);
    }

    @Override
    public Object readCDMSimpleArray(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        return readCDMSimpleArrayWithCtx(parent, key, null, origin, shape);
    }

    /**
     * Recovers an {@link IDataItem} and reads its array, and returns it as a
     * single dimension array, without using any lock
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @return A single dimension array (example: <code>double[]</code>)
     */
    protected Object readCDMSimpleArrayWithCtx(LogicalGroup parent, IKey key, ContextDataItem ctxDataItem, int[] origin,
            int[] shape) {
        Object result = null;
        checkDictionary();
        if (ctxDataItem != null) {
            parent = ctxDataItem.getParentGroup();
            key = ctxDataItem.getDataItemKey();
        }
        if ((parent != null) && (key != null)) {
            try {
                IDataset dataset = parent.getDataset();
                boolean wasClosed = isDataSetClosed(dataset);
                if (wasClosed) {
                    dataset.open();
                }
                try {
                    IDataItem item = getDataItem(ctxDataItem, parent, key);
                    if (item != null) {
                        IArray array;
                        if ((origin != null) && (shape != null)) {
                            array = item.getData(origin, shape);
                        } else {
                            array = item.getData();
                        }
                        if (array != null) {
                            result = extractArrayValue(item, array, true);
                        }
                    }
                } catch (Exception e) {
                    result = null;
                    LoggerFactory.getLogger(applicationId).error("Could not read array", e);
                }
                maybeCloseDataset(dataset, wasClosed);
            } catch (DataAccessException e) {
                LoggerFactory.getLogger(applicationId).error("Could not read array", e);
            }
        }
        return result;
    }

    @Override
    public Object readCDMComplexArrayWithCtx(ContextDataItem ctxDataItem, int[] origin, int[] shape) {
        return readCDMComplexArray(null, null, origin, shape, ctxDataItem);
    }

    @Override
    public Object readCDMComplexArray(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        return readCDMComplexArray(parent, key, origin, shape, null);
    }

    /**
     * Recovers an {@link IDataItem} and reads its array, and returns it as a multi dimension array, without using any
     * lock
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @return A multi dimension array (example: <code>double[][]</code>)
     */
    protected Object readCDMComplexArray(LogicalGroup parent, IKey key, int[] origin, int[] shape,
            ContextDataItem ctxDataItem) {
        Object result = null;
        checkDictionary();
        if (ctxDataItem != null) {
            parent = ctxDataItem.getParentGroup();
            key = ctxDataItem.getDataItemKey();
        }
        if ((parent != null) && (key != null)) {
            try {
                IDataset dataset = parent.getDataset();
                boolean wasClosed = isDataSetClosed(dataset);
                if (wasClosed) {
                    dataset.open();
                }
                try {
                    IDataItem item = getDataItem(ctxDataItem, parent, key);

                    if (item != null) {
                        IArray array;
                        if ((origin != null) && (shape != null)) {
                            array = item.getData(origin, shape);

                        } else {
                            array = item.getData();
                        }
                        if (array != null) {
                            result = extractArrayValue(item, array, false);
                        }
                    }
                } catch (Exception e) {
                    result = null;
                    LoggerFactory.getLogger(applicationId).error("Could not read array", e);
                }
                maybeCloseDataset(dataset, wasClosed);
            } catch (DataAccessException e) {
                LoggerFactory.getLogger(applicationId).error("Could not read array", e);
            }
        }
        return result;
    }

    protected FlatData readCDMFlatComplexArrayWithCtx(LogicalGroup parent, IKey key, int[] origin, int[] resultShape,
            ContextDataItem ctxDataItem) {
        FlatData result = null;
        checkDictionary();
        result = readCDMFlatComplexArray(parent, key, origin, resultShape, ctxDataItem);
        return result;
    }

    protected FlatData[] readCDMFlatComplexArrayBlockWithCtx(LogicalGroup parent, IKey key, int[] origin,
            int[] resultShape, ContextDataItem ctxDataItem, int blockSize) {
        FlatData[] result = null;
        checkDictionary();
        result = readCDMFlatComplexArrayBlock(parent, key, origin, resultShape, ctxDataItem, blockSize);
        return result;
    }

    @Override
    public FlatData readCDMFlatComplexArrayWithCtx(ContextDataItem ctxDataItem, int[] origin, int[] resultShape) {
        return readCDMFlatComplexArrayWithCtx(null, null, origin, resultShape, ctxDataItem);
    }

    @Override
    public FlatData[] readCDMFlatComplexArrayBlockWithCtx(ContextDataItem ctxDataItem, int[] origin, int[] resultShape,
            int blockSize) {
        return readCDMFlatComplexArrayBlockWithCtx(null, null, origin, resultShape, ctxDataItem, blockSize);
    }

    @Override
    public FlatData readCDMFlatComplexArray(LogicalGroup parent, IKey key, int[] origin, int[] resultShape) {
        return readCDMFlatComplexArrayWithCtx(parent, key, origin, resultShape, null);
    }

    @Override
    public FlatData[] readCDMFlatComplexArrayBlock(LogicalGroup parent, IKey key, int[] origin, int[] resultShape,
            int blockSize) {
        return readCDMFlatComplexArrayBlockWithCtx(parent, key, origin, resultShape, null, blockSize);
    }

    @Override
    public IDataItem getDataItem(ContextDataItem ctxDataItem) {
        IDataItem item = null;
        if (ctxDataItem != null) {
            item = getDataItemNoCheck(ctxDataItem);
        }
        return item;
    }

    protected IDataItem getDataItemNoCheck(ContextDataItem ctxDataItem) {
        IDataItem item = null;
        LogicalGroup parent = ctxDataItem.getParentGroup();
        IKey key = ctxDataItem.getDataItemKey();
        item = ctxDataItem.getDataItem();
        if (((item == null) || (item.getDataset() == null)) && (parent != null) && (key != null)) {
            // Context not initialized
            item = parent.getDataItem(key);
            ctxDataItem.setDataItem(item);
        }
        return item;
    }

    /**
     * Recovers an {@link IDataItem}, knowing a {@link ContextDataItem}
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param ctxDataItem The {@link ContextDataItem}.
     * @return An {@link IDataItem}
     */
    protected IDataItem getDataItem(ContextDataItem ctxDataItem, LogicalGroup parent, IKey key) {
        IDataItem item = null;
        if (ctxDataItem == null) {
            if ((parent != null) && (key != null)) {
                item = parent.getDataItem(key);
            }
        } else {
            item = getDataItemNoCheck(ctxDataItem);
        }
        return item;
    }

    /**
     * Recovers an {@link IDataItem}, reads its array, and returns it as a {@link FlatData}, without using any lock
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @param origin The full position in stack
     * @param resultShape The data shape to extract
     * @param ctxDataItem The {@link ContextDataItem} that buffers known information about the {@link IDataItem}
     * @return A {@link FlatData}
     */
    protected FlatData readCDMFlatComplexArray(LogicalGroup parent, IKey key, int[] origin, int[] resultShape,
            ContextDataItem ctxDataItem) {
        FlatData result = null;
        boolean check = false;
        if (MetricsUtils.isUseMetrics() && (ctxDataItem != null)) {
            IKey tmp = ctxDataItem.getDataItemKey();
            if ((tmp != null) && ObjectUtils.sameObject(MetricsUtils.getItemCheckableKey(), tmp.getName())) {
                check = true;
            }
        }

        if (ctxDataItem != null) {
            parent = ctxDataItem.getParentGroup();
            key = ctxDataItem.getDataItemKey();
        }
        if ((parent != null) && (key != null)) {
            long time = -1, checkOpenTime = -1, itemRecoveringTime = -1, getDataTime = -1, extractTime = -1;
            try {
                if (MetricsUtils.isUseMetrics()) {
                    time = System.currentTimeMillis();
                }
                IDataset dataset = parent.getDataset();
                boolean wasClosed = isDataSetClosed(dataset);
                if (wasClosed) {
                    dataset.open();
                }
                if (MetricsUtils.isUseMetrics()) {
                    checkOpenTime = System.currentTimeMillis() - time;
                    time = System.currentTimeMillis();
                }
                try {
                    IDataItem item = getDataItem(ctxDataItem, parent, key);
                    if (MetricsUtils.isUseMetrics()) {
                        itemRecoveringTime = System.currentTimeMillis() - time;
                        time = System.currentTimeMillis();
                    }
                    if (item != null) {
                        IArray array;
                        if ((origin != null) && (resultShape != null)) {
                            int[] tempShape = resultShape;
                            if (resultShape.length < origin.length) {
                                tempShape = new int[origin.length];
                                int maxIndex = origin.length - resultShape.length;
                                for (int i = 0; i < maxIndex; i++) {
                                    tempShape[i] = 1;
                                }
                                System.arraycopy(resultShape, 0, tempShape, maxIndex, resultShape.length);
                            }
                            array = item.getData(origin, tempShape);
                        } else {
                            array = item.getData();
                        }
                        if (MetricsUtils.isUseMetrics()) {
                            getDataTime = System.currentTimeMillis() - time;
                            time = System.currentTimeMillis();
                        }
                        if (array != null) {
                            Object value = extractArrayValue(item, array, true);
                            result = new FlatData(resultShape, value);
                        }
                        if (MetricsUtils.isUseMetrics()) {
                            extractTime = System.currentTimeMillis() - time;
                            if (check) {
                                CHECK_OPEN_TIME.addAndGet(checkOpenTime);
                                ITEM_RECOVERING_TIME.addAndGet(itemRecoveringTime);
                                GET_DATA_TIME.addAndGet(getDataTime);
                                EXTRACT_TIME.addAndGet(extractTime);
                            }
                        }
                    }
                } catch (Exception e) {
                    result = null;
                    LoggerFactory.getLogger(applicationId).error("Could not read array", e);
                }
                maybeCloseDataset(dataset, wasClosed);
            } catch (DataAccessException e) {
                LoggerFactory.getLogger(applicationId).error("Could not read array", e);
            }
        }
        return result;
    }

    protected FlatData[] readCDMFlatComplexArrayBlock(LogicalGroup parent, IKey key, int[] origin, int[] resultShape,
            ContextDataItem ctxDataItem, int blockSize) {
        FlatData[] result = null;
        boolean check = false;
        if (MetricsUtils.isUseMetrics() && (ctxDataItem != null)) {
            IKey tmp = ctxDataItem.getDataItemKey();
            if ((tmp != null) && ObjectUtils.sameObject(MetricsUtils.getItemCheckableKey(), tmp.getName())) {
                check = true;
            }
        }

        if (ctxDataItem != null) {
            parent = ctxDataItem.getParentGroup();
            key = ctxDataItem.getDataItemKey();
        }
        if ((parent != null) && (key != null) && (blockSize > 0)) {
            long time = -1, checkOpenTime = -1, itemRecoveringTime = -1, getDataTime = -1, extractTime = -1;
            try {
                if (MetricsUtils.isUseMetrics()) {
                    time = System.currentTimeMillis();
                }
                IDataset dataset = parent.getDataset();
                boolean wasClosed = isDataSetClosed(dataset);
                if (wasClosed) {
                    dataset.open();
                }
                if (MetricsUtils.isUseMetrics()) {
                    checkOpenTime = System.currentTimeMillis() - time;
                    time = System.currentTimeMillis();
                }
                try {
                    IDataItem item = getDataItem(ctxDataItem, parent, key);
                    if (MetricsUtils.isUseMetrics()) {
                        itemRecoveringTime = System.currentTimeMillis() - time;
                        time = System.currentTimeMillis();
                    }
                    if (item != null) {
                        IArray array;
                        if (resultShape == null) {
                            // Can't extract a block of data without knowing the data shape
                            array = null;
                        } else {
                            if (origin == null) {
                                // try to start at first position
                                int[] fullShape = item.getShape();
                                if (fullShape == null) {
                                    origin = new int[resultShape.length];
                                } else {
                                    origin = new int[fullShape.length];
                                }
                            }
                            // resultShape is not at the same rank as stack shape -> extract tempShape and set blockSize
                            int[] tempShape = resultShape;
                            if (resultShape.length < origin.length) {
                                // blockRank is the rank of the result block
                                int blockRank = resultShape.length + 1;
                                tempShape = new int[origin.length];
                                // dimIndex is the index at which to set blockSize in tempShape
                                int dimIndex = origin.length - (blockRank);
                                for (int i = 0; i < dimIndex; i++) {
                                    tempShape[i] = 1;
                                }
                                tempShape[dimIndex] = blockSize;
                                System.arraycopy(resultShape, 0, tempShape, dimIndex + 1, resultShape.length);
                            } else {
                                // resultShape is already at stack rank: find the dimension at which to set blockSize
                                tempShape = resultShape.clone();
                                int start = -1;
                                for (int size : tempShape) {
                                    if (size == 1) {
                                        start++;
                                    } else {
                                        break;
                                    }
                                }
                                if (start > -1) {
                                    tempShape[start] = blockSize;
                                }
                            }
                            array = item.getData(origin, tempShape);
                        } // end if (resultShape == null) ... else
                        if (MetricsUtils.isUseMetrics()) {
                            getDataTime = System.currentTimeMillis() - time;
                            time = System.currentTimeMillis();
                        }
                        if (array != null) {
                            Object[] valueBlock = extractBlockOfFlatArrayValues(item, array, blockSize);
                            if (valueBlock != null) {
                                result = new FlatData[valueBlock.length];
                                for (int i = 0; i < valueBlock.length; i++) {
                                    result[i] = new FlatData(resultShape, valueBlock[i]);
                                }
                            }
                        }
                        if (MetricsUtils.isUseMetrics()) {
                            extractTime = System.currentTimeMillis() - time;
                            if (check) {
                                CHECK_OPEN_TIME.addAndGet(checkOpenTime);
                                ITEM_RECOVERING_TIME.addAndGet(itemRecoveringTime);
                                GET_DATA_TIME.addAndGet(getDataTime);
                                EXTRACT_TIME.addAndGet(extractTime);
                            }
                        }
                    }
                } catch (Exception e) {
                    result = null;
                    LoggerFactory.getLogger(applicationId).error("Could not read array", e);
                }
                maybeCloseDataset(dataset, wasClosed);
            } catch (DataAccessException e) {
                LoggerFactory.getLogger(applicationId).error("Could not read array", e);
            }
        }
        return result;

    }

    /**
     * Checks whether a value should be converted to unsigned.
     * 
     * @param value The value
     * @param parentItem The {@link IDataItem} that may know whether
     * @param profile Whether to profile time consuming for that value.
     * @return The value, converted or not.
     */
    protected static Object checkUnsigned(Object value, IDataItem parentItem, boolean profile) {
        long time = -1;
        if (profile) {
            time = System.currentTimeMillis();
        }
        if (parentItem.isUnsigned()) {
            // convert unsigned value into bigger encoding
            value = ArrayUtils.convertUnsignedArray(value);
        }
        if (profile) {
            UNISGNED_TIME.addAndGet(System.currentTimeMillis() - time);
        }
        return value;
    }

    /**
     * Extracts an array value from an {@link IArray}, converting unsigned elements when necessary
     * 
     * @param parentItem The {@link IDataItem} that knows whether values are unsigned
     * @param array The concerned {@link IArray}
     * @param flat Whether to extract flat value
     * @return An array of expected dimensions. <code>null</code> if <code>parentItem</code> or <code>array</code> is
     *         <code>null</code>.
     */
    public static Object extractArrayValue(IDataItem parentItem, IArray array, boolean flat) {
        Object value = null;
        if ((parentItem != null) && (array != null)) {
            boolean profile = flat && MetricsUtils.shouldProfile(parentItem.getShortName());
            long time = -1;
            if (profile) {
                time = System.currentTimeMillis();
            }
            IArrayUtils utils = array.getArrayUtils();
            // XXX CDMA (at least nexus plugin) is not efficient in extracting ND arrays.
            // So, we extract 1D array then convert it to ND, which is more efficient (less time consuming).
            value = utils.copyTo1DJavaArray();
            if (profile) {
                COPY_TIME.addAndGet(System.currentTimeMillis() - time);
            }
            value = checkUnsigned(value, parentItem, profile);
            if (!flat) {
                value = ArrayUtils.convertArrayDimensionFrom1ToN(value, array.getShape());
            }
            // TODO reactivate code and undo hack once CDMA correction is committed
//            value = (flat ? array.getArrayUtils().copyTo1DJavaArray() : array.getArrayUtils().copyToNDJavaArray());
//            value = toUnsigned(value, parentItem, check);
        }
        return value;
    }

    /**
     * Extracts a block of flat array values from an {@link IArray}, converting unsigned elements when necessary
     * 
     * @param parentItem The {@link IDataItem} that knows whether values are unsigned
     * @param array The concerned {@link IArray}
     * @param blockSize The number of data in the block
     * @return An array of expected dimensions. <code>null</code> if <code>parentItem</code> or <code>array</code> is
     *         <code>null</code>.
     */
    protected static Object[] extractBlockOfFlatArrayValues(IDataItem parentItem, IArray array, int blockSize) {
        Object[] value = null;
        if ((parentItem != null) && (array != null)) {
            boolean profile = MetricsUtils.shouldProfile(parentItem.getShortName());
            long time = -1;
            if (profile) {
                time = System.currentTimeMillis();
            }
            IArrayUtils utils = array.getArrayUtils();
            // XXX CDMA (at least nexus plugin) is not efficient in extracting ND arrays.
            // So, we extract 1D array then convert it to ND, which is more efficient (less time consuming).
            value = utils.copyBlockTo1DJavaArrays(blockSize);
            if (profile) {
                COPY_TIME.addAndGet(System.currentTimeMillis() - time);
            }
            value = (Object[]) checkUnsigned(value, parentItem, profile);
            // TODO reactivate code and undo hack once CDMA correction is committed
//            value = (flat ? array.getArrayUtils().copyTo1DJavaArray() : array.getArrayUtils().copyToNDJavaArray());
//            value = toUnsigned(value, parentItem, check);
        }
        return value;
    }

    @Override
    public byte readByte(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        byte result;
        try {
            result = readByteWithException(parent, key, origin, shape);
        } catch (CDMAException e) {
            result = 0;
            LoggerFactory.getLogger(applicationId).error(e.getMessage(), e.getCause());
        }
        return result;
    }

    @Override
    public Byte readByteObject(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        Byte result;
        try {
            result = readByteObjectWithException(parent, key, origin, shape);
        } catch (CDMAException e) {
            result = null;
            LoggerFactory.getLogger(applicationId).error(e.getMessage(), e.getCause());
        }
        return result;
    }

    private byte[] recoverByte(LogicalGroup parent, IKey key, int[] origin, int[] shape) throws CDMAException {
        byte[] value = null;
        checkDictionary();
        if ((parent != null) && (key != null)) {
            IDataset dataset = parent.getDataset();
            boolean wasClosed = isDataSetClosed(dataset);
            if (wasClosed) {
                dataset.open();
            }
            try {
                IDataItem item = parent.getDataItem(key);
                if (item != null) {
                    IArray array;
                    if ((origin != null) && (shape != null)) {
                        array = item.getData(origin, shape);
                    } else {
                        array = item.getData();
                    }
                    value = NumberArrayUtils.extractByteArray(extractArrayValue(item, array, true));
                }
            } catch (CDMAException e) {
                throw e;
            } catch (Exception e) {
                throw new CDMAAccessException(COULD_NOT_READ_BYTE, e);
            }
            maybeCloseDataset(dataset, wasClosed);
        }
        return value;
    }

    /**
     * Recovers an {@link IDataItem} and reads its <code>byte</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @return A <code>byte</code>
     * @throws CDMAException if a problem occurred while trying to read data
     */
    public byte readByteWithException(LogicalGroup parent, IKey key, int[] origin, int[] shape) throws CDMAException {
        byte result = 0;
        byte[] value = recoverByte(parent, key, origin, shape);
        if ((value != null) && (value.length > 0)) {
            result = value[0];
        }
        return result;
    }

    /**
     * Recovers an {@link IDataItem} and reads its <code>byte</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @return A <code>byte</code>
     * @throws CDMAException if a problem occurred while trying to read data
     */
    public Byte readByteObjectWithException(LogicalGroup parent, IKey key, int[] origin, int[] shape)
            throws CDMAException {
        Byte result = null;
        byte[] value = recoverByte(parent, key, origin, shape);
        if ((value != null) && (value.length > 0)) {
            result = Byte.valueOf(value[0]);
        }
        return result;
    }

    @Override
    public short readShort(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        short result;
        try {
            result = readShortWithException(parent, key, origin, shape);
        } catch (CDMAException e) {
            result = 0;
            LoggerFactory.getLogger(applicationId).error(e.getMessage(), e.getCause());
        }
        return result;
    }

    @Override
    public Short readShortObject(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        Short result;
        try {
            result = readShortObjectWithException(parent, key, origin, shape);
        } catch (CDMAException e) {
            result = null;
            LoggerFactory.getLogger(applicationId).error(e.getMessage(), e.getCause());
        }
        return result;
    }

    private short[] recoverShort(LogicalGroup parent, IKey key, int[] origin, int[] shape) throws CDMAException {
        short[] value = null;
        checkDictionary();
        if ((parent != null) && (key != null)) {
            IDataset dataset = parent.getDataset();
            boolean wasClosed = isDataSetClosed(dataset);
            if (wasClosed) {
                dataset.open();
            }
            try {
                IDataItem item = parent.getDataItem(key);
                if (item != null) {
                    IArray array;
                    if ((origin != null) && (shape != null)) {
                        array = item.getData(origin, shape);
                    } else {
                        array = item.getData();
                    }
                    value = NumberArrayUtils.extractShortArray(extractArrayValue(item, array, true));
                }
            } catch (CDMAException e) {
                throw e;
            } catch (Exception e) {
                throw new CDMAAccessException(COULD_NOT_READ_SHORT, e);
            }
            maybeCloseDataset(dataset, wasClosed);
        }
        return value;
    }

    /**
     * Recovers an {@link IDataItem} and reads its <code>short</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @return A <code>short</code>
     * @throws CDMAException if a problem occurred while trying to read data
     */
    public short readShortWithException(LogicalGroup parent, IKey key, int[] origin, int[] shape) throws CDMAException {
        short result = 0;
        short[] value = recoverShort(parent, key, origin, shape);
        if ((value != null) && (value.length > 0)) {
            result = value[0];
        }
        return result;
    }

    /**
     * Recovers an {@link IDataItem} and reads its <code>short</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @return A <code>short</code>
     * @throws CDMAException if a problem occurred while trying to read data
     */
    public Short readShortObjectWithException(LogicalGroup parent, IKey key, int[] origin, int[] shape)
            throws CDMAException {
        Short result = null;
        short[] value = recoverShort(parent, key, origin, shape);
        if ((value != null) && (value.length > 0)) {
            result = Short.valueOf(value[0]);
        }
        return result;
    }

    @Override
    public int readInt(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        int result;
        try {
            result = readIntWithException(parent, key, origin, shape);
        } catch (CDMAException e) {
            result = 0;
            LoggerFactory.getLogger(applicationId).error(e.getMessage(), e.getCause());
        }
        return result;
    }

    @Override
    public Integer readInteger(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        Integer result;
        try {
            result = readIntegerWithException(parent, key, origin, shape);
        } catch (CDMAException e) {
            result = null;
            LoggerFactory.getLogger(applicationId).error(e.getMessage(), e.getCause());
        }
        return result;
    }

    private int[] recoverInt(LogicalGroup parent, IKey key, int[] origin, int[] shape) throws CDMAException {
        int[] value = null;
        checkDictionary();
        if ((parent != null) && (key != null)) {
            IDataset dataset = parent.getDataset();
            boolean wasClosed = isDataSetClosed(dataset);
            if (wasClosed) {
                dataset.open();
            }
            try {
                IDataItem item = parent.getDataItem(key);
                if (item != null) {
                    IArray array;
                    if ((origin != null) && (shape != null)) {
                        array = item.getData(origin, shape);
                    } else {
                        array = item.getData();
                    }
                    value = NumberArrayUtils.extractIntArray(extractArrayValue(item, array, true));
                }
            } catch (CDMAException e) {
                throw e;
            } catch (Exception e) {
                throw new CDMAAccessException(COULD_NOT_READ_INT, e);
            }
            maybeCloseDataset(dataset, wasClosed);
        }
        return value;
    }

    /**
     * Recovers an {@link IDataItem} and reads its <code>int</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @return An <code>int</code>
     * @throws CDMAException if a problem occurred while trying to read data
     */
    protected int readIntWithException(LogicalGroup parent, IKey key, int[] origin, int[] shape) throws CDMAException {
        int result = 0;
        int[] value = recoverInt(parent, key, origin, shape);
        if ((value != null) && (value.length > 0)) {
            result = value[0];
        }
        return result;
    }

    /**
     * Recovers an {@link IDataItem} and reads its <code>int</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @return An {@link Integer}
     * @throws CDMAException if a problem occurred while trying to read data
     */
    protected Integer readIntegerWithException(LogicalGroup parent, IKey key, int[] origin, int[] shape)
            throws CDMAException {
        Integer result = null;
        int[] value = recoverInt(parent, key, origin, shape);
        if ((value != null) && (value.length > 0)) {
            result = Integer.valueOf(value[0]);
        }
        return result;
    }

    @Override
    public long readLong(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        long result;
        try {
            result = readLongWithException(parent, key, origin, shape);
        } catch (CDMAException e) {
            result = 0;
            LoggerFactory.getLogger(applicationId).error(e.getMessage(), e.getCause());
        }
        return result;
    }

    @Override
    public Long readLongObject(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        Long result;
        try {
            result = readLongObjectWithException(parent, key, origin, shape);
        } catch (CDMAException e) {
            result = null;
            LoggerFactory.getLogger(applicationId).error(e.getMessage(), e.getCause());
        }
        return result;
    }

    private long[] recoverLong(LogicalGroup parent, IKey key, int[] origin, int[] shape) throws CDMAException {
        long[] value = null;
        checkDictionary();
        if ((parent != null) && (key != null)) {
            IDataset dataset = parent.getDataset();
            boolean wasClosed = isDataSetClosed(dataset);
            if (wasClosed) {
                dataset.open();
            }
            try {
                IDataItem item = parent.getDataItem(key);
                if (item != null) {
                    IArray array;
                    if ((origin != null) && (shape != null)) {
                        array = item.getData(origin, shape);
                    } else {
                        array = item.getData();
                    }
                    value = NumberArrayUtils.extractLongArray(extractArrayValue(item, array, true));
                }
            } catch (CDMAException e) {
                throw e;
            } catch (Exception e) {
                throw new CDMAAccessException(COULD_NOT_READ_LONG, e);
            }
            maybeCloseDataset(dataset, wasClosed);
        }
        return value;
    }

    /**
     * Recovers an {@link IDataItem} and reads its <code>long</code> data, without using any lock
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @return A <code>long</code>
     * @throws CDMAException if a problem occurred while trying to read data
     */
    protected long readLongWithException(LogicalGroup parent, IKey key, int[] origin, int[] shape)
            throws CDMAException {
        long result = 0;
        long[] value = recoverLong(parent, key, origin, shape);
        if ((value != null) && (value.length > 0)) {
            result = value[0];
        }
        return result;
    }

    /**
     * Recovers an {@link IDataItem} and reads its <code>long</code> data, without using any lock
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @return A Long
     * @throws CDMAException if a problem occurred while trying to read data
     */
    protected Long readLongObjectWithException(LogicalGroup parent, IKey key, int[] origin, int[] shape)
            throws CDMAException {
        Long result = null;
        long[] value = recoverLong(parent, key, origin, shape);
        if ((value != null) && (value.length > 0)) {
            result = Long.valueOf(value[0]);
        }
        return result;
    }

    @Override
    public float readFloat(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        float result;
        try {
            result = readFloatWithException(parent, key, origin, shape);
        } catch (CDMAException e) {
            result = Float.NaN;
            LoggerFactory.getLogger(applicationId).error(e.getMessage(), e.getCause());
        }
        return result;
    }

    /**
     * Recovers an {@link IDataItem} and reads its <code>float</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @return A <code>float</code>
     * @throws CDMAException if a problem occurred while trying to read data
     */
    protected float readFloatWithException(LogicalGroup parent, IKey key, int[] origin, int[] shape)
            throws CDMAException {
        float result = Float.NaN;
        checkDictionary();
        if ((parent != null) && (key != null)) {
            IDataset dataset = parent.getDataset();
            boolean wasClosed = isDataSetClosed(dataset);
            if (wasClosed) {
                dataset.open();
            }
            try {
                IDataItem item = parent.getDataItem(key);
                if (item != null) {
                    IArray array;
                    if ((origin != null) && (shape != null)) {
                        array = item.getData(origin, shape);
                    } else {
                        array = item.getData();
                    }
                    float[] value = NumberArrayUtils.extractFloatArray(extractArrayValue(item, array, true));
                    if ((value != null) && (value.length > 0)) {
                        result = value[0];
                    }
                }
            } catch (CDMAException e) {
                throw e;
            } catch (Exception e) {
                throw new CDMAAccessException(COULD_NOT_READ_FLOAT, e);
            }
            maybeCloseDataset(dataset, wasClosed);
        }
        return result;
    }

    @Override
    public double readDouble(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        double result;
        try {
            result = readDoubleWithException(parent, key, origin, shape);
        } catch (CDMAException e) {
            result = Double.NaN;
            LoggerFactory.getLogger(applicationId).error(e.getMessage(), e.getCause());
        }
        return result;
    }

    /**
     * Recovers an {@link IDataItem} and reads its <code>double</code> data
     * 
     * @param parent The parent {@link LogicalGroup}
     * @param key The {@link IKey} to recover the {@link IDataItem}
     * @return A <code>double</code>
     * @throws CDMAException if a problem occurred while trying to read data
     */
    protected double readDoubleWithException(LogicalGroup parent, IKey key, int[] origin, int[] shape)
            throws CDMAException {
        double result = Double.NaN;
        checkDictionary();
        if ((parent != null) && (key != null)) {
            IDataset dataset = parent.getDataset();
            boolean wasClosed = isDataSetClosed(dataset);
            if (wasClosed) {
                dataset.open();
            }
            try {
                IDataItem item = parent.getDataItem(key);
                if (item != null) {
                    IArray array;
                    if ((origin != null) && (shape != null)) {
                        array = item.getData(origin, shape);
                    } else {
                        array = item.getData();
                    }
                    double[] value = NumberArrayUtils.extractDoubleArray(extractArrayValue(item, array, true));
                    if ((value != null) && (value.length > 0)) {
                        result = value[0];
                    }
                }
            } catch (CDMAException e) {
                throw e;
            } catch (Exception e) {
                throw new CDMAAccessException(COULD_NOT_READ_DOUBLE, e);
            }
            maybeCloseDataset(dataset, wasClosed);
        }
        return result;
    }

    protected Object readVariableDouble(LogicalGroup parent, IKey key, int[] expectedShape, Object defaultValue) {
        Object result = null;
        checkDictionary();
        if ((parent != null) && (key != null)) {
            try {
                IDataset dataset = parent.getDataset();
                boolean wasClosed = isDataSetClosed(dataset);
                if (wasClosed) {
                    dataset.open();
                }
                try {
                    IDataItem item = parent.getDataItem(key);
                    if (item != null) {
                        IArray array = item.getData();
                        int[] shape = array.getShape();
                        boolean isScalar = true;
                        for (int dim : shape) {
                            if (dim != 1) {
                                isScalar = false;
                                break;
                            }
                        }
                        double[] value = NumberArrayUtils.extractDoubleArray(extractArrayValue(item, array, true));
                        if (isScalar) {
                            if ((value != null) && (value.length > 0)) {
                                result = value[0];
                            }
                        } else {
                            int[] effectiveShape;
                            if (expectedShape == null) {
                                effectiveShape = shape;
                            } else {
                                effectiveShape = expectedShape;
                            }
                            result = ArrayUtils.convertArrayDimensionFrom1ToN(value, effectiveShape);
                        }
                    }
                } catch (Exception e) {
                    result = Double.NaN;
                    LoggerFactory.getLogger(applicationId).error(COULD_NOT_READ_VARIABLE_DOUBLE, e);
                }
                maybeCloseDataset(dataset, wasClosed);
            } catch (DataAccessException e) {
                LoggerFactory.getLogger(applicationId).error(COULD_NOT_READ_VARIABLE_DOUBLE, e);
            }
        }
        if (result == null) {
            result = defaultValue;
        }
        return result;
    }

    protected Object readVariableFloat(LogicalGroup parent, IKey key, int[] expectedShape) {
        Object result = null;
        checkDictionary();
        if ((parent != null) && (key != null)) {
            try {
                IDataset dataset = parent.getDataset();
                boolean wasClosed = isDataSetClosed(dataset);
                if (wasClosed) {
                    dataset.open();
                }
                try {
                    IDataItem item = parent.getDataItem(key);
                    if (item != null) {
                        IArray array = item.getData();
                        int[] shape = array.getShape();
                        boolean isScalar = true;
                        for (int dim : shape) {
                            if (dim != 1) {
                                isScalar = false;
                                break;
                            }
                        }
                        float[] value = NumberArrayUtils.extractFloatArray(extractArrayValue(item, array, true));
                        if (isScalar) {
                            if ((value != null) && (value.length > 0)) {
                                result = value[0];
                            }
                        } else {
                            int[] effectiveShape;
                            if (expectedShape == null) {
                                effectiveShape = shape;
                            } else {
                                effectiveShape = expectedShape;
                            }
                            result = ArrayUtils.convertArrayDimensionFrom1ToN(value, effectiveShape);
                        }
                    }
                } catch (Exception e) {
                    result = Float.NaN;
                    LoggerFactory.getLogger(applicationId).error(COULD_NOT_READ_VARIABLE_FLOAT, e);
                }
                maybeCloseDataset(dataset, wasClosed);
            } catch (DataAccessException e) {
                LoggerFactory.getLogger(applicationId).error(COULD_NOT_READ_VARIABLE_FLOAT, e);
            }
        }
        return result;
    }

    @Override
    public String readString(LogicalGroup parent, IKey key, int[] origin, int[] shape) {
        String result = EMPTY;
        IDataItem item;
        IDataset dataset;
        if ((parent == null) || (key == null)) {
            item = null;
            dataset = null;
        } else {
            checkDictionary();
            dataset = parent.getDataset();
            item = parent.getDataItem(key);
        }
        try {
            boolean wasClosed = isDataSetClosed(dataset);
            if (wasClosed) {
                dataset.open();
            }
            result = readString(item, origin, shape);
            maybeCloseDataset(dataset, wasClosed);
        } catch (DataAccessException e) {
            LoggerFactory.getLogger(applicationId).error(COULD_NOT_READ_STRING, e);
        }
        return result;
    }

    @Override
    public String readString(IDataItem item, int[] origin, int[] shape) {
        String result;
        try {
            result = readStringWithException(item, origin, shape);
        } catch (CDMAException e) {
            result = EMPTY;
            LoggerFactory.getLogger(applicationId).error(e.getMessage(), e.getCause());
        }
        return result;
    }

    /**
     * Recovers an {@link IDataItem} and reads its <code>String</code> data
     * 
     * @param item The {@link IDataItem}
     * @param origin THe origin of the sub data to read in the data item
     * @param shape The shape of the sub data to read in the data item
     * @return A <code>String</code>
     * @throws CDMAException if a problem occurred while trying to read data
     */
    protected String readStringWithException(IDataItem item, int[] origin, int[] shape) throws CDMAException {
        String result = EMPTY;
        checkDictionary();
        if (item != null) {
            try {
                if (item.isScalar() && Character.TYPE.equals(item.getType())) {
                    result = item.readScalarString();
                } else {
                    IArray array;
                    if ((origin != null) && (shape != null)) {
                        array = item.getData(origin, shape);
                    } else {
                        array = item.getData();
                    }
                    if (array != null) {
                        Object value = array.getArrayUtils().copyTo1DJavaArray();
                        if (value == null) {
                            result = null;
                        } else {
                            // Some plugins send Char[] instead of String[]
                            if (value.getClass().getComponentType() == char.class) {
                                result = new String((char[]) value);
                            } else {
                                String[] strValue = (String[]) array.getArrayUtils().copyTo1DJavaArray();
                                result = strValue[0];
                            }
                        }
                    }
                }
            } catch (CDMAException e) {
                throw e;
            } catch (Exception e) {
                throw new CDMAAccessException(COULD_NOT_READ_STRING, e);
            }
        }
        return result;
    }

    @Override
    public double[] getIntensityHistorised(String im, LogicalGroup monitorGroup) {
        double[] result = null;
        if ((im != null) && (monitorGroup != null)) {
            checkDictionary();
            try {
                IDataset dataset = monitorGroup.getDataset();
                boolean wasClosed = isDataSetClosed(dataset);
                if (wasClosed) {
                    dataset.open();
                }
                IFactory factory = Factory.getFactory(monitorGroup.getFactoryName());
                if (factory != null) {
                    try {
                        IKey intensityKey = factory.createKey(INTENSITY_KEY);
                        pushFilter(ATTR_EQUIPMENT_NAME, im, intensityKey);
                        result = NumberArrayUtils
                                .extractDoubleArray(readCDMSimpleArray(monitorGroup, intensityKey, null, null));
                    } catch (Exception e) {
                        result = null;
                        LoggerFactory.getLogger(applicationId).error("Could not read intensity history", e);
                    }
                }
                maybeCloseDataset(dataset, wasClosed);
            } catch (DataAccessException e) {
                LoggerFactory.getLogger(applicationId).error("Could not read intensity history", e);
            }
        }
        return result;
    }

    @Override
    public double getGain(String im, LogicalGroup monitorGroup) {
        double gain = Double.NaN;
        if ((im != null) && (monitorGroup != null)) {
            checkDictionary();
            IFactory factory = Factory.getFactory(monitorGroup.getFactoryName());
            if (factory != null) {
                try {
                    IKey gainKey = factory.createKey(MONITOR_GAIN_KEY);
                    pushFilter(ATTR_EQUIPMENT_NAME, im, gainKey);
                    gain = readDouble(monitorGroup, gainKey, null, null);
                } catch (Exception e) {
                    gain = Double.NaN;
                    LoggerFactory.getLogger(applicationId).error("Could not read intensity monitor gain", e);
                }
            }
        }
        return gain;
    }

    @Override
    public double getBias(String im, LogicalGroup monitorGroup) {
        double bias = Double.NaN;
        if ((im != null) && (monitorGroup != null)) {
            checkDictionary();
            IFactory factory = Factory.getFactory(monitorGroup.getFactoryName());
            if (factory != null) {
                try {
                    IKey biasKey = factory.createKey(MONITOR_BIAS_KEY);
                    pushFilter(ATTR_EQUIPMENT_NAME, im, biasKey);
                    bias = readDouble(monitorGroup, biasKey, null, null);
                } catch (Exception e) {
                    bias = Double.NaN;
                    LoggerFactory.getLogger(applicationId).error("Could not read intensity monitor bias", e);
                }
            }
        }
        return bias;
    }

    @Override
    public int[] getNamedDataShape(String name, LogicalGroup dataGroup) {
        int[] shape = null;
        if ((name != null) && (dataGroup != null)) {
            checkDictionary();
            try {
                IDataset dataset = dataGroup.getDataset();
                boolean wasClosed = isDataSetClosed(dataset);
                if (wasClosed) {
                    dataset.open();
                }
                IFactory factory = Factory.getFactory(dataGroup.getFactoryName());
                if (factory != null) {
                    try {
                        IKey namedDataKey = factory.createKey(NAMED_DATA_KEY);
                        pushFilter(ATTR_LONG_NAME, name, namedDataKey);
                        IDataItem item = getDataItem(null, dataGroup, namedDataKey);
                        if (item != null) {
                            shape = item.getShape();
                        }
                    } catch (Exception e) {
                        shape = null;
                        LoggerFactory.getLogger(applicationId).error("Could not read named data", e);
                    }
                }
                maybeCloseDataset(dataset, wasClosed);
            } catch (DataAccessException e) {
                LoggerFactory.getLogger(applicationId).error("Could not read named data", e);
            }
        }
        return shape;
    }

    @Override
    public FlatDoubleData getNamedData(String name, LogicalGroup dataGroup) {
        double[] value = null;
        int[] shape = null;
        if ((name != null) && (dataGroup != null)) {
            checkDictionary();
            try {
                IDataset dataset = dataGroup.getDataset();
                boolean wasClosed = isDataSetClosed(dataset);
                if (wasClosed) {
                    dataset.open();
                }
                IFactory factory = Factory.getFactory(dataGroup.getFactoryName());
                if (factory != null) {
                    try {
                        IKey namedDataKey = factory.createKey(NAMED_DATA_KEY);
                        pushFilter(ATTR_LONG_NAME, name, namedDataKey);
                        ContextDataItem ctxDataItem = new ContextDataItem(dataGroup, namedDataKey);
                        value = NumberArrayUtils.extractDoubleArray(
                                readCDMSimpleArrayWithCtx(dataGroup, namedDataKey, ctxDataItem, null, null));
                        IDataItem item = ctxDataItem.getDataItem();
                        if (item != null) {
                            shape = item.getShape();
                        }
                    } catch (Exception e) {
                        value = null;
                        LoggerFactory.getLogger(applicationId).error("Could not read named data", e);
                    }
                }
                maybeCloseDataset(dataset, wasClosed);
            } catch (DataAccessException e) {
                LoggerFactory.getLogger(applicationId).error("Could not read named data", e);
            }
        }
        FlatDoubleData data;
        if ((value == null) || (shape == null)) {
            data = null;
        } else {
            data = new FlatDoubleData(shape, value);
        }
        return data;
    }

    protected boolean isDataSetClosed(IDataset dataset) {
        return ((dataset != null) && (!dataset.isOpen()));
    }

    protected void pushFilter(String parameterName, String parameterValue, IKey concernedKey) {
        FilterAttribute parameterToPush = new FilterAttribute(parameterName, parameterValue);
        for (IFilter parameter : concernedKey.getFilterList()) {
            if (parameterToPush.equals(parameter)) {
                parameterToPush = null;
                break;
            }
        }
        if (parameterToPush != null) {
            concernedKey.pushFilter(parameterToPush);
        }
    }

    protected String getSpectraKey(String detector) {
        String key;
        if (detector == null) {
            key = null;
        } else {
            Pattern scientaPattern = Pattern.compile(SCIENTA_PATTERN);
//          Pattern keithleyPattern = Pattern.compile(KEITHLEY_DETECTOR);
            Pattern xiaPattern = Pattern.compile(XIA_DETECTOR_PATTERN);
            Pattern mbsPattern = Pattern.compile(MBS_DETECTOR_PATTERN);
            if (scientaPattern.matcher(detector).matches()) {
                key = SPECTRA_SCIENTA_KEY;
            } else if (xiaPattern.matcher(detector).matches()) {
                key = SPECTRA_XIA_KEY;
            } else if (mbsPattern.matcher(detector).matches()) {
                key = SPECTRA_MBS_KEY;
            } else {
                key = SPECTRA_KEY;
            }
        }
        return key;
    }

    protected void pushDetectorSubPart(Acquisition acquisition, LogicalGroup group, IKey key) {
        if ((acquisition != null) && (acquisition.getDetector() != null)
                && (!acquisition.getDetector().trim().isEmpty())) {
            pushFilter(ATTR_EQUIPMENT_NAME, acquisition.getDetector(), key);
        }
    }

    /**
     * Reads image stack in a scan and updates its associated {@link ScanData}
     * 
     * @param energyScanData The {@link ScanData}
     * @param factory The referent {@link IFactory}
     * @param dataGroup The parent {@link LogicalGroup} of the image stack
     */
    protected void loadImageStack(ScanData energyScanData, IFactory factory, LogicalGroup dataGroup) {
        int[] reducedShape = null;
        ImageStack imageStack = null;
        IKey imagesKey = factory.createKey(IMAGES_KEY);
        IDataItem nameItem = dataGroup.getParentGroup().getDataItem(factory.createKey(STACK_NAME_KEY));
        pushDetectorSubPart(energyScanData.getParent(), dataGroup, imagesKey);
        IDataItem imageStackDataItem = dataGroup.getDataItem(imagesKey);
        if (imageStackDataItem != null) {
            int[] shape = imageStackDataItem.getShape();
            if (shape != null) {
                if (shape.length > 2) {
                    reducedShape = Arrays.copyOf(shape, shape.length - 2);
                } else if (shape.length == 2) {
                    reducedShape = new int[] { 1 };
                }
            }
            imageStack = new ImageStack(dataGroup, imagesKey, nameItem, this, shape);
        }
        if (reducedShape != null) {
            energyScanData.setStackShape(reducedShape);
        }
        energyScanData.setImages(imageStack);
    }
//
//    /**
//     * Reads a scalar stack in a scan and updates its associated {@link ScanData} stack shape, if asked
//     * 
//     * @param energyScanData The {@link ScanData}
//     * @param factory The referent {@link IFactory}
//     * @param dataGroup The parent {@link LogicalGroup} of the scalar stack
//     * @param keyId The id of the key to identifies the scalar data
//     * @param dataDisplayName The data name to display in case of error
//     * @param updateStackShape Whether to update {@link ScanData}'s stack shape
//     * @param pushDetectorSubPart Whether to push detector subpart in key
//     * @return A {@link Map}
//     */
//    protected Map<PositionKey, Double> extractScalarStackData(ScanData energyScanData, IFactory factory,
//            LogicalGroup dataGroup, String keyId, String dataDisplayName, boolean updateStackShape,
//            boolean pushDetectorSubPart) {
//        int[] shape = null;
//        Map<PositionKey, Double> scalarStack = new LinkedHashMap<PositionKey, Double>();
//        if ((energyScanData != null) && (factory != null) && (dataGroup != null) && (keyId != null)
//                && (dataDisplayName != null)) {
//            IKey scalarsKey = factory.createKey(keyId);
//            if (pushDetectorSubPart) {
//                pushDetectorSubPart(energyScanData.getParent(), dataGroup, scalarsKey);
//            }
//            IDataItem scalarStackDataItem = dataGroup.getDataItem(scalarsKey);
//            try {
//                if (scalarStackDataItem != null) {
//                    IArray scalarArray = scalarStackDataItem.getData();
//                    shape = scalarArray.getShape();
//                    ISliceIterator sliceIterator = null;
//                    sliceIterator = scalarArray.getSliceIterator(0);
//                    if (sliceIterator != null) {
//                        int[] sliceShape = sliceIterator.getSliceShape();
//                        if (sliceShape != null) {
//                            while (sliceIterator.hasNext()) {
//                                int[] sliceOrigin = sliceIterator.getSlicePosition();
//                                double value = NumberArrayUtils.extractDoubleArray(
//                                        extractArrayValue(scalarStackDataItem, sliceIterator.getArrayNext(), true))[0];
//                                scalarStack.put(new PositionKey(sliceOrigin), value);
//                            } // end while (sliceIterator.hasNext())
//                        }
//                    }
//                }
//            } catch (Exception e) {
//                LoggerFactory.getLogger(applicationId).error( "Could not read " + dataDisplayName, e);
//                shape = null;
//                scalarStack.clear();
//            }
//            if (updateStackShape && (shape != null)) {
//                energyScanData.setStackShape(shape);
//            }
//        }
//        return scalarStack;
//    }

    /**
     * Reads a scalar stack in a scan and updates its associated {@link ScanData} stack shape, if asked
     * 
     * @param energyScanData The {@link ScanData}
     * @param factory The referent {@link IFactory}
     * @param dataGroup The parent {@link LogicalGroup} of the scalar stack
     * @param keyId The id of the key to identifies the scalar data
     * @param dataDisplayName The data name to display in case of error
     * @param updateStackShape Whether to update {@link ScanData}'s stack shape
     * @param pushDetectorSubPart Whether to push detector subpart in key
     * @return A {@link SoftFlatDoubleArray}
     */
    protected SoftFlatDoubleArray extractScalarStackData(ScanData energyScanData, IFactory factory,
            LogicalGroup dataGroup, String keyId, String dataDisplayName, boolean updateStackShape,
            boolean pushDetectorSubPart) {
        int[] shape = null;
        SoftFlatDoubleArray scalarStack = null;
        if ((energyScanData != null) && (factory != null) && (dataGroup != null) && (keyId != null)
                && (dataDisplayName != null)) {
            IKey scalarsKey = factory.createKey(keyId);
            if (pushDetectorSubPart) {
                pushDetectorSubPart(energyScanData.getParent(), dataGroup, scalarsKey);
            }
            IDataItem scalarStackDataItem = dataGroup.getDataItem(scalarsKey);
            try {
                if (scalarStackDataItem != null) {
                    shape = scalarStackDataItem.getShape();
                    IArray scalarArray = scalarStackDataItem.getData();
                    scalarStack = new SoftFlatDoubleArray(
                            new ContextDataItem(dataGroup, scalarsKey, scalarStackDataItem), this,
                            new FlatDoubleData(shape, NumberArrayUtils
                                    .extractDoubleArray(extractArrayValue(scalarStackDataItem, scalarArray, true))));
                }
            } catch (Exception e) {
                LoggerFactory.getLogger(applicationId).error("Could not read " + dataDisplayName, e);
                shape = null;
            }
            if (updateStackShape && (shape != null)) {
                energyScanData.setStackShape(shape);
            }
        }
        return scalarStack;
    }

    /**
     * Reads a spectrum stack in a scan and updates its associated {@link ScanData} stack shape, if asked
     * 
     * @param energyScanData The {@link ScanData}
     * @param factory The referent {@link IFactory}
     * @param dataGroup The parent {@link LogicalGroup} of the scalar stack
     * @param keyId The id of the key to identifies the scalar data
     * @param dataDisplayName The data name to display in case of error
     * @param updateStackShape Whether to update {@link ScanData}'s stack shape
     * @param updateSpectrumX Whether to update {@link ScanData}'s parent {@link Acquisition}' spectrum X
     * @return A {@link Map}
     */
    protected SpectrumStack extractSpectrumStackData(ScanData energyScanData, IFactory factory, LogicalGroup dataGroup,
            String keyId, String dataDisplayName, boolean updateStackShape, boolean updateSpectrumX) {
        int[] reducedShape = null;
        Acquisition acquisition = energyScanData.getParent();
        SpectrumStack specStack = null;
        Map<PositionKey, SoftIndexedData<double[]>> spectra = new LinkedHashMap<>();
        if ((energyScanData != null) && (factory != null) && (dataGroup != null) && (keyId != null)
                && (dataDisplayName != null)) {
            IKey spectrumStackKey = factory.createKey(keyId);
            pushDetectorSubPart(energyScanData.getParent(), dataGroup, spectrumStackKey);
            IDataItem spectrumStackItem = dataGroup.getDataItem(spectrumStackKey);
            int[] shape = null;
            if (spectrumStackItem != null) {
                shape = spectrumStackItem.getShape();
                if (reducedShape == null) {
                    reducedShape = shape;
                    if ((reducedShape != null) && (reducedShape.length > 1)) {
                        reducedShape = Arrays.copyOf(reducedShape, reducedShape.length - 1);
                    }
                }
            }

            if (LOAD_FULL_SPECTRUM_STACK) {
                IArray spectrumStackArray = null;
                if (spectrumStackItem != null) {
                    try {
                        spectrumStackArray = spectrumStackItem.getData();
                    } catch (DataAccessException e) {
                        spectrumStackArray = null;
                    }
                }
                if (spectrumStackArray != null) {
                    ISliceIterator sliceIterator = null;
                    try {
                        sliceIterator = spectrumStackArray.getSliceIterator(1);
                    } catch (Exception e) {
                        LoggerFactory.getLogger(applicationId).error("Could not read " + dataDisplayName, e);
                        sliceIterator = null;
                    }
                    // loop through spectrum stack
                    if (sliceIterator != null) {
                        int[] sliceShape = null;
                        try {
                            sliceShape = sliceIterator.getSliceShape();
                        } catch (Exception e) {
                            LoggerFactory.getLogger(applicationId).error("Could not read " + dataDisplayName, e);
                            sliceShape = null;
                        }
                        if (sliceShape != null) {
                            try {
                                ContextDataItem spectrumStackContext = new ContextDataItem(dataGroup, spectrumStackKey,
                                        spectrumStackItem);
                                specStack = new SpectrumStack(spectra, reducedShape, shape[shape.length - 1]);
                                while (sliceIterator.hasNext()) {
                                    IArray spectrumArray = sliceIterator.getArrayNext();
                                    int[] sliceOrigin = sliceIterator.getSlicePosition();
                                    int[] origin, reducedOrigin;
                                    if (sliceOrigin.length == shape.length) {
                                        origin = sliceOrigin;
                                    } else {
                                        origin = Arrays.copyOf(sliceOrigin, shape.length);
                                    }
                                    if (sliceOrigin.length == reducedShape.length) {
                                        reducedOrigin = sliceOrigin;
                                    } else {
                                        reducedOrigin = Arrays.copyOf(sliceOrigin, reducedShape.length);
                                    }
                                    double[] spectrum = NumberArrayUtils.extractDoubleArray(
                                            extractArrayValue(spectrumStackItem, spectrumArray, true));
                                    if (updateSpectrumX && (acquisition != null)) {
                                        if ((acquisition.getSpectrumX() == null) && (spectrum != null)) {
                                            double[] spectrumX = new double[spectrum.length];
                                            for (int i = 0; i < spectrumX.length; i++) {
                                                spectrumX[i] = i;
                                            }
                                            acquisition.setSpectrumX(spectrumX);
                                        }
                                    }
                                    spectra.put(new PositionKey(reducedOrigin), new SoftIndexedDoubleArray(
                                            spectrumStackContext, origin, sliceShape, this, spectrum));
                                } // end while (sliceIterator.hasNext())
                            } catch (Exception e) {
                                LoggerFactory.getLogger(applicationId).error("Could not read " + dataDisplayName, e);
                                shape = null;
                                spectra.clear();
                            }
                        } // end if (sliceShape != null)
                    } // end if (sliceIterator != null)
                }
            } else {
                specStack = new SpectrumStack(dataGroup, spectrumStackKey, this, shape, reducedShape);
                if (updateSpectrumX && (acquisition != null)) {
                    if ((acquisition.getSpectrumX() == null) && specStack.getSpectrumX() != null) {
                        acquisition.setSpectrumX(specStack.getSpectrumX());
                    }
                }

            }
        }
        if (updateStackShape && (reducedShape != null)) {
            energyScanData.setStackShape(reducedShape);
        }
        return specStack;

    }

    /**
     * Extracts some spectrum data
     * 
     * @param energyScanData The {@link ScanData}
     * @param factory The referent {@link IFactory}
     * @param dataGroup The parent {@link LogicalGroup} of the scalar stack
     * @param keyId The id of the key to identifies the scalar data
     * @param dataDisplayName The data name to display in case of error
     * @return A <code>double[]</code>
     */
    protected double[] extractSpectrumData(ScanData energyScanData, IFactory factory, LogicalGroup dataGroup,
            String keyId, String dataDisplayName) {
        double[] spectrum = null;
        if ((energyScanData != null) && (factory != null) && (dataGroup != null) && (keyId != null)
                && (dataDisplayName != null)) {
            IKey key = factory.createKey(keyId);
            IDataItem dataItem = dataGroup.getDataItem(key);
            if (dataItem != null) {
                try {
                    IArray totalArray = dataItem.getData();
                    spectrum = NumberArrayUtils.extractDoubleArray(extractArrayValue(dataItem, totalArray, true));

                } catch (DataAccessException e) {
                    LoggerFactory.getLogger(applicationId).error("Could not read " + dataDisplayName, e);
                    spectrum = null;
                }
            }
        }
        return spectrum;
    }

    /**
     * Reads total spectrum in a scan and updates its associated {@link ScanData}
     * 
     * @param energyScanData The {@link ScanData}
     * @param factory The referent {@link IFactory}
     * @param dataGroup The parent {@link LogicalGroup} of the image stack
     */
    protected void loadTotalImage(ScanData energyScanData, IFactory factory, LogicalGroup dataGroup) {
        IKey totalKey = factory.createKey(TOTAL_IMAGE_KEY);
        IDataItem totalDataItem = dataGroup.getDataItem(totalKey);
        FlatData totalImage = null;
        if (totalDataItem != null) {
            try {
                IArray totalArray = totalDataItem.getData();
                int[] shape = totalDataItem.getShape();
                totalImage = new FlatData(shape, extractArrayValue(totalDataItem, totalArray, true));

            } catch (DataAccessException e) {
                LoggerFactory.getLogger(applicationId).error("Could not read total image", e);
                totalImage = null;
            }
        }
        if (energyScanData != null) {
            energyScanData.setTotalImage(totalImage);
        }
    }

    /**
     * Reads the channel scale
     * 
     * @param factory The referent {@link IFactory}
     * @param regionName The concerned redion
     * @param parentGroup The {@link LogicalGroup} that is parent of the information to read
     * @param filterToPush The {@link IFilter} to push to the {@link IKey}
     * @return A <code>double[]</code>
     * @throws DataAccessException If a problem occurred while accessing information
     */
    protected double[] readScientaChannelScale(IFactory factory, String regionName, LogicalGroup parentGroup,
            List<IFilter> filterToPush) throws DataAccessException {
        double[] result = null;
        IKey channelScaleKey = factory.createKey(CHANNEL_SCALE_KEY);
        if (filterToPush != null) {
            for (IFilter filter : filterToPush) {
                channelScaleKey.pushFilter(filter);
            }
        }
        IDataItem channelScaleDataItem = parentGroup.getDataItem(channelScaleKey);
        if ((channelScaleDataItem != null) && (channelScaleDataItem.getData() != null)) {
            result = NumberArrayUtils
                    .extractDoubleArray(extractArrayValue(channelScaleDataItem, channelScaleDataItem.getData(), true));
        }
        return result;
    }

    protected double[] readMBSEnergyScale(IFactory factory, String regionName, LogicalGroup parentGroup)
            throws DataAccessException {
        double[] result = null;
        IKey channelScaleKey = factory.createKey(ESCALE_KEY);
        result = (double[]) readCDMSimpleArray(parentGroup, channelScaleKey, null, null);
        return result;
    }

    protected double[] readXScale(IFactory factory, LogicalGroup parentGroup) throws DataAccessException {
        double[] result = null;
        IKey channelScaleKey = factory.createKey(XSCALE_KEY);
        result = (double[]) readCDMSimpleArray(parentGroup, channelScaleKey, null, null);
        return result;
    }

    @Override
    public AbstractDataSource<ExperimentConfig> getExperimentConfigSource() {
        return experimentConfigSource;
    }

    @Override
    public AbstractDataSource<Acquisition> getAcquisitionSource() {
        return acquisitionSource;
    }

    @Override
    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    @Override
    public boolean isCanceled() {
        return canceled;
    }

}
